﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace HansardsSplitter
{
    class Program
    {
        static string[] DAY_START_PHRASES = new string[] {
            "^the speaker took the chair",
            "^the house met"
        };
        static void Main(string[] args)
        {
            Console.WriteLine("Drag file/folder:");
            string path = Console.ReadLine();
            path = path.Trim('"');

            bool isDir = (File.GetAttributes(path) & FileAttributes.Directory) == FileAttributes.Directory;

            String[] files = isDir ? Directory.GetFiles(path, "*.txt") : new string[] { path };

            if (!Directory.Exists("output"))
            {
                Directory.CreateDirectory("output");
            }
            int day = 0;
            for (int filei = 0; filei < files.Length; filei++)
            {
                string text = File.ReadAllText(files[filei]);
                int daySplitIndex = -1;
                for (int dspii = 0; dspii < DAY_START_PHRASES.Length; dspii++)
                {
                    if (Regex.IsMatch(text, DAY_START_PHRASES[dspii], RegexOptions.IgnoreCase | RegexOptions.Compiled | RegexOptions.Multiline))
                    {
                        daySplitIndex = dspii;
                        break;
                    }
                }
                if (daySplitIndex == -1)
                {
                    Console.WriteLine("Can't find suitable split for " + Path.GetFileName(files[filei]));
                    File.AppendAllText("bad.log", Path.GetFileName(files[filei]) + "\r\n");
                    continue;
                }
                string[] days = Regex.Split(text, "(?:" + DAY_START_PHRASES[daySplitIndex] + ")", RegexOptions.IgnoreCase | RegexOptions.Compiled | RegexOptions.Multiline);
                
                Console.WriteLine(Path.GetFileName(files[filei]) + " has " + days.Length + " days");
                int prevYear = 0;
                for (int dayi = 0; dayi < days.Length; dayi++)
                {
                    string dayText = days[dayi];
                    var dates = dayText.Split("\r\n".ToCharArray(), StringSplitOptions.RemoveEmptyEntries)
                        .Select(line =>
                            {
                                DateTime parsed;
                                if (DateTime.TryParse(line, out parsed))
                                {
                                    return parsed;
                                }
                                return DateTime.MinValue;
                            }).Where(dt => dt != DateTime.MinValue)
                            .GroupBy(dt=>dt)
                            .OrderByDescending(grp=>grp.Count()).ToArray();
                    string name = "";
                    if (dates.Count() > 0)
                    {
                        DateTime mostCommon = dates.First().Key;
                        if (!dates.Any(dt => dt.Key.Year == prevYear))
                        {
                            prevYear = mostCommon.Year;
                        }
                        else
                        {
                            mostCommon = dates.First(dt => dt.Key.Year == prevYear).Key;
                        }
                        
                        name = day + "." + mostCommon.ToString("MMM_dd_yyyy");
                    }
                    else
                    {
                        name = day + "";
                    }
                   
                    day++;
                    Console.WriteLine(name);
                    File.WriteAllText("output/" + name + ".txt", dayText);
                }
                
            }
        }
    }
}
