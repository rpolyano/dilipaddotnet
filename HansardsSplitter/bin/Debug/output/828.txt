OFFICIAL REPORT
OP THE
DEBATES
OP THE
HOUSE OF COMMONS
OF THE
DOMINION OF CANADA
SECOND SESSION-ELEVENTH PARLIAMENT
9-10 EDWARD VII., 1909-10
VOL. XCIV
COMPRISING THE PERIOD FROM THE SEVENTEENTH DAY OF JANUARY TO THE TWENTY-FIRST DAY OF FEBRUARY, INCLUSIVE.
OTTAWA
PRINTED BY C. H. PARMELEE, PRINTER TO THE KING’S MOST EXCELLENT MAJESTY 1909 in
House of Commons Sebntcs
SECOND SESSION-ELEVENTH PARLIAMENT
HOUSE OF COMMONS.
Monday, January 1Y, 1910.
