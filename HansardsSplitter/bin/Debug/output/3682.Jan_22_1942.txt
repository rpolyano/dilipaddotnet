 at three o’clock, the Speaker, the Honourable James Allison Glen, in the chair.
Mr. Speaker read' a communication from the Governor General’s secretary, announcing that His Excellency the Governor General would proceed to the Senate chamber at three p.m. on this day, for the purpose of formally opening the session of the dominion parliament.
A message was delivered by Major A. R. Thompson, Gentleman Usher of the Black Rod, as follows:
Mr. Speaker, His Excellency the Governor General desires the immediate attendance of this honourable house in the chamber of the honourable the Senate.
Accordingly the house went up to the Senate chamber.
And the house having returned to the Commons chamber:
OATHS OE OFFICE
Right Hon. W. L. MACKENZIE KING (Prime Minister) moved for leave to introduce Bill No. 1, respecting the administration of oaths of office.
Motion agreed to and bill read the first time.
GOVERNOR GENERAL’S SPEECH
Mr. SPEAKER:	I have the honour to
inform the house that when the house did attend His Excellency the Governor General this day in the Senate chamber, His Excellency was pleased to make a speech to both houses of parliament. To prevent mistakes, I have obtained a copy, which is as follows: Honourable Members of the Senate:
Members of the House of Commons:
There no longer can be any question as to the character and scope of the present war. It is a world-wide conflict between irreconcilable
44561—1
forces. On every continent, including the confines of our own, and on all oceans, forces that aim at world domination oppose forces that seek the preservation of freedom. In every quarter of the globe, civilization is confronted by savagery.
The conflict can have but one of two outcomes. Either tyranny, based on terror and brutality, must be overthrown; or the free peoples of the world, one and all, slowly but eventually, will be reduced to a state of bondage. Upon the outcome depends, for generations, the future well-being of mankind.
There are strong reasons for belief in the overthrow of tyranny, and the ultimate triumph of freedom. The axis powers: Germany, Italy and Japam now fight as one. So do the peoples of the British commonwealth, of the United States, Russia, China and the many other nations that, on the first day of this new year, united in a pact to fight together until victory is achieved. The marshalling of the free forces of the greatest industrial nations in the world makes clear the scale upon which the conflict will now be waged on many fronts.
During the present session, opportunity will be afforded for the fullest consideration and discussion of Canada’s war effort, actual and prospective. My advisers will submit to you the measures deemed essential to our national security, and for the prosecution of the war to the utmost of our strength-in accordance with the government’s policy of a total national effort for total war, you will be asked to approve a balanced programme for further increases in the armed forces and in the production of munitions of war and of ^o^8?1^8, The increase in the armed forces will involve an expansion of the establishment of the Canadian army overseas. You will also be asked, as an integral part of Canada’s direct war effort, to approve a contribution to Britain of vast quantities of munitions, foodstuffs and supplies.
The government’s policy of national selective service will be extended, as generally and rapidly as may be necessary, to effect the orderly and efficient employment of the men and women of Canada for the varied purposes of war. You will be advised of the means the government proposes to adopt, to effect as complete as possible a mobilization of the material resources and man-power of the country in direct furtherance of a total national effort.
My advisers believe that the magnitude and balanced nature of Canada’s war effort is being obscured and impaired by controversy concerning commitments with respect to the methods of raising men for military service which were made prior to the spread of the war to all parts of the world.
The government is of the opinion that, at this time of gravest crisis in the world’s history, the administration, subject only to. its responsibility
REVISED EDITION
2
Governor General’s Speech
COMMONS
to parliament, should in this connection and irrespective of any previous commitments, possess complete freedom to act in accordance with its judgment of the needs of the situation as they may arise.
My ministers accordingly will seek, from the people, by means of a plebiscite, release from any obligation arising out of any past commitments restricting the methods of raising men for military service.
Members of the House of Commons:
You will be asked to make financial provision bn an unprecedented scale for the expansion and maintenance of Canada’s armed forces and for war production.
You will be asked to make financial provision for implementing agreements with the provinces to facilitate the orderly and equitable mobilization of the financial resources of the country to the utmost limit of its capacity.
Honourable Members of the Senate:
Members of the House of Commons:
I am confident that in no particular will the Canadian people fail in the full discharge of any of their great responsibilities. At this time, when vision and wisdom are so greatly needed in the affairs of the world, I pray that Almighty God may guide and bless your deliberations.
Right Hon. W. L. MACKENZIE KING (Prime Minister) moved:
That the speech of His Excellency the Governor General to both houses of parliament be taken into consideration on Friday next, and that this order have precedence over all other business, except the introduction of bills, government notices of motion and government orders, until disposed of.
Motion agreed to.
STANDING COMMITTEES Right Hon. W. L. MACKENZIE KING (Prime Minister) moved:
That a special committee be appointed to prepare and report with all convenient speed, lists of members to compose the standing committees of this house, under standing order 63, said committee to be composed of Messrs. Mackenzie (Vancouver Centre), Michaud, Casselman (Grenville-Dundas), Taylor, and Coldwell.
Motion agreed to.
INTERNAL ECONOMY COMMISSION Right Hon. W. L. MACKENZIE KING (Prime Minister) presented the following message from His Excellency the Governor General:
The Governor General transmits to the House of Commons a certified copy of an approved minute of council appointing the Honourable T. A. Crerar, Minister of Mines and Resources, the Honourable J. L. Ilsley, Minister of Finance, the Honourable J. E. Michaud, Minister of Fisheries, and the Honourable J. A. MacKinnon, Minister of Trade and Commerce, to act with the Speaker of the House of Commons as commissioners for the purposes and under the provisions of chapter 145 of the revised statutes of Canada, 1927, intituled An Act Respecting the House of Commons.
[Mr. Speaker.]
THE WAR
DECLARATION BY UNITED NATIONS AT WASHINGTON, JANUARY 1, 1942, AND RELATED DOCUMENTS
Right Hon. W. L. MACKENZIE KING (Prime Minister): I should like to table a copy of the declaration by United Nations, done at Washington, January 1, 1942, and the following related documents: Declaration of principles, known as the Atlantic Charter, by the Prime Minister of the United Kingdom and the President of the United States of America, August 14, 1941; resolutions approving the Atlantic Charter and providing for the reprovisioning of Europe after the war, adopted at the inter-allied meeting held in London, September 24, 1941, and with reference to these, the tripartite pact signed at Berlin, September 27, 1940, and referred to in the above declaration by the United Nations.
Mr. COLDWELL: Will the document tabled by the right hon. the Prime Minister (Mr. Mackenzie King) be printed as part of the Votes and Proceedings so that we may all have it?
Mr. MACKENZIE KING:	They are
printed in both English and French in pamphlet form and will be distributed in that form to all hon. members.
LEGATIONS
Right Hon. W. L. MACKENZIE KING (Prime Minister):	At the last sitting of the
last session my hon. friend the leader of the opposition (Mr. Hanson) spoke to me about my having omitted to answer a question which was upon the order paper with reference to the French legation at Ottawa, and also one with reference to Canadian representation, in Eire. Unfortunately I did not have with me at the moment the answers which had been prepared in the department, but I have them with me now and I should like to have them recorded on Hansard in to-day’s proceedings.
REPRESENTATION OF VICHY GOVERNMENT IN CANADA
Mr. CHURCH:
1.	Has the Vichy government of France an ambassador or envoy in Canada?
2.	If so, where is he located, who are the members of his staff; is there any inspection of the same, or regulation of the embassy?
3.	Is it intended to olose this legation?
4.	Has the government received any request from His Majesty’s government of Great Britain to keep it open? If so, from whom?
5.	Does this Vichy office in Ottawa conduct any dominion or empire business with Vichy?
6.	If so, what is the amount of such business and of what nature i» it?
3
JANUARY 22, 1942
Canadian Regiments at Hong Kong
Mr. MACKENZIE KING:
1.	The French government is represented in Canada by a minister, Mr. Ristelhueber.
2.	His office is in the French Legation on Sussex street in Ottawa. The members of his staff are Mr. de Lageneste, First Secretary, Mr. Treuil, Commercial Attache and Mr. du Bois-berranger, Secretary. The French Legation is subject to the same regime and obligations as legations of other countries in Canada.
3.	No.
4.	The Prime Minister of the United Kingdom has expressed the view that it was desirable to continue the existing arrangements with regard to diplomatic representation as between France and Canada.
5.	Yes.
6.	It is not in the public interest to answer this question.
REPRESENTATION OF CANADIAN GOVERNMENT IN EIRE
Mr. CHURCH:
1.	Does the government maintain an envoy or minister in Eire?
2.	If so, who is he, who are the members of his staff and what are their respective salaries and other allowances?
3.	What is the annual cost to Canada?
4.	Do Australia and New Zealand maintain similar offices in Eire, and where are they located?
5.	Has Canada trade commissioners in Dublin ?
6.	If so, are these envoys not duplications of services rendered by trade agents?
Mr. MACKENZIE KING:
1.	There is a Canadian High Commissioner accredited to Ireland.
2.	Mr. John Doherty Kearney, high commissioner, salary, $7,500, allowance, $8,000; Mr. E. J. Garland, secretary, salary, $3,540, allowance, $2,000; Miss E. O’Malley, stenographer grade 3, salary, $1,620, allowance, $250; Mr. B. Chambers, clerk messenger, salary, $465.
3.	1940-41—$30,281.49.
4.	Australia and New Zealand do not maintain similar offices in Ireland.
5.	There is a Canadian trade commissioner in Dublin.
6.	There is no duplication of services.
CLERK ASSISTANT
Mr. SPEAKER: I have the honour to lay before the house a copy of order in council, P.C. 454, dated January 21, 1942, concerning the superannuation of Mr. T. M. Fraser, Assistant Clerk of the House.
I have assigned Mr. C. W. Boyce, Chief of English Journals, to act as Assistant Clerk of the House pending a permanent appointment to be made by the governor in council.
<4561—li
ACTING DEPUTY SERGEANT-AT-ARMS
Mr. SPEAKER: I have the honour to inform the house that I have assigned J. Laundy to be acting Deputy Sergeant-at-Arms during the present session.
THE WAR
CANADIAN REGIMENTS AT HONG KONG
Hon. R. B. HANSON (Leader of the Opposition):	Mr. Speaker, if the routine
business of this afternoon has now been completed I desire the indulgence of the house while I make a brief statement, somewhat in the nature of a matter of privilege. After careful perusal and intensive study of the statement made yesterday by the Minister of National Defence (Mr. Ralston) regarding the Hong Kong expedition, I have now come to the conclusion that a very serious situation is revealed and one with which I feel bound to deal. When I made -my statement yesterday I was not dealing specifically with the question of man-power.
I do not believe that anyone who reviews the minister’s statement will find in it a satisfactory answer to the questions which have been asked by the public about Hong Kong in particular, and the man-power situation in Canada generally. A thorough study of the minister’s statement has revealed to me certain facts which, as a non-military man, I did not fully appreciate at the time that very technical and, may I add, subtle statement was made. Having since reviewed it carefully, I am impelled to make a further statement, and I crave the indulgence of this house while I do so.
In my opinion the country is entitled to further information and I propose to indicate the method by which I believe that information should be made available. The fact that from 138 to 148 men with less than minimum training were sent to Hong Kong definitely and inescapably establishes the serious lack of trained man-power in Canada. It must be assumed that if trained man-power was available, they would have been sent. One cannot easily believe that the Minister of National Defence, or the military officials, took untrained men if trained men were available.
This fact raises squarely the question of the state of reserves of trained man-power which we have in Canada. The matter is one of such vital importance that I do not believe it can be properly settled without an open and immediate investigation by a committee of this house. How can we as members of parliament, and how can the people of this country pass upon the question of compulsory service and kindred subjects until we and they have full,
COMMONS
Canadian Regiments at Hong Kong
exact and precise information regarding the present standing of trained man-power in Canada?
The second fact made evident by the minister’s statement is that the failure to provide universal carriers, which are fighting vehicles, and other transport at the port of embarkation at the proper time rests with the Canadian authorities. The direct responsibility for this failure must be determined and those responsible must be disciplined. The third fact revealed by the minister’s statement is that the commander of the expedition, Brigadier Lawson, was disappointed at the lack of mechanical transports which he should have had. His cable from the boat leads to no other conclusion.
Having regard to all these factors, the country is entitled to know why the expedition was not recalled until such time as the transport and equipment could be provided to go along with the expedition. These are questions upon which the country is entitled to receive specific and factual information, and the only manner in which such information can be provided satisfactorily is by a searching public investigation by a committee of this house. I think the minister himself should welcome a public investigation. A departmental investigation will not satisfy public opinion. Therefore I invite him to set up a special committee of this house to investigate the factual and exact situation regarding what happened in connection with sending these men to Hong Kong.
Right Hon. W. L. MACKENZIE KING (Prime Minister): Perhaps my hon. friend will allow me to say a word as there is some responsibility on the part of the government as a whole in a matter of this kind. As a matter of special privilege my hon. friend has asked for permission to make a statement. I took no exception to my hon. friend so doing, but I wish he had been careful to refrain from using words in his statement that I do not think were at all justified. He referred to the statement made yesterday by my colleague, the Minister of National Defence (Mr. Ralston), as a subtle statement. I am sure that any hon. members of the house who listened to the statement of the Minister of Defence regarded it as a very straightforward, honest and frank statement, and I would add that my hon. friend in his reply yesterday, unless I misunderstood him, more or less so^ designated it, but since that time he has evidently been receiving information from outside sources and to-day has presented us with the result of his further communications with [Mr. E. B. Hanson.]
others. May I say to my hon. friend that the government will certainly grant, and grant immediately, a committee of investigation into this matter. We welcome the opportunity of so doing, and the committee will be granted just as soon as opportunity permits.
Mr. M. J. COLDWELL (Rosetown-Biggar): Mr. Speaker, we welcome the committee which the leader of the opposition (Mr. Hanson) has asked for and which the Prime Minister so readily granted, but may I ask that the order of reference be wide enough to permit the committee to inquire how the force was asked for, who decided it should go, and under what conditions it went. I suggest that there is something involved in that which should be looked into by a committee of this house.
Mr. MACKENZIE KING: I think my hon. friend and colleague (Mr. Ralston) in his statement yesterday made all these points clear, but I can assure my hon. friend that there will be no desire on the part of the government to limit the investigation in any essential particular.
Mr. POULIOT: Mr. Speaker, on a question of privilege, and referring to page 4476 of Hansard of last session, the Hansard of yesterday, where I said that: “the Minister of National Defence should resign on account of the Hong Kong affair,” the record is not complete because immediately after I had said that, the leader of the opposition (Mr. Hanson) shouted “Order” to prevent my saying anything further. That was, as always, a complete contradiction; he does not know what he wants.
On motion of Mr. Mackenzie King the house adjourned at 4.15 p.m.
Friday, January 23, 1942
