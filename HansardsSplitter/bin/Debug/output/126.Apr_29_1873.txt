 at 3 p.m.
Prayers
MANUFACTURE OF SUGAR
Mr. JOLY said that when he introduced his resolution for the encouragement of the manufacture of sugar, it was desired that the matter should be allowed to stand over. This was a fortnight ago, and he would like it to be taken up as soon as possible.
Hon. Mr. TILLEY said there would be no objection to taking up the Bill on Thursday.
* * *
REPORTS PRESENTED
Hon. Mr. CAMPBELL presented a report of the General Committee on Elections.
Hon. Mr. BLANCHET presented the fourth report of the Committee on Railways, Canals, and Telegraph Lines.
* * *
INSPECTION LAW
Mr. TOBIN asked for information respecting the inspection law.
Hon. Mr. TILLEY said it was then with the Committee on Banking and Commerce, and was on the papers for tomorrow.
* * *
INTIMIDATION OF VOTERS
Hon. Mr. MACKENZIE desired before the Orders of the Day were called to again call the attention of the Government to a matter he had brought up last week when he had read a letter from the London Post officer Inspector that was clearly intended to intimidate Postmasters in the exercise of their legitimate franchise. On that occasion he stated that he would say nothing about the matter until the hon. gentleman had time to communicate with this official or to receive a communication from him. He now desired to ask if the leader of the Government had any communication to make to the House on the subject.
Hon. Sir JOHN A. MACDONALD said he would attend to that matter on Thursday.
* * *
PACIFIC RAILWAY
Hon. Mr. BLAKE asked if the hon. gentleman was yet in a position to say whether he intended to submit to Parliament that clause of the Pacific Railway Charter that required the sanction of Parliament.
Hon. Sir JOHN A. MACDONALD said the Government had not yet made up their minds on the subject. The reason was that they were daily in expectation of receiving a communication from the Pacific Railway delegation in England respecting the success and progress of their negotiations. The Government did not desire to bring down any resolution until they had further information on the subject. By the arrival of the next mail he might probably be able to give a definite answer.
Hon. Mr. HOLTON said it appeared to him that instead of waiting to hear from the delegation in England to determine what course to take, the very fact that there was a delegation in England making a proposal to raise money under a charter which required the sanction of Parliament, was the strongest possible reason why the sense of Parliament should be asked at the earliest possible moment, because Parliament might, to a certain extent, be compromised if engagements were entered into in England, founded upon the assumption that Parliament would give its sanction to those exceptional provisions of the Charter.
Hon. Sir JOHN A. MACDONALD said Parliament would in no way be compromised.
Hon. Mr. BLAKE: Then we understand that the hon. gentleman expects the result of negotiations will be such that a proposition will be placed before Parliament in the same way as if no delegation had been sent at all.
Hon. Sir JOHN A. MACDONALD: Parliament will in no way be shackled.
* * *
INDIAN DEPREDATIONS IN THE NORTHWEST
Hon. Mr. MACKENZIE asked if the Government had received any information respecting the rumours in the papers of Indian depredations in the North-West. He hoped there was no truth in such rumours, but if there was, he presumed the Government must have official intelligence.
COMMONS DEBATES
388
April 29, 1873
Hon. Sir JOHN A. MACDONALD said the Government had no information of any raid or incursion of the Indians in any way. The rumours had reached the Government, and one would suppose that where there was so much continuous rumour there must be some foundation for it. However, the Government had received no information about it from Manitoba. He had received a telegram in cypher from Manitoba, but he was not able to ascertain from it whether anything had occurred or not. He had telegraphed for further information.
* * *
SAVINGS BANKS
On motion of Hon. Mr. TILLEY, the House went into Committee on the Bill to amend the Act respecting certain savings banks in the Province of Ontario and Quebec, as amended by the Standing Committee on Banking and Commerce.
The Bill was adopted as amended, reported, and the report concurred in.
Third reading was fixed for Thursday.
* * *
DECK LOADS
Hon. Mr. MITCHELL moved the House into Committee on the Act respecting deck loads, Hon. Mr. McDONALD (Pictou) in the chair.
Mr. PALMER said the bill had been greatly altered in Committee, and it had been rendered less objectionable than at first. The principle, however, was vicious, but it might not prove very objectionable, as its provisions could be very easily evaded. He referred to the immense importance of the shipping interests of the Dominion, and also to the necessity to take every means to lessen loss of life at sea, and mentioned the recent action taken in the English House of Commons by Mr. Plimsoll, which was in the very direction he had previously advocated for Canada. The principle of inspection was applied to very many articles, and why should not the same principle be applied to the shipping of the country. Wooden ships had been found preferable to iron ships and had consequently become very valuable.
Mr. Plimsoll had introduced a bill in England providing for an inspection of ships, the provisions of which he explained, and urged that unless Canada took measures to have an inspection and ship register of her own, the result would be very detrimental to her ships. He was sure shipowners would be very willing to pay all the expenses of an inspection. He referred to the working of the English Lloyds register as being very unfair to Canadian ships, but condemned the bill before the House as being wrong in principle in laying down strict rules instead of providing a proper system of inspection.
It was urged that the law must be passed to prevent the casualties that had occurred in the West India lumber trade, but he denied that the casualties as alleged had even occurred. He was well acquainted with the subject personally, and he was sustained in what he said by information he had received from the most eminent shipowners in the Province and he knew that the loss of life in the trade in question had been infinitesimally small. The deck load law, however, had been evaded before, and it could be very easily done again and he only opposed it because it was taking the place of what he believed ought to be enacted, an efficient system of inspection.
He urged that the measure should be postponed so as to allow members representing the constituencies interested to make enquiry into the matter, and to ascertain what course was calculated to be the most effective and most desirable. We had only to free our ships from control of foreign interests and an immense advantage would be gained. No such restrictions as those proposed were in effect in England, or the States, and the result in Canada would only be to harass and hamper the trade. He would have opposed every line of it did he not believe it could be so easily evaded. He believed a vessel was safer with a deck load than without, but urged a commission of enquiry instead of passing the present bill.
Hon. Mr. YOUNG (Montreal West) spoke of the great importance of the matter as affecting the commercial interests, and also as connected with the loss of human life, and quoted figures showing that a very large preponderance of lumber-laden ships that had been lost in years past had carried deck loads. He thought it was the urgent duty of the House, in the interests of humanity, to take steps to prevent any further loss of the lives of the poor seamen. He approved of the bill as a step in the right direction, and thought the only objection was that it did not abolish deck loads altogether. He thought also that the time excepted should be from the first of September instead of the first of October. The high rates of insurance occasioned a great loss to the country commercially. He did not think the statement of the member for Saint John as to the small loss of life in the East Indian timber trade was in accordance with the fact, and he read a statement made by the Minister of Marine and Fisheries in the Committee on Banking and Commerce, showing a very large loss of life and property to have really occurred.
Mr. PALMER said this very statement confirmed what he had said, as many of the accidents had not been occasioned by deck loads at all.
Hon. Mr. YOUNG (Montreal West) said that the statistics showed that an immense loss of life had been occasioned by the practice of carrying deck loads, and he quoted from statements made by the Consul-General at Havana on the subject urging stringent regulations to check the practice. There could be no doubt that the carrying of deck loads increased the loss of life and also the rates of insurance. He urged that Canada should follow the course taken in England thirty years ago and limit the carrying of deck loads from the first of September, and if the Government would not
COMMONS DEBATES
April 29, 1873
389
assent to this he should take the sense of the House on the question. Parliament was bound in every way to do everything possible to protect the life of her seamen.
Mr. DOULL agreed that it was the duty of the House to prevent a loss of life from improper loading of vessels, but he did not think the bill would have the effect desired. This bill was unfair, as it made no distinction between vessels of different builds. What was wanted was not merely legislation for or against deck loads, but a bill to prevent any vessels from going to sea over-loaded. He would offer no opposition to the bill if the Minister of Marine (Hon. Mr. Mitchell) would add a clause to prevent unseaworthy vessels from going to sea.
Hon. Sir FRANCIS HINCKS read an extract from an English newspaper containing an expression of the sympathy of the seamen of Liverpool with Mr. Plimsoll. There had also been a meeting of the shipowners of Liverpool when a similar feeling was expressed.
Mr. PALMER denied that many of the losses attributed to deck loads were really to be ascribed to that cause. The Council of the Saint John Board of Trade had drawn up a memorial as the result of a meeting of the Board of Trade, in which they stated that not a life had been lost in vessels in the European trade going to Saint John during the last five years through deck loads. The object of the commission in England was to prevent unseaworthy and overladen ships from going to sea, and that was what ought to be done here. In the West Indian trade from Saint John, but very few losses of life had occurred from deck loads. He believed that the vessels which had been lost when they were carrying deck loads would have been lost without deck loads. He continued to argue in favour of a Government inspection, of which the whole cost should be paid by the shipowners.
Mr. DOMVILLE could not agree with the member for Saint John. He contended that the council of the Saint John Board of Trade was a very different body from the Board of Trade. He read a letter from Captain Stockton, a shipowner, shipbuilder and master mariner, in which an opinion was expressed that this bill was just and humane and that deck loads ought to be regulated according to the size and build of the ship.
Hon. Mr. YOUNG (Montreal West) moved in amendment, that the 1st of September be substituted for the first of October in regard to vessels sailing from the St. Lawrence.
Hon. Mr. MITCHELL objected to amending the bill now, after its having been reported unanimously by the Committee on Banking and Commerce. The member for Saint John was mistaken in saying that no lives had been lost through deck loads on ships in the European trade within the last five years.
Mr. BURPEE (Saint John City and County) objected to this amendment after the action of the Committee.
Mr. DOULL also objected to amendment, but would support an amendment fixing the date at the 15th of September.
Hon. Mr. MITCHELL thought the bill was sufficiently stringent, and that it would be made too stringent to fix the date the 1st of September. After hearing the objection of the member for Pictou (Mr. Doull), he would not accept the amendment.
Hon. Mr. SMITH (Westmorland) gave his entire concurrence to the Bill.
On division the amendment was lost by a majority of 30 votes.
The Bill was then adopted without amendment and reported, and fixed for third reading tomorrow.
* * *
PILOTAGE
Hon. Mr. MITCHELL moved the second reading of the bill respecting pilotage.
Hon. Mr. HOLTON hoped the Minister of Marine would allow the bill to go to the Committee on Banking.
Hon. Mr. CAUCHON thought that at this late period of the session the bill ought not to be sent to the committee, but it should be discussed in the House.
Hon. Mr. MITCHELL while willing to oblige his hon. friend from Chateauguay (Hon. Mr. Holton) hoped he would consent to discuss the bill in Committee of the Whole. He was afraid that if the bill were sent to the Committee on Banking and Commerce, it would not come back in time to be passed this session.
Hon. Mr. HOLTON in that case would insist that the second reading should not be taken today, as the bill was not yet printed in French.
After some remarks from Messrs. CAUCHON and PALMER the bill was allowed to stand over.
* * *
FROM THE SENATE
The SPEAKER read a message from the Senate, reporting that the following bills had been passed, as sent from the Commons;—
Bill to extend the Grand Trunk Arrangements Act.
Bill to amend the Act relating to Port Wardens at Quebec.
Bill to provide for the examination of witnesses under oath by Committee of the Senate and House of Commons under certain circumstances.
COMMONS DEBATES
390
April 29, 1873
The SPEAKER read another message from the Senate announcing that permission had been given the Hon. Messrs. Macpherson, Cochrane, Foster, Chapais and Campbell to attend and give evidence before the Committee in reference to Mr. Huntington’s charges in relation to the Pacific Railway.
The SPEAKER also read another message from the Senate, with amendments to the following bills:—
Bill incorporating the Isolated Risk Fire Insurance Company.
Bill to incorporate the Three Rivers Bank.
Also, the following bills of their own—
Bill respecting the Central Prison of Ontario.
Bill to unite the Beaver and Toronto Mutual Fire Insurance Companies.
* * *
ISOLATED RISK COMPANY
On motion of Hon. Mr. MACKENZIE, the amendments to the Isolated Risk Insurance Company incorporation bill were read a first and second time, and concurred in.
* * *
EXAMINATION OF WITNESSES ON OATH
Hon. Mr. MACKENZIE enquired if the Government intended to take any steps to have the Royal Assent given to the bill enabling Committees to examine witnesses under oath.
Hon. Sir JOHN A. MACDONALD said his hon. friend could not expect an answer to this question now, as he had only just been made aware that the bill had been passed.
* * *
FIRST READINGS
On motion of Hon. Mr. CAMERON (Cardwell), the bill respecting the Central Prison of Ontario was read a first time.
* * *
KENT CONTROVERTED ELECTION
Mr. MACKAY presented a report of the Committee on Kent Controverted Election, stating that the Committee had found that the recognizance to the petition were insufficient and therefore recommended that the petition should be dismissed.
Mr. MACKAY moved that the Committee on the Kent Controverted Election be dissolved.
Mr. COSTIGAN intimated his intention of moving an amendment, as the Committee had no right to enter into the question of recognizance.
Hon. Mr. MACKENZIE asked the Speaker if the motion was necessary, or if the Committee was not ipso facto dissolved on the presentation of their final report.
Hon. Mr. BLAKE was of that opinion.
Hon. Mr. McDONALD (Pictou) said no doubt that the final report of the Committee was the end of the matter but there might be a difficulty in regard to the wording of the report. The committee had not found that the sitting member was or was not duly elected, but merely recommended that the petition should be dismissed.
After some discussion the motion was, on the recommendation of Hon. Sir JOHN A. MACDONALD withdrawn.
* * *
PRINTING
Mr. STEPHENSON presented the fifth report of the Joint Committee on Printing.
It being six o’clock, The SPEAKER left the chair.
AFTER RECESS
SUPERANNUATION ACT
On motion of Hon. Mr. TILLEY, the Bill to amend the Civil Service Superannuation Fund was read a second time and referred to Committee of the Whole.
In Committee, Mr. JOLY moved an amendment to the effect that in the case of any person who, after the age of 25 years, enters the public service possessing scientific acquirements, which he could not have obtained in the Civil Service, the Governor in Council may add any number of years not exceeding ten to the years of his service in applying the Superannuation Act to him.
Hon. Mr. TILLEY said the question had been considered by the Government, and they had not considered it expedient to adopt it.
The Bill was reported, read a third time, and passed.
* * *
INSPECTION OF GAS
On motion of Hon. Mr. TUPPER, the bill to provide for the inspection of gas and gas metres was read a second time and referred to the Committee on Banking and Commerce.
COMMONS DEBATES
April 29, 1873
SECOND AND THIRD READINGS
The following Bills were read a second time, reported from Committee, read a third time and passed:—
Hon. Mr. LANGEVIN—To amend the Act respecting Joint Stock Companies to construct works to facilitate the transmission of timber down the rivers and streams.
Hon. Mr. TUPPER—Respecting the ocean mail service, with the amendment giving the Government as well as Sir Hugh Allan power to terminate the contract at any time by giving one year’s notice.
The House then went into Committee, Mr. MASSON in the chair.
A desultory discussion ensued, after which the bill was reported as amended.
The amendments were read first and second times and concurred in.
The bill was then read a third time and passed.
* * *
NAVIGATION IMPROVEMENTS
On motion of the Hon. Mr. TILLEY—The bill to authorize the loan of one and a half million dollars to be expended in the improvement of the navigation of Lake St. Peter and the River St. Lawrence and to authorize the imposition of tolls, should it be necessary to meet the interest thereon was read a second time, passed through Committee, and was reported.
* * *
THE INTERCOLONIAL RAILWAY
On motion of Hon. Mr. LANGEVIN—the bill to amend the Act respecting the construction of the Intercolonial Railway was read a second and third time and passed.
* * *
PICTOU HARBOUR
On motion of Hon. Mr. MITCHELL—the bill in relation to the harbour of Pictou was read a second time, passed through committee and was read a third time and passed.
* * *
SUPPLY
The items under the heading of “Indian”, as passed:—
Annual grant to Indians, Quebec Annual grant to Indians, Nova Scotia Annual grant to Indians, New Brunswick To purchase blankets for aged and infirm Indians of Ontario and Quebec and transport thereof
Annuities payable to Indians in the Northwest Territories under Treaty No. l, viz:—Broken Head River Band 93 persons
Fort Alexander Band, 320 persons Fort Garry Indians, 233 persons Pembina Indians, 312 persons Portage la Prairie Band, 425 persons St. Peter’s Band, 1,493 persons
Annuities payable to Indians in the Northwest Territories, under Treaty No. 2.—Fairford River Bands, 299 persons
Lake Manitoba Band, 160 persons
Riding Mountain, Fort Ellice, and Dauphin Lake Bands, 113 persons
Water Hen and Crane River Bands, 176 persons Barons River Band, 447 persons
Fort Francis, Rainy Lake and Contiguous Bands, 1,000 persons
Salaries of the Commissioners of the Northwest Territories, Assistant Commissioners, agents, interpreters, school teachers, and medical officers; travelling expenses of Commissioners, and agent’s office, furniture, medicines and contingencies
Supplies for Indians attending to receive annuities and on other occasions
Farming stock, to be furnished to chiefs not yet supplied
To meet the expenses in connection with Treaties to be made with the tribes of Indians on the Saskatchewan
To pay expenses connected with the Indians of British Columbia
391
follows, were
$400
$3,300
$3,200
$1,600
$279
$960
$699
$936
$1,275
$4,479
$897
$480
$339
$528
$1,341
$3,000
$10,900
$5,000
$1,500
$10,000
$29,000
The House then went into Committee of Supply.
$80,113
COMMONS DEBATES
392
April 29, 1873
In answer to Hon. Mr. Holton,
Hon. Mr. TILLEY said Mr. Provencher was now the Indian Commissioner, receiving a salary of $2,000.
In answer to Mr. Young (Waterloo South),
Hon. Sir JOHN A. MACDONALD said it was expected that immigration would flow into the Saskatchewan Valley and it would be necessary for the Government to make treaties with the Indians there. For this purpose the $10,000 were asked.
Mr. MILLS pointed out that the Indians on the territory west of Lake Superior that belonged to Ontario should be dealt with by the Ontario Government.
Hon. Sir JOHN A. MACDONALD said it made no matter how the boundary was settled, the Indians could only be dealt with by the Crown as represented by the Governor General.
Mr. MILLS contended that the lands there were Crown lands owned by the Local Government, who had to deal with the Indians living on them.
Hon. Sir JOHN A. MACDONALD held that the Indian lands were not public lands at all.
Hon. Mr. MACKENZIE pointed out that the word “reserved” in the Act implied that the lands were public lands, which were reserved for the Indians. He asked if the government had not recently received communications respecting the Indians between Fort William and Fort Garry?
Hon. Sir JOHN A. MACDONALD: Not that I am aware of.
Hon. Mr. MACKENZIE said the miners up there had been put to a great deal of trouble by the Indians, and he understood that the matter had been brought under the notice of the Government. It was absolutely necessary that some arrangement should speedily be made with those Indians, as no doubt the land there would be offered for sale.
Mr. MILLS asked if the tribes of Indians inhabiting the territory in Ontario which had not been transferred to the Local Government by treaty should become extinct, to whom would the land revert? Would it revert to the Dominion Government simply because it had not been transferred by treaty? This showed that the Indian lands could only be regarded as Crown Lands.
Hon. Mr. WOOD wished to know how the item for expenses connected with the Indians in British Columbia came to be put in the estimates. In the other Provinces the Local Governments paid such expenses, and there was nothing in the arrangement with British Columbia requiring the Dominion to assume these expenses. Was this state of things to be continued?
Mr. De COSMOS held that the Dominion Government alone had the arrangement of Indian affairs, and they should pay the expenses.
The items were passed, also the following items, under the head of “Miscellaneous”:—
Printing Canada Gazette	$3,330
Postage	$1,200
Miscellaneous printing	$5,000
Unforeseen expenses: expenditure thereof to be under Order in Council, and a detailed account thereof to be laid before Parliament during the first fifteen days of the next session	$5°,00°
Expenses connected with ascertaining correct time
at Ottawa, and firing of noon gun	$400
For purchase of life boats and life preservers, and maintenance of same, rewards for saving lives and	$9,400
investigations into wrecks and casualties
Mr. TOBIN said he observed there was an increase in the sum asked for under this head of $2,000. He wished to know if the government intended to apply any of this amount to the relief of the poor fishermen of Prospect, who when the starving, half-naked, passengers from the Atlantic were thrown upon their shores, gave them all they had in food and clothing. Foremost among those who had afforded this assistance was the Rev. John Ancient who had saved life by his pluck and energy, and had earned for himself the title of the hero of the Atlantic wreck. This was not only empty praise, but had taken a practical shape in New York, Boston, and in Halifax, where a considerable amount had already been raised for him.
He mentioned the matter that Parliament might have an opportunity of exhibiting its appreciation of many daring, and successful and energetic efforts to save human life. He hoped that if a sum had not been already provided for this purpose it was the intention of the Government in this $2,000 to make provision for the fishermen of Prospect, and the Rev. Mr. Ancient in particular.
Hon. Sir JOHN A. MACDONALD said the circumstances referred to by hon. gentleman had been brought prominently under the notice of the Government by Sir Hastings Doyle, Lieutenant Governor of Nova Scotia, in a despatch which was not in the hands of the Minister of Marine and Fisheries, and he had no doubt a report on the subject would be prepared and submitted to the House before prorogation.
Commutation in lieu of remission of duties on articles imported for the use of the army and navy to be apportioned by Order in Council	$10,000
To provide for examination and classification of
Masters and Mates of Mercantile Marine	$7,000
COMMONS DEBATES
April 29, 1873
To provide for one-half of the British share of the expenditure in reference to surveys of the boundary line between Canada and the United States of America, on the 49th parallel north latitude	$120,000
To pay half of the cost of surveying the boundary line between Ontario and the Northwest Territory, revote	$12,000
Surveys in Manitoba, Northwest Territory	$250,000
Pay and maintenance of Dominion forces in Manitoba, viz:—343 officers, non-commissioned officers and men, including the expense of providing barrack accommodation, contingencies, et cetera	$140,000
Reserve militia stores, third and last instalment due The Imperial Government, on the purchase of reserve stores on withdrawal of regular troops in 1870-1871, for the year ending 30th June 1874	$144,9°°
Mr. YOUNG (Waterloo South) called attention to the fact that no revenue appeared in the public accounts from the Canada Gazette.
Hon. Mr. TILLEY said he would explain on concurrence.
Hon. Mr. MACKENZIE said that it could not be possible that $1,200 was needed for postage on the Gazette.
Hon. Mr. TILLEY promised an explanation on concurrence.
In answer to Hon. Mr. Mackenzie,
Hon. Mr. TILLEY said the plan pursued for surveying the lands in the Northwest was to pay the surveyors so much per township. He would give particulars on concurrence.
Hon. Sir JOHN A. MACDONALD said he believed the men had refused to go back this spring, unless they got an increase in pay. With reference to the force in Manitoba, it was not proposed to diminish that force now. To do so would cause great dismay in that country, where the embers of the troubles of 1869 were still hot, and might be turned into a flame. Besides that, there was some apprehension of Indian troubles. The force was small, but it gave confidence to the people.
He would bring down on the following day a series of resolutions for the establishment of a mounted police, which would be of more service to that part of the country. They would be mounted on strong, hardy horses and would be able to move rapidly from one part of the country to the other, and would supply the place of the present military force.
393
Mr. YOUNG (Waterloo South) asked if he understood rightly that this force was to consist of 150 men.
Hon. Sir JOHN A. MACDONALD: Yes; and the presence of the force now in the Province, which consisted of 300 men, would be dispensed with.
The following items respecting Customs were passed:—
Salaries and contingent expenses of the several ports, viz, in the Province of Ontario, $187,246; Quebec $176,214; New Brunswick, $79,736; Nova Scotia $97,240; Manitoba and North-West Territory, $11,800; British Columbia $24,000.
Salaries and travelling expenses of Inspectors of Ports $11,000.
Contingencies of head office, covering printing, stationery, advertising, telegraphing et cetera, for several ports of entry $15,000.
Hon. Mr. TILLEY called attention to the increase of these sums upon last year, which were as follows:—Ontario, $14,000; Quebec $8,067; New Brunswick $7,360; Nova Scotia $3,927; Manitoba and North-West Territories, $8,000; British Columbia, $4,000. He also explained that it had been found necessary to raise the salaries of officers in consequence of their increased expenses.
Hon. Mr. MACKENZIE said last year he had complained of the discrepancies in the salaries, and this year he observed similar differences, which he referred to in detail.
The Committee rose and reported progress, and asked leave to sit again.
* * *
EXAMINATION OF WITNESSES ON OATH
Hon. Mr. MACKENZIE asked if the leader of the Government could now state whether the Oaths Bill would be assented to tomorrow.
Hon. Sir JOHN A. MACDONALD said he could not. He would be able to tell tomorrow.
Hon. Mr. MACKENZIE said it did appear to him that the bill had been unnecessarily delayed in the Senate, and he did not see why the Royal Assent need be delayed.
PETITION
Mr. DOMVILLE submitted a petition of the Canada Cable Company, praying for the suspension of the standing rules of the House respecting Private Bills.﻿COMMONS DEBATES
394
April 29, 1873
The petition was received, and the House adjourned at l.20 a.m.
* * *
NOTICES OF MOTIONS
Mr. GIBSON—On Thursday next—Enquiry of Ministry whether it is the intention of Government to enlarge and improve the Williamsburg Canal this season or not, and if so, what are the improvements contemplated.
Hon. Sir JOHN A. MACDONALD—On Tuesday next—the following resolution: “That it is expedient to provide first that every judge of any of the Provincial courts who becomes liable to be called upon to try any election petition or to act as a member of an election court shall receive an allowance for the same of $100 for each election petition tried by him, in addition to his salary as such judge of a Provincial court, and a further allowance of $10 per diem for each day during which he is necessarily engaged in the trial of an election petition and his travelling expenses when absent on any such duties from his place of residence; and second, every judge ad hoc appointed to try an election petition shall receive a like allowance of $100 for each election petition tried by him, and a further allowance of $10 per diem for each day during which he is necessarily engaged in the trial of an election petition or at a sitting of the Election Court, and his travelling expenses when absent on any such duties from his place of residence. Third, and such allowances shall be paid out of any unappropriated moneys forming part of the Consolidated Revenue Fund of Canada, on the report of the Auditor General that they have been claimed and are due.
That the travelling and other expenses incurred by the Sheriff or other officer in consequence of any sittings for the trial of an election petition and providing a court room and accessories, shall be defrayed in like manner as other incidental expenses payable by the Dominion under this Act. The reasonable expenses incurred by any person in appearing to give evidence at the trial of an election petition under this Act, according to the scale allowed to witnesses on the trial of civil actions in the Superior Courts of Law in the same Province, may be allowed to such person by a certificate under the hand of the Judge or of the Clerk of the Election Court, or the prescribed officer; and such expenses of providing a court in other cases shall be deemed the costs of the party calling the witnesses and shall be taxed against such party interested in the trial of such petition as the judge may determine. Fourth, that the duties to be performed by the clerk or other prescribed officer of any election Court, under this Act or rules of the Court shall, if the Election Court consist of judges of any Dominion or Provincial Court or Courts, be performed by such officer or officers of the Court or Courts last mentioned as the Judges of the Election Court may appoint; and if the Election Court consisted of judges
appointed ad hoc then by such person or persons as the Government may appoint to act as such clerk or other prescribed officer; and the remuneration to be allowed in either case for such services shall be fixed by the Governor in Council on the report of the Election Court in question.”
Mr. WITTON—On Tuesday next—Resolution that in view of the great importance to the whole community of the growing manufacturers of the Dominion, it is expedient and highly desirable that the fullest information should be sought by the Government regarding the utilization of raw materials in various processes of manufacture which it is the special object of the forthcoming exhibition at Vienna to show.
Mr. PALMER—Thursday—Committee of the Whole on the following resolution, “That in the opinion of this House it is expedient to provide for the inspection and classification of all sea going vessels built in Canada.”
Mr. PALMER—Thursday—Committee of the Whole, to consider the following resolution, “That in the opinion of this House, a Commission should forthwith issue to make enquiry with regard to the alleged unseaworthiness of Canadian ships, arising from over loading, deck-loading, defective construction condition, equipment, form, or machinery, age or improper storage, and also to enquire into the present system of inspection of sea going vessels and marine insurance, and the state of the law on the liabilities of ship owners for injury to these whom they employ, and the effect of under-measuring ships and to suggest the best remedy for the removal of such evils as may have arisen from the matters aforesaid.”
Mr. EDGAR—Thursday—Address for statement showing, first, the name of the steamship companies or shipowners whose vessels were employed during the year 1872 to carry to Canada the immigrants who received assisted passages from the Government; second, the price or different prices paid to the vessel owners in 1872 for each adult so carried, distinguishing the portion paid by the Government and the portion otherwise paid; third copies of any advertisements asking for offers or tenders for steamship owners to any emigrant receiving assisted passengers in 1872 or 1873, and the names of the newspapers in which such advertisements were published; fourth the names of any steamship company or ship owners with whom engagements had been entered into for the carriage of emigrants receiving assisted passengers in 1873, and the different rates or prices to be received by the respective vessel owners for each adult so carried, distinguishing the portion payable by the Government, and the portion otherwise payable.
Mr. JETTE—Thursday—A Bill entitled an Act to abolish the property qualification of members of the House of Commons.
COMMONS DEBATES
April 30, 1873
395
HOUSE OF COMMONS
Wednesday, April 30, 1873
