HOUSE OF COMMONS DEBATES
OFFICIAL REPORT
SECOND SESSION-THIRTY-THIRD PARLIAMENT
37 Elizabeth II
VOLUME XIII, 1988
COMPRISING THE PERIOD FROM THE TWENTY-SIXTH DAY OF MAY, 1988 TO THE FIFTH DAY OF JULY, 1988
INDEX ISSUED IN A SEPARATE VOLUME
Published under authority of the Speaker of the House of Commons by the Queen’s Printer for Canada
Available from Canadian Government Publishing Centre, Supply and Services Canada, Ottawa, Canada K1A 0S9.
15781
HOUSE OF COMMONS
Thursday, May 26, 1988
