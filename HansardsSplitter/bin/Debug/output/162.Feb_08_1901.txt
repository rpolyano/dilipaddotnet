 at Three o’clock.
A Message was delivered by R6n6 Edouard Kimber. Esq., Gentleman Usher of the Black Rod ;
Mr. Speaker :
His Excellency the Governor General desires the immediate attendance of this Honourable House in the Senate Chamber.
Accordingly the House went up to the Senate Chamber.
Then the Hon. Louis Philippe Brodeur Speaker elect, said :
May it please Your Excellency :
The House of Commons has elected me their Speaker, though I am but little able to fulfil the important duties thus assigned me. If in the performance of those duties, I should at any time fall into error, I pray that the fault may be imputed to me, and not to the Commons, whose servant I am, and who, through me, the better to enable them to discharge their duty to their King and country, humbly claim all their undoubted rights and privileges, especially that they may have freedom of speech in their debates, access to Your Excellency’s person at all seasonable tiufes, and that their proceedings may receive from Your Excellency the most favourable consideration.
The Hon. The Speaker of the Senate then said :
Mr. Speaker :
I am commanded by His Excellency the Governor General to declare to you that he freely confides in the duty and attachment of the House of Commons to His Majesty’s person and government, and not doubting that their proceedings will be conducted with wisdom, temperance and prudence, he grants and upon all occasions will recognize and allow their constitutional privileges.
I am commanded also to assure you that the Conmons shall have access to His Excellency
5
FEBRUARY 7, 1901
6
upon all seasonable occasions, and that their proceedings, as well as your words and actions, will constantly receive from him the most favourable construction.
Then His Excellency the Governor General, was pleased to open Parliament by a Speech from the Throne.
And the House being returned,
Mr. SPEAKER. I have the honour to state that the House, having attended on His Excellency the Governor General in the Senate Chamber, I informed His Excellency that the choice of Speaker had fallen on me, and, in your names, and on your behalf, I made the usual claim for your privileges, which His Excellency was pleased to confirm to you.
FIRST READING.
Bill (No. 1) respecting the Administration of Oaths of Office.—(Sir Wilfrid Laurier.)
SPEECH FROM THE THRONE.
Mr. SPEAKER. I have the honour to inform this House that when the House attended His Excellency the Governor General this day in the Senate Chamber, His Excellency was pleased to make a Speech to both Houses of Parliament, and, to prevent mistakes, I have obtained a copy of tlie Speech, which is as follows :—
Honourable Gentlemen of the denote :
Gentlemen of the House of Commons :
Since our last meeting the Empire has been called on to lament the demise of Her late Majesty Queen Victoria. The universal regret and sympathy with which the tidings of her decease have been received throughout the entire civilized world, afford the best testimony to the manner in which she has, at all times, discharged her duties, both as a woman and a sovereign, throughout her unprecedentedly long and glorious reign, and I will venture to add that in no portion of her vast territories were those sentiments more profoundly felt than in the Dominion of Canada.
You will, I am sure, take early action to express your sympathy with the Royal Family in their bereavement and your loyalty to the new Sovereign.
The Canadian contingents to South Africa have nearly all returned, and it affords me a very great gratification to be able to assure you that the valour and good conduct of our Canadian soldiers have called forth the highest encomiums from the several commanders under whom they have served during the arduous contest.
The union of the several provinces of Australia into one confederation, upon lines closely resembling those on which our own Dominion has been established, marks another important step towards the consolidation of the outlaying portions of the Empire, and, I am well assured, will call forth your m- st, sincere congratulations to the new commonwealth.
Acting on the advice of my ministers, I had, previously to the great grief which has fallen
li
upon the nation, tendered an invitation on your behalf to His Excellency the Duke of Cornwall and York to conclude his intended visit to Australasia by one to the Dominion of Canada, and I am glad to be able to inform you that His Royal Highness has been pleased to signify his acceptance of the same. I still hope that that visit may not be considered impossible. I have no doubt of the warmth of the welcome with which he will he received.
My government has learned with great satisfaction of the progress being made with the Pacific cable scheme, and I trust that nothing may occur to delay its early completion.
Last summer I made a tour through Canada as far as Dawson City, and was everywhere received with unqualified proofs of devotion and loyalty. During my journey, I was, from personal observation, much impressed with the great activity displayed in the development of the mining and agricultural industries of the country, and with the substantial increase in its population. The thrift, energy and law-abiding character of the immigrants are a subject of much congratulation, and afford ample proof of their usefulness as citizens of the Dominion.
It gives me great pleasure to note the excellent display made by Canada at the Universal Exposition in Paris. The fine quality and varied character of Canadian natural and industrial products is evidenced by the number of awards won In nearly every class of the competition. It is a remarkable testimony to the effectiveness of our cold storage transportation facilities, that fresh fruit grown in Canada secured a large number of the highest awards. It is extremely gratifying to observe that, as a result of the display of Canadian resources, considerable foreign capital has found its way to Canada for investment and large orders from foreign countries have been received for Canadian goods.
The improvement of the St. Lawrence route continues to engage the very careful attention of my government. During the past year ship channels have been widened and deepened, additional lights and buoys have been provided and, in a short time, there will be telegraph and cable communication with Belle Isle. These additional securities will tend to make safer and more efficient than ever our great waterway between the lakes and the Atlantic.
I am glad to observe that the revenue and (he general volume of trade continue undiminished, and even show a moderate increase over the very large figures attained during the past year.
Measures will be submitted to you for the better supervision of the export trade in food products, and also in connection with the Post Office, the Pacific cable and various other subjects.
Gentlemen of the House of Commons :
The accounts for the past year will be laid before you.
The Estimates for the succeeding year will likewise be placed upon the Table at an early date.
Honourable Gentlemen of the Senate :
Gentlemen of the House of Commons :
I commend to your earnest consideration the measures to be submitted to you, invoking the Divine blessings upon the important labours on which you are again entering.
COMMONS
8
The PRIME MINISTER moved :
That the Speech ot His Excellency the Governor General to both Houses of the Parliament of the Dominion of Canada, he taken into consideration on Monday next.
Motion agreed to.
SELECT STANDING COMMITTEES.
The PRIME MINISTER (Sir Wilfrid Laurier) moved :
That Select Standing Committees of this House for the present session be appointed for the following purposes :—1. On Privileges and Elections. 2. On Expiring Laws.	3. On
Railways, Canals and Telegraph Lines.	4. On
Miscellaneous Private Bills. 5. On Standing Orders. 6. On Printing. 7. On Public Accounts. 8. On Banking and Commerce. 9. On Agriculture and Colonization,—which said committees shall severally be empowered to examine and inquire into all such matters and things as may be referred to them by the House; and to report from time to time their observations and opinions thereon, with power to send for persons, papers and records.
Motion agreed to.
ADJOURNMENT—NEW LEADER OF THE OPPOSITION.
The PRIME MINISTER (Sir Wilfrid Laurier.) Before I move the adjournment of the House, I desire to give notice that to-morrow, if it be convenient to my lion, friends on the other side, I propose to move an address to His Majesty the King, of which I intimated the nature yesterday. I will avail myself of this opportunity to tender my congratulations, as well as the congratulations of this side of the House, to my hon. friend the senior member for Halifax (Mr. Borden) on his advancement to the high office of leader of His Majesty’s loyal opposition. Of course, it would not be fitting for me to offer any suggestion whatever, or any observations, as to what should be the internal policy of the Conservative party ; but speaking as a citizen of Canada, I am quite sure that the elevation of my hon. friend to the position that he now occupies must be very gratifying to himself. Speaking personally, and as leader of the House, it affords me much pleasure to think in advance that the relations between my hon. friend and myself will be always pleasant and cordial. I am well aware that it will be my painful duty on many occasions to dissent from the views of my hon. friend because, as it is his misfortune to be in the wrong on the main question. I do not anticipate that he can be in the right on the minor ones. But we will agree to disagree as we have done in the past, and I am quite sure that my hon. friend will believe in my absolute sincerity when I tell
Mr. SPEAKER.
him that I hope, with all my heart, he may continue to exercise for a long, long period, the functions of leader of the opposition. I beg to move the adjournment of the House.
Mr. It. L. BORDEN (Halifax). Mr. Speaker, I thank the right hon. gentleman for the kindness and courtesy which he has so well expressed in the remarks he has just seen fit to make with regard to myself. He will permit me, however, to say, in passing, that if I should remain leader of the opposition for as long a period as that joke is old, it will be wholly beyond my own expectations, and beyond the expectations of the lion, gentlemen on this side of the House. It will be our painful duty, I expect, on some occasions, to differ with the right hon. gentleman and those who support him, as to what will be the best interests of the country, but I can only say that I shall add my efforts to his in the direction that our differences shall be adjusted, so far as they can be adjusted in this House, in a kindly and courteous manner. In saying that, I am sure that I voice the sentiments of every hon. gentleman on this side of the House. It is needless for me to say that I have accepted the trust which has been reposed in me by hon. gentlemen on this side of the House with a great deal of diffidence and hesitation. That diffidence and that hesitation have not been inspired by any fear of the loyalty or fidelity of lion, gentlemen on this side of the House, in which I have the most perfect confidence. They have rather been inspired by my own comparative inexperience in public life, and by the fact that I doubted my own capacity to follow in the footsteps of those great men, who, in times past, have filled the position of leader, on one side or other, in the House, of the party which I now have the honour to lead in this House. I remember also that the traditions of tiiis House have been sustained by great men who are now on the other side of the House, but who for many years fought the battles of their party with more or less varying fortune from the side of the House which is now occupied by my hon. friends and myself. It will be quite convenient for us to dispose of the motion to which the right hon. gentleman has referred to-morrow, and I am sure that the motion which has been suggested will receive the most cordial support from hon. gentlemen on this side of the House.
REPORT.
Report of the Joint Librarians of Parliament.— (Mr. Speaker.)
On motion of the Prime Minister the House adjourned at 3.50 p.m.
9
FEBRUARY 8, 1901
10
HOUSE OF COMMONS.
Friday, February 8, 1901.
