HOUSE OF COMMONS DEBATES
OFFICIAL REPORT
FIRST SESSION—THIRTY-SECOND PARLIAMENT
32 Elizabeth II
VOLUME XXI, 1983
COMPRISING THE PERIOD FROM THE NINTH DAY OF MARCH, 1983 TO THE TWENTY FIRST DAY OF APRIL, 1983
INDEX ISSUED IN A SEPARATE VOLUME
Published under authority of the Speaker of the House of Commons by the Queen’s Printer for Canada
Available from Canadian Government Publishing Centre, Supply and Services Canada, Hull, Quebec, Canada K1A 0S9.
23589
HOUSE OF COMMONS
Wednesday, March 9, 1983
