CANADA
HOUSE OF COMMONS DEBATES
OFFICIAL REPORT
THIRD SESSION—THIRTIETH PARLIAMENT 27 Elizabeth II
VOLUME IV, 1978
COMPRISING THE PERIOD FROM THE SECOND DAY OF MARCH, 1978 TO THE SEVENTEENTH DAY OF APRIL, 1978
INDEX ISSUED IN A SEPARATE VOLUME
Published under authority of the Speaker of the House of Commons by the Queen's Printer for Canada Available from Canadian Government Publishing Centre, Supply and Services Canada, Hull, Quebec, Canada K1A 0S9, at 5 cents per copy or $3.00 per session
80033-1%
HOUSE OF COMMONS
Thursday, March 2, 1978
3375
