O F Fie IAL REPORT
OF THE
DEBATES
HOUSE
Ojf Gu’jvI
OF THE
DOMINION OF CANADA
SIXTH SESSION—SEVENTH PARLIAMENT
59 VICTORIA, 1896
VOL. XLI.
COMPRISING THE PERIOD PROM THE SECOND DAY OP JANUARY TO THE THIRTEENTH DAY OF MARCH INCLUSIVE.
OTTAWA
print® m & k. dawson, pkekser tv tsee qu.
EXCELLENT HA3H9TY
ISM
oat
MEMBERS OF THE GOVERNMENT
OF THE
HON. SIR MACKENZIE BOWELL, K.C.M.G.
AT THE OPENING OF THE
SIXTH SESSION OF THE SEVENTH PARLIAMENT
1896
President of the Council (Premier)
Postmaster General.. ............
Minister of Marine and Fisheries.
Without Portfolio................
Minister of Finance......... ..
Minister of Justice..............
Minister of Railways and Canals..
Minister of Public Works.........
Secretary of State (Acting)......
Minister of the Interior.........
Minister of Trade and Commerce. Minister of Militia and Defence...
Minister of Agriculture..........
Without Portfolio................
Hon. Sir Mackenzie Bowell, K.C.M.G.
Hon. Sir Adolphe P. Caron, K.C.M.G.
Hon. John Costiuan.
Hon. Sir Frank Smith, Kt.
Hon. George E. Foster.*
Hon. Sir Charles Hibbert Tuppkr, K.C.M.G.* Hon. John Haggart.*
Hon. J. A. Odimet.
Hon. J. A. Ouimet.
Hon. T. Maynk Daly.
Hon. W. B. Ives.*
Hon. A. R. Dickey.*
Hon. W. H. Montague.* lion. Donald Ferguson.
[The above formed the Cabinet.]
Solicitor General.....................................
Controller of Customs.................................Hon. John F. Wood*.
Controller of Inland Revenue......... ................Hon. Edward Gawlrr Prior.
CABINET FORMED 15th JANUARY, 1896.
President of the Council (Premier).
Secretary of State...............
Postmaster General...............
Minister of Marine and Fisheries..
Without Porfcfc, j...............
Minister of Finance...... .......
Minister of Railways and Canals..
Minister of Public Works.........
Minister of the Interior.........
Minister of Trade and Commerce.
Minister of Justice..............
Minister of Agriculture..........
Without Portfolio................
Controller of Customs............
Controller of Inland Revenue.....
Minister of Militia and Defence...
.Hon. Sir Mackenzie Bowell, K.C.M.G.
.Hon. Sir Charles Tupper, Bart., G.C.M.G., C.B .Hon. Sir Adolphe P. Caron, K.C.M.G.
.Hon. John Costigan.
.Hon. Sir Frank Smith, Kt.
Hon. George E. Foster.
.Hon. John Haggart.
.Hon. J. A. Ouimet.
.Hon. T. Maynk Daly.
Hon. W. B. Ives.
.Hon. A. R. Dickey.
.Hon. W. H. Montague.
.Hon. Donald Ferguson.
.Hon. John F. Wood.
.Hon. Edward Gawlsr Prior.
.Hon. Alphonse Desjardins.
Clerk of the Privy Council
John J. McGee, Esq.
* Resigned Portfolios 5th January, 1893.
IV
OFFICERS OF THE HOUSE OF COMMONS.
OFFICERS OF THE HOUSE OF COMMONS:
Hon. Peter White................................... Speaker.
Joseph G. H. Bergeron, M.P..........................Deputy Speaker.
John G. Bourinot, Esq...............................Clerk of the House.
Francois Fortunat Rouleau, Esq....................... .Clerk Assistant.
Lieut.-Col. Henry Robert Smith.  ...................Sergeant-at-Arras.
Official Reporters :
George B. Bradley ..............................Chief Rej>orter.
Stephen A. Abbott.............................. ^
E. Joseph Duggan................................
Albert Horton.....................................
J. O. Marceau.................................. )■ Reporters.
Thos. P. Owens..................................
Alphonse Desjardins............................
A. C. Campbell..................................,
J. Charles Boyce...............................Assistant to Chief Reporter.
ALPHABETICAL LIST
OF THE
CONSTITUENCIES AND MEMBERS
OK THE
HOUSE OF COMMONS
Sixth Session of the Seventh Parliament of the Dominion of Canada
1896
Aldington—George W. W. Dawson.
Albert—Richard Chapman Weldon.
Alberta---Donald Watson Davis.
Algoma—George Hugh Macdonell.
Annapolis—John B. Mills.
Antigonish—Colin F. Mclsaac.
Argenteuil—Thomas Christie.
Assiniboia, East—William Walter McDonald. Assiniboia, West—Nicholas Flood Davin.
Bagot—Flavien Dujxmt.
Bkacce—Joseph God bout.
Beauharnois—Joseph Gedeon Horace Bergeron. Bsllechasse—Guillaume Amyot. *
Bebthier—Cleophas Beausoleil.
Bonaventure—William Le Boutillier Fauvel. Bothwell—Hon. David Mills.
Brant, N. Riding—James Somerville.
Brant, S. Riding—William Paterson. Brocevillk—Hon. John Fisher Wood.
Brome—Eugene A. Dyer.
Bruce, E. Riding—Henry Cargill.
Bruce, N. Riding—Alexander McNeill.
Bruce, W. Riding—Janies Rowand.
f Hector F. McDougall.
Oapb Breton— •! David McKeen.f
vSir Charles Tupper, Bart.? Cardwell—William Stubbs.
Carleton (N.B.)—Newton Ramsay Colter. Carleton (O.)—William T. Hod gins.
Cariboo—Frank S. Barnard.
Chambly—Raymond Prefontaine.
Champlain—Onesime Carignan.
Charlevoix—Charles Angers.?
Charlotte—Arthur Hill Gillmor.
Chateauguay—James Pollock Brown.
* Died on or about 30th March, t Resigned; appointed to the Senate, 21st February ? Elected during session; took seat 11th February.
Chicoutimi and Saguenay —Louis do Gonzagu Bellcy,
Colchester—William A. Patterson.
Compton—Rufus Henry Pojk\
Cornwall and Stormont—Darby Bergin. Cumberland—Hon. Arthur R. Dickey.
Digby—Edward Charles Bowers.
Dorchester—Cyrille Emile Vaillaneourt. Drummond and Arthabaska—Joseph Lavergne. DuNDAs—Hugo H. Ross.
Durham, E. Riding—Thomas Dixon Craig.
Di rham, W. Riding—Robert Beith.
Elgin, E. Riding—Andrew B. Ingram.
Elgin, W. Riding- George Elliott Casey.
Essex, N. Riding—William McGregor.
Essex, S. Riding—Henry W. Allan.
Frontenac—Hiram A. Calvin.
G-asp£—Louis Zephirin Joncas.
Glengarry—Roderick R. McLennan.
(* loucester—Theot ime Blanchard.
Grenville, S. Riding—John Dowsley Reid.
Grey, E. Riding—Thomas S. Sproule.
Grey, N. Riding—James Masson.
Grey, S. Riding—George Landerkin. Guysborough—Duncan C. Fraser.
Haldimand—Hon. Walter H. Montague.
I Thomas E. Kenny.
HALib^x	\john Fitz-William Stairs.
Halton—David Henderson.
/ Alexander McKay.
Hamilton j Samuel S. Ryekman.
Hants—Alfred Putnam.
Hastings, E. Riding—William B. Northrup. Hastings, N. Riding—A. W. Carscallen.
Hastings, W. Riding—Henry Corby.
Hochklaga—Severin Lachapelle.
Huntingdon—Julius Soriver.
vi
LIST OF MEMBERS OF THE HOUSE OF COMMONS.
Huron, E. Riding—Peter Macdonald.
Huron, S. Riding—John McMillan.
Huron, W. Riding—Malcolm Colin Cameron. *
Ibervillb—Fran$ois Bechard.
Inverness—Hugh Cameron.
Jacques-Cartier—Napoleon Charbonneau. Joliette—Urbain Lippe.
Kamouraska—Henry George Carroll.
Kent (N.B.)—George V. Mclnemey.
Kent (O.)—Archibald Campbell.
Kino’s (N.B.)—Hon. George Eulas Foster. King's (N.S.)—Frederick W. Borden.
^ ,,	/ Augustine Colin Macdonald.
Kino’s (P.K.I.)- tJohll McLwul.
Kingston—James Henry Metcalfe.
IiAMBTON, E. Riding—George Moncrieff.
Lambton, W. Riding—Janies Frederick Lister. Lanark, N. Riding—Bennett Rosamond.
Lanark, S. Riding—Hon. John Graham Haggart. Laprairie— Louis Conrad Pelletier.
L’Assomption—Horniisdas Jeannotte.
Laval—Hon. Joseph Aldric Ouimet.
Leeds and Grenville, N. Riding—Charles Frederick Ferguson.
Leeds, S. Riding—George Taylor.
Lennox—Uriah Wilson.
L£vis— Pierre Malcolm Guay.
Lincoln and Niagara- William Gibson.
Llsgar—Arthur Wellington Ross.
LTslet—J. Israel Tarte.
London—Hon. Sir John Carling, K.C.M.G.f Lotbinierk—C6me Isaie Rinfret.
Lunenburg—Charles Edwin Kaulbnch.
Marquette—Nathaniel Boyd.
Maskinong£—Joseph Hormisdas Legris.
Meg antic—Louis J. Cflte, alias Frechette. Middlesex, E. Riding—Joseph Henry Marshall. Middlesex, N. Riding—William H. Hutchins. Middlesex, S. Riding—Robert Boston.
Middlesex, W. Riding—William Frederick Roome. Missisquoi—George Barnard Baker. +
Monck—Arthur Boyle.
Montcalm —Louis E. Dugas.
Montmagnv—Philippe A. Choquette.
Montmorency—Arthur J. Turcotte.
Montreal, Centre—James McShane.
Montreal, East—Alphonse Telesphore Lepine. Montreal, West—Sir Donald Smith, K.C.M.G. Muskoka—William Edward O’Brien.
Napierville—Dominique Monet.
New Westminster—Gordon E. Cortwuld.
Nioolet—Joseph Hector Leduc.
Norfolk, N. Riding—John Charlton.
i	■.. - ■-	— —. .	..... ■	.	..	■■ . .	..	--- --
* Elected during session; took seat 22nd January, f* Appointed to the Senate, 23rd April.
£ Appointed to the Senate, 7th January.
Norfolk, S. Riding—David Tisdale.
Northumberland (N.B.)— /	Adams.*
I James Robinson, f
Northumberland (O.) E. R,—Edward Cochrane. Northumberland (O.) W. R.—George Guillefc.
Ontario, N. Riding-John A. McGillivray. Ontario, S. Riding—William Smith.
Ontario, W. Riding—James David Edgar.
^	f Sir James A. Grant, K.C.M.G.
Ottawa (City)-	R„billard.
Ottawa (County)—Charles Ramsay Devlin.
Oxford, N. Riding--James Sutherland.
Oxford, S. Riding—Hon. Sir Richard Cartwright, K.C.M.G.
Peel- Joseph Featherston.
Perth, N. Riding - James Nieol Grieve.
Perth, S. Riding—William Pridham.
Peterborough, E. Riding—John Burnham. Peterborough, W. Riding-James Stevenson.
/Hon, Sir Charles Hibbert Tup|x*r, Pictou—| K.C.M.G.
v John McDougald.
Pontiac—John Bryson.*
PoRTNKUF—Arthur Delisle.
Prescott—Isidore Proulx.
i Stanislas F. Perry.
Pkinoe (P.E.I.)—{ Y|H)
Prince Edward—Archibald CampUdl Miller. Provencher—Alphonse A. C. LaRiviere.
Quebec, Centre—Francis Langelier.
Quebec, East—Hou. Wilfred Laurier.
Quebec, West—Thomas McGreevy.
Quebec (County)—Jules J. T. Fremont.
Queen’s (N.B.)—George Frederick Baird.
Queen’s (N.S.)—Francis Gordon Forbes.
/ Louis Henry Davies.
Queen’s (P.E.I.)— \ will:am Welsh.
Renfrew, N. Riding—Hon. Peter White.
Renfrew, S. Riding—John Ferguson. Restigouche—John McAlister.
Richelieu—Arthur Ain»e Biruneau.
Richmond (N.S.)—Joseph A. Gillies.
Richmond and Wolfe (Q.)—Clarence C. Cleveland. Rimouski—Hon. Sir Adolphe Caron, K.C.M.G. Rouvillk—Louis Philips Brodeur.
Russell—William Cameron Edwards.
St. Hyacinthk—Michel E. Bernier.
St. John (N.B.) City—Ezekiel McLeod.
«	»	^ ^ x	^	/ J. Douglas Hazen.
St. John (N. B.), City and Co. | john A. chealey.
St. Johns (Q.)—Francois Bourassa.
St. Maurice—Francois Severe L. Desaulniers.
* Appointed to the Senate, 7th January, t Elected during sessson ; took seat 12th February. X Died on or about 20th January.
LIST OF MEMBERS OF THE HOUSE OF COMMONS.
' « •
Vll
Saskatchewan—Day Hart MacDowell. Selkirk—Hon. Thomas Mayne Daly. Shepford—John Robbins Sanborn. Shelburne—Nathaniel W. White. Sherbrooke—Hon. William Bullock Ives. SimcqKj E. Riding—William H. Bennett. Simcoe, N. Riding—Dalton McCarthy. Simcoe, S. Riding—Richard Tyrwhitt. Soulanges—James William Bain. Stanstead—Timothy Byron Rider. Sunbury—Robert Duncan Wilmot.
T^mtscouata—Paul Etienne Grandbois. Terrebonne—Pierre Leclair.
Three Rivers—Hon. Sir Hector Langevin, K.C. M.G. Toronto, Centre—George Ralph R. Cockburn. Toronto, East—Emerson Coatsworth, jun.
Toronto,West— Frederick Charles Denison, C.M.G.* Two Mountains—Joseph Oirouard.
Vancouver Island—Andrew Haslam.
Yaudreuil—Henry Stanislaus Harwood.
VERCHisRKS—C. A. Geoffrion.
f Hon. Edward Gawler Prior. Victoria <B.C.)-(Thomas Karle_
Victoria (N.B.)—Hon. John Costigan.
Viotoria (N.S.)—John Archibald McDonald. Victoria (O.) N. Riding—Samuel Hughes. Victoria (O.) S. Riding—Charles Fairbairn.
Waterloo, N."Riding—Isaac Erb Bowman. Waterloo, S. Riding—James Livingston. Welland—James A. Lowell.
Wellington, C. Riding—Andrew Semple. Wellington, N. Riding—James McMullen. Wellington, S. Riding—James Innes. Wentworth, N. Riding—Thomas Bain. Wentworth, S. Riding—Franklin M. Carpenter. Westmoreland—Henry A. Powell.
Winnipeg—Joseph Martin.
Yalk—John Andrew Mara.
Yam ask a—Roch Moise Samuel Mignault. Yarmouth—Thomas Barnard Flint.
York (N.B.)—Thomas Temple.*
York (O.) E. Riding—William Findlay Maclean. York (O.) N. Riding—William Mulock.
York (O.) W. Riding—Hon. N. Clarke Wallace.
* Died on or about 15th April.
* Appointed tc the Senate, 7th January.
SELECT COMMITTEE APPOINTED TO SUPERVISE THE PUBLICATION OF THE OFFICIAL REPORTS OF THE DEBATES OF THE^HOUSE.
Beausoleil, Mr. CleopKas (Berihier).
BIschard, Mr. Frangois (Iberville).
Cameron, Mr. Hugh (Inverness).
Charlton, Mr. John (Worth Norfolk).
Craig, Mr. Thomas I). (East Durham).
Davin, Mr. Nicholas Flood (West Assiniboia). Earle, Mr. Thomas (Victoria, B.C.).
Hazen, Mr. J. Douglas (St. John City and County).
Innes, Mr. James (South Wellington).
LaRivi&re, Mr. Alphonse A. C. (Proveueher). Lupine, Mr. Alphonse T&esphore (East Montreal), Scriver, Mr. Julius (Huntingdon).
Somerville, Mr. James (North Brant).
Taylor, Mr. George (South Leeds).
Weldon, Mr. R. Chapman (Albert).
Chairman .—Mr. Alphonse A. C. LaRiv^ERE (Proveueher).
LIST OP PAIRS DURING THE SESSION.
««•
via
LIST OF PAIRS DURING THE SESSION.
On Mr. Laurikr’s proposed amendment (6 m. h.) to i	Ministerial,
Sir Charles Tupper’s motion for Second Reading |	CLEVELAND,
of Bill 58 (The Remedial Act, Man.) 20th j	CHESLKV,
March:—
Opposition,
Mr. LAVERUNE, Mr. FORBES.
Ministerial.	Opposition.
Sir DONALD SMITH, Mr. ROW AND, Mr. MONTAGUE, Mr. DENISON.
On Mr. O’Brien’s amendment to Sir Charles Tipper’s projosed motion {reSaturday Sitting of the House) 2nd April:—
On Mr. Ouimkt's amendment to Sir Charles Tupper’s proposed motion (re Saturday Sittings of the House) 2nd April
Mr. HASLAM,	Mr.	McSHANK,
Mr. DICKEY,	Mr.	PR^FONTAINE,
Sir DONALD SMITH, Mr.	(iKOFFRION,
Mr. JONCAS,	Mr.	MCDONALD
(Huron)
| Mr. HASLAM, i Mr. DICKEY, j Sir DONALD SMITH, I Mr. JONCAS,
! Mr. CLEVELAND, Mr. CHESLEY,
Mr. McSHANE,
Nlr. PREFONTAINE, Mr. GEOFFRION,
Mr. McDonald
(Huron),
Mr. LAVER!INK,
Mr. FORBES.
i
House of Commons debates
SIXTH SESSION-SEVENTH PARLIAMENT
HOUSE OF COMMONS.
Thursday, 2nd January, 1896.
Of Hon. John Joseph Curran, Member for the Electoral District of Montreal Centre, by the acceptance of an office of emolument under the Crown, to wit : that of a Judge of the Superior Court of the Proviuee of Quebec.
The Parliament, which had been prorogued from time to time, was now commanded to assemble on the 2nd day of January, 1S9C, for the despatch of business.
