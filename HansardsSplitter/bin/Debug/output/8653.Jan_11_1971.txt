CANADA
HOUSE OF COMMONS
DEBATES
OFFICIAL REPORT
THIRD SESSION—TWENTY-EIGHTH PARLIAMENT 19-20 Elizabeth II
VOLUME III, 1971
COMPRISING THE PERIOD FROM THE ELEVENTH DAY OF JANUARY, 1971, TO THE TWELFTH DAY OF FEBRUARY, 1971, INCLUSIVE
INDEX ISSUED IN A SEPARATE VOLUME
Published under the authority of the Speaker of the House of Commons by the Queen's Printer for Canada Available from Information Canada, Ottawa, Canada
23786—1*
2247
HOUSE OF COMMONS
Monday, January 11, 1971
