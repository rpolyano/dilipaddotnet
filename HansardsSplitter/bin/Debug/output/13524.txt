Technical and Bibliographic Notes / Notes techniques et bibliographiques
The Institute has attempted to obtain the best original copy available for filming. Features of this copy which may be bibliographically unique, which may alter any of the images in the reproduction, or which may significantly change the usual method of filming are checked below.
L'lnstitut a microfilme le meilleur exemplaire qu’il lui a ete possible de se procurer. Les details de cet exemplaire qui sont peut-etre uniques du point de vue bibliographique, qui peuvent modifier une image reproduite, ou qui peuvent exiger une modification dans la methode normale de filmage sont indiques ci-dessous.
□
□
□
□
□
□
□
□
□
□
□
Coloured covers /
Couverture de couleur
Covers damaged /
Couverture endommagee
Covers restored and/or laminated / Couverture restauree et/ou pelliculee
Cover title missing /
Le titre de couverture manque
□
□
□
0
□
Coloured pages / Pages de couleur
Pages damaged / Pages endommagees
Pages restored and/or laminated /
Pages restaurees et/ou pelliculees
Pages discoloured, stained or foxed/ Pages decolorees, tachetees ou piquees
Pages detached / pages detachees
Coloured maps /
Cartes geographiques en couleur
Coloured ink (i.e. other than blue or black) / Encre de couleur (i.e. autre que bleue ou noire)
Coloured plates and/or illustrations /
Planches et/ou illustrations en couleur
Bound with other material /
Relie avec d’autres documents
Only edition available /
Seule edition disponible
Showthrough / Transarence
0
□
□
Quality of print varies /
Qualite inegale de I’impression
Includes supplementary materials Comprend du materiel supplemental
Pages wholly or partially obscured by errata slips, tissues, etc., have been refilmed to ensure the best possible image / Les pages totalement ou partiellement obscurcies par un feuillet d’errata, une pelure, etc., ont ete filmees a nouveau de fagon a obtenir la meilleure image possible.
Tight binding may cause shadows or distortion along interior margin J La reliure serree peut causer de I’ombre ou de la distorsion le long de la marge interieure.
Blank leaves added during restorations may appear within the text. Whenever possible, these have been omitted from filming / II se peut que certaines pages blanches ajoutees lors d’une restauration apparaissent dans le texte, mais, lorsque cela etait possible, ces pages n’ont pas ete filmees.
Opposing pages with varying colouration or discolourations are filmed twice to ensure the best possible image / Les pages s’opposant ayant des colorations variables ou des decolorations sont filmees deux fois afin d’obtenir la meilleure image possible.
Additional comments / Commentaires supplementaires:
Pagination is as follows: [1], [815]-1467, [i]-xlviii p. Page 876 is incorrectly numbered page 786.
^ f
0 hi
DEBATES
OP THE
HOUSE OP COMMONS
OF THE
DOMINION OF CANADA.
PUBLISHED BY AUTHORITY OF THE HOUSE OF COMMONS.
G. B. BBADLEY, Chief Reporteb.
THIRD SESSION-FOURTH PARLIAMENT.
44° VICTORIA, 1881.
\
/
VOL. XI
COMPRISING THE PERIOD FROM THE SECOND DAY OF FEBRUARY, 1881, TO THE
TWENTY-FIRST DAY OF MARCH, 1881.
SECOND VOLUME OF THE SESSION.
1881.
THIRD SESSION, FOURTH PARLIAMENT.
Volume XI.	2nd Volume of the Session.
HOUSE OF COMMONS.
Wednesday, 2nd February, 1881.
