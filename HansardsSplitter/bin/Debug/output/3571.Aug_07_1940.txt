 at 12.15 p.m.
NATIONAL REGISTRATION
STATEMENT OP MAYOR OP MONTREAL—REFERENCE TO REPORT APPEARING IN STAR WEEKLY
On the orders of the day:
Hon. GROTE STIRLING (Yale):	I
desire to draw attention to a statement which appeared in the Star Weekly of Saturday, August 3. I will read two short paragraphs, if I may be permitted. It is apropos of the action of the mayor of Montreal recently:
Premier King said the house could rely on the government to see that the laws of the country were duly upheld. He promised to make a statement later.
95826—165
REVISED EDITION
2606
The Prime Minister
COMMONS
“I declare myself peremptorily against national registration,” Mr. Hanson said. “It is unequivocally a measure of conscription. Parliament, according to my belief, has no mandate to vote conscription.”
It seems to me most unfortunate that on so serious a matter as this the leader of the opposition (Mr. Hanson) should have been thus misreported.
THE PRIME MINISTER
CONGRATULATIONS ON TWENTY-FIRST ANNIVERSARY OF ELECTION AS LEADER OF THE LIBERAL PARTY
On the orders of the day:
Hon. GROTE STIRLING (Yale); I have already had the pleasure of shaking the Prime Minister’s hand on this occasion, but I feel sure that had the leader of the opposition (Mr. Hanson) been present this morning he would have desired to add his congratulations to my own and those of others on the occasion of the right hon. gentleman having attained his majority as leader of the Liberal party.
Right Hon. ERNEST LAPOINTE (Minister of Justice):	I desire to join with my
hon. friend (Mr. Stirling) in making reference to the twenty-first anniversary of the Prime Minister’s election to leadership of the Liberal party. I do so as one who has enjoyed not only the closest cooperation but the most intimate friendship with the Prime Minister, and I am happy to offer him my congratulations and good wishes. I believe, in fact I know, that he is the only leader of a political party in the world who is still at the head of his country after twenty-one years of leadership. He has gone through six general elections and has been successful in five—which I confess may not be for all members of the house an unmixed blessing; however, it is quite an achievement. I know that all hon. members will join with me when I express admiration for the Prime Minister’s talents, his energy, his tremendous capacity for work, all of which have been placed at all times at the service of Canada. To-day, in this crisis, he is not only the man of a party but he is the man of Canada, the man of his country. In the contest in which we are engaged he represents the spirit of Canada, with all its vigour, its vitality, its sincerity and its eagerness to achieve victory. I am happy indeed, speaking as the dean of the house, which I do not do very often, to offer our congratulations to the Prime Minister. Et je puis dire qu’en le faisant je parle au nom de ma province. tMr. Stirling.]
Mr. VINCENT DUPUIS (Chambly-Rou-ville):	Mr. Speaker, on this very happy
occasion I cannot resist the temptation to say a word on behalf of my own district. It is now twenty-one years, as has been said already, since the national Liberal convention met in Ottawa to choose a successor to Sir Wilfrid Laurier. The delegates decided to vote in favour of a distinguished young man, the grandson of a great patriot, well known by the labour class for his work on their behalf. When called upon by Sir Wilfrid Laurier, upon the suggestion of Sir William Mulock, he organized the Department of Labour and became the first minister of labour in Canada. But I am sure that one of the principal motives which animated the delegates in their choice was his unlimited fidelity to Sir Wilfrid, in victory or in defeat.
I shall not undertake to recite all that he has accomplished since that time, except to repeat what the right hon. Minister of Justice (Mr. Lapointe) has already said, that the Prime Minister is the only leader of a party in the world who is still in office after so many years of leadership.
The main object of his life has been to maintain unity in this country, and after twenty-one years of leadership I am sure that the right hon. gentleman could repeat the words uttered by his predecessor in Strathroy in 1908, during the course of a political campaign. Sir Wilfrid Laurier said:
It is now twenty years since I assumed the leadership of the Liberal party. When my friends chose me to be their standard-bearer I swore to myself that I would give the task the whole of my life, my soul and my body, and that I have done. My days cannot be very long now.—
I hope the days of the present Prime Minister will be very long.
But whether they are long or short, I shall ever treasure as the most holy thing in my life the confidence which has been placed in me by men who were not of my own kith and kin. I have endeavoured to maintain the principle that the Liberal party is broad enough, that Liberal principles are large enough, to give an equal share of justice and liberality to all men, no matter what may be their race or religion. This is the feeling that has animated me, and this is the feeling which shall animate me to the end. If I am to be remembered after I have gone to my grave I would rather it should be because my name has been attached to the great work of advancing the unification of the races forming the Canadian nation. When my life comes to the end. if my eyes close upon a Canada more united than I found it twenty years ago, when I assumed the leadership of the Liberal party, I shall not have lived in vain, and I shall die in peace.
AUGUST 7, 1940
The Prime Minister
2607
Therefore I desire to join, with other hon. members of this house in expressing the hope that the right hon. gentleman may be spared for many more years to serve his country.
Mr. JEAN-FRANCOIS POULIOT (Temis-couata): Mr. Speaker, as one of the senior members of the House of Commons, and a Liberal of the old school, I am delighted to join with my esteemed leader from the province of Quebec—whom we are all so glad to see with us to-day—in the tribute that he has so eloquently paid to the leader of the house, the Prime Minister, my chief. There is no greater pleasure than to recognize such an anniversary as this, to extend our congratulations and to express our good wishes—and say it with flowers. I am not endowed, sir, with the gift of flowery language, but the congratulations that I offer and the good wishes that I convey to the Prime Minister, not only on my own behalf but on behalf of all the private members, are none the less sincere.
Before taking my seat I wish also to pay my tribute to you, Mr. Speaker. You have had years of legal training, and you have a thorough understanding of parliamentary procedure and constitutional law. You have honoured the chair of this house. Every hon. member has been delighted to express his satisfaction in having you in the chair, and the house has always upheld your rulings.
May I say also that although I have not always agreed with Mr. Deputy Speaker, the chairman of committee of the whole, nevertheless he did very well and I offer him also my congratulations and best wishes.
Hon. T. A. CRERAR (Minister of Mines and Resources): Mr. Speaker, it is appropriate that someone from the western part of our dominion should join in 'the felicitations that have been extended to the Prime Minister on the occasion of the twenty-first anniversary of his election to the leadership of the Liberal party. It is a notable thing that he should have retained, not only undiminished but in an increasing degree with the passing of the years, the confidence of his party. It is a simple statement of fact that the right hon. gentleman has never stood so high in the estimation of those of his political faith as he does at this moment. We all know, that in the cross-fire of political warfare, if it may be so described, and in the heat of political controversy, hard things are sometimes said. But when we sweep all that away and look ' at the picture in its clear perspective it must be said of the Prime Minister that he enjoys, both at home and abroad, an esteem and standing greater than he has ever before held in his whole public career.
We are passing through strenuous days; a great surge of unprecedented events is coming upon us. It is well sometimes to think of the events of the past and learn from them some lessons for the future.
I simply wish to join my voice with that of the others who have spoken in appreciation of the services the Prime Minister has rendered to Canada, and to testify to the outstanding place he occupies in the esteem and affection of the people.
Mr. J. H. BLACKMORE (Lethbridge): Mr. Speaker, I feel that those whom I have the honour to represent in this house would be less pleased if I omitted to extend my best wishes to the Prime Minister on this occasion, and I should myself feel less content did I neglect to say a word at this time. I think the right hon. gentleman has a unique record, as has been pointed out already, and has much cause for satisfaction. I do not know that I have ever heard of anjmne else who achieved what he has achieved in the way of long public life, retaining high esteem over a period of twenty-one years. I trust that he may be able to echo in a somewhat prophetic vein, Shall I say, the superb words of Browning:
Gro-w old along with me!
The best is yet to be,
The last of life, for which the first was made. Right Hon. W. L. MACKENZIE KING (Prime Minister): Mr. Speaker, I have been informed that the deputy of His Excellency the Governor General has arrived at the parliament buildings and is waiting to give the royal assent to the bills which remain to be assented to. In the circumstances I hope hon. members who have spoken in such kindly terms in reference to the period of my leadership of the Liberal party, and hon. members who have received their words so graciously, will pardon me if I do not use more than a word or two in acknowledging what has been said.
Naturally one has reason to be deeply grateful that at the end of twenty-one years of leadership of a political party one should be privileged to listen to the expressions of good-will it has been my privilege to hear to-day. I am indeed profoundly grateful for the confidence which I have enjoyed on the part of so large a number of the citizens of Canada over such a long period of years. I am particularly grateful for the loyal support of the party to which I belong, particularly that of the members of the government and hon. members in this House of Commons, which has made this confidence possible; and may I say I am in no less measure deeply grateful for the good-will which has been extended by
2608
The Prime Minister
COMMONS
members of all political parties regardless of differences that may exist between the principles and policies we respectively advocate.
I can assure you, Mr. Speaker, that no one is more surprised than I am, looking back upon the convention of 1919 to which reference has been made this morning, to find myself still head of the Liberal party. It has been a long road. I was going to say it has been a long period of time, but as I look back the time seems indeed veiy short. If I asked myself how it has all come about, I would have to re-echo the statement I have just made; it has been the associations, personal and political, that I have been privileged to enjoy during my life. As I think of my home in my early days, I recall that there social service, problems of social well-being and other public questions formed part of the daily conversation of the household. In that way I suppose it was, I became interested in public affairs. Later came the association, which has been referred to by the hon. member for Chambly-Rouville (Mr. Dupuis), with Sir Wilfrid Laurier, the members of his government and the members of the House of Commons of that time; and throughout the twenty-one years since 1919 there has been the association with the most loyal and devoted body of associates that the leader of any political party could ever wish to have. My friend and colleague t'he right hon. Minister of Justice (Mr. Lapointe) has spoken of that association. Any good fortune I have had in the leadership of the party is due in large measure to him; it is something I owe as well in t'he greatest possible degree to every colleague with whom I have been associated during the time I have been privileged to be at the head of the party and to have direction of public affairs in parliament.
I do not know that at this moment I should attempt to say more. I believe, however,
I can honestly state that in these twenty-one years of responsibility in leadership, I have tried above everything else to keep before me two aims which I believe to lie deep in the hearts of the Canadian people. One is that to which reference has already been made, namely to preserve the unity of our country, to do or to sanction nothing which would tend to destroy that unity; but rather to do everything which lies within one’s power to further it. The other has been an aim equally dear,
I believe, to the hearts of Canadians generally: it is to maintain, to extend and to defend freedom in its many individual and national aspects. This I have sought to do to the extent of my ability. I have had before me also in my public life one other aim which I hope I have been fortunate enough to carry [Mr. Mackenzie King.]
out at least in some degree. It has been that no word of mine uttered in political discussion, either in parliament or on the platform, would ever inflict a wound in the breast of any political opponent or indeed injure the feelings of anyone with whom I might be drawn into public controversy.
Once again, Mr. Speaker, to those who have been so kind as to speak as they have this morning, particularly to my hon. friend the acting leader of the opposition (Mr. Stirling) for the words he has spoken on his own behalf and on behalf of the leader of the opposition; to my hon. friend the leader of the Social Credit party (Mr. Blackmore), who also has spoken on behalf of other hon. members opposite; and to the other hon. members and to my colleagues, who have spoken on this side, and to all who have so generously shown their approval of these many expressions of good will, may I say that I am indeed more than deeply grateful. I suppose the greatest of all rewards that can come to anyone in public life is to be worthy of the esteem and regard of one’s fellow men. If in the slightest degree I have earned that regard over these twenty-one years I shall feel that I have had more than my full share of reward for such public service as it has been possible for me to render during that time.
THE ROYAL ASSENT A message was delivered by Major A. R. Thompson, Gentleman Usher of the Black Rod, as follows:
Mr. Speaker, the deputy of His Excellency the Governor General desires the immediate attendance of this honourable house in the chamber of the honourable the senate.
Accordingly, Mr. Speaker with the house went up to the Senate chamber.
And having returned,
Mr. SPEAKER informed the house that the deputy of His Excellency the Governor General had been pleased to give in His Majesty’s name royal assent to the following bills:
An act to amend the Salaries Act.
An act respecting The Ottawa Electric Company and the Ottawa Gas Company.
An act respecting The Cedars Rapids Manufacturing and Power Company.
An act to amend the Yukon Act.
An act to amend the Northwest Territories Act.
An act respecting The Detroit and Windsor Subway Company.
An act to amend the Naval Service Act.
An act to amend the Civil Service Superannuation Act, 1924.
An act to amend The Department of Munitions and Supply Act.
An act to incorporate Pool Insurance Company.
AUGUST 7, 1940
The Royal Assent
2609
An act to amend the Customs Tariff.
An act to amend the Income War Tax Act.
An act to incorporate The Stanstead & Sherbrooke Insurance Company.
An act respecting a certain wharf of Saguenay Terminals Limited.
An act to incorporate Sisters Servants of Mary Immaculate.
An act to amend The Cheese and Cheese Factory Improvement Act.
An act to amend the Penitentiary Act and the Penitentiary Act, 1939.
An act to amend The Tariff Board Act.
The Excess Profits Tax Act, 1940.
An act respecting Treachery.
An act to amend The Excise Act, 1934.
An act to amend the Special War Revenue Act.
An act to amend the Royal Canadian Mounted Police Act.
An act to authorize the provision of moneys to meet certain capital expenditures made and capital indebtedness incurred by the Canadian National Railways System during the calendar year 1940, to provide for the refunding of financial obligations and to authorize the guarantee by His Majesty of certain securities to be issued by the Canadian National Railway Company.
An act to establish an Unemployment Insurance Commission, to provide for Insurance against Unemployment, to establish an Employment Service, and for other purposes related thereto.
An act respecting the payment of compensation for the taking of certain property for war purposes.
An act to amend The Prairie Farm Assistance Act, 1939.
An act to amend An Act respecting debts due to the Crown.
An act to amend The Canadian Wheat Board Act, 1935.
An act for the relief of Elizabeth Pauline Tingley Kidd.
An	act	for	the	relief	of	Nancy	Patricia
Lytle Rowat.
An act for the relief of Henry Carl Mayhew.
An	act	for	the	relief	of	Laura	Lucrezia
Green Stinson.
An act for the relief of Irene Nellie Kon Simpson.
An act for the relief of Elma Jane Harris Aspell.
An	act	for	the	relief	of	Edith	Leanora
Holland Bonet.
An act for the relief of Dorothy Lavinia Worsley Baker.
An act for the relief of Eugene Belanger.
An act for the relief of Rebecca Cohen.
An act for the relief of Ethel Cahan Naihouse.
An act for the relief of John Roy Fumerton.
An act for the relief of Paul Edouard Tardif.
An act for the relief of Pearl Aizanman Morris.
An	act	for	the	relief	of	Molly	Goldfarb
Goldberg.
An act for the relief of Muriel Agnes Martin Beech.
An act for the relief of Alfred Reinhold Roller.
An act for the relief of Sarah Kerzner Spil-berg.
An act for the relief of Christina Smith Dunlop Andrique.
An act for the relief of Anna Shepherd.
An act for the relief of Margaret Somerville Sickinger.
An act for the relief of Romain Cleophas Moreau.
An act for the relief of Dorothy Florence Donn Martin.
An act for the relief of Phoebe Doris Edge Pot>t.
An act for the relief of Filomena Grego Sauro.
An act for the relief of Kathleen Irene Mae Stephens Morrissey.
An act for the relief of Dorothy Frances Poyser MacDermid.
An act for the relief of Sheila Alice Dolly Young Dodge.
An act for the relief of Margaret Louise MacDonald Russell.
An act for the relief of Edward James Holt
An act for the relief of Peter Logush.
An act for the relief of Goldie Wolfe Goldberg.
An act for the relief of Ethel Witkov Myers.
An act for the relief of Tilly Fishman Constantine.
An act for the relief of Rachel Ruth Leven-stein Schwartz.
An act for the relief of Eleanor Mabel Campbell Townsend.
An act for the relief of Isabel Margaret Gill Bacon.
An act for the relief of Michele Fiorilli.
An act for the relief of Gertie Schwartz Simak.
An act for the relief of Geneva Clementine Hurley Picard.
An act for the relief of Rene Gaudry.
An act for the relief of Fanny Costom Cope-lovitch.
An act for the relief of William Gerald Dickie.
An act for the relief of Agnes Dorothy Smith Bruneau.
An act for the relief of John Eric Pitt.
An act for the relief of Dennis Calvert Kerby.
An act for the relief of Camille Perks.
An act for the relief of Maria Cecilia Patricia Gatien Rowell.
An act for the relief of Lemuel Athelton Lewis.
An act for the relief of Joseph Philias Hector Sauvageau.
An act for the relief of John Bernard Hughes.
An act for the relief of Annie Block Smilovitch.
An	act	for	the	relief	of	Charles-Auguste
Armand Lionel Beaupre.
An	act	for	the	relief	of	Albert Lennox
Brown.
An	act	for	the	relief	of	Talitha Emily
Findlay.
An act for the relief of Joseph Armand Odilon Boucher.
An	act	for	the	relief	of	Doris Bertha
iSchwartz.
An	act	for	the	relief	of	Lilias Augusta
Shepherd Harris.
An act for the relief of Forest Wentworth Hughes.
An act for the relief of Margaret Florence Stewart Corley.
An act for the relief of Moora Lipsin Sager-macher, otherwise known as Mary Lipsin Sager.
An act for the relief of Robert Tester Gordon.
2610
Prorogation of Parliament
COMMONS
To these bills the royal assent was pronounced by the Clerk of the Senate in the following words:
In His Majesty’s name the Honourable the Deputy Governor General doth assent to these bills.
Then the Honourable the Speaker of the House of Commons addressed the Honourable Deputy Governor General as follows:
May it Please Your Honour:
The Commons of Canada have voted supplies required to enable the government to defray certain expenses of the public service.
In the name of the commons, I present to Your Honour the following bills:
An act for granting to His Majesty certain sums of money for the public service of the financial year ending the 31st March, 1941.
An act for granting to His Majesty certain sums of money for the public service of the financial year ending the 31st March, 1941.
To which bills I humbly request Your Honour’s assent.
To these bills the Clerk of the Senate, by command of the deputy of His Excellency the Governor General, did thereupon say:
In His Majesty’s name, the Honourable the Deputy Governor General thanks his loyal subjects, accepts their benevolence, and assents to these bills.
On motion of Mr. Mackenzie King the house adjourned at 1.10 p.m., until Tuesday, November 5, at 3 o’clock.
Tuesday, November 5, 1940
