 at three o’clock.
NEW MEMBER FOR MONTMORENCY
Mr. Langlois, the newly elected member for Montmorency, was introduced by Hon. Mr. Langevin and Mr. Simard, and took his seat.
ELECTION PETITIONS—RESTIGOUCHE ELECTION
The Speaker informed the House that he had examined recognizances to the election petitions relating to St. Hyacinthe, Joliette, Montreal East, and Argenteuil, and found them unobjectionable; also, that he had issued a writ during the recess for a new election for Restigouche, in room of the Hon. John McMillan, who had accepted an office and resigned his seat, which writ was not yet returned.
THE LAMIRANDE CASE
Hon. Mr. Langevin presented a number of returns to addresses. One of these consisted of the papers in the Lamirande case. He stated that among these would be found the opinions of the law officers of the Crown, in the case, which were brought down with their own consent, and it was not desired that this should be considered a precedent.
NOTICES OF MOTION
Were then called, but none were proceeded with.
Sir John A. Macdonald said that on account of the thinness of House (not above 70 members being present) he would not ask the Orders to be called. He then moved that the House adjourn.
The House at a quarter past three adjourned till to-morrow at 3 o’clock.
99038—23
HOUSE OF COMMONS
Friday, March 13, 1868
