HOUSE OF COMMONS DEBATES
OFFICIAL REPORT
SECOND SESSION-THIRTY-FOURTH PARLIAMENT 38-39 Elizabeth II
VOLUME VI, 1989
COMPRISING THE PERIOD FROM THE TWENTIETH DAY OF DECEMBER, 1989 TO THE TWENTY-FIRST DAY OF FEBRUARY, 1990
INDEX ISSUED IN A SEPARATE VOLUME
Published under authority of the Speaker of the House of Commons by the Queen’s Printer for Canada
Available from Canada Communication Group — Publishing, Supply and Services Canada, Ottawa, Canada K1A 0S9.
7247
HOUSE OF COMMONS
Wednesday, December 20, 1989
