 at 5.40 p.m.
Prayers
[Translation]
Mr. Speaker: I wish to inform the House that pursuant to order made on Friday, April 12, 1991, I have recalled the House today for the sole purpose of granting royal assent to a bill.
* * *
MESSAGE FROM THE SENATE
Mr. Speaker: I have the honour to inform the House that a message has been received from the Senate informing this House that the Senate has passed Bill C-91, an Act to amend the Financial Administration Act and other Acts in consequence thereof.
* * *
THE ROYAL ASSENT
Mr. Speaker: I have the honour to inform the House that a communication has been received as follows:
Rideau Hall,
Ottawa
May 8 1991
Sir,
I have the honour to inform you that the Hon. John Sopinka, Puisne
Judge of the Supreme Court of Canada, in his capacity as Deputy
Governor General, will proceed to the Senate Chamber today, the 8th
day of May, 1991, at 5.45 p.m., for the purpose of giving Royal Assent to certain Bills.
Yours sincerely,
Judith A. LaRocque Secretary to the Governor General
THE ROYAL ASSENT
[Translation]
A message was delivered by the Gentleman Usher of the Black Rod as follows:
Mr. Speaker, the Honourable Deputy to the Governor General desires the immediate attendance of this honourable House in the chamber of the honourable the Senate.
Accordingly, Mr. Speaker with the House went up to the Senate chamber.
• (1750)
And being returned:
Mr. Speaker: I have the honour to inform the House that when the House went up to the Senate Chamber the Deputy Governor General was pleased to give, in Her Majesty’s name, the Royal Assent to the following Bills:
Bill C-91, an Act to amend the Financial Administration Act and other Acts in consequence thereof—Chapter No. 24.
It being 6 p.m., pursuant to order made on Friday, April 12, 1991, this House again stands adjourned to the call of the Chair.
The House adjourned at 5.53 p.m.
[The Second Session of the Thirty-fourth Parliament was prorogued on Sunday, May 12,1991, by proclamation of His Excellency the Governor General.]