OFFICIAL REPORT
OP THE
DEBATES
OP THE
HOUSE OF COMMONS
OP THE
DOMINION OF CANADA
FIRST SESSION-ELEVENTH PARLIAMENT
9 EDWAED VII., 1909
VOL XCII
COMP,RISING THE PERIOD1 PROM THE SEVENTH DAY OP MAY TO THE NINETEENTH DAY OP MAY, INCLUSIVE.
OTTAWA
PRINTED BY C. H. PARMELEE, PRINTER TO THE KING’S MOST EXCELLENT MAJESTY
Douse of Commons Debates
FIRST SESSION—ELEVENTH PARLIAMENT
HOUSE OF COMMONS.
Friday, May 7, 1909.
