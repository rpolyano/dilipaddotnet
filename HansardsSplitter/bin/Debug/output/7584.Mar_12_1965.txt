CANADA
HOUSE OF COMMONS DEBATES
OFFICIAL REPORT
SECOND SESSION—TWENTY SIXTH PARLIAMENT
13 ELIZABETH II
VOLUME XII, 1965
COMPRISING THE PERIOD FROM THE TWELFTH DAY OF MARCH, 1965, TO THE SECOND DAY OF APRIL, 1965, INCLUSIVE
INDEX ISSUED IN A SEPARATE VOLUME
20220—777J
ROGER DUHAMEL, F.R.S.C.
queen’s PRINTER AND CONTROLLER OF STATIONERY OTTAWA, 1965
12277
HOUSE OF COMMONS
Friday, March 12, 1965
