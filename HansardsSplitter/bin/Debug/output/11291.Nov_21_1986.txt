HOUSE OF COMMONS DEBATES
OFFICIAL REPORT
SECOND SESSION—THIRTY-THIRD PARLIAMENT
35 Elizabeth II
VOLUME II, 1986
COMPRISING THE PERIOD FROM THE TWENTY-FIRST DAY OF NOVEMBER, 1986 TO THE TWENTY-SEVENTH DAY OF JANUARY, 1987_
INDEX ISSUED IN A SEPARATE VOLUME
Published under authority of the Speaker of the House of Commons by the Queen’s Printer for Canada
Available from Canadian Government Publishing Centre, Supply and Services Canada, Ottawa, Canada K1A 0S9.
1393
HOUSE OF COMMONS
Friday, November 21, 1986
