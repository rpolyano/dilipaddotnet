 at 2.30 p.m.
[Translation]
THE LATE HON. DANIEL JOHNSON
EXPRESSIONS OF REGRET ON PASSING OF QUEBEC PREMIER
Right Hon. P.-E. Trudeau (Prime Minister):
Mr. Speaker, the background of our great country is composed of telluric elements of great resistance. Each one of these elements contributes to the strength and personality of our country. From day to day those elements become more obvious, for it will not always be so. From time to time one is tempted to doubt the vigour of the Canadian identity, but in times of distress or of deep reflection, the powerful ties which unite us as Canadians stand out boldly.
Mr. Speaker, one of those ties is the stature of the men who are dedicated to public life in our country. Another tie is the fact that those in power in Canada have always, each in his own way, dedicated themselves to the welfare and the progress of Canada.
It may be said that Canadians have been well served by the men in their public life and that, when one of them passes away, regardless of what part of the country he comes from, it means a loss for the whole country. When a man of Daniel Johnson’s stature leaves us, it is not merely the Canadians of that part of the country who are dealt a blow, but Canada as a whole.
[English]
Daniel Johnson exemplified those traits which any political leader, or any man, could well choose as standards of measurement: dedication to his country and his province; pride in the achievements and beauty of his culture and its language; courage in the face of adversities both personal and political; calmness and moderation on occasions when a lesser man would have exhibited neither; loyalty to his party and its convictions; devotion to his family.
[Translation]
All Canadians know that our country is going through a period of capital importance
for our future and that the basic values themselves are those which are sometimes questioned when the future is discussed.
There is no doubt that Mr. Daniel Johnson was one of the distinguished participants and interlocutors in that important dialogue which is now going on between Canadians and through which he was trying to find the common values on which the country could agree and unite. We will not forget that his objective was the recognition and the protection of the rights of French Canadians, since that protection is a matter of conscience for all Canadians and it is also, I think, the objective of all good men in Canada.
The people Mr. Johnson represented in the legislature, those who had elected him, those he had served for many years in his riding, know that they could not have a more conscientious, more energetic and, may I say. gracious spokesman.
On behalf of the government of Canada I express my most heartfelt sympathy to his family, to Quebeckers and to the government of Quebec.
Death could not come, Mr. Speaker, at a more untimely moment for this great Canadian.
Hon. Robert L. Stanfield (Leader of the Opposition): Mr. Speaker, I was much grieved to hear of the death of Hon. Daniel Johnson.
His friends knew how heavy was his task but we hoped he had completely recovered and was able to resume his duties.
His death is a cruel loss for his family and deprives the country of an experienced and very talented politician. The country loses a man who, even in cases of disagreement at the official level, liked to retain cordial personal relations with the people he met.
He was a gentleman and those who knew him will regret his courtesy, his warmth and his charm.
• (2:40 p.m.)
[English]
Daniel Johnson lived his life in the public service, Mr. Speaker. He was first elected to the legislature of Quebec in a by-election late
490	COMMONS
Tributes to Late Premier Johnson in 1946. He was re-elected six times and served his legislature as deputy speaker and his province as a member of cabinet. Nearly seven years ago he was elected to lead his party. He led it well, and in June, 1966, became the premier of his province, one of the most important and difficult positions in this country. It is a good record, sir.
[Translation]
He was premier of Quebec at a time when the evolution of events in that province caused concern in other parts of Canada. It was therefore inevitable that he should be to a certain extent a controversial figure in his country.
I think that, at the present time, a premier of Quebec who loves his people and his country and wants to be loyal to them cannot help but be a controversial figure.
His course was a difficult one, but he followed it with patience and good humour.
He travelled across the country with his family. He wanted to get to know the country as well as he knew the province of Quebec where he was born.
Daniel Johnson wanted to build both Canada and Quebec, and he devoted his life to building a country where all Canadians could fulfil their aspirations.
On behalf of my party as an officer of the House of Commons and personally, because I had the opportunity to work with Premier Daniel Johnson and because I respected and considered him highly, I wish to offer my sincere sympathy to his family, his friends and colleagues as well as to the fellow-citizens of the deceased premier.
'[English]
Mr. Stanley Knowles (Winnipeg North Centre): Mr. Speaker, I join with the Prime Minister and the Leader of the Opposition in ■expressing our deep sense of shock at the death of Premier Daniel Johnson, and our sincere condolences to the government and people of Quebec and to the family of our late, distinguished Canadian. I speak on behalf of our party and of my colleagues who are here with me. I might add that a short while ago, over the telephone, our parliamentary leader, the hon. member for York South (Mr. Lewis), asked me to express his deep regret that illness prevented him from being here today to express his sorrow on this occasion.
We were all shocked this morning to hear .the news that came to us. Only yesterday we
.[Mr. Stanfield.]
DEBATES	September 26, 1968
watched and heard Daniel Johnson on television. He seemed to be in good health; certainly he was in good form as he discussed issues that are of concern to us in this country.
When we think of him we are conscious of the fact that he was an articulate exponent of his point of view, a point of view shared by many of our French speaking fellow Canadians. It must also be said that although he expressed strongly and vigorously his point of view, a great deal of credit goes to him for the fact that we have developed a state of dialogue in this country with respect to relations between French speaking and English speaking Canadians, and with respect to relations between Ottawa and Quebec.
Mr. Johnson, as I say, was a vigorous exponent of his views but he was also a loyal Canadian who believed that the destiny of this country is for us to remain together and build a strong and united Canada. Not only will the people of Quebec miss him, but Canada as a whole can ill afford to lose such a leader at this time.
[Translation]
Mr. Speaker, on behalf of my colleagues of the New Democratic Party, I wish to express my most sincere sympathy to Mrs. Johnson and to the members of her family.
Mr. Gilbert Rondeau (Shefford):	Mr.
Speaker, on behalf of the Ralliement Credi-tiste I wish to join with the other members of the house to say how we were deeply grieved this morning at the news of the sudden death of Hon. Daniel Johnson, premier of the province of Quebec.
Such a death will be deeply mourned by all the Quebec people and the whole French Canadian nation, and especially by the people of Bagot county, a great part of which is included in Shefford constituency which I have the honour to represent here and which has just lost its member of parliament.
As premier of his province since 1966, and member for Bagot since 1946, he might still, on account of his experience and talent, have served the province of Quebec and all the French Canadian people for many years to come.
In its unfathomable ways Providence has decided that he would no longer be with us after giving yesterday the most eloquent and the most important press conference of his whole political career, as if it were to be the crowning achievement of his work.
September 26, 1968	COMMONS
How forcefully are we reminded of the fragility of life. Although it is full of great possibilities, it is at the same time limited and conditioned by its span and its events.
Mr. Johnson had the courage of his convictions. He had his own disposition, of course. I knew him very well because on many occasions, as members of a good part of the same riding, we presided at the opening of the baseball or hockey season and at the inauguration of sports centres or schools. That is how I came to know the premier of Quebec as a public man and also as a good father.
On television yesterday he said something which impressed me this morning. He said that his illness had made him more human. But today his ailment takes him away from humans and puts an end to his life.
This afternoon he was going to inaugurate the Manicouagan dam which is the most important hydro-electric project in Quebec.
Hon. Maurice Duplessis died in Scheffer-ville, an important mining centre in Quebec. His successor, Hon. Daniel Johnson, died on duty in Manicouagan, an hydro-electric centre in the same Quebec area, in circumstances similar to those which preceded the death of Mr. Duplessis, who liked to look upon Hon. Daniel Johnson as his own son.
DEBATES	491
Tributes to Late Premier Johnson
History will rate Daniel Johnson as a politician and a statesman who has definitely marked out French Canadian political life. His love of work, his loyalty to his province, his clearsightedness will be missed greatly in Quebec. The strenuous political life which he had set for himself and the responsibilities which he had to assume have unfortunately taken away too soon this dearly beloved father from his family.
On behalf of the Ralliement Creditiste we express our deepest sympathy to Mrs. Johnson, her children and all his relatives, and especially his constituents in the riding of Bagot.
Some of his political enemies might silently rejoice. As far as we are concerned, we share the grief of the whole French Canadian people.
Mr. Trudeau: Mr. Speaker, I move, seconded by the hon. Leader of the Opposition, that this house do now adjourn.
Mr. Speaker: Pursuant to the motion of the right hon. Prime Minister (Mr. Trudeau), seconded by the hon. Leader of the Opposition (Mr. Stanfield), this house stands adjourned until eleven o’clock tomorrow morning.
Motion agreed to and the house adjourned at 2.50 p.m.






493
HOUSE OF COMMONS
Friday, September 27, 1968
