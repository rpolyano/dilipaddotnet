 at Three o’clock.
Prayers.
REPORTS.
Sir CHARLES TUPPER presented, pursuant to a Resolution of the House of the 20th February, 1882, a report giving full information on all subjects affecting the Canadian Pacific Railway, up to the latest date, and particularly all details as to:
1. The selection of the route ; 2. The progress of the work : 3. The selection or reservation of land; 4. The payment of money ; 5. The laying out of branches ; 6. The progress thereon; 7. The rates of tolls for passengers and freight; 8. The particulars required by the Consolidated Railway Act and amendments thereto, up to the end of the previous fiscal year; 9. Like particulars up to the latest practicable date before the presentation of the return ; 10. Copies of all Orders in Council and of all correspondence between the Government and the railway company, or any member or officer of either, relating to the affairs of the company.
Sir LEONARD TILLEY presented, pursuant to a Resolution of the House of the 20th February, 1882, a report of the Canadian Pacific Railway, in account with the Government of Canada, viz.: -Rails Advance Account, Land Grant Bond Account, Current Account and Subsidy Account; also, a Memorandum as to substitution of Canadian Pacific Railway of Credit Talley Stock for $1,000,000 cash deposit, and a Schedule of Correspondence as to Canadian Pacific Land Grant Bonds.
BRANCH PRINCE EDWARD ISLAND RAILWAY.
Mr. HACKETT enquired, Whether it is the intention of the Government to build this year the branch railway from Cape Traverse to the main line of the Prince Edward Island Railway, for which a sum of money was voted at the last Session of Parliament ?
^ Sir CHARLES TUPPER. It is the intention of the Government to proceed with the work in the spring.
Sir John A. Maoeonald.
FRAUDS IN RELATION TO PUBLIC CONTRACTS.
On the order for the second reading of Bill (No. 5) for the better prevention of fraud in relation to contracts involving the expenditure of public moneys (Mr. Casgrain), being read,
Sir JOHN A. MACDONALD. The House is very thin, and I would ask my hon. friend to allow this Bill to stand over. We will give him plenty of opportunity to bring it forward.
Mr. CASGRAIN. I shall be very glad to accommodate my hon. friend, the more so because on one occasion he did not accommodate me.
Sir JOHN A. MACDONALD. We will turn over a new leaf.
Order allowed to stand.
MOTION FOR RETURN.
The following Motion for a Return was agreed to:—
Copies of the judgments in the case of Russell and the Queen in the Supreme Court of Canada and the Privy Council, and of the judgments in any Provincial Courts of inferior jurisdiction, or in the Supreme Court of Canada, in all cases raising the question of the righ t of a Provincial Legislature to pass laws affecting, regulating or restraining the number or character of persons licensed to sell intoxicating liquors, or the times of such sale.—(Mr. Blake.)
Sir JOHN A. MACDONALD moved the adjournment of the House.
Motion agreed to; and (at 3:45 o’clock p.m.) the House adjourned.
HOUSE OF COMMONS,
Friday, 23rd February, 1883.
