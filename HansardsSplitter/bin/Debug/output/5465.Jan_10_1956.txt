 at three o’clock, the Speaker in the chair.
Mr. Speaker read a communication from the secretary to the Governor General, announcing that His Excellency the Governor General would proceed to the Senate chamber at three o’clock on this day, for the purpose of formally opening the session of the dominion parliament.
A message was delivered by Major C. R. Lamoureux, Gentleman Usher of the Black Rod, as follows:
Mr. Speaker, His Excellency the Governor General desires the immediate attendance of this honourable house in the chamber of the honourable the Senate.
Accordingly, Mr. Speaker with the house went up to the Senate chamber.
And the house being returned to the Commons chamber:
VACANCIES
Mr. Speaker: I have the honour to inform the house that during the recess I received communications notifying me that several vacancies had occurred in the representation, as follows:
Of L. Philippe Picard, Esquire, member for the electoral district of Bellechasr-e, by resignation.
Of J. Gaspard Boucher, Esquire, member for the electoral district of Bestigouche-Madawaska, by decease.
Of Jean-Frangois Pouliot, Esquire, member for the electoral district of Temiscouata, consequent upon his being summoned to the Senate.
67509—1
Of Honourable C. G. Power, member for the electoral district of Quebec South, consequent upon his being summoned to the Senate.
Of David Arnold Croll, Esquire, member for the electoral district of Spadina, consequent upon his being summoned to the Senate.
Of Honourable Alcide Cote, member for the electoral district of St. Jean-Iberville-Napierville, by decease.
Accordingly, I addressed my warrants to the chief electoral officer for the issue of new writs of election for the said electoral districts.
NEW MEMBERS
Mr. Speaker: I have the honour to inform the house that during the recess the Clerk of the House received from the chief electoral officer certificates of the election and return of the following members:
Of Francis Gavan Power, Esquire, for the electoral district of Quebec South.
Of Jean-Paul St. Laurent, Esquire, for the electoral district of Temiscouata.
Of Ovide Laflamme, Esquire, for the electoral district of Bellechasse.
Of Joseph Charles Van Horne, Esquire, for the electoral district of Restigouche-Mada-waska.
Of Charles E. Rea, Esquire, for the electoral district of Spadina.
Of J. Armand Menard, Esquire, for the electoral district of St. Jean-Iberville-Napier-ville.
NEW MEMBERS INTRODUCED
Ovide Laflamme, Esquire, member for the electoral district of Bellechasse, introduced by Right Hon. L. S. St. Laurent and Hon. Jean Lesage.
J. Armand Menard, Esquire, member for the electoral district of St. Jean-Iberville-‘Napierville, introduced by Right Hon. L. S'. St. Laurent and Hon. Roch Pipard.
HOUSE OF COMMONS
2
Speech from the Throne
Francis G. Power, Esquire, member for the electoral district of Quebec South, introduced by Right Hon. L. S. St. Laurent and Hon. Hugues Lapointe.
Jean-Paul St. Laurent, Esquire, member for the electoral district of Temiscouata, introduced by Right Hon. L. S. St. Laurent and Hon. Hugues Lapointe.
Joseph Charles Van Horne, Esquire, member for the electoral district of Restigouche-Madawaska, introduced by Hon. George A. Drew and Mr. G. W. Montgomery.
Charles E. Rea, Esquire, member for the electoral district of Spadina, introduced by Hon. George A. Drew and Mr. Roland Michener.
OATHS OF OFFICE
Eight Hon. L. S. St. Laurent (Prime Minister) moved for leave to introduce Bill No. 1, respecting the administration of oaths of office.
Motion agreed to and bill read the first time.
SPEECH FROM THE THRONE
Mr. Speaker: I have the honour to inform the house that when the house did attend His Excellency the Governor General this day in the Senate chamber, His Excellency was pleased to make a speech to both houses of parliament. To prevent mistakes, I have obtained a copy, which is as follows: Honourable Members of the Senate:
Members of the House of Commons:
It is with pleasure that I greet you as you resume your parliamentary duties at the beginning of this new year.
Since you last met there have been significant international developments. Some of them have been welcome as releasing tensions in certain parts of the world while others unfortunately have had the contrary effect. My ministers remain convinced of the need to maintain the defences of the free nations as a deterrent to war. A strong North Atlantic Treaty Organization and adequate protection for this continent are in their view fundamental to the preservation of peace and the security of Canada.
Security, however, cannot rest on arms alone. The government, therefore, is continuing its constant efforts, through diplomacy and negotiation and through the United Nations and other international agencies, to bring about better understanding between nations.
A meeting of commonwealth prime ministers will be held in London in June to consider matters of mutual interest. My prime minister has accepted the invitation to attend.
Meanwhile my ministers are looking forward to the visit to Ottawa in February of the prime minister and the foreign secretary of the United Kingdom.
The annual meeting of the consultative committee of the Colombo plan agreed that this constructive work should be continued for a further period and you will be asked to authorize Canada's continued participation in the plan, as well as in the United Nations technical assistance program.
[Mr. Speaker.]
A royal commission has been appointed to consider and report upon the development and financing of television and sound broadcasting in Canada.
The royal commission to examine and report upon our economic prospects is proceeding with its inquiry.
The year just ended has been the most productive in our nation’s history. More men and women have been employed than ever before. Our harvests have been abundant. Our trade has exceeded all earlier levels. A record number of houses has been built.
Canada has enjoyed, on the whole, a high level of prosperity. Some sectors of the economy have not fully participated in this increased well-being. In particular, although sales of wheat in the past five years have been at record levels, an unprecedented series of bumper harvests has made necessary the storage of abnormal stocks of grain both in elevators and on farms. Lack of space in elevators has limited the ability of producers to deliver grain as early as usual in the marketing year. In order to provide an immediate source of funds where they may be needed, my ministers will ask you to authorize guarantees for bank loans to producers secured by their grain.
You will also be asked to authorize the treasury to bear the cost from August 1, 1955 of storage and interest charges on wheat held by the wheat board over and above its normal carryover levels. As a consequence of this arrangement, returns to producers will not be depressed by carrying costs on abnormal carryover.
A higher level of employment this winter than last seems to be assured and the improvements you made to the Unemployment Insurance Act last year will provide a better coverage for those who are temporarily without work, particularly in the winter season. Various departments and agencies of government have sought to arrange their construction programs to provide more employment during the winter season. It is gratifying to note that many other employers are endeavouring to follow the same policy.
In October my ministers held a conference in Ottawa with the premiers and other ministers of all the provinces to consider financial and other relations between the federal and provincial governments. Following this useful discussion and some further correspondence, my ministers have placed before all provincial governments a specific proposal for federal-provincial fiscal arrangements to commence next year.
The conference established a committee of ministers from all governments to consider health insurance programs and the work of this committee is now under way.
My ministers have also conferred with provincial ministers to consider and develop an arrangement for sharing the costs of assistance to unemployed persons not eligible for unemployment insurance benefits, and in need. Detailed agreements have been submitted to the provinces. You will be asked to approve the legislation necessary to implement this program.
You will be asked to provide for the construction, jointly with the government of Ontario, of a gas pipe line across northern Ontario from the Manitoba boundary to Kapuskasing, to be leased to Trans-Canada Pipe Lines Limited, which is to build connecting lines in western and eastern Canada. My ministers consider this government participation is necessary to make this important national project possible at this time.
You will also have laid before you amendments to the Trans-Canada Highway Act to accelerate the completion of essential links in the trans-Canada highway and to continue, in co-operation with provincial governments, the work of constructing that highway up to the agreed standards.
JANUARY 10, 1956
Amendments to the National Housing Act will be laid before you. The rapid growth of our centres of population has been a spectacular feature of our national development since the last war. and in that growth wide use has been made of the National Housing Act by our citizens, particularly in suburban areas. One amendment now to be proposed is designed to increase assistance to encourage redevelopment of older sections of our cities to their best use.
Early last year the home improvement section of the act was proclaimed. It has contributed greatly toward the betterment of many existing houses. An amendment will be laid before you to increase the amounts of loans available for this purpose.
A joint committee of both houses will be proposed to review the progress and programs of the federal district commission in developing the national capital.
In the field of industrial development you will be asked to extend the scope of the Industrial Development Bank. You will also be asked to increase the size of loans which may be made by the Canadian farm loan board and to amend the Farm Improvement Loans Act.
My ministers also plan to introduce a bill to provide that women receive equal pay with men for equal work in industries which are under federal jurisdiction.
You will be asked to revise the Royal Canadian Mounted Police Act and the Dominion Succession Duty Act. You will be requested to consider amendments to the Canadian Wheat Board Act. the Supreme Court Act, the Excise Act, the Canada Shipping Act, the Small Loans Act, the Navigable Waters Protection Act, the Prairie Farm Rehabilitation Act, the Prairie Farm Assistance Act, the Canadian Citizenship Act and the Indian Act.
Members of the House of Commons:
You will be asked to make provision for all essential services.
Honourable Members of the Senate:
Members of the House of Commons:
May divine Providence bless your deliberations.
Right	Hon.	L.	S.	St.	Laurent (Prime
Minister) moved:
That the speech of His Excellency the Governor General to both houses of parliament be taken into consideration on Wednesday next.
Motion agreed to.
THE LATE HON. ALCIDE COTE
(Translation) :
Right	Hon.	L.	S.	St.	Laurent (Prime
Minister): Mr. Speaker, I am deeply moved at this time as I rise to pay tribute, at the very beginning of this session, to the memory of the late Alcide Cote, who died at his St. Jean home on August 7 last. Every hon. member must have been as shocked as I was on learning of the death of a colleague esteemed by all, at an age which still held the promise of many more years of fruitful labour in the public service. While we were not unaware of the seriousness of his illness, we had thought that his return to this house, on June 8 last, after a few months’ rest, justified the assumption that before too long he would be able to resume all his duties. The 67509—1J
3
Tribute to Hon. Alcide Cote spontaneous applause, from all parts of the house, which greeted his return here on that occasion, told more eloquently than I am able to say today of the affection and high esteem we all had for him.
The late Alcide Cote could proudly claim direct descent from one of the first Canadian settlers. After completing his studies at the College of St. Jean and the University of Montreal, he practised law in the city of St. Jean, of which he was mayor from 1945 to 1949. For several years he took an active part in the initiatives of the bar association of his province, of the union of municipalities, of the chamber of commerce and of numerous other associations.
Mr. Cote had first been elected to the House of Commons in 1945. His constituents had renewed his mandate in the general elections of 1949 and 1953. In 1952, he had been sworn as a member of the privy council and appointed postmaster general.
Mr. Cote’s participation in public affairs, in the field of his profession and in municipal and federal politics is abundant evidence of the interest he had dedicated to the welfare of his fellow citizens. The passing of our colleague has caused deep regret among the members of this house as well as in all the other spheres in which he had carried out his beneficent activities. And it is, I believe, the spontaneousness and sincerity of such regrets which constitute the most touching tribute that could be paid to his memory.
(Text) :
Since we last met, Mr. Speaker, death has removed from our midst an esteemed and helpful colleague, Hon. Alcide Cote, at a time in his life when it might have been expected that he could look forward to many more years of useful activity.
I am sure I speak for all members of the house in saying that Mr. Cote will be remembered in this assembly for his gentlemanly nature and his deep consideration for his fellow men, which made him one of the most popular members of the house. Those endearing qualities were combined with a deep sense of responsibility as a citizen. Throughout his career in the professional, municipal and federal fields, he worked hard and conscientiously for the welfare of the people who had put their trust in him, and gave of himself unstintingly in the service of the nation as a whole.
Another trait of his personality which I am sure all hon. members would have me recall
4
HOUSE OF
Tribute to Hon. Alcide Cote was his great modesty and his deep humility. It was typical of his natural modesty that when a new postmaster general was to be appointed, in 1952, he was perhaps the most surprised of all members of the house that he should be called upon to fill that position.
I know, Mr. Speaker, that all members would wish to join with me in expressing at this time our sympathy to Mr. Cote’s sisters and brothers and the other members of the family in their bereavement.
Hon. George A. Drew (Leader of the Opposition): Mr. Speaker, I know that every hon. member of this house will join in the words of sympathy that have been expressed by the Prime Minister. We extend our sympathy to the members of the government with which Mr. Cote was associated and to all of his colleagues, because there had been that close association day by day which had brought him very much in contact with them.
I should like to emphasize, however, that Hon. Alcide Cote was a warm friend of every member of this house. I think it can be said without any question that there was not a single member of this house who had not responded to his genial and cordial approach to the problems that came up in discussion here. Naturally in a democratic chamber there was not always complete agreement about the manner in which things should be done, but it was always characteristic of him that the discussions which took place were carried on in the kindly and generous manner to which the Prime Minister has referred.
I perhaps have good reason to know something of what it means to be welcomed back by the members of this house, and I recall the time last year when we were all happy to welcome the Hon. Alcide Cote back into this house, thinking he was returning to his duties. It made it doubly tragic when we heard a short time afterward that he had suffered a heart attack and later was taken away very suddenly.
I simply wish to join fully, on behalf of myself and my associates, in the words of sympathy expressed by the Prime Minister and to extend to the members of his family the sincere sympathy that I know is felt by every hon. member of this house.
Mr. M. J. Coldwell (Roseiown-Biggar): Mr.
Speaker, I am quite sure the sentiments expressed by the Prime Minister and the Leader of the Opposition are indeed shared by all members of this house. Mr. Cote was a very charming personality, a very good friend and one who should have been able to look forward to a long career in this parliament and country. As has already been said, it is tragic to know that he was cut off in the
[Mr. St. Laurent (Quebec East).]
COMMONS
prime of life and has been lost to the country, the government, his party and the province where he lived.
My closest association with him was at the United Nations in the autumn of 1953, when in the absence of the Secretary of State for External Affairs he acted as chairman of the delegation. I am quite sure that those of us who were associated with him throughout the number of weeks we were in New York will recollect the fine personality that chaired our meetings, the genial comrade who in the evenings, when the day was over, sat around and proved himself to be a very fine companion.
We all join in the sympathy that has already been expressed to the members of his family, to his brothers and sisters. I wish also to extend to the government and to the members of the Liberal party our sympathy in the loss of one of their devoted colleagues. I am sure all members of the house feel that we have lost from among us one whom we regarded as a fine personality and a good friend.
Mr. E. G. Hansell (Macleod): Mr. Speaker, we in this corner of the house wish to join those who have preceded me in expressing our respects to one of our departed members.
Every so often it becomes a rather trying duty for members of this house to rise and pay tribute to one of our colleagues who has departed this life. Our duties here and our lives and our associations in this house are, generally speaking, very pleasant. Sometimes our pleasant associations are marred by the departure through death of members whom we honour and respect as fellow workers for a better country and a greater nation. It is perhaps a double tragedy when we realize that one is taken from us at such an early age.
There is not much one can say apart from paying our respects and our tributes to a colleague well respected and well loved. Members of this group pay their respects to the memory of Mr. Cote and wish to extend their deepest sympathy to his near relatives and friends.
INTERNAL ECONOMY COMMISSION
Right Hon. L. S. St. Laurent (Prime Minister) presented the following message from His Excellency the Governor General:
The Governor General transmits to the House of Commons a certified copy of an approved minute of council appointing the Hon. James J. McCann, Minister of National Revenue, the Hon. Stuart S. Garson, Minister of Justice, the Hon. Hugues Lapointe, Minister of Veterans Affairs, and the Hon. W. E. Harris, Minister of Finance, to
JANUARY 10, 1956
5
act with the Speaker of the House of Commons as commissioners for the purposes and under the provisions of the one hundred and forty-third chapter of the Revised Statutes of Canada, 1952, intituled: An act respecting the House of Commons.
STANDING COMMITTEES
Right Hon. L. S. St. Laurent (Prime Minister) moved:
That a special committee be appointed to prepare and report, with all convenient speed, lists of members to compose the standing committees of this house under standing order 65, said committee to be composed of Messrs. Harris, Lapointe, Tustin, Quelch and Weir.
Motion agreed to.
HOUSE OF COMMONS
APPOINTMENT OF DEPUTY CHAIRMAN OF COMMITTEES OF THE WHOLE
Right Hon. L. S. St. Laurent (Prime Minister) moved:
That Edward T. Applewhaite, Esquire, member for the electoral district of Skeena, be appointed deputy chairman of committees of the whole house.
Motion agreed to.
House of Commons LIBRARY OF PARLIAMENT
Report of the joint librarians of parliament. —Mr. Speaker.
PIPE LINES
TRANS-CANADA PIPE LINES--REQUEST FOR
TABLING OF AGREEMENTS
Hon. George A. Drew (Leader of the Opposition): Mr. Speaker, before the Prime Minister moves the adjournment I wish to direct a question to him through you. In view of the inclusion in the speech from the throne of a reference to the trans-Canada pipe line, I wonder whether the Prime Minister could make arrangements for the tabling tomorrow of the draft agreement between the government of Canada and the government of the province of Ontario as well as the draft agreement between the proposed crown company and Trans-Canada Pipe Lines Limited.
Right Hon. L. S. St. Laurent (Prime Minister): I will endeavour to ascertain if those documents can be tabled tomorrow.
On motion of Mr. St. Laurent the house adjourned at 4.25 p.m.
7
HOUSE OF COMMONS
Wednesday, January 11, 1956
