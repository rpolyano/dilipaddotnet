Technical and Bibliographic Notes / Notes techniques et bibliographiques
The Institute has attempted to obtain the best original copy available for filming. Features of this copy which may be bibliographically unique, which may alter any of the images in the reproduction, or which may significantly change the usual method of filming are checked below.
L’lnstitut a microfilme le meilleur exemplaire qu’il lui a ete possible de se procurer. Les details de cet exemplaire qui sont peut-etre uniques du point de vue bibliographique, qui peuvent modifier une image reproduite, ou qui peuvent exiger une modification dans la methode normale de filmage sont indiques ci-dessous.
□
□
□
□
□
□
□
□
□
□
□
Coloured covers /
Couverture de couleur
Covers damaged /
Couverture endommagee
Covers restored and/or laminated /
Couverture restauree et/ou pelliculee
Cover title missing /
Le titre de couverture manque
Coloured maps /
Cartes geographiques en couleur
Coloured ink (i.e. other than blue or black) /
Encre de couleur (i.e. autre que bleue ou noire)
Coloured plates and/or illustrations /
Planches et/ou illustrations en couleur
Bound with other material /
Relie avec d’autres documents
Only edition available /
Seule edition disponible
Tight binding may cause shadows or distortion along interior margin / La reliure serree peut causer de I’ombre ou de la distorsion le long de la marge interieure.
Blank leaves added during restorations may appear within the text. Whenever possible, these have been omitted from filming / II se peut que certaines pages blanches ajoutees lors d’une restauration apparaissent dans le texte, mais, lorsque cela eta it possible, ces pages n’ont pas ete filmees.
□
□
□
□
0
0
□
□
□
Additional comments /	Various pagings.
Commentaires supplementaires:
Coloured pages / Pages de couleur
Pages damaged / Pages endommagees
Pages restored and/or laminated /
Pages restaurees et/ou pelliculees
Pages discoloured, stained or foxed/
Pages decolorees, tachetees ou piquees
Pages detached / pages detachees
Showthrough / Transarence
Quality of print varies /
Qualite inegale de I’impression
Includes supplementary materials Comprend du materiel supplemental
Pages wholly or partially obscured by errata slips, tissues, etc., have been refilmed to ensure the best possible image / Les pages totalement ou partiellement obscurcies par un feuillet d’errata, une pelure, etc., ont ete filmees a nouveau de fagon a obtenir la meilleure image possible.
Opposing pages with varying colouration or discolourations are filmed twice to ensure the best possible image / Les pages s’opposant ayant des colorations variables ou des decolorations sont filmees deux fois afin d’obtenir la meilleure image possible.
OFFICIAL, REPORT
OF THE
DEBATES
OF THE
HOUSE OF COMMONS
OF THE
DOMINION OF CANADA.
THIRD SESSION-SIXTH PARLIAMENT.
52° VICTORIA, 1889
V OL. XXVII.
COMPRISING THE PERIOD FROM THE THIRTY-FIRST DAY OF JANUARY TO THE TWENTY-SECOND DAY OF MARCH, INCLUSIVE, 1889.
OTTAWA:
PRINTED BY BROWN CHAMBERLIN, PRINTER TO THE QUEEN’S MOST EXCELLENT MAJESTY.
18T9.
MEMBERS OF THE GOVERNMENT
OF THE
RT. HON. SIR JOHN A. MACDONALD, G.C.B.
AT THE OPENING OF THE 3rd SESSION OF THE SIXTH PARLIAMENT,
1889.
President of the Council (Premier) Minister of Public Works	*
Minister of Railways and Canals • Minister of Customs -	-
Minister of Militia and Defence -Minister of Agriculture •	•
Minister of Inland Revenue -	-
Without Portfolio	-	-	•
Secretary of State	-	-	-
Minister of Justice	•	•	•
Minister of Finance	•	•	•
Without Portfolio	-	-	-
Minister of Marine and Fisheries * Postmaster-General	.	.	-
Minister of the Interior -	.
Right Hon. Sir John A. Macdonald, G.C.B, &o. Sir Hector Lours Langsvin, K.C.M.G., C. B. Hon. John Henrt Pope.*
Hon. Mackenzie Bowell.
Sir Adolphe P. Caron, K.C.M.G.
Hon. John Carling.
Hon, John Costigan.
Hon. Frank Smith.
Hon. Joseph Adolphe Cfapleau.
Sir John Sparrow David Thompson, K.C.M G. Hon. George Eulas Foster.
Hon. John Joseph Caldwell Abbott.
Hon. Charles Hibbert Tupper.
Hon. John Graham Haggart.
Hon. Edgar Dewdnrt.
Clerk of Ike Privy Council -	-	-	-	- John Joseph McGee, Esq.
OFFICERS OF THE HOUSE OF COMMONS.
Hon* Joseph Aldrio Ouimet..........................Speaker.
John G. Bourinot, Esq..............................Clerk of the House.
Donald W. Maodonell, Esq...........................Sergeant-at-Arms.
Francois Fortunat Rouleau, Esq. .... Clerk Assistant.
OFFICIAL REPORTERS.
George B. Bradlet...................................Chief	Reporter.
Stephen A. Abbott...............................\
E.	JoSEPHjDUGGAN...............................I
Albert Horton.................................../
F.	R. Maroiau	..........................i Beporters.
J. O. Marceau...................................i
Thos. Jno. Richardson...........................1
Thos. P. Owens................................../
Jno. Chas. Boyce................................Assistant to Chief Reporter.
’..Died 1st April, 1888.
ALPHABETICAL LIST
07 THE
CONSTITUENCIES AND MEMBERS
07 THl
HOUSE OF COMMONS.
THIRD SESSION or thk SIXTH PARLIAMENT of thi DOMINION OF CANADA,
1889.
Addington—John W. Bell.
Albebt—Richard Chapmao Weldon.
Alberta—Donald Watson Davis,
Algoma—Simon J. Dawson.
Annapolis—John B. Mills.
Antigonish—Hon. Sir John S. D. Thompson, K.C.M.G, Argenteuil—James 0. Wilson.
Assiniboia, East—Hon. Edgar Dewdney.
Assiniboia, West—Nicholas Flood Bavin.
Bagot—Flavien Dupont.
Be attoe— Joseph God bout.
Beauhabnois—Joseph G&Won Horace Bergeron. Billeohasse—Gaillaume Amyot.
Bsbthieb -C16ophas Beausoleil.
Bonaventure —Louis Joseph Riopel.
Bothwell—Hon. David Mills.
Bbant, N. Biding—James Somerville.
Bbant, S. Riding—William Paterson. Bbockyille— John Fisher Wood.
Bboms— Sydney Arthur Fisher.
Bbuce, E. Riding—Henry Cargill.
Brijce, N. Riding—Alexander McNeill.
Bruoe, W. Riding—James RoWafid.
Cardwell—Robert Smeaton White. Cableton (N.B.)—Frederick Harding Hale. Cableton (O.)—George Lemuel Dickinson. Cariboo—Frank S. Barnard.
Chambly—Raymond Pr6fontahre. Champlain—Hippdlyte MofftplaisTf.
Chateauguay—Edward Holton.
Chicoutimi and Saguenay—Paul Couture. Colchester—Hon. Sir Adams G. Archibald, K.O.M.G. Compton—Hon. John Henry Pope.*
Cornwall and Stormont—Darby Bergin. Cumberland—Arthur R. Dickey.
Digby—Herbert Ladd Jones.
Do&ohester—Honor6 J. J. B. Chouinard.
Drummond and Arthabaska—Joseph Lavergne. Dundas— Charles Erastus Hickey.
Durham, E. Riding—Henry Alfred Ward,
Durham, W. Riding—Hon. Edward Blake.
Elgin, E. Riding—John H. Wilson.
Elgin, W. Riding—George Elliott Casey,
Essex, N. Riding—James Oolebrooke Patterson.
Essex, S Riding—James Brien.
Fbontenao—Hon. George Airey Kirkpatrick.
Gasp#-Louis Z, Joncas.
Glengarby—P. Purcell.
Gloucester—Kennedy F. Barns.
Grisvillb, S. Riding—Walter Shady.
Grey, E. Riding—Thomas S. Sproule.
Grey, N. Riding—James Masson.
Grey, S. Riding—George Landerkin.
Guysbobough—John A. Kirk.
Haldimand— Charles Wesley Colter.f
ft.tt.av I Hon. Alfred G. Jones. Halifax-j Thoma8 & KeDny,
Halton—John Waldie.
Hamilton —
Adam Browft. Alexander McKay.
CHARLEvotkJ-gitedjji CffflOtt. Chablotti —Arthur Hill Gillmor.
* Died, l»t April, 1888.
f Elected 80th January; took scat lltb February, and sat for balance oi Section.
vi
LIST OF MEMBERS OF THE HOUSE OF COMMONS.

Hants—Alf ied Pain am.
Hastings, E. Biding—Samuel Bar ion Burdett. Hastings, N. Riding—Hon. Mackenzie Bowel]. Hastings, W. Biding—Henry Corby. Hochelaga—Alphonse Desjardins.
Huntingdon—Julias Scriver.
Huron, E. Biding—Peter Macdonald.
Huron, S. Biding—John McMillan.
Huron, W. Biding—Robert Porter.
Iberville—Francois Bdchard. Inverness—Hugh Cameron.
Jacques Cartier—Desiid Girouard. Joliette—Hilaire Neveu.
Kamoubaska—Alexis Dessaint.
Kent (N.B.)—Pierre Amand Landry.
Kent (0.)—Archibald Campbell.
King’s (N.B.)—Hon. George E. Foster.
King’s (N.S.)—Frederick W. Borden.
King's (P.E.I.)- \ Peter A„d“’Pk“
v '	( James Edwin Robertson.
Kingston—Bt. Hon. Sir J. A. Macdonald, G.C.B.
Montmagny—P. Aug. Choquette.
Montmorency—Charles Langelier.
Montreal, Centre—John Joseph Curran. Montreal, Bast—Alphonse Telesphore Ldpine. Montreal, West—Sir Donald A. Smith, K C.M.G. Muskoka—William Edward O’Brien.
Napiebvill*—Louis St. Marie.
New Westminster—Donald Chisholm.
Nioolet—Fabien Boisvert.
Norfolk, N. Biding—John Charlton.
Norfolk, S. Riding—David Tisdale. Northumberland (N.B.)—Hon. Peter Mitchell. Northumberland (O.) E.B.—Edward Cochrane. Northumberland (O.) W.R.—George Guillet
Ontario, N. Biding—Frank Madill.
Ontario, S. Riding—William Smith.
Ontario, W. Biding—James David Edgar.
____f William Goodhne Perley.
Ottawa (City) | HooorS Bobillard.
Ottawa (County)—Alonzo Wright.
Oxford, N Riding—James Sutherland.
Oxford, S. Biding—Hon. Sir B. J. Cartwright, K.C. M.G.
Lambton, E. Biding—George MoncriefF.
Lambton, W. Biding—James Frederick Lister.
Lanark, N. Biding—Joseph Jamieson.
Lanark, S. Riding—Hon. John Graham Haggart. Laprairie—Cyrille Doyon.
L’Assomption—Joseph Ganthier,
Laval—Hon. Joseph Aldric Onimet.
Leeds and Grenville, N. Biding—Charles Frederick Ferguson.
Leeds, S. Riding—George Taylor.
Lennox—Uriah Wilson.
Lfcvis—Pierre Malcolm Guay.
Lincoln and Niagvra—John Ch i les Rjkert.
Lis gar—Aithur Wellington Ross.
L’Islet—Philippe Baby Casgrain.
London—Hon. John Carling.
LoTBiNii iE—Come Xeaie Rinfret.
Lunenburg—James Daniel Eisenhuuer.
Marquette—Robert Watson.
Maskinong£—Charles Jeidmie Coulombe.
MkGANTic - George Turcot.
Middlesex, E. Riding—Joseph Henry Marshall. Middlesex, N. Riding—Timothy Coughlin.
Middlesex, S. Biding—James Armstrong.
Middlesex, W. Biding—William Frederick Roome. Missisquoi David Bishop Meigs.
Monck—Arthur Boyle.
Montcalm— Ola&s Thdrien,
Peel—William A. McCulla.
Perth, N. Riding—Samuel Rollin Hesson.
Perth, S. Biding—James Trow.
Peterborough, E. Riding—John Lang.
Peterborough, W. Riding—James Stevenson.
f Hon. Charles Hibbert Tupper. Pioior-|JohiMoDongald	™
Pontiac—John Bryson.
Portneuf—Joseph E. A. De St. Georges.
Prescott—Simon Labrosse,
Prince Edward—John Milton Piatt.
Provengher—Alphonse A. C, La Bividre.
Quebec, Centre—Francois Langelier.
Quebec, East—Hon. Wilfred Laurier.
Quebec, West—Hon. Thos. McGreevy.
Quebec (County)—Hon. Sir Adolphe P. Caron, K.C.M.G. Queen’8 (N.B.)—George F. Baird.
Queen’s (N.S.)—Joshua Newton Freeman.
Quksn’8 lP.B.I.)-{"^8^-'
Renfrew, N. Riding—Peter White.
Renfrew, S. Biding—John Ferguson. Bestigouche—George Moffat.
Richelieu—Jean Baptiste Labelle.
Richmond (N.S.)—Edmund Power Flynn. Richmond and Wolfe (Q.)—William Bullock Ives.
vii
LIST OF MEMBERS OF THE HOUSE OF COMMONS.
I
Rimouski—J. B. Romuald Fieet.
Bouville—George Auguste Gigaalt.
Bussell—William Cameron Edwards.
St. Hyacinths, Michel E Bernier.
St. John (N.B.) City—John V. Ellis.
c T___rtTn.™. ,	. f Charles N. Skinner.
St. John (N.B.) City and County { Charle6 WoBley Weldon
St. John’s (Q.)—Francois Bourassa.
St. Maurice—Francois Severe L. Deeaulniers. Saskatchewan—D. H. Macdowall.
Selkirk—Thomas Mayne Daly.
Shkpfobd—Antoine Andet.
Shelburne -Lieut.-General J. Wimburn Lanrie. Sherbrooke —Robert Newton Hall.
Simooe, E. Riding—Herman Henry Cook.
Simooe, N. Riding—Dalton McCarthy.
Simooe, S. Riding—Richard Tyrwhitt.
Soulanges— James William Bain.
Stanstead—Charles C. Colby.
Sunbury—Robert Dnncan Wilmot, jun.
T^misoouata—Paul Etienne Grandbois.
Terrebonne—Hon. J. A. Chapleau.
Three Rtvers—Hon. Sir H. L. Langevin, K.C.M.G. Toronto, Centre—George Ralph R, Cock burn.
Toronto, East—John Small.
Toronto, West—Frederick Charles Denison, C.M.G.
Two Mountains—Jean Baptiste Daoust.
j Vancouver Island—David William Gordon. t Yaudreuil—Hugh MoMillan.
YsBCHkass—Hon. Fllix Geoffrion.
K^wKio,
Victoria (N.B.)—Hon. John Costigan.
Victoria (N.S.)—John Archibald McDonald.
Victoria (0.) N. Riding—John Augustas Barron, Victoria (O.) S. Riding—Adam Hudspeth.
Waterloo, N. Riding—Isaao Erb Bowman.
Waterloo, S. Riding—James Livingston,
Welland -John Ferguson.
Wellington, C. Riding—Andrew Semple.
Wellington, N. Riding— James MoMullen.
Wellington, S. Riding—James Innes.
Wentworth, N. Riding—Thomas Bain.
Wentworth, S. Riding—Franklin Wentworth Carpenter. Westmoreland—Josiah Wood.
Winnipeg—William Bain Scarth.
Yale—John Andrew Mara.
Yamaska—Fabien Vanassc,
Yarmouth—John Lovitt.
York (N.B.)—Thomas Temple.
York (O.) E. Riding—Hon. Alexander Mackenzie,
York (O.) N. Riding—William Mulook.
York (O.) W. Riding—N. Clarke Wallaoe.
SELECT COMMITTEE APPOINTED TO SUPERVISE THE PUBLICATION OF THE OFFICIAL REPORTS
OF THE DEBATES OF THE HOUSE.
BEohaed, Mr. Fracgois (Iberville). Charlton, Mr. John (North Norfolk). Colby, Mr. Charles C. (Stanstead). Davin, Mr. N. F. ( West Assiniboia), Desjardins, Mr. Alphonse (Hochelaga). Ellis, Mr. John V. (St. John, N.B., City). Innes, Mr. James (South Wellington).
Prior, Mr. Edward Gawler ( Victoria, B.C.) Soriver, Mr. Jnles (Huntingdon). Somerville, Mr. James ( West Bruce). Taylor, Mr. George (South Leeds)*
Tupper, Mr. Charles H. (Pictou).
Vanabse.Mr. Fabien (Yamaska).
Weldon, Mr. R. Chapman (Albert).
Chairman s Mr. Alphonse Desjardins (Hochelaga),
LIST OP PAHS fissure THE SESSION.
LIST OF PAIRS DURING THE SESSION.
On Mr, Mulock’s proposed resolution (Removal of Duty on
Artificial Fertilisers) 13th February t —
Ministerial.
Sib A. ARCHIBALD.
Mb. J0N0A8.
Mb. FERGUSON (Leeds). Mb. PRIOR.
Hob. Mb. KIRKPATRICK. Mb. MoOARTHT.
Mb. LABELLE.
Opposition.
Hon. Mb. MACKENZIE. Mb. GODBOUT.
Mb. 80RIVER.
Mb. FISHER.
Mb. WALDIE.
Mb. PURCELL.
Mb. PREPONTAINE.
On Mr. Landerkin’s proposed resolution (Rebate Of Duty oh Own) 14th February
Sib A. ARCHIBALD. Hob. Mb. POPE.
Mb. FERGUSON.
Mb. CURRAN.
Mr JONOA8.
Mb. McCarthy.
Sib D. A. SMITH.
Hob. Mb. DEWDNEY.
Hob. Mb. MACKENZIE. Hob. Mb. BL 1KB.
Mb. BERNIER.
Mb. WELSH.
MA PREFONT?AIN®. Mb. MULOCK.
Mb. HOLTON.
Mb. OflAfcLfON.
On Sir Richard Cartwright’s proposed resolution (Address to Her Majssty re Commercial Treaties) 18th Feb-
ruary :—
Sib A. ARCHIBALD.
Hob. Mb. POPE.
Mb. MoMILLAN.
Mb. STEVENSON.
Mb. WILMOT.
Mb. SHANLY.
Mb. LEPINE.
Mb. FERGUSON (Welland).
Mb. RYKERT.
Mb. COOKBURN.
Mb. DAOUST.
Mb. /AMIESON.
Mb. COLBY.
Mb. KENNY.
Hob. Mb. MACKENZIE.
Hob. Mb. BLAKE.
Mb. LAYERGNE.
Mb. COOK.
Mb. momullen.
Mb BE1U30LEIL.
Mb. TURCOT.
Mb. WELSH.
Mb. MULOCK.
Mb OHOQUETTE.
Mb. LANGELIBR (Montmorency) Mb. ScHlYER.
MB. BURDEfT.
Hob. Mb. JONES (Halifax).
On Mr. Jamieson’s proposed resolution (Prohibition of Intoxicating Liquors) iht February:—*
Mb. BROWN.	Mb.	SUTHERLAND.
Hob. Mb. TUPPER.	Mb.	DAYIES.
Mb. MARSHALL.	Mb. WR&B0# (St 36fa).
On Mr. Watson’s amendment (Maximum Rate on Goal) to Mr. Shanly's motion for third reading of Bill 14, incorporating the Alberta Railway and Coal Company, 26th Febrnary
Mb. MoGREEYY.
Mb. STEYEN30N.
Mb. CORBY,
Mb. WILSON (ArgenteuU).
Kb. SPROULB.
Mb. LANGELIBR (Quebec). Mb. BARRON.
Mb. WALDIE.
Mb. DESSAINT.
Mb. QUAY,
Minitteridl.	Opposition.
Mb MA3SON.	Mb. BEAUSOLBIL.
Mr. HICKEY.	Mb. RINFRET.
Mb. DAOUST.	Mb. DO YON.
Mb. WOOD (Brockville).	Hon. Mb. JONES (Hhlifai).
Mb. FERGUSON (Leeds).	Sib R J. CARTWRIGHT.
Mb. FERGUSON (Welland).	Mb. LANGELIBR (Montmorency).
Mb. PRIOR.	Mb. PrEFONTAINE.
Mb. GIROUARD.	Mb. BEOHARD.
mb. McCarthy.	Mb. WELSH.
t Mr. Laurier’s proposed resolution (Fisheries), in ametid-	
ment to motion for March:—	Committee of Supply, 1st
Hon. Mb. POPE.	Hon. Mb. BLAKE.
Mb. TISDALE.	Mb. EDWARDS.
Mb. Me KEEN.	Mb. MULOCK.
Mb MONCRIEFF.	Mb SUTIERLAND.
Mb. STEVENSON '	Mb COOK.
Mb. CARPENTER.	Mb. LISTER.
Mb. WARD.	Mb. LAYERGNE.
Mb. BOtLE.	Mb. McMillan.
Mb. BERGIN.	Mb. SCRIYBR.
Mb. BRYSON.	Mb. BERNIER.
Mb. PUTNAM.	Mb. ROBERTSON. -
mb. meigs.	Mb. HUDSPETH.
Mb. MOFFAT.	Mb. HALL.
Mb. CAMERON.	Mb. HOLTON.
Mb. OORBY.	Mb. OASEY.
Mb. LABROSSE.	Mb. LIVINGSTON.
Ml. BUAHTH.	Mb. AMYOT.
Sir Richard Cartwright’s proposed resolution (Unre-	
strioted Reciprocity)	in amendment to motion for
Committee of Supply, 19th March :—	
Hon. Mb. POPE.	Hon. Mb BLAKE.
Mb. McKAY.	Mb. LAYERGNE.
Sib D A. 8MITH.	Mb. GSOFFRION.
Sib A. ARCHIBALD.	Hon. Mb. MACKENZIE.
Mb. TISDALE.	Mb. COLTER.
Gib. LAURIE.	Mb. AMYOT.
Mb. THERIEN.	Mb. FISET.
On Mr. Lanrier’s amendment (Repeal) to Sir John Thom-
son’s motion for second reading of Bill 4, respecting the KleCtOffll Pftttohise, 3rd April:—	
Mb. HALL.	Mb. AUDET.
Mb. GIROUARD.	Mb. BEAUSOLBIL.
Mb. IVES.	Mb. GEOFFRION.
Mb. DAVIS.	Mb. EDWARDS.
Sib A. ARCHIBALD.	Hob. Mb. MACKENZIE.
Hob. Mb DEWDNEY.	Mb. MoINTYRE.
Mb. RYKERT.	Mb. WELSH.
Mb. COLBY.	Mb. COULOMBS.
Gbn. L aURIE.	Mb. BERGERON.
Sib D. A. SMITH.	Mb. YEO.
Hob. Mb. GOSTIQAN.	Mb. COOK.
kb. McCarthy.	Mb. HARBOR.
Liw « pAias amam *hb nasior
ir
On Mr. Ellis' amendment (s:x months’ hostt) to Mr. Topper’s motion for third reading of Bill 129, tq amend the Fisheries Aet, 9th April:—
Ma. WOOD (Brockville).	Mb. WALDII.
Mb. MoGBEEYT.	Mb. GEQFWIION.
Mb. OORBY.
Mb. OHISHOLM. Mb. TISDALE. On. LAURIE.
Mb. FI3HER.
Mb. PUROBLL.
Mb. LIFIMSSIK)#. Mb. WILSON (Blgiq).
On Sir Bichard Cartwri^ht’i 1888) in amendment Ajpril:—
Sib A. ARCHIBALD.
Mb. JONCAS.
Mb. PRIOR
Mb. WOOD (Bro<*viUe).
Hon. Mb. KIRKPATRICK. Mb. OHISHOLM.
i proposed resolution (L<p%a of to Committee of Supply, 10th
Hon. Mb. MACKENZIE. Mb. EDWARDS.
Mb. HALE.
Mb EI3ENHAUER.
Mb. LIVINGSTON.
Mb. UEOFFRION.
On Mr. Janes’ (Halifax) amendment (Reduction of Salaries) to Sir John A. Macdonald’s motion for second read-
ing of Bill 118, respecting Pensions to Mounted Police Force, 15th April
Sib A. ARCHIBALD. Sib D. A. SMITH. Mb. MoKAY.
Mb. SGARTH.
Mb. MOffCRIEFF. Mb. OHISHOLM,
Mb. STEVENSON. Mb. DALY,
Hon Mb MACKENZIE. Mb EDGAR.
Mb. LAVERGN8.
Mb- YEO.
Mb. MULOOK,
Mb. PRE FONTAINE. Mb, OOOK.
Mb. LANGELIER.
On Mr. Charlton’s amendment to Sir John Thompson’s motion for third reading of Bill 4, respecting the
Electoral Franchise} 15th April:—
Sib A. ARCHIBALD. Sib D. A. SMITH.
Mb. MoKAY.
Mb. SCARTH.
Mb. MONOREIFF. Mb. OHISHOLM.
Mb. STEVENSON. Ma. DALY-Mb. JONCAS.
Mb. RIOPEL.
Mb. WRIGHT.
Mb. SKINNER.
Hon. Mb. MACKENZIE*. Mb. EDGAR.
Mb. LAYERGNE.
Mb. YEO.
Mb MUlOOK.
Mb. PRfiFONrAINR,
Mb. C OOK
Mb. LANGELIER,
Mb. DESSAINT.
Mb- Di St. GEORGES. Hon. Ma* HI TO HELL Mb. HALE.
On Mr, Davies’ (P.EJ.) amendment:—
(Same as last) with the addition of Mb. DAWSON.	Mb. SORIVER.
On Mr. White’s (Renfrew) amendment (Registration Fee on Letters) to Mr. Haggart’s motion for third reading of Bill 93, to amend the Post Office Act, 15th April:—
(Same as last) with the addition of Mb. XcKEEN.	Mb. WELSH*
T
On Mr. Holton's proposed resolution (Customs Seizure*) ia amendment to Committee of Supply, l§th April:—
8m A. ARCHIBALD.
Mb. MoKAY.
Mb. SOARTH.
Mb. FERGUSON (Welland), Mb. OHISHOLM.
Mb. COUGHLIN.
Mb. TISDALE.
Mb. WRIGHT.
Sib D. A. SMITH*
Hon. Mb. MACKENZIE Mb* LAYERGNE.
Mb. YEO.
Mb. BUROETr.
Mb MULOOK.
Mb. BfiOHARD.
Mb. EDGAR.
Mb. OOOK.
Mb* GlQEWMOjr.
On Mr. Fisher’s proposed resolution (Intoxicating Liquors in North-West Territories) in amendment to Committee of Supply, 17th April
Sib A. ARCHIBALD.
Mb SCARTH.
Mb. RYKERT.
Mb FERGUSON (Welland). Mb BOISVERT.
Mb. LABELLE.
Mb. DESJARDINS.
Mb. TAYLOR.
Mb. HI0KHY-Mb. MADILL.
Hon. Mb, FOSTER.
Mb. LANDRY.
Mb. OHISHOLM.
Hon. Mb. MACKENZIE. Mb. YEO.
Mb. EDWARDS.
Mb. BURDETT.
Mb. TURCOT.
Mb QQUrURE.
Mb. LAYERGNE.
Mb. TROW. Mb.LANDSRKIN.
Mb INNIS.
Mb CHARLTON,
Mb. CHQUINARD.
Mb. GEOFFRKXN.
On Mr. Lauriers amendment (re Steamship Service, H,C. and Australia) to Mr. Foster’s motion tq OOGOHr in resolution reported firqm’Committee of Whole, 2dnd
April j-w
Mb. MoKAY.
Mb. SOARTH.
Mb. RYKERT.
Mb. FERGUSON (WeUaad). Mb BOISVERT.	‘
Mb. HUDSPETH.
Mb. JAMIESON.
Mb. WHITE (Renfrew).
Mb. WOPRL.
Mb. LEPINE,
Mb. FERGUSON (Renfrew). Mr. BOYLE.
Mb DESIGN,
Mb. STEVENSON.
Sm JOHN A. MAQIIQN4 LOME. WILSON (Argeateu.il). Mb. COUGHLIN-Mb WOOD (Brockrllle).
Mb. OHISHOLM,
Mb. HICKEY.
Mr LAVEEGNB.
Mb. YEO.
Mb. EDWARDS.
Mb. BURDETT.
Mb. TUROOTTE.
Mb. BARRON.
Mb. FISHER.
Mb. WALDIB.
Ma. CHOQOETTI.
Mb. LANGKLIflft ^feec),
Mb MEIGS.
Mb. EORIVBR.
Mb. PURCELL.
Mb. OOOK. '
&r B,1 CARTWRIGHT.
Mb. CHARLTON,
Mb GEOFFRION.
Mb BERNIER.
Mb Da St.
Mb. LANGELIER(M^G^j^n^i).
On Mr. Barron’s proposed resolution (Sale of Timber on Indian Reserves) in amendment to Committee of Supply, 23rd April:—
Sir A. ARCHIBALD.	Hoa. Mb. MACKENZIE.
Mb. SOARTH.	Mr. YEO.
Mb. FERGUSON (Welland). Mb. BURDETT.
Mb. BAIN	Mb. Da ST. GEORGES.
Mb. LANDRY.	Mb. OHOUINARD.
Mb. WOOD (Broekrille).	Mb. EDWARDS.
Sm JOHN A. MACDONALD. Mb. ROWAND.
Mb. HALL.	Mb. SORIVER.
Mb. OHISHOLM.	Mb. KIRK.
Mb. COUGHLIN.	Mb. FLYNN.
Mb. SPROULE,	Mb. MoINTYRE.
f
list op pairs during the session
On Mr, Mnlock’s proposed resolution (Militia Clothing) in amendment to Committee of Supply, 25th April:— Sib A. ARCHIBALD.	Hon. Mb. MACKENZIE.
Hob. Mb. TOPPER.
Mb. ROOMS.
Mb WILSON (Lennox). Mb. JONOAS.
Mb. McK AY.
Mb.SOARTH.
Mb. FERGUSON (Welland). Mb. MONORIEFF.
Mb. CHISHOLM.
Mb. O'BRIEN.
Hob. Mb. OOSTIGAN.
Sib JOHN A. MACDONALD. Mb CORBY.
Hob. Mb. TOPPER.
Mb. LAVERGNE.
Mb. YEO.
Mb. BITRDETT.
Mb. LISTER.
Mb. Db ST. GEORGES. Mb. MEIGS.
Hob. Mb. BLAKE.
Hob. Mb. L ItJRIER. Mb. BOWMAN.
Mb EDGAR.
Mb. WHITE (Renfrew). Mb. WOOD (Brockville) Mb. HICKEY.
Mb WRIGHT.
Mb. WHITE (Cardwell). Mb. HESSON. Mb.PERLRY.
Mb. SKINNER.
Mb. CARGILL.
Mr. EDGAR.
Mb. McDonald (Huron). Mb. COUTURE.
Mb. LAVERGNE.
Mb. MULOOK.
Mb. OHOUINARD.
Mb. LANGELIER (Quebec). Mb. HALE.
Hob. Mb. MITCHELL.
Mb. LIVINGSTON.
Mb. EDWARDS.
Mb. SORIVEB.
Mb. LANDERKIN.
Mb. ROOMS. Mb. HALL.
Mb WRIGHT.
Mb. MoDONALD (Huron). Mb. SORIYER.
Mb. OASGR1IN.
The following paired on all Sobjects for balanoe of Session,
Hob. Mb CHAPLEAU. Mb. MAODOWALL. Bib D. A. SMITH.
Mb. AMYOT.
Mb. OHOQUETTE. Mb. GILLHOR.
On Mr. Barron's proposed resolution (Duty on Saw Logs) in amendment to Committee of Supply, 26th April:—
Sib A. ARCHIBALD.
Mb McKAY.
Mb. SOARTH.
Mb.FERGUSON (Welland). Mb. MONORIEFF.
Mr. CHISHOLM.
Mb. O’BRIEN.
Hon. Mb. OOSTIGAN.
Mb. CORBY.
Mb. TOPPER.
Mb. ROOMS.
Mb. WILSON (Lennox).
Mb. JONOAS.
Mb. WHIT® (Renfrew).
Mb. HICKEY.
Mb. WRIGHT.
Mb. WHITE (Cardwell).
Hos. Mb. MACKENZIE.
Mb. LAVERGNE.
Mb. YEO.
Mb. BUROETT.
Mb. LISTER.
Mb. Db St. GEORGES.
Mb. MEIGS.
Hob. Mb. BLAKE.
Mb. BOWMAN.
Mr. EDGAR.
Mb. M0DONV.LD (Huron). Mb. COUTURE.
Mr. LAVERGNE.
Mb. SORIVER.
Mb. LANGELIER (Quebec). Mb. HALE.
Hob. Mb. MITCHELL.
On Sir Richard Cartwright’s amendment (re Manitoba Penitentiary) to motion to ooncur in resolution reported from Committee of Snpply, 26th April:—
Sib A. ARCHIBALD.
Mb. SOARTH.
Mb. FERGUSON (Welland). Mb. MONORIEFF.
Mb. CHISHOLM.
Mb. O’BRIEN.
Hob. Mb. OOSTIGAN.
Mb. CORBY.
Hob. Mb. MACKENZIE. Mb. YEO.
Mb. BURDETT.
Mb. LISTER.
Mb. Db St. GEORGES. Mb. MEIGS.
Hob. Mb. BLAKE.
Mb. BOWMAN.
29th April:—	
Mb. MoDOUGALD (P.ctou),	Mb FLYNN.
Mb. VANASSE.	Mb. B0URAS3A.
Mb. THfiRIEN.	Mb. GAUTHIER.
Mb. DESAULNIERS.	Mb. BEAUSOLEIL.
Mb. JAMIESON.	Mb. BgOHARD.
Mb. WILSON (Argenteuil).	Mb. GUAY.
Mb. FREEMAN.	Mb LANGELIER (Montmorency)
Mb. NEVEU.	Mb GIGAULT.
Sib A. ARCHIBALD.	Hoh Mb. MACKENZIE.
Mr AUDET.	Mb TURCOT.
Mb. DALY.	Mb. WELSH.
Mb. WOOD (SrockTille).	Mb. LANGELIER (Quebec).
Mb. BELL.	Mb. BERNIER.
Mb. GRANDBOIS.	Mb. FISET.
Mb BAIRD.	Mb. HALE.
Mb. CARPENTER.	Mb. AMYOT.
Mb. HESSON.	Mb. LIVINGSTON.
Mb SMITH (Ontario).	Mb. SEMPLE.
Mb. COULOMBS.	Mb. RINFRET.
Mb. BOYLE.	Mb. DE3SAINT.
Mb. FERGUSON (Welland).	Mb. BURDETT.
Mb. SOARTH.	Mb YEO.
Mb. MON CRIEFF.	Mb LISTER.
Mb. 0 HIS HOLM.	Mb. Da St. GEORGES.
Mb O’BRIEN.	Mb. MErGS.
Mb. CORBY.	Mb. BOWMAN.
Mb. ROOMS.	Mb. MoDONALD (Huron).
Mb. WILSON (Lennox).	Mb. COUTURE.
Mb. CARGILL.	Mb LAHDERKIN.
Mb. TISDALE.	Mb. LANG.
Mb. MASSON.	Mb. ROWAND.
Mb. FERGUSON (Renfrew).	Mb. GODBOUT.
Mb. MARSHALL.	Mb. MoMILLAN (Huron).
Mb. HUDSPETH.	Mr. BARRON.
Mb. MILLS (Annapolis).	Mb. ROBERTSON.
Mb. PUTNAM ob LANDRY.	Mb. EISENHAUER.
o«5ic 0f (ftommon*) g etatus
THIRD SESSION, SIXTH PARLIAMENT.^ VIC.
HOUSE OF COMMONS.
Thursday, 31st January, 1889.
The Parliament, whioh had been prorogued from time to time, was now commanded to assemble on the 31st day of January, 1889, for the Despatch of Business.
