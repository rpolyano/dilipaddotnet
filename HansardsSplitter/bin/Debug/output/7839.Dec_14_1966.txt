
CANADA
HOUSE OF COMMONS DEBATES
OFFICIAL REPORT
FIRST SESSION—TWENTY SEVENTH PARLIAMENT
15 ELIZABETH II
VOLUME XI, 1988-67
COMPRISING THE PERIOD FROM THE FOURTEENTH DAY OF DECEMBER, 1966 TO THE TWENTY FOURTH DAY OF JANUARY, 1967, INCLUSIVE
INDEX ISSUED IN A SEPARATE VOLUME
23033—703J
ROGER DUHAMEL, F.R.S.C.
queen’s PRINTER AND CONTROLLER OF STATIONERY OTTAWA, 1967
11111
HOUSE OF COMMONS
Wednesday, December 14, 1966
