HOUSE OF COMMONS
DEBATES
LABOUR CAN'T- iRAVAIL
RECEIVED -1 LC'J
OFFICIAL REPORT
AN 2 8 1994
LIBRARY - B1BLIQTHEQUE
THIRD SESSION—THIRTY-FOURTH PARLIAMENT
40 Elizabeth II
VOLUME II, 1991
COMPRISING THE PERIOD FROM THE TENTH DAY OF JUNE, 1991 TO THE TWENTY-FOURTH DAY OF SEPTEMBER, 1991
INDEX ISSUED IN A SEPARATE VOLUME
Published under authority of the Speaker of the House of Commons ty the Queen’s Printer for Canada
Available from Canadian Communication Group — Publishing. Supply and Services Canada, Ottawa, Canada K1A 0S9.
1319
HOUSE OF COMMONS
Monday, June 10, 1991
