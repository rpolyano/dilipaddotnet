 at 3 o’clock.
PENITENTIARIES
On motion of Sir J. A. Macdonald the Bill respecting Penitentiaries and the Directors thereof was considered in Committee of the Whole, Mr. Kirkpatrick in the Chair.
It was then reported, and read a third time and passed.
COMMITTEE ON SUPPLY
Hon. Mr. Rose moved that the House resolve itself again into Committee of Supply.
Hon. Mr. Holton moved in amendment, seconded by Mr. Mackenzie, that all the words after “That” in the original motion be omitted, and the following inserted instead thereof—“That the recent constitutional changes have rendered it necessary to complete the organization of all branches of the public service throughout the Dominion; that in this organization the strictest economy should be observed, that all unnecessary executive departments and all superfluous offices should be abolished; that all executive salaries should be diminished; that all unnecessary or indifferent officers be removed; that the salaries of all officers of the Dominion Government of similar grades in Previous Provinces should be equalized, and that payment of salaried officers for special services should be forbidden by law.” He said it seemed now to be the fixed determination of the House that there should be economy in the administration of public affairs. He had the honour a few nights ago of presenting a resolution in this direction, and although it was not accepted by vote of the House, he had the great gratification last night of seeing one important part of it accepted by the House. Objection had been taken to his grouping several separate propositions in one motion. Objection had also been taken in the mode in which he proposed to deal with the subject. Endeavouring to profit by the discussion on that occasion, he had now framed a motion which he thought would meet the approbation of a large majority of the House.
99038—41
Indeed he hoped his honourable friends opposite, in view of the discussions they had had, would at once acquiesce in it, and enable them to go into Committee of Supply with the declaration of principle which he now invited the House to make. A good deal had been said about motions in this form and in this connection being regarded as motions necessarily of want of confidence. He begged to declare that he did not bring forward this motion with that view, nor would he bring forward any motion having the effect of a vote of want of confidence. In England, every week, motions of this character and in this connection were put without ministers ever daring to insult the intelligence of the House by affirming that such motions were “ex-necessitate” motions of want of confidence. He would not delay the House by going over the points which in recent discussions had been debated so fully, and should therefore content himself with putting in the Speaker’s hands the motion in amendment just read.
Sir John A. Macdonald thought the honourable member for Chateauguay ought to be satisfied with recent legislation respecting economy. The present motion of the honourable member was a mere rapid declaration in favour of economy and efficient administration, and he (Sir John) did not know why on the present occasion these matters of efficiency and economy should come up before the House. There was neither reason, fitness nor purpose in the motion, and it could have no result that he could see further than to delay the business of the House. It was not specially germane to the matter before the House, and was altogether such a motion, was brought on at such a time, and showed such an evident desire on the part of the honourable gentleman to convey by a side wind a direct censure on the administration, that the Government could not with respect to themselves or their position accept the motion in any way than as a vote of want of confidence. Where was the necessity for the motion? The Government themselves had invited attention to the whole subject.
Mr. Mackenzie said it would be recollected that at a previous period, when the threefold motion to which allusion had been made
630
COMMONS DEBATES
May 5,1868
came up, several gentlemen supporting the administration declared their adherence to each of the three parts of the motion to a greater or lesser extent, but they had objected to the three-fold capacity of the motion, and in order to meet the views of gentlemen taking this particular ground the present motion had been framed.
Hon. Mr. Chauveau—Because honourable gentlemen opposite seem to become all things to all men.
Mr. Mackenzie said the motion bad been framed for the convenience of those taking the particular objection alluded to. If, as the Government aver, this was part of the policy they proposed to adopt, then they could have no objection to the motion. The House had reason to believe that in reorganizing the departments the Government had not shown a due regard to economy, and it was of the last importance to the country, that the reorganization of the departments should be placed on a proper footing, and that they should have the opinion of the House to sustain and guide them in making the necessary organizations and initiating a proper policy. His own opinion was, judging from the number of departments created, and the excessive number of officers attached to them, that the Government were preceding without that regard to economy which ought to characterize their proceedings. It was at the initiation of a new system that the best opportunities offered for placing matters on a proper footing, and it was merely with a view of having this done the motion had been proposed. If the Government continued to meet it as a vote of want of confidence, he could only say that he looked on such conduct as trifling with the business of the House and the country.
Sir G. E. Cartier strongly opposed the motion in French, urging on his followers to resist it as one aimed at the Government.
Mr. Dufresne was sorry the member for Chateauguay had brought this measure forward. It only caused a delay of the business of the House, and all that was asked for by it would be obtained by the Government measures before the House.
Hon. Mr. Chauveau congratulated the Government on their manly and straightforward course on this motion. He believed that a necessity existed for economy, and hoped to see the House follow after it, but from his heart he detested the spasmodic outcry peri-
[Mr. Mackenzie (Lambton).]
odically made for retrenchment, for over and over again it had been shown to be a piece of humbug.
Dr. Parker charged the Government with pursuing a career of reckless extravagance, and maintained that on this question as on that of the Governor-General’s salary, the Government ought to allow their followers to act as they pleased. But the Government make this a vote of want of confidence, and violently appeal to their adherents to rally to their support. He would vote for the motion.
Hon. Mr. Fisher said that there was a proposition contained in the motion before the House which, coming as he did from New Brunswick, he could scarcely oppose. He alluded to the discrimination against New Brunswick made in the salaries; he objected to the Government making this motion a want of confidence, and said he would vote for it.
Mr. Bolton, with the understanding that the Government would take up the matter alluded to in the motion at an early day, and apply a remedy, would vote against the amendment.
The House then divided on the amendment, which was lost—yeas 36; nays 94.
Yeas—Bodwell, Bourassa, Bowman, Burpee, Cameron (Huron), Connell, Coupal, Dorion, Farris, Fisher, Geoffrion, Godin, Holton, Kempt, Kierzkowski, McDonald, (Glengarry), Macfarlane, Mackenzie, Mc-Conkey, McMonies, Mills, Morison (Victoria), Oliver, Paquet, Parker, Redford, Rymal, Scatcherd, Senecal, Snider, Stirton, Thompson (Haldimand), Wallace, Wells, Whitehead, Young—Total, 36.
Nays—Ault, Bechard, Bellerose, Benoit, Bertrand, Blanchet, Bolton, Bowell, Bown, Brown, Burton, Caldwell, Cameron (Peel), Campbell,	Carling, Caron, Cartier, Cart-
wright, Casault, Cayley, Chamberlin, Chauveau, Cheval, Cimon, Colby, Costigan, Crawford (Brockville), Crawford (Leeds), Desfaulmier,	Dobbie,	Drew,	Dufresne,	Dunkin,
Ferguson,	Fortin,	Galt,	Gaucher,	Gaudet,
Gendron,	Grant,	Gray,	Grover,	Hagar,
Holmes, Howland, Huot, Hurdon, Irvine, Jackson, Johnson, Jones (Leeds and Grenville), Kirkpatrick, Langevin, Langlois, Lawson, Macdonald (Sir John A.), McDonald (Middlesex), McGill, Masson (Soulanges), Masson (Terrebonne), McCarthy, McDougall, McMillan, Merrit, Morris, Morrison, Munroe, Perry, Pinsonneault, Pope, Pozer, Pouliot,
May 5,1868
COMMONS DEBATES
631
Renaud, RobitaiUe, Ross (Champlain), Ross (Dundas), Ross (Prince Edward), Ryan (Kings), Ryan (Montreal West), Simard, Simpson, Sproat, Stephenson, Tilley, Tremblay, Walsh, Webb, White, Wilson, Wood, Workman, Wright—Total, 94.
The House then went into Committee of Supply, Col. Gray in the Chair—and passed the items—for the construction of Railway between Halifax and Pictou, $200,000; construction of Railway between Windsor and Pictou, $200,000; construction of Railway between Windsor and Annapolis, $300,000; construction European and N.A. Railway, $31,750. To meet stock in Western Extension, $180,000; to meet subsidy in Western Extension, $150,000; to meet subsidies to other lines, N.B., $141,000; towards location Intercolonial Railway, $50,000.
The next item was $95,305.31 for canals.
Mr. Mackenzie said the sum had been spent without any authority whatever from Parliament. He regretted exceedingly that his honourable friend opposite (Mr. McDougall) should have thought it consistent with his honour as a Minister of the Crown to endeavour to smuggle a vote of this kind through Parliament for a work which Parliament had never authorized. It was an entirely new work.
Hon. Mr. McDougall—No.
Mr. Mackenzie—It was, and when the Government asked for money for public works in the early part of the session he declared there were no new works included in those on which the money voted was to be spent. In a case such as the present, according to Parliamentary practice, the Minister should ask for a Bill of Indemnity. The money had been all expended before the matter was brought under the notice of Parliament; and still further to blacken the transaction, he found that one James Goodwin, of Ottawa, got the tender for the work, although his tender was $12,000 higher than that of others; and still worse, the department allowed Goodwin to break his contract, and whereas he agreed to do the work of excavation for $1.20 per yard they now gave him $1.50 merely on his own representation that he had lost money when he held the contract at $1.20. The department made no inquiry, but quietly gave this man 30 cents per yard more than the fair paying price at which he had contracted to do the work.
99038—4H
Hon. Mr. Johnson agreed with the view the Ministers ought to have brought down a Bill of Indemnity in this matter.
Hon. Mr. McDougall reproved the member for Lambton for speaking dogmatically. The circumstances under which the work had been undertaken fully justified the action of the department. Action which had throughout been taken only after thorough investigation. In the first place the department had necessarily power to deal with these works without the authority of Parliament, so that it could not be said to be unconstitutional to spend money in advance of a Parliamentary vote. The work too was one of very great importance, and it was so necessary the repairs should be made, that private persons trusting to be reimbursed had undertaken the work. As to the tender, it was well known that numbers of persons tendered for public works at such rates as they could not possibly perform the work; for trusting that once the work was begun it would be pushed to completion and more money would be advanced. It was not advisable that any such contractors should get hold of the work, and hence it had been let, though at an advanced figure, to a responsible man.
Mr. Currier explained that if ever there was a work of necessity and one which would turn out profitable, it was this canal.
Mr. Jones took exception to the remark of the Commissioner of Public Works, that the tender had not been let to the other competitors because they were not responsible men. He denied that statement, so far at least, as some of the contractors were concerned. He objected to the mismanagement of the Public Works Department in this particular.
Mr. Alonzo Wright spoke of the immense trade coming through this canal, and thought the expenditure fully justified.
Hon. Mr. Dorion opposed the item.
It was carried, and the House rose at six o’clock.
After the recess.
The remaining items for canal, for harbours, $11,000; slides and booms, $10,000; Parliamentary and Departmental Buildings, $55,000; Rideau Hall, including the purchase of the property and the furniture, $122,000. This includes the lease for one year of eleven acres of land adjoining the hall, with option of purchase. Custom House, Quebec, $3,000; being a reduction of $1,000 on the sum in the estimates.
632
COMMONS DEBATES
May 5. 1868
The item Montreal Post Office, $4,000, was struck out, as some extensive alterations in that building are contemplated.
In the items of lighthouses, the sum for Point St. Laurent was made $10,000.
The items for Nine Mile Point, Kingston, were struck out.
The remaining items under head of lighthouses were carried.
The item $15,500 for roads and bridges was also carried with $70,000 for arbitration and awards; $58,000 rents and repairs on public buildings.
On the item $10,000 for purchase of land for construction of hospital and quarantine station at Halifax.
Mr. McDonald (Lunenburg), stigmatized the matter as a job and a fraud on the Dominion.
Hon. Mr. Rose said the Government would see that the Dominion got a fair return for its expenditure.
The item passed and the balance of items for public works, also items for lighthouses and coast services $192,501,53, and for ocean and river steamer services, $136,600.
The item $13,400 for fisheries, Quebec and Ontario, passed.
On the item $5,000 for salaries of about fifty-seven overseers, ranging from $300 to $400 each and of local officers and guardians, including their disbursements—it was objected that the maintenance of this service was not binding on the Dominion.
Hon. Mr. Rose said that in some respects they had extended to Nova Scotia the system that prevailed in Canada.
Hon. Mr. Holton thought it very extraordinary that the system which prevailed in Quebec and Ontario, where fishing was only a minor interest, should be extended to Nova Scotia and New Brunswick, where fishing was not a minor interest, and where, it was to be supposed, they had adopted a more efficient system for the protection of their interests than had been in use in Quebec and Ontario.
Sir J. A. Macdonald said that the Government would make inquiry into the matter, and if, as had been contended, the Dominion was not responsible for the maintenance of these overseers, the Government would be only too happy to be rid of the expense.
The item was carried.
As to the item under the head Fisheries, $5,000—indemnity of fishermen who had acted under faith in existing law,
Hon. Mr. Rose said he wished it struck out, as the Government intended to discontinue the bounty system.
Mr. Jones (Halifax) claimed that this was a direct breach of faith by the Government, as they had led the people of the Lower Provinces to believe that the Bounty system would be continued and extended if Union candidates were returned. A dispatch to that effect had been forwarded to an official in the Lower Provinces by the Minister of Marine and Fisheries.
Mr. Blake laughingly exonerated the Government from the charges preferred by the member from Halifax. The charge was that the Government had broken faith, as they promised to extend the system of Bounties to Nova Scotia if the Union candidates were returned. Now the honourable gentleman making the charge was an “Anti”, and hence it was clear that the Government was exonerated. (Laughter).
Mr. Mackenzie said that the famous dispatch alluded to ought to be produced. (Laughter). It would be quite a curiosity.
Sir J. A. Macdonald said he had just been informed that the dispatch in question ran thus—“I will press on the Government of the Dominion the extension to Nova Scotia of the Canadian system of Bounties.”
The item was struck out.
The remaining items under the Fisheries were carried.
The item $144,648.47, indemnities under Seignorial Acts carried; also the item $8,490 annuities and grants to Indians.
The following items then passed:—Culling timber, $70,500; railway and steamboat inspection, $12,162.
On the item included in the latter, of $3,550 for railway inspection,
Mr. Jones (Leeds) said he never could understand what these overseers did. When an accident happened, they came along and made a report on the subject, but he never yet heard of a case in which an accident had been prevented by these inspectors. (Loud and continued laughter).
Hon. Mr. Rose said he never heard of such a case either. (Laughter).
May 5,1866
COMMONS DEBATES
633
The items amounting to $50,368.42 under the heading Miscellaneous, were carried.
On the item $520,016 for collection of Customs,
Hon. Mr. Tilley explained, in reply to Mr. Holton, that the percentage in the whole revenue collected was greater in Nova Scotia and New Brunswick than in Ontario and Quebec. Still the salaries of the officers were not so large in the former as in the latter office, for the performance of the same duties. How the greater expense came to attach to the Maritime Provinces in this matter was easily explained. Nearly three-fifths of the whole revenue of Ontario and Quebec was collected at one port, Montreal, and hence the cost of collection was cheaper than in the Lower Provinces, where the revenue had to be collected in 60 or 70 ports. It was his intention during the recess to see whether the number of these smaller ports in Nova Scotia and New Brunswick, and elsewhere in the Dominion, could not be reduced without impairing the efficiency of the public service.
Mr. Anglin said the promises of retrenchment and economy by the Ministry sounded very hollow in the ears of the Lower Provinces. Instead of a cheaper system, as had been promised under Confederation, there had been a large increase in the numbers and salaries of Customs Officers in the Lower Provinces of and about the 1st July last.
Mr. Mackenzie said the customs collection at Quebec last year amounted to about half a million and the expenses were $55,000 or considerably over 10 per cent of the collection. At Hamilton $50,000 more was collected than at Quebec, and the charge was only $17,000. The income at Toronto was nearly a million, and the entire expenses of collection was only about $25,000, or considerably less than 2£ per cent. Surely there was some room for enquiry as to the enormous charges at Quebec. Again, at Rimouski he found there were a collector and two landing waiters, and the revenue collected was absolutely nothing. It might be necessary to keep an officer there at a small salary, but jwhat could be the use of two landing waiters?
Hon. Mr. Tilley said that at ports like Quebec and Montreal, there was a large expenditure, where no revenue was received. For example, when a vessel arrived, a tide waiter was put on board—an expense which
did not require to be incurred at Toronto. The expense of the bonding system thereat was very large.
Mr. Mackenzie—But at Montreal the expenses of collecting are only a little over 2 per cent.
Hon. Mr. Tilley said the discrepancy in the cost of collecting at different ports was one of the points to which he proposed to give his earnest attention at an early day.
The item was agreed to, as also the items for collection of Inland Revenue and Post Office Revenue. In connection with the latter item,
Mr. Mackenzie urged the propriety of providing better mail accommodation for the north shores of Lake Huron and Superior. He said he would urge the matter more formally on the attention of the Government, and ask exact information as to the nature of the arrangements now existing, when the estimates for 1868 and ’69 were before the House.
The items—Public Works, , $730,742, and collection minor revenue, $20,000—were agreed to, being the last of the ordinary estimates for 1867 and '68.
The supplementary estimates for the same year were also agreed to, without discussion.
The Committee then proceeded with the estimates for 1868 and ’69.
On the first item—Salaries of four Lieut,-Governors, $30,000—
Hon. Mr- Chauveau said at the concurrence he would make some remarks on this subject! They cofild not increase the sum mentioned in the estimates, but they could at least give their opinions.	:
Mr. Jones (Halifax) said he quite agreed with the Premier of Quebec as to the propriety of increasing the salaries of the Lieutenant-Governors.
The item was agreed to,
On the second item—Salaries and Contingencies of Departments—$550,000, to be distributed under the provisions of the Civil Service and Contingencies Act.
Hon. Mr. Rose said he asked the House to pass this sum en bloc. The House would have an opportunity of discussing the details in connection with Civil Service Act.
Mr. Mackenzie asked if Government were to introduce a Bill founded on the resolution
634
COMMONS DEBATES
May 5,1868
of last night with reference to the Governor-General’s salary after the resolution was concurred in.
Sir J. A. Macdonald said it was not their intention to do so. It would be open, however, to any member of the House to introduce such a Bill.
Hon. Mr. Hollon thought it very irregular to found a vote of the kind on an Act (the Civil Service Act) which as yet had no existence, and to carry through which no serious step had yet been taken.
Hon. Mr. Rose said if the Civil Service Bill how before the House did not pass, the vote would be distributed in accordance with the provisions of the existing Civil Service Act. The vote was not so large as the vote for the present year. It was $30,000 less. This reduction he expected to effect by means of the Civil Service and Contingencies Act.
The item was agreed to.
On the item—addition to the salaries of certain judges of Nova Scotia and New Brunswick.
Sir John A. Macdonald said that it was improper that the item of the judges’ salaries should come up annually. Hence a Bill was being prepared and would be introduced this session to fix the salaries.
Hon. Mr. Holton suggested that it might be better to take this item out of the estimates, and omit the discussion on the matter until the Government introduced the resolutions on which their promised Bill would be founded.
Sir G. E. Cartier advised settling this matter at once. It would certainly advance the discussion a stage.
The item passed.
On the item $25,000 for frontier police.
Sir John A. Macdonald, in reply to Mr. Young, stated that the Government were creating a Dominion police force. The necessity for having such a body of men under control of the Government had been felt, and small forces were now stationed at Ottawa, Sarnia and elsewhere, under control of Mr. McMicken, Stipendiary Magistrate for Ontario. This force had nothing to do with the detective service, but was for frontier and
IMr. Mackenzie (Lambton) J
international service, for the Dominion buildings, and for such other Dominion service as would be found necessary in the way of guarding police buildings.
Hon. Mr. Holdon inquired if it would not be necessary for Government to introduce a Bill on the subject.
Sir John A. Macdonald said it would.
Mr. Blake said in that case it would be better to drop the item from the estimates and deal with it when it came before the House in the measure to be brought down by Government.
Sir John A. Macdonald replied that he preferred the House should pass the item. It was more convenient at this stage to pass the items in the order in which they stood.
The item passed.
On the item, $30,000 for harbour, river and Government police, Quebec and Montreal, there was a discussion—several members contending that as Montreal and Quebec derived very great benefits from the shipping, the people of those cities ought to bear this charge and not the Dominion.
The item, however, passed, and the remaining item under the head, Administration of Justice; also all the items under the head, Penitentiaries.
On the item for legislation,
Mr. Mackenzie said that there would be a discussion. He understood the Government to have taken charge of the Contingencies, but he had been told that recently the Speaker of the Senate had expended $4,000 on some operations connected with the rooms in the Senate quarter of the public buildings —which expenditure had been unauthorized, it was said, by the Department of Public Works.
Hon. Mr. McDougall said that the presiding officers of both Houses had, during the recess, represented to the department that several alterations should be made in the building, some of the applications had been sanctioned and others were not, but it was said that some of the alterations objected to would be entered on at the risk of the officer ordering them. In the other House extensive alterations, such as new kitchen ranges connected
May 5,1868
COMMONS DEBATES
635
with the saloon were proposed, but he had objected to them, not thinking any necessity existed for the change.
Mr. Mackenzie hoped that as far as the House of Commons was concerned, the Chairman of Contingencies would look sharply after such expenditure, and do what was possible to prevent a repetition of unauthorized expenditure.
Mr. D. A. McDonald said that in regard to one matter, the fitting up of baths, the Speaker was not altogether responsible. He (Mr. McDonald) and others had felt the
necessity for such a convenience, had suggested it, and he was willing to take the responsibility of this action in the matter.
The items down to the 22nd passed.
The items for scientific institutions, for arts, agriculture, and statistics, and for immigration and quarantine also passed.
The Committee then rose, reported the resolutions, and obtained leave to sit again to-morrow.
The House adjourned at a quarter past one o’clock.
HOUSE OF COMMONS
Wednesday, May S, 18S8
