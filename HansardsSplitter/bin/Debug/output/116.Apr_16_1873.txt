 at 3 p.m.
Prayers
Mr. RYMAL presented the report of the Committee on Standing Orders.
* * *
BILLS INTRODUCED
The following bills were introduced:—
Mr. RYAN—To extend the powers of the Montreal Telegraph Company.
Mr. DOMVILLE—To incorporate the Canada Marine Insurance Company.
Mr. CHISHOLM—To grant to the Hamilton and Milton Road Company the powers prayed for in their petition.
Mr. PALMER—To continue and make perpetual the Insolvent Act of 1869, and all Acts passed in amendment thereof.
Hon. Mr. CARLING—To enable the Great Western Railway to further extend and improve its connections.
Hon. Mr. CARLING—To incorporate the Great Western and Lake Shore Junction Railway Company.
* * *
NEW BRUNSWICK SCHOOL ACT
Mr. MERCIER moved that the correspondence, et cetera, relative to the New Brunswick School Act be referred to the Committee on Printing.—Carried.
* * *
LIGHTS AND BEACONS
Mr. SAVARY asked whether the Government intend to take any steps for the erection of a beacon light at Church Point, Port Acadian, in St. Mary’s Bay, county of Digby, in accordance with the prayer of the petition for that object; also, whether the Government intend to place a bell buoy on Dartmouth ledge at the
entrance of the grand passage, Bay of Fundy, during the ensuing season.
Hon. Mr. MITCHELL said now that the attention of the Government had been called to these important matters, enquiries would be made in relation to them.
* * *
CANAL PRIVILEGES
Mr. BEAUBIEN asked whether it is the intention of the Government to grant to the manufacturers along the canal at Cote Saint-Paul the facilities of exit which the Government promised them along the canal at the time when those manufacturers leased the water powers, as appears by certain notarial deeds.
Hon. Mr. LANGEVIN: It is.
* * *
APPOINTMENT OF POSTMASTER
Mr. MERCIER asked, first, whether F. Ponton, Esq., has resigned his office of postmaster of St. Angele de Monnoir in the district of Saint-Hyacinthe, County of Rouville; second, whether any person has been appointed as his successor, who such successor is, and at whose recommendation has he been appointed; third, whether: Benonie Lozelle, Esq., of the same parish had not make application in time for the said office; whether he was not qualified to fill the vacancy, and whether he was not recommended to this Government by the Rev. Eloi Ponton, cure of the parish, Victor Robert, Esq., member for the County in the Local Legislature, Honore Mercier, member for the County of Rouville in the House of Commons, and by a large number of other persons.
Hon. Mr. TUPPER said Mr. Ponton resigned on February the 28th, 1873, and his successor, Mr. M. O’Carroll, was appointed on March the 22nd, 1873, at the recommendation of Hon. Mr. Langevin. He gave the names of those who recommended Mr. Lozelle and those who recommended Mr. O’Carroll.
* * *
MAIL SERVICE
Mr. PRICE asked whether it is the intention of the Government to organize a daily mail from Quebec to Chicoutimi and Murray Bay, with a tri-weekly mail from Chicoutimi to Lake Saint-Jean and from Murray Bay to Bersimis, as well as postal communication for
COMMONS DEBATES
272
April 16, 1873
the north shore of the River St. Lawrence from Bersimis downwards.
Hon. Mr. TUPPER said it was the intention of the Government to organize a daily mail from Quebec to Chicoutimi and Murray Bay. The other subjects mentioned were under the consideration of the Government.
* * *
INSPECTION OF FISH, ET CETERA
Mr. DUGUAY asked whether it is the intention of the Government to introduce during the present session, a bill to provide for the inspection of fish, fish oil, butter, cheese and lard, exposed for sale in the markets of the different towns and cities of the Dominion.
Hon. Mr. TILLEY said the general Inspection Bill was printed, and would be distributed either that afternoon or tomorrow, and the hon. gentleman would find in it clauses relating to this subject.
* * *
DUTY ON TEA AND COFFEE
Mr. WILKES moved for the correspondence respecting the imposition of ten per cent duty on tea and coffee imported from the United States. There was a public rumour abroad that restrictions had been imposed upon the action of the Government by the Imperial Ministry, and the result was a delay in the imposition of the ten per cent duty. It was desirable that the facts should be made known to the House, and that they should be informed of the reasons urged by the Government which had induced the imperial Government to withdraw their objections.
Hon. Mr. TUPPER had no objection to the motion, which was carried.
* * *
SIMCOE NORTH ELECTION
Mr. COOK moved for a return of the aggregate sum of money supplied to the returning officer for the north riding of the county of Simcoe during the late elections for the Commons, for the purpose of meeting the expenses of the said election and remunerating the persons employed as deputy returning officers in connection with the sub-divisions in which they severally officiated, and the amount paid to each deputy returning officer for said services, and all disbursements attendant upon the discharge of his official duties.
He stated that the Government appointed another gentleman previously to the last election to fill the office of returning officer, and it was reported that after he received the writ he acted in a most partisan manner and that he used illegal means for the furthering of the return of the gentleman supporting the Government. He wished
to know by whose choice the selection was made, and he hoped the returns would be sent down, as there was a good deal of dissatisfaction upon the subject, and it was desired that the amount of money expended at the election should be given.—Carried.
* * *
PRINTING
Mr. STEPHENSON moved the adoption of the first and second reports of the Joint Committee on Printing.
Hon. Mr. MACKENZIE moved to add to the motion “and that the report of the Clerk of the Printing Committee” be inserted in the votes and proceedings of the House tomorrow.
Mr. STEPHENSON said he had heard that the meetings of this Committee had been very few indeed. The Session was now considerably advanced, and they could not expect to remain in session much longer. Some very important papers had been referred to the Committee, and he trusted means would be taken to have the printed immediately, as the questions involved were of very considerable importance.
Mr. STEPHENSON said by the book of the Clerk in the office of Records, the member for Lambton had the document in reference to the Allan contract from the 14th or 15th March till the 5th of April.
Hon. Mr. MACKENZIE said he had not had that document at all. It was printed before the House met, though not distributed.
Mr. STEPHENSON said it was only printed for the use of the members of the Government.
The motion, as amended, was carried.
* * *
BEET ROOT SUGAR
Mr. JOLY moved for a Committee of the Whole on a resolution on the subject of the manufacture of beet-root sugar in Canada:— “That in order to encourage the introduction of the manufacture of beet-root sugar in Canada, it is advisable to adopt such legislation as would secure it against the imposition of excise duties for the next ten years.” He said that in moving this, he did so not with the desire of advancing free trade ideas, but with the view of enabling this industry to grow and prosper.
He said the object was that the House would undertake for ten years not to stamp out an industry which would be of very great advantage to the country. No protection was asked, but only that those engaged in the industry should have no obstructions placed in their way. It had been shown that the beet root could be raised in Canada as well as in any country, and referring to the numbers of young men leaving Canada for the States, he said he believed the
COMMONS DEBATES
April 16, 1873
273
cultivation of the beet root, and the manufacture of sugar therefrom, would be a great means of preventing those young men from leaving our country, as it would provide profitable occupation for great numbers.
He then referred to the state of the industrial classes in England, which he said must make us hesitate before trying to make Canada a manufacturing country, but that the manufacture of beet root sugar would not be attended with the evils known in England, as the manufacture could be carried on by each individual farmer in his own home in the country district, and so young people would have full employment at home. From his own knowledge of the manufacture in France, he could state that beet root sugar could be made fully equal to the finest cane sugar. Fully, 1,000 acres of land would have to be carefully cultivated in order to produce a sufficient amount of the root to yield a million pounds of sugar, and the work being so hard, ought not, therefore, to have any restrictions upon it.
The only objection possible was that the amount of foreign sugar imported might be diminished and consequently there might be a decreased revenue, but he did not think this would take place for many years to come. As the difficulties in the way of the industry were so great that no one would engage in it unless they knew that for ten years to come the Government would undertake to impose no burden upon the work, but if they had this assurance, very large amounts would be invested in the industry.
Hon. Mr. TILLEY said the question was rather an important one, as if the industry should become so important, as was mentioned, it might affect the revenue to the extent of one or two million dollars, and he asked that the motion might be allowed to stand until the Government had had an opportunity of considering it.
Mr. JOLY had no objection to the request, but urged that the proposition could not possibly affect the revenue, as seemed to be feared.
Hon. Mr. YOUNG (Montreal West) was in favour of what was called in England a “free breakfast table.” No excise duty was levied on maple sugar.
Hon. Mr. TILLEY: And they do not ask for protection for ten years.
Hon. Mr. YOUNG (Montreal West) while admitting this, said he did not think the manufacture of beet root sugar should be treated differently from that of maple sugar, and he did not think any legislation was necessary at all. He thought, however, that manufactures ought to be encouraged as much as possible in Canada, and hoped that very soon we should have reciprocity with the States in manufactured goods as well as agricultural produce.
Mr. BODWELL was in favour of the motion. The soil of Upper Canada was admirably adapted to the cultivation of beet root, and the industry had been entered into in his county, but had fallen
though in consequence of the supposed policy of the government, as it was thought that no protection of such enterprises could be obtained. If the necessary protection were granted, the industry would benefit the country immensely.
Hon. Mr. DORION (Napierville) spoke of the large amount which would have to be invested to make the enterprise a success, and said all that was asked was that for ten years no excise duty should be imposed. He thought anything that would tend to cheapen the necessaries of life should be encouraged. Beet root raised in Canada had been found to yield more sugar than that raised in England, but so many changes had occurred in the tariff that people were not willing to engage in the enterprise without some guarantee. He thought a committee should enquire into the matter.
Mr. BEAUBIEN as seconder of the motion, said that it was not asked so that the revenue should be decreased, but on the contrary, it was hoped that a great source of revenue would be established. When the manufacture of beet sugar began to interfere with the revenue, duty might be imposed upon it. A very large revenue was derived in France from the manufacture, but in that country the industry had at first been fostered and protected. The same retails would occur in Canada. The same thing was being done every day by municipal corporations to encourage other branches of industry.
The question should not only be looked at from an agricultural point of view, but as an inducement for emigration, because to make the enterprise successful it would have to be entered into on a very large scale. He produced a sample of the sugar manufactured near Toronto, and said he was sure that no member who would taste it would oppose the motion.
Hon. Mr. ROSS (Champlain) said the province of Quebec was generally in favour of the establishment of this manufacture, which would prove, ultimately, of the greatest importance to the whole Dominion.
On the suggestion of Hon. Sir John A. Macdonald, Mr. RICHARD (Megantic) moved the adjournment of the debate. —Carried.
Hon. Sir JOHN A. MACDONALD suggested that, as many members of the House had been honoured by the commands of His Excellency to be elsewhere at night, there should be no evening sitting, and if this were agreed to he would also suggest that the hour from five to six should be given to private bills, instead of that from half-past seven to half-past eight.
Hon. Mr. MACKENZIE stated that he had no objection, and the suggestion was then agreed to.
* * *
THE GRAND TRUNK ARRANGEMENTS ACT
Hon. Mr. CAMERON (Cardwell) moved the second reading of the bill to extend the provisions of the Grand Trunk Arrangements
COMMONS DEBATES
274
April 16, 1873
Act of 1862, so far as relates to certain preferential bonds for a further period, and for other purposes as amended by the Standing Committee on Railways.
Hon. Mr. CAUCHON raised a question of order. This bill ought to have been introduced in committee in accordance with the following rule:—“This House will not proceed upon any portion, proceeding or bill for granting any money or for releasing or compounding any sum of money owing to the Crown, except in committee of the whole House.” This rule was passed in 1707, and continued to the present time, as laid down by May.
Hon. Mr. CAMERON (Cardwell) said the original bill did not originate in committee, and this bill did not require to originate in committee. There was no grant of money by the Crown and no compromise with the Crown, or additional change in any form or shape which would render it necessary to introduce the bill in Committee of the Whole.
Hon. Mr. CAUCHON said this was a case of compounding. There were three million sterling owing to the country, besides interest. These three million had been placed behind certain other securities, and now it was proposed to put them behind another two million sterling. If that was not compounding he did not know the meaning of the word.
Hon. Mr. MACKENZIE said the question was whether this Act would put the Crown in a worse position to reuse upon its lien than it stood informedly.
Hon. Sir JOHN A. MACDONALD said “releasing” meant the releasing of the whole debt, and “compounding” meant taking a part in settlement of the whole. This proposal did neither. It merely postponed the debt to the Crown which might be for the advantage of the debt. The Parliamentary rule was that every measure commenced by bill, unless some special reason was given by the rules and practice of the House. This bill could not be considered to release either the whole or a part of the three million due to the Crown, but was simply one to improve the road by an additional expenditure to two million.
The SPEAKER said “compounding” was strictly the taking less than the thing that was due. That was not so in this case, and he thought the hon. member might proceed with his bill.
Hon. Mr. MACKENZIE asked that a petition presented yesterday from a Mr. Baker, of England, be read.
Hon. Mr. DORION (Napierville) asked that a petition he had just presented from Mr. Higgins, of England, might be received.
Hon. Mr. CAMERON (Cardwell) had no objection, and the petitions were then read.
Hon. Mr. CAUCHON said of course he was bound to abide by the decision of Mr. Speaker, but he was not convinced. (Cries of "Order. ") He would have no objection to the proposed arrangement
if he were convinced that the promises made by the Company would be fulfilled. So long as the present state of things continued, the line would never prosper. The people in England were deceived every year, but they seemed to like it. (Laughter.) The Company tried to crush every other enterprise in the land, and many strange things had been occurring lately; for, within the few last days, former enemies seemed to have become perfectly good friends.
Last year Mr. Potter was carried all over the road like a fatted calf, though the line itself was so rough as to make travelling on it like riding in an Irish jaunting car. If the road were properly managed it would pay properly. He maintained that the trains were most irregular, and said when he himself last came up, when there was no obstructions and when the line was perfectly clear, the very fences had to be burnt for fuel. He then referred to the obstructions placed in the way of rival enterprises by the Grand Trunk Railway, and especially the North Shore Railway, reading from a report of the latter Company on this subject, and urging that such opposition should not be allowed.
He then read a report of some remarks made by the President of the Grand Trunk Railway in England deprecating rival lines, and said, if Mr. Potter had seen the country between Montreal and Quebec on the North Shore, he would have found it more settled and more thickly populated than was the South Shore when the Grand Trunk Railway was constructed, while the difficulty of passing between Point Levis and Quebec would be altogether unavoided. He then read further extracts from Mr. Potter’s speeches in England respecting the North Shore Railway, and maintained that many of the statements were utterly untrue, and that the lands given for the construction were valuable. He continued speaking in this strain till six o’clock when the House adjourned.
* * *
NOTICES OF MOTION
Mr. De COSMOS—On Friday next—Enquiry of the Ministry what provision does the Government intend to make for masters and mates of vessels within British Columbia, and for granting certificates of competency to the same.
Mr. De COSMOS—On Friday next—Address to His Excellency the Governor General, praying that a copy of the report of the special agent of the Inland Revenue Department respecting British Columbia be laid before the House.
Mr. De COSMOS—On Friday next—Enquiry of the Ministry whether a Bill will be introduced to repeal 30 Vic., Cap. 86, consolidated statutes of British Columbia.
Mr. De COSMOS—On Monday next—Address to His Excellency the Governor General praying that steps may be taken to provide for the payment of the Judge of the Court of ViceAdmiralty of British Columbia by salary instead of by fees as at present.
COMMONS DEBATES
April 16, 1873
275
Mr. De COSMOS—On Friday next—Address to His Excellency the Governor General for a copy of the report of the Superintendent of Indian Affairs for British Columbia for 1872-3, with any subsequent correspondence concerning the Indian Affairs of the said Province.
Mr. RICHARD (Megantic)—On Friday next—Enquiry of Ministry whether, in view of the important fact that our imports of iron last year formed more than one-ninth of the total imports. Is it the intention of the Government, by any and what measures, to facilitate the development of our important iron mines?
Hon. Mr. TILLEY—On Friday next—Committee of the Whole on resolution that it is expedient to amend the Acts respecting Insurance Companies, 31 Vic., Cap. 48, and 34 Vic., Cap. 9, by providing for the appointment of an Insurance Inspector, whose duty it shall be to examine and report upon the business in Canada, and for the payment of certain annual contributions by such companies towards defraying the expenses of such inspector.
Mr. BROUSE—Enquiry of Ministry whether a full survey of those lands belonging to the Dominion and known as the Thousand Islands has been ordered by the Government; if so, what progress has been made; when surveyed, will they be offered for sale; and under what conditions will such sale take place.
Hon. Mr. CAMERON (Cardwell)—On Friday next—Bill to amend the law relating to Bills of Exchange and Promissory Notes.
Mr. FISET—On Monday next—Inquiry of Ministry whether it is the intention of the Government to appoint one or several superintendents on the Intercolonial Railway; and if so, whether it is proposed to make at an early date such an appointment for that part of the line lying between Rimouski and Riviere du Loup.
Mr. FISET—On Monday next—Enquiry of the Ministry whether it is the intention of the Government to take possession of that part of the Intercolonial Railway between Rimouski and Riviere du Loup immediately on its completion, or, if not, whether the Government will effect any arrangement with the Grand Trunk pending the completion of the Intercolonial.
Also—On Monday next—Enquiry of Ministry whether it is the intention of the Government to establish a daily mail between Metis and Matane, in accordance with the prayer of the petition by the merchants and other interested parties of the parishes of Sandy Bay, Riviere Blanche, and Matane.
Mr. TASCHEREAU—On Monday next—Address to His Excellency the Governor General, praying for a statement in detail, with copies of receipts and vouchers of the sums paid by the Dominion Government to James Oliva, Esq., of the village of Montmagny, for his services and expenditures as Census Commissioner for 1871, and those of his enumerators for District No. 163, Montmagny.
COMMONS DEBATES
April 17, 1873
277
HOUSE OF COMMONS
Thursday, April 17, 1873
