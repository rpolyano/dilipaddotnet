 at Three o’clock, the Speaker in the Chair.
Mr. SPEAKER read a communication from the Governor General’s Secretary announcing that His Royal Highness would proceed to the Senate Chamber at 3 p.m. on Tuesday, the 18th instant, for the purpose of formally opening the session of the Dominion Parliament.
A message was delivered by Major Ernest J. Chambers, Gentleman Usher of the Black Rod, as follows:
Mr. Speaker, His Royal Highness the Governor General desires the immediate attendance ot this honourable House in the Chamber of the honourable the Senate.
Accordingly the House went up to the Senate Chamber.
And the House being returned to the Commons Chamber:
VACANCY. '
Mr. SPEAKER: I have the honour to in form the House that during the recess I received notification of a vacancy having occurred in the representation of the electoral district of Westmorland, in the province of New Brunswick, consequent upon the demise of the Hon. Henry Robert Emmerson. I accordingly have issued my warrant to the Clerk of the Crown in Chancery ito make out a new writ of election for the said electoral district.
1
OATHS OF OFFICE BILL.
On motion of Sir Robert Borden, Bill No. 1, respecting the Administration of Oaths of Office, was introduced and read the first time.
THE GOVERNOR GENERAL’S SPEECH.
Mr. SPEAKER: I have the honour to inform the House that when the House did attend His Royal Highness the Governor General this day in the Senate Chamber, His Royal Highness was pleased to deliver a Speech to both Houses of Parliament. To prevent mistakes I have obtained a copy, which is as follows:
Honourable Gentlemen of the Senate:
Gentlemen of the House of Commons:
Very grave events vitally affecting the interests of all His Majesty's dominions have transpired since prorogation. The unfortunate outbreak of war made it immediately imperative for my ministers to take extraordinary measures for the defence of Canada and for the maintenance of the honour and integrity of our Empire.
With respect to such of these measures as may require the sanction and approval of Parliament, the necessary legislative proposals will be submitted for your consideration. Other bills authorizing additional measures which are essential for the public safety will also be presented to you without .delay. ’
Gentlemen of the House of Commons:
Estimates will be laid before you to provide for expenditure which has been or may be caused by the outbreak of hostilities.
Honourable Gentlemen of the Senate:
Gentlemen of the House of Commons:
The critical period into which we have just entered has aroused to the full the patriotism and loyalty which have always actuated the Canadian people. From every province and indeed from every community the response to the call of duty has been all that could be desired. The spirit which thus animates Canada inspires also His Majesty’s dominions throughout the world; and we may be assured that united action to repel the common danger will not fail to strengthen the ties that bind together those vast dominions in the possession and enjoyment of the blessings of British liberty.
As representative of His Majesty the King, I must add my expression of thanks and admiration for the splendid spirit of patriotism
RKVISED EDITION.
2
COMMONS
and generosity that has been displayed throughout the length and breadth of the Dominion.
On motion of Sir Robert Borden, it was ordered that the Speech of His Royal Highness the Governor General to both Houses of Parliament be taken into consideration on Wednesday next.
BUSINESS OF THE HOUSE.
On motion of Sir Robert Borden, it was resolved, that Government notices of motion and Government orders have precedence over all other business, except questions and notices of motions for the production of papers, on Mondays, Wednesdays and Thursdays; that on Wednesdays the rule adjourning the House at six o’clock, p.m., be suspended, and that the order of business and the hour of meeting be the same as on Tuesdays.
THE EUROPEAN WAR.
PAPERS PRESENTED.
Copies of Orders in Council from August 2 to August 15, 1914, bearing on the outbreak of hostilities in Europe; copy of correspondence by cable between the Governor General and the Secretary of State for the Colonies, from August 1 to August 15, 1914; copy of correspondence between the Prime Minister and the Hon. George H. Perley, from August 4 to August 13, 1914.—Sir Robert Borden.
Copy of Order in Council, No. P.C. 1313, dated the 18th of 'May, 1914, relating to the organization of a Naval Volunteer Force.— Hon. J. D. Hazen.
On motion of Sir Robert Borden, the House adjourned at 3.50 p.m.
Wednesday, August 19, 1914.
