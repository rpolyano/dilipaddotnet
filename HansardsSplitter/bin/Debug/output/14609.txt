 at Three o’clock.
Prayers.
FIRST READINGS.
Bill (No. 2) to permit reciprocity in wrecking and the towing of vessels and rafts.—(Mr. Trow for Mr. Charlton.)
Bill (No. 3) to admit vessels registered in the United States to wrecking, towing and coasting privileges in Canadian waters.—(Mr. Ferguson, Welland, for Mr. Patterson, Essex.)
Bill (No. 4) to permit foreign vessels to aid vessels wrecked or disabled in Canadian waters.— (Mr. Kirkpatrick.)
Bill (No. 5) to make further provision as to the prevention of cruelty to animals, and to amend chapter 172 of the Revised Statutes of Canada.— (Mr. White, Cardwell, for Mr. Brown.)
BILLS OF EXCHANGE AND PROMISSORY NOTES.	'
Sir JOHN THOMPSON moved for leave to introduce Bill (No. 6) relating to Bills of Exchange and Promissory Notes. He said : This is the Bill which received some consideration at the hands of the House last Session, and I introduce it now in accordance with the understanding then arrived at.
Motion agreed to, and Bill read the first time.
ELECTION ACT AMENDMENT.
Mr. JONCAS moved for leave to introduce Bill (No. 7) further to amend the Dominion Elections Act, chapter 8 of the Revised Statutes of Canada.
Mr. MILLS (Bothwell). Explain.
Mr. JONCAS. It is the same Bill I presented last year, and is purely of local interest. It asks to put Gasp^ on the same footing as Algoma in the Province of Ontario, and Cariboo in the Province of British Columbia ; since the delays now provided for are not sufficient to allow the returning officer to post up his proclamations in time.
Motion agreed to, and Bill read the first time.
27
[COMMONS]
28
MAPLE HILL POST OFFICE.
Mr. LANDERKIN asked, Whether the Maple Hill Post Office, in the County of Bruce, has been closed ? If so, why ? Is it the intention of the Government to re-open it ?
Mr. HAGGART. The post office of Maple Hill has been closed. It was closed on account of the resignation of the postmaster. It is the intention of the Government to re-open that office.
TRIAL OF ROBERT VOLLET.
Mr. LANDERKIN asked, Do the Government propose defraying the expenses of the trial of Robert Yollet, of Durham, who was tried at the assizes in Walkerton last autumn ?
Sir JOHN THOMPSON. We have had no application on that subject.
ELECTORAL DIVISION OF SHAWINEGAN.
Mr. DESAULNIERS asked, Whether the Government have been informed of the death of the Hon. James Ferrier, Senator, appointed for the electoral division of Shawinegan, in the Province of Quebec ? If they have been so informed, why has this vacancy in the Senate not been filled, when the Counties of St. Maurice and Maskinonge have made known to the proper persons, through their representatives, the views of the electorate ?
Sir JOHN A. MACDONALD. The Government has been informed of the death of the Hon. James Ferrier ; the appointment of his successor is still under the consideration of the Government.
PAYMENT ^0 PRINCE EDWARD ISLAND.
Mr. PERRY asked, Has the Government of Prince Edward Island drawn any money from capital at Ottawa since the 14th January, 1889? If so, what amount, and when drawn ?
Mr. FOSTER. The Government of Prince Edward Island has not drawn any money from capital at Ottawa since the 14th January, 1889.
I
THREE PER CENT. LOAN OF 1888.
Mr. WHITE (Cardwell) asked, Whether any portion of the 3 per cent, loan of 1888 has been' purchased for the sinking fund since June 30, 1889 ?
‘ If so, what amount, at what date and at what price ?
Mr. FOSTER. The following amounts have been purchased out of the 3 per cent, loan of 1888 on sinking fund account:
Date.	Amt. purchased.	Rate.	Accrued Int.	Net rate.
July...		456,456.8ft	95}	nil	£95 10 0
Oct...		 92,591.34	j 94f i m	3 mos Jp.c. 3 “ Jp.c.	94 0 0 94 10 0
Nov...			 247,589.50	951 ) 96	~ 4 “ 1 p.c. 4 “ 1 p.c.	94	17 6 95	0 0
Dec...		 97,315.44	? 964 i m	5 “ l)p.c. 5 “ lip.c.	95 5 0 95 2 6
	$493,953.14	Average cost.		95 16 6
			Net cost.	94 18 4
	CONTRACTORS’		CHEQUES.	
Mr.	McMULLEN	asked,	, Whether it	is cns-
tomarv to transfer amounts put up by contractors from Chartered Banks to the Government Savings Banks ? Has such been done in any case during the last year ?
Mr. Joncas.
Mr. FOSTER. It is not customary to transfer amounts put up by contractors from Chartered Banks to the Government Savings Banks, and I know of no case in which such has been done.
DISALLOWANCE OF PROVINCIAL BILLS.
Mr. LANDERKIN moved for :
Statement showing the total number of Bills disallowed since Confederation, also the total number disallowed each year during the same period; giving the titles of the Bills, the Province where passed, and the reason for said disallowance.
Mr. McCARTHY. I would suggest that my Lon. friend make a little change in the motion he has just made, and insert the following :—
And the reasons for such disallowance so far as to show whether each Act was not within the competence of the Provincial Legislature, or upon grounds of public policy; also, in like manner, the Bills reserved for the pleasure of His Excellency the Governor General to which his assent was nofgiven, showing the reasons for such refusal.
The hon. gentleman’s motion asks the reason for such disallowance. Well, that means, of course, the printing of a very long correspondence in each case, and down to a certain date we are already in possession of that. I suppose what the hon. gentleman desires to have is a short synopsis of the Bills disallowed, and shortly the reasons for this disallowance. If the hon. gentleman will accept this suggestion I will not move it as an amendment.
Sir JOHN THOMPSON. I may be allowed to explain that last Session there was laid on the Table of the House a second blue book on this question which showed the title of every Act reported upon, giving the full text of the report in every case ; and that brought the subject down to the end of the calendar year of 1888, I think. If this motion carries it will be simply necessary to bring down the supplementary papers to those which have been laid on the Table so far. Besides that, in the blue hook which contains that report as brought down last Session, the hon. gentleman will find a carefully prepared table showing the year in which every Act was passed in each Province, the chapter of the Act, the title of the Act, the observations that were made upon it, and the date of the Order in Council; so that not only is the return asked for by the hon. gentleman already down to a certain date, and a very recent date, but it is also in the form of a synopsis which is very convenient for reference, and which the hon. gentleman will find, I think, to answer all his purposes.
Mr. LAURIER. I think my hon. friend might readily accept the suggestions of the Minister of Justice; that would provide all the information which, I presume, he has in view by making this motion.
Mr. LANDERKIN. I only wish to have an extension of the return that has been already brought down. The suggestion of the hon. member for North Simcoe (Mr. McCarthy) is worthy of consideration, and I have no objection to adopt the suggestion he has made.
Mr. MCCARTHY. The information is all included in the returns already made, but I thought the hon. gentleman simply desired to obtain it in some short form.
Mr. LANDERKIN. That is the idea.
30
29
[JANUARY 21. 1890.]
Mr. McCarthy. If it could be shortly stated, without giving any reasons, whether the action taken was on grounds of public policy, or on the ground that it was ultra vires, it would be desirable ; but perhaps it would not always be easy to make the distinction.
Mr. LANDERKIN. I suppose if the return will be brought down in English and French, the hon. gentleman will not object to that.
Sir JOHN A. MACDONALD. If the hon. gentleman will allow it, the information will cover matters since the date of the last return.
Mr. LANDERKIN. Certainly. I hope it will not be disallowed because it is going to be in French too.
Motion agreed to.
RETURNS ORDERED.
Return giving the names of all persons who were tried before a Magistrate for selling whiskey to Indians in the County of Grey or Bruce, in the year 1888-89; together with all papers, documents and letters on the subject; also the name of the party who laid the information, the name of the Magistrate before whom it was tried, the name of the constable employed and the name of the lawyer retained in each case, together with the decisions of the Magistrate, stating the fines imposed, if any; also, if any appeals were made from the decisions of the Magistrate, stating before what Judge the appeals were tried and what was the result; giving the cost of each trial before the Magistrate, and of each appeal before the Judge, together with the name, occupation and post office address of every person who received money for any service whatever, either at the trial at the Magistrate’s court or at the appeal before the Judge ; the total cost of all the trials, the total fines imposed and collected. If costs were refused at any trial, giving the reason for such refusal; also showing whether any of the Indians who received intoxicating liquors were electors of Bruce under the Electoral Franchise Act of Canada. —(Mr. Landerkin.)
Return showing the number of Dominion Franchise Voters Lists of 1889, printed outside the Government Printing Bureau, the names of the offices in which they were printed, and the amount paid for the printing of each respectively.—(Mr. Innes.)
Copy of Government Engineer’s report of survey of harbors of Pinette and Wood’s Island, and also copyof report of survey of New London Harbor and Breakwater, in the Province of Prince Edward Island.—(Mr. Welsh.)
Return showing the amounts of money deposited in the several Savings Banks in the Dominion, and in the several Post Office Savings Banks, the location of each, and the gross amount of deposits in each on the 30th of June and December last.—(Mr. McMullen.)
SELECT STANDING COMMITTEES.
Sir JOHN A. MACDONALD moved :
That a special committee of seven members be appointed to prepare and report with all convenient speed, lists of members to compose the select standing committees ordered by the House on Thursday, the 16th instant, and that Sir Hector Langevin, Sir Richard Cartwright, Sir John Thompson, Messrs. Bowell, Laurier, Mills (Bothwell) and the mover do compose the said committee.
Motion agreed to.
CONTRACTORS’ CHEQUES.
Mr. FOSTER. I would like to call the attention of my hon. friend, who asked with reference to the contractors’ cheques, to information which I have had sent to me since I gave the answer to his question; and I will refer him for a complete answer to C-12 Auditor General’s report of 1887-88, in which the report of the Deputy Finance Minister and the Order in Council are given in full, and
which may in some little degree modify what I stated in answer to his question.
BEHRING SEA FISHERIES.
Mr. LAURIER. I would like to ask the right hon. gentleman whether it is his intention to lay on the Table the papers connected with the Behring Sea question ?
Sir JOHN A. MACDONALD. It is not the present intention to lay them on the Table. We may be able to do so during the course of the Session.
Mr. LAURIER. That is very vague.
Sir RICHARD CARTWRIGHT. When papers of that kind are referred to in the Speech from the Throne, the practice always has been to lay them on the Table as a matter of course. I think I have heard the hon. gentleman insist on our following this practice, on a former occasion, when we were on that side, and unless my memory is entirely at fault, that is the practice always followed in England.
Sir JOHN A. MACDONALD. The subject is of importance and of course properly should be alluded to in the Speech from the Throne. My hon. friend stated that the language was very vague. The language is vague and purposely vague.
Mr. MILLS (Bothwell). No doubt.
Sir JOHN A. MACDONALD. Discussions are going on at Washington, and I have every reason to believe, as His Excellency put it, that a satisfactory solution of that question will be reached. In the meanwhile it is not in the public interest that the papers should now be laid on the Table.
Mr. MILLS (Bothwell). Not in the Government’s interest.
Sir RICHARD CARTWRIGHT. We have met unusually early, and I presume the Government business is in a state of forwardness. I would ask the Minister of Finance whether he expects to be able shortly to bring down the Estimates ?
Mr. FOSTER. I expect to have the Estimates probably about the middle of this week.
Sir JOHN A. MACDONALD moved the adjournment of the House.
Motion agreed to ; and House adjourned at 3.40 p.m.
HOUSE OE COMMONS.
Tuesday, 21st January, 1890.
