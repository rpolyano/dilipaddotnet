 at eleven o’clock.
HIS MAJESTY THE KING
EXPRESSION OF GRATIFICATION AT PROGRESS TOWARD RECOVERY
Right Hon. L. S. St. Laurent (Prime Minister): Mr. Speaker, before you ordered a moment ago that the doors be opened you led us all in our usual prayers, including that for the health and well-being of His Majesty the King, which I am sure we all followed with special fervour this morning.
We have each one of us been so relieved at the constant good news of the king’s progress towards a speedy recovery since the grave surgical operation which he underwent two weeks ago—good news that was confirmed to me by Her Royal Highness The Princess Elizabeth when I had the honour of welcoming her and the Duke of Edinburgh at Dorval yesterday—that I am sure we would all wish you, sir, to convey to His Majesty on our behalf, with the expression of our respectful duty and of our loyal devotion, our gratification at his constant progress towards recovery and our sincere good wishes.
After conferring with the leader of the opposition (Mr. Drew) and the leaders of the other groups as to what might be the best way of giving public expression to our sentiments in that regard at this time, in a manner in which each one of us would feel he had some personal part, I venture to propose, with your permission, that we do now rise and sing together “God! Save the King”.
Whereupon the members of the house rose and sang
God Save the King
PROROGATION OF PARLIAMENT
Mr. Speaker: I have the honour to inform the house that I have received the following communication:
Ottawa, September 22, 1951
Sir:
I have the honour to inform you that the Right Honourable Thibaudeau Rinfret, Chief Justice of Canada, acting as Deputy of His Excellency the Governor General, will proceed to the Senate chamber at 11.30 a.m. on Tuesday, the 9th October, for the purpose of proroguing the fourth session of the twenty-first parliament.
I have the honour to be, sir,
Your obedient servant,
J. F. Delaute,
Assistant Secretary to the Governor General.
NEW MEMBERS
Mr. Speaker: I have the honour to inform the house that during the adjournment the Clerk of the House has received from the chief electoral officer certificates of the election and return of the following members, viz.:
Of Gordon Churchill, Esquire, for the electoral district of Winnipeg South Centre.
Of J. Angus MacLean, Esquire, for the electoral district of Queens.
Of Howard Meeker, Esquire, for the electoral district of Waterloo South.
Of Walter Dinsdale, Esquire, for the electoral district of Brandon.
NEW MEMBERS INTRODUCED
Gordon Churchill, Esquire, member for the electoral district of Winnipeg South Centre, introduced by Mr. George A. Drew and Mr. J. G. Diefenbaker.
J. Angus MacLean, Esquire, member for the electoral district of Queens, introduced by Mr. George A. Drew and Mr. W. Chester S. McLure.
Howard Meeker, Esquire, member for the electoral district of Waterloo South, introduced by Mr. George A. Drew and Mr. L. E. Cardiff.
Walter Dinsdale, Esquire, member for the electoral district of Brandon, introduced by Mr. George A. Drew and Mr. J. A. Ross.
VACANCY
Mr. Speaker: I have the honour to inform the house that during the adjournment I received the resignation of Arthur LeRoy Smith, Esquire, as member for the electoral district of Calgary West.
Accordingly I have issued my warrant to the chief electoral officer to make out a new writ of election for the said electoral district.
COUNCIL OF EUROPE
INVITATION TO SEND OBSERVERS TO OCTOBER SESSION
Mr. Speaker: I have the honour to inform the house that I have received a communication from Mr. P. H. Spaak, president of the consultative assembly of the Council of Europe, in which he invites the Canadian parliament to send observers to the second part of the current session of the assembly
5046
HOUSE OF COMMONS
Council of Europe
which will open at Strasbourg on October 15. I have written to Mr. Spaak thanking him for this invitation and advising him that I would bring it to the attention of parliament. I also informed him that as parliament would not open until October 9 it might not be possible to make the necessary arrangements to have observers appointed. I did, however, assure him that the members of the Canadian parliament would be deeply interested in what takes place at Strasbourg.
ORDERS IN COUNCIL
TABLING OF SUMMARY, JUNE 1 TO SEPTEMBER 30, 1951
Right Hon. L. S. St. Laurent (Prime Minister): I should like to lay on the table the summary of orders in council passed during the period June 1 to September 30, 1951.
EMERGENCY POWERS ACT
TABLING OF FOUR ORDERS IN COUNCIL
Right Hon. L. S. St. Laurent (Prime Minister): I should like to table four orders under the Emergency Powers Act which have not yet been published. They are Nos. 3417, 3484, 4558, and 5122. No. 3417 of July 4, 1951, relates to the disposition of offences committed prior to the coming into force of the code of service discipline; P.C. No. 3484 of August 8, 1951, concerns the operation of certain radio stations in Canada by the United States government, notwithstanding the provisions under the Radio Act; P. C. No. 4558 of August 29, 1951, appointed the transport controller; No. 5122 of September 26, 1951, concerns weighing over of grain at elevators. I am sure that these have already been brought to the public notice but as yet they have not been published in Orders and Regulations. I thought it would be convenient for members to have them available on the table of the house.
PROROGATION OF PARLIAMENT
A message was delivered by Major C. R. Lamoureux, Gentleman Usher of the Black Rod, as follows:
Mr. Speaker, the Right Honourable the Deputy Governor General desires the immediate attendance of this honourable house in the chamber of the honourable the Senate.
Accordingly, Mr. Speaker with the house went up to the Senate chamber.
The Deputy Governor General was pleased to close the fourth session of the twenty-first parliament of Canada with the following speech:
Honourable Members of the Senate:
Members of the House of Commons:
The people of Canada, in common with His Majesty’s subjects elsewhere, were deeply concerned that the state of the King’s health made
[Mr. Speaker.]
a surgical operation necessary, and they rejoice at the rapid progress of the King’s recovery.
My ministers are gratified that the world situation did not require the resumption of the session before the date originally set when you concluded your deliberations in June. The international situation continues nevertheless to give constant concern to the government.
In Korea, despite prolonged discussions, it has not yet been possible to arrange a cease-fire and hostilities are continuing. To this United Nations’ action to defeat aggression, our Canadian forces are making an effective contribution.
Since you adjourned, the state of war with Germany has been terminated by proclamation, and normal diplomatic relations have been established with the federal republic of Germany. A treaty of peace has been signed with Japan which you will be asked to approve at a forthcoming session.
While hostilities have been confined to restricted areas, the government is convinced that general peace can be assured only by the continued buildup of the combined strength of the free nations.
To this end, my ministers welcomed the holding in Ottawa of the most recent meeting of the council of the North Atlantic Treaty Organization. Certain of the recommendations of the council will require to be considered at a future session.
Arrangements are now under discussion with India and Pakistan for certain projects to be financed from the contribution you approved to the Colombo plan for co-operative economic development in south and southeast Asia.
You also made provision for a Canadian contribution to the United Nations relief and rehabilitation program in Korea.
At the outset of the session, you enacted a measure to vest in the governor in council additional powers which might be necessary to ensure adequate defence preparations to meet the emergency arising out of the present international situation.
The legislation to establish the Department of Defence Production has been implemented and the new department is in full operation.
Agreements are being worked out with the provincial governments to implement the measure you enacted to provide for federal contributions for the payment of old age assistance to those in need between 63 and 70 years of age. Registration is being proceeded with for universal contributory pensions to persons over 70 years of age to facilitate administration when the necessary legislation has been enacted.
You also enacted a separate measure respecting allowances for blind persons; as well as several measures extending the benefits of the veterans charter, and providing for an increase in pensions to certain groups of veterans and their dependents.
You authorized the provision of grants to universities and equivalent institutions of higher learning.
The requirement of the fiat in the case of petitions of right has been abolished.
The Indian Act was completely revised and the new legislation is now in operation. The Post Office Act was also extensively revised.
Other measures were enacted respecting grants to municipalities; the regulation of consumer credit; the grading of dairy products; the construction of a railway from Sherridon to Lynn Lake in the province of Manitoba; and the bequests of Laurier house and Kingsmere.
Among other measures, you amended the Canadian Citizenship Act; the Supreme Court Act; the Prairie Farm Rehabilitation Act; the Federal District Commission Act; the Dominion Elections Act; the Judges Act; the Northwest Territories Act;
OCTOBER 9, 1951
5047
the Yukon Act; the Central Mortgage and Housing Corporation Act; the National Housing Act; the Foreign Exchange Control Act; the Canadian Wheat Board Act; the Emergency Gold Mining Assistance Act; and the Criminal Code.
Your approval was given to agreements between Canada and France, and Canada and Sweden respecting income tax and to a convention between Canada and France respecting succession duties.
Members of the House of Commons:
I thank you for the provision you have made for the public services and for defence obligations on a scale unprecedented in time of peace.
Prorogation of Parliament
Honourable Members of the Senate:
Members of the House of Commons:
Our people will, I am sure, continue to pray for the complete restoration of the health of the King and for the blessing of Divine Providence upon our nation and the forces of our own and other nations who are striving to maintain the rule of law in the relations between nations.
This concluded the fourth session of the twenty-first parliament.
END OF SESSION