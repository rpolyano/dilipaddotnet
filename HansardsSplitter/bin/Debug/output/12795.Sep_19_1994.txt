HOUSE OF COMMONS DEBATES
OFFICIAL REPORT
FIRST SESSION—THIRTY-FITH PARLIAMENT 43 Elizabeth II
VOLUME V, 1994
COMPRISING THE PERIOD FROM THE NINETEENTH DAY OF SEPTEMBER, 1994 TO THE NINETEENTH DAY OF OCTOBER, 1994
INDEX ISSUED IN A SEPARATE VOLUME
Published under authority of the Speaker of the House of Commons by the Queen’s Printer for Canada
Available from Canadian Communication Group — Publishing, Supply and Services Canada, Ottawa, Canada K1A 0S9.
5785
HOUSE OF COMMONS
Monday, September 19,1994
