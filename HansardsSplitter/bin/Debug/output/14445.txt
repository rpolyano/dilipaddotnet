Technical and Bibliographic Notes / Notes techniques et bibliographiques
The institute has attempted to obtain the best original copy available for filming. Features of this copy which may be bibliographically unique, which may alter any of the images in the reproduction, or which may significantly change the usual method of filming are checked below.
L’lnstitut a microfilme le meilleur exemplaire qu’il lui a ete possible de se procurer. Les details de cet exemplaire qui sont peut-etre uniques du point de vue bibliographique, qui peuvent modifier une image reproduite, ou qui peuvent exiger une modification dans la methode normale de filmage sont indiques ci-dessous.
□
□
□
□
□
□
□
□
□
0
□
0
Coloured covers /
Couverture de couleur
Covers damaged /
Couverture endommagee
Covers restored and/or laminated /
Couverture restauree et/ou pelliculee
Cover title missing /
Le titre de couverture manque
Coloured maps /
Cartes geographiques en couleur
Coloured ink (i.e. other than blue or black) /
Encre de couleur (i.e. autre que bleue ou noire)
Coloured plates and/or illustrations /
Planches et/ou illustrations en couleur
Bound with other material /
Relie avec d’autres documents
Only edition available /
Seule edition disponible
Tight binding may cause shadows or distortion along interior margin / La reliure serree peut causer de I’ombre ou de la distorsion le long de la marge interieure.
Blank leaves added during restorations may appear within the text. Whenever possible, these have been omitted from filming / II se peut que certaines pages blanches ajoutees lors d’une restauration apparaissent dans le texte, mais, lorsque cela eta it possible, ces pages n’ont pas ete filmees.
□
□
□
0
0
0
0
□
□
□
Additional comments / Commentaires supplemental:
Various pagings. Includes some text
Coloured pages / Pages de couleur
Pages damaged / Pages endommagees
Pages restored and/or laminated /
Pages restaurees et/ou pelliculees
Pages discoloured, stained or foxed/
Pages decolorees, tachetees ou piquees
Pages detached / pages detachees
Showthrough / Transarence
Quality of print varies /
Qualite inegale de I’impression
Includes supplementary materials Comprend du materiel supplemental
Pages wholly or partially obscured by errata slips, tissues, etc., have been refilmed to ensure the best possible image / Les pages totalement ou partiellement obscurcies par un feuillet d’errata, une pelure, etc., ont ete filmees a nouveau de fagon a obtenir la meilleure image possible.
Opposing pages with varying colouration or discolourations are filmed twice to ensure the best possible image / Les pages s’opposant ayant des colorations variables ou des decolorations sont filmees deux fois afin d’obtenir la meilleure image possible.
in French.
OFFICIAL REPORT
or THl
DEBATES
or THE
HOUSE OP COMMONS
or THE
DOMINION OF CANADA.
FOURTH SESSION —FIFTH PARLIAMENT.
49° VICTORIA, 1886.
VOL. XXI.
COMPRISING THE PEEIOD FROM THE TWENTY-FIFTH DAY OF FEBRUARY TO
THE NINETEENTH DAY OF APRIL, 1888.
OTTAWA:
PRINTED RF MACLEAN, ROGER A CO, WELLINGTON STREET.
1888.
MEMBERS OF THE GOVERNMENT
OF THE
RT. HON. SIR JOHN A. MACDONALD, G.C.B.
AT THE OPENING OF THE 4th SESSION OF THE FIFTH PABLIAUENT.
1886.
President of the Council (Premier) Minister of Finance	.	.	.
Minister of Justice.... Minister of Publio Works .	.
Minister of Railways and Canals . Minister of Agriculture	.	.
Minister of Customs	.	.	.
Minister of Interior	.	.	.
Minister of Militia and Defence . Minister of Marine and Fisheries Postmaster-General	.	.	.
Minister of Inland Revenue	.
Without Portfolio .	.	.	.
Secretary of State	.	.	.
. Right Hon. Sir John A. Macdonald, G.C.B., &o. . Hon. Archibald Woodbury MoLrlan.
• Hon. John Sparrow David Thompson.
. Sir Hector Louis Lanoevin, K.C.M.G., C.B.
. Hon. John Henry Pope.
. Hon. John Carling.
. Hon. Mackenzie Bo well.
. Hon. Thomas White.
, Sir J. P. R. Adolphe Caron, K.C.M.G.
. Hon. George Eulas Foster.
, Sir Alexander Campbell, K.C.M.G,
. Hon. John Costigan.
. Hon. Frank Smith.
. Hon. Joseph A. Chapleau.
Clerk of the Privy Council..........................John J. McGee, Esq.
OFFICERS OF THE HOUSE OF COMMONS.
Hon. George Airey Kirkpatrick .	.	.	*	. Speaker.
John G. Bourinot, Esq;......................... • Clerk of the House.
Donald W. Maodonell, Esq..........................Sergeant-at-Arms.
Francois Fobtunat Rouleau, Esq....................Clerk Assistant.
OFFICIAL REPORTERS.
George B. Bradley ,	.	.	.	. t	.	• Chief Reporter.
Stephen A. Abbott....................\
Joseph C. Duggan ........ 1
George Eyvel
Albert Horton,	Reporters.
J. O. Maroeau........................I
F. fi. Maroeau ...	...	.1
Thos. Jno. Richardson ...	... I
Jno. Chas. Boyce............... . Assistant to Chief Reporter.
ALPHABETICAL LIST
Of THE
CONSTITUENCIES AND MEMBERS
OF TEE
HOUSE OF COMMONS
FOURTH SESSION of th* FIFTH. PARLIAMENT op the DOMINION of CANADA.
1880.
Addington—John W. Boll.
Albeet—John Wallace.
Algoma—Simon J. Dawson.
Annapolis—William Hallett Ray.
Antigonish—Hon. John S. D. Thompson.
Argenteuil—Hon. J. J. C. Abbott.
Bagot—Flavien Dapont.
Beauce—Thomas Linidre Tasoherean.
Beauhaenois—Joseph GM6on Horace fiercej?on. Belleohasse—Guillaume Amyot. *
Beethibe—E. Ootavian Cutbbert.
Bona venture—L. J. Riopel.
Bothwell—Hon. David Mills.
Brant, N. Biding—James Somerville.
Brant, S. Biding—William Paterson.
Brockvii*le—John Fisher Wood.
Brome—Sydney Arthur Fisher.
Bruce, E. Biding—Bapert Mearse Wells.
Bruce, N. Biding—Alexander McNeill Bruce, West Riding—James Somerville.
Cardwell—Hon. Thomas White.
Carleton (N.B.)—David Irvine.
Carleton (O.)—Rt. Hon, Sir John A. Macdopald,Gt.CuB Cariboo—James Reid.
Ckavblt—Pierre Basile Benoit.
Champlain—Hippolyte Montplaigir.
Charlevoix—Simon Xavier Cimon.
Charlotte—Arthur Hill Gillmor.
Chateauguat—Edward Holton.
Chicoutimi and Saguenay—Jean Alfred Gagne. Colchester—Hon. Archibald Woodbury McLelan. Compton—Hon. John Henry Pope.
Cornwall and Stormont—Darby Bergin.
Cumberland—Charles James Townshend.
Digby—Hon. William B. Vail.
Dorchester—Charles Alexander Lesage.
Drummond and Arthabaska—Disir6 Olivier Bourbefu]. Dundas—Charles Eraetus Hiokey.
Durham, E. Riding—Henry Alfred Ward.
Durham, W. Biding—Hon. Edward Blake.
Elgin, E. Biding—John H. Wilson.
Elgin, W. Biding—George Elliott Casey,
Essex, N. Riding—James Colebrooke Patterson.
Essex, S. Biding—Lewis Wigle.
Frontenao—Hon. George Airey Kirkpatriok.
Gasp3—Pierre Fortin.
Glengarry—Donald Macmaster.
Gloucester—Kennedy F. Burns.
Grenville, 8. Riding—Walter Shanly.
Grey, E. Biding—Thomas S. Sproule.
Grey, N. Biding—Benjamin Allen.
Grey, S. Riding—George Landerkin.
Guysborough—John A. Kirk.
Haldimand -David Thompson. tt ., r». ▼ i Malaehy Bowes Daly.
HALIFAX— | John F Stairs>
Halton—William McCraney.
Tr.„rr	f Francis Edwin Kilveyt.
Hamilton- j Thomas Robertson.
Hants—W. Henry Allison.
Hastings, E. Riding—John White.
Hastings, N. Biding—Hon. Mackenzie Bowell.
Hastings, W. Riding—Alexander Robertson.
Hoohilaga— Alphonse Desjardins.
Huntingdon—Julius Scriver.
LIST OF MEMBERS OF THE HOUSE OF COMMONS,
Vi
Huron, E. Biding—Thomas Farrow.
Huron, S. Biding—Hon.Sir Richard J. Cartwright, K.CM.G, Huron, W. Riding—Malcolm Colin Cameron.
Iberville—Francois B^chard.
Inverness—Hugh Cameron.
JaoquesCartier—D6sir6 Girouard.
Joliette—Edouard Guilbeault.
Napierville —Mdddric Catudal.
New Westminster—Joshua Attwood B. Homer. Nioolet—Athanase Gandet.
Norfolk, N. Riding—John Charlton.
Norfolk, S. Biding—Joseph Jackson. Northumberland (N.B.)—Hon. Peter Mitchell. Northumberland (O.) E. Riding—Edward Cochrane. Northumberland (O.) W. Biding—George Guillet.
Kamourabka—Charles Bruno Blondeau.
Kent (N.B.)—Pierre Amand Landry.
Kent (0.)—Henry Smyth.
King’s (N.B.)—Hon. George E. Foster.
King's (N.S.)— Douglas B. Woodworth.
■tr » /DFTN f Peter Adolphus McIntyre. King s (P.E.I.) j ^agUStine Colin Macdonald.
Kingston—Alexander Gunn.
Ontario, N. Riding—Alexander Peter Cookburn.
Ontario, S. Riding—Francis Wayland Glen.
Ontario, W. Riding—James David Edgar.
rwn.w* /yv+tt'x ( Charles H. Mackintosh. Ottawa (City)- j JoB6ph Taeai.
Ottawa (County)—Alonzo Wright.
Oxford, N. Riding—James Sutherland.
Oxford, S. Riding—Archibald Harley.
Lambton, E. Riding—J. H. Fairbank.
Lambton, W. Riding—James Frederick Lister,
Lanark, N. Riding-Joseph Jamieson.
Lanark, S. Riding—John Graham Haggart.
Laprairie—Alfred Pinsonneault.
L’Assomption— Hilaire Hurteau.
Laval—Joseph Alddric Ouimet.
Leeds and Grenville, N. Riding—Charles Frederick Ferguson.
Leeds, S. Riding—George Taylor.
Lennox—Mathew William Pruyn.
LA vis— Pierre Maloolm Guay.
Lincoln and Liagara—John Charles Rykert.
Lisgab—Arthur Wellington Ross.
L'Islet—Philippe Baby Casgrain.
London—Hon. John Carling.
Lotbini^re—C6me Isaie Rinfret.
Lunenburg—Charles Edwin Kaulbaoh.
Marquette—Robert Watson.
MASKiNONGfi—Alexis Lesieur Desaulniers.
Megantio—Francois Langelier.
Middlesex, E. Riding—Duncan MacMillan.
Middlesex, N. Riding—Timothy Coughlin.
Middlesex, S. Riding—James Armstrong.
Middlesex, W. Riding—Donald M. Cameron.
Missisquoi—George Barnard Baker.
Monok—Lauohlan MoCallum.
Montcalm—Firmin Dugas.
Montmagnt—Ph. Landry.
Montmorency—Pierre Vincent Valin.
Montreal, Centre—John Joseph Curran.
Montreal, East—Charles Joseph Coursol.
Montreal, West—Matthew Hamilton Gault.
Muskoka—William Edward O’Brien.
Peel—James Fleming.
Perth, N. Riding—Samuel Rollin Hesson. Perth, S. Riding—James Trow. Peterborough, E. Riding—John Burnham. Peterborough, W. Riding—George Hilliard,
Pontiac—John Bryson.
Portneuf—Joseph E. A. De St. Georges, Prescott—Simon Labrosse.
PElKOt(P.E.L)-{^“dT“t-
Prince Edward—John Milton Platt. Provenoher—Joseph Royal.
Quebec, Centre—Joseph Guillaume Boss4, Quebec, East—Hon. Wilfred Laurier.
Quebec, West—Hon. Thos. McGreevy.
Quebec (County)—Hon. Sir Joseph P. R. A. Caron,K.C.M.G. Queen’s (N.B.)—George Gerald King.
Queen’s (N.S.)—James F. Forbes.
Quisn’s (p.fl.i.)~ |lTi8J?enTPaTie8;_•
'	( John Theophilus Jenkins.
Renfrew, N. Riding—Peter White.
Renfrew, S. Riding—Robert Campbell, Restigouche—George Moffat.
Richelieu—Louis Huet Massue.
Richmond (N.S.)—Henry N. Paint.
Richmond and Wolfe (Q.)—William Bullock Ives. Rimouski—Louis Adolphe Billy.
Rouville—George Auguste Gigault.
Russell—Moss Kent Dickinson.
St. Hyacinthe—Michel E. Bernier.
St John (N.B.) City—Frederick E. Barker.
ST. John (N.B.) City and County- { “ £
LIST OF MEMBERS OF THE HOUSE OF COMMONS.
VII
St John (Q.)—Francois Bour&ssa.
St. Maurice—Louis Ldon L. Desaulniers.
^t.tttrk—Hugh Sutherland.
Sheword—Michel Anger.
Shelburne—Thomas Robertson.
Sherbrooke—Robert Newton Hall.
Simooe, E. RidiDg—Herman Henry Cook.
Simcoe, N. Riding—Dalton McCarthy.
Simooe, S. Riding—Richard Tyrwhitt.
Soulanges—Jaraee William Bain.
Stanstead—Charles C. Colby.
Sunburt—Charles Bnrpee.
T^miscouata—Paul Etienne Grandbois.
Terrebonne—Hon. J. A. Chapleau.
Three Rivers—Hon. Sir Hector Louis Langevin, K.C.M.G. Toronto, Centre—Robert Hay.
Toronto, East—John Small.
Toronto, West—James Beaty, Jr.
Two Mountains—Jean Baptiste Daoust.
Vancouver Island—David William Gordon. Yaudreuil—Hugh McMillan.
YerchIires—Hon. Felix Geoffrion.
Vioioaii (B.C.)-1 Nofr,S,™kespeare'.
Victoria (N.B.)—Hon. John Costigan.
Victoria (N.S.)—Chas. Jas. Campbell.
Victoria (O.) N. Riding—Heotor Cameron. Victoria (O.) S. Riding—Joseph R. Dnndas.
Waterloo, N. Riding—Hugo Kranz.
Waterloo, S. Riding—James Livingston. Welland—John Ferguson.
Wellington, C. Riding—George Turner Orton. Wellington, N. Riding—James McMullen( Wellington, S. Riding—James Innes. Wentworth, N. Riding—Thomas Bain. Wentworth, S. Riding—Lewis Springer. Westmoreland—Josiah Wood.
Winnipeg—Thomas Soott.
Yale—Francis Jones Barnard.
Yamaska—Fabien Vanasse.
Yarmouth—Joseph RobbinB Kinney.
York (N.B.)—Thomas Temple.
York (O.) E. Riding—Hon. Alexander Mackenzie. York (O.) N. Riding—William Mulock.
York (O.) W. Riding—Nathaniel C. Wallaoe.
SELECT COMMITTEE APPOINTED TO SUPERVISE THE PUBLICATION OF THE OFFICIAL REPORTS
OF THE DEBATES OF THE HOUSE.
BfcHARD, Mr. Francois (Iberville).
Bergin, Mr. Darby (Cornwall and Stormont). Charlton, Mr. John (North Norfolk).
Colby, Mr. Charles C. (Stanstead). Desjardins, Mr. Alphonse (Hochelaga). Innes, Mr. James (South Wellington).
Royal, Mr. Joseph (Provencher),
Soriver, Mr. Julius (Huntingdon). Somerville, Mr. James (West Bruce). Taylor, Mr. George (Leeds).
Wood, Mr. Josiah ( Westmoreland). Woodworth, Mr. Douglas B. (King's, N.S.)
Chairman Mr. Alphonse Desjardins (Hochelaga),
LIST OF PAIRS DURING ’THE SESSION.
~Ym
LIST OF PAIRS DURING THE SESSION,
On amendment of Sir Hector Langevin to resolution of Mr. Landry (Monttasgny), censuring Governnwnt for executing Riel, 15th March, 1886 :—
For.	Against.
Mb. PLATT.	Mb. MqCARTHY.
On Mr. Blake’s resolution respecting Home RulCfcr Breland, 6 th May :
For.
Mb. EDGAR. Mb. LISTER.
Against. Mb. BERGIN. Mb. BOSSE.
On Mr. Cameron’s (Inverness) amendment to Mr. Kirk’s motion respecting subsidy to Nova Sootia,	1st April :
Mb. DODD.	Mb. FISHER.
Mb. KAULBAOK.	Mb. SHAKESPEARE.
Mb. KINNEY.	Mr. CUTHBERT.
On Mr. Laurier’s amendment (extinguishment of Indian Title of the Half-Breeds in the North-West Terri-
On Mr. Blake’s amendment to place Canada Temperance Act on Government Orders, 12th May:
Mb. KINNEY.	Mb. BERNIER.
On Mr. Mills’ amendment (constitution of the Senate) to motion to go Into Committee of Supply, 14th May>— Mb. COOK.	Mb. ROBERTSON (Hamilton)
tories) to motion to 20th April:—
Mb. OOOK.
Mb. FORBES.
Mb. MoINTYRE.
Mb. WELL8.
go into Committee of Supply,
Mb. BURNS.
Mb. KAULBAOK.
Mb. WALLACE (Albert).
Mb. BOSSft
On Mr. Mitchell’s amendment (abolition of flour and coal duties) to motion to go into Committee of Supply 20th May:—
Mb. FISHER.	Mb. WARD*
On Mr. Mitchell's amendment to motion for House into Committee on Bill respecting the North-West Central Railway Company, 3rd May:
Mb. LANGELI5R.	Mb. BOSSfi.
On Mr. Charlton's amendment (timber limits and coal lands) to mutton to go into Committee of Supply, 4th May:
Mb. BLAKE.	Mb. POPE.
Mb. BURPEE.	Mb. McNEILL.
Mb. GEOFFRION.	Mb. MASSUE.
Mr. GUNN.	Mb. HALL.
Mb. KIRK.	Mb.	FERGUSON.
Mb. LANGELIER.	Mr. BOSSfi.
Mb. RAY.	Mb. MOFFATT.
Mb. VAIL.	Mb. CARLING.
On Sir Riohard Cartwright’s ture) to motion to go 29 th May:
Mr. AUGER.
Mb. BERNIER.
Mb. BURPEE.
Mb. COCKBURN.
Mr. FISHER.
Mb. FLEMING.
Mb. IRVINE.
Mr. mu.
Mb. RAY.
Mb. TUPPER.
amendment (public expehdi-into Committee of Supply,
Mb. BAKES.
Mb. CURRAN.
Mb. MOFFATT.
Mb. HAY.
Mb. WARD.
Mb. MASSUE.
Mb. SHAKESPEARE.
Mb. BURNS.
Mb. DODD.
Mb. JACKSON.
On Mr. Mil’s’ amendment to refer back to Committee the Franchise Bill, 31st May :
Same as 29th May with the addition of Mb. WELLS.	Mb. WRIGHT.
On Mr. Mulock’s amendment to motion for House into Committee on Bill respecting the North-West Central Railway Co., 3rd May:
Mb. GUNN.
On Mr. Mulock’s amendment to refer Bill respecting the North-West Central Railway Company to Select Standing Committee on Railways, &c., 31st May Same as the last.
Mr. MoCALLUM.
mu 0f dtommm^ States
FOURTH SESSION, FIFTH PARLIAMENT.—49 VIC.
HOUSE OF COMMONS.
Thursday, 25th February, 1886.
The Parliament, which had been prorogued from time to time, was now commanded to assemble on the 25th day of February, 1886, for the despatch of business.
