HOUSE OF COMMONS
DEBATES
OFFICIAL REPORT
FIRST SESSION—THIRTY-THIRD PARLIAMENT 35 Elizabeth II
VOLUME IX, 1986
COMPRISING THE PERIOD FROM THE TWENTY-FIRST DAY OF APRIL, 1986 TO THE THIRTIETH DAY OF MAY, 1986
INDEX ISSUED IN A SEPARATE VOLUME
Published under authority of the Speaker of the House of Commons by the Queen's Printer for Canada
Available from Canadian Government Publishing Centre. Supply and Services Canada. Ottawa. Canada KlA 0S9
12451
HOUSE OF COMMONS
Monday, April 21, 1986
