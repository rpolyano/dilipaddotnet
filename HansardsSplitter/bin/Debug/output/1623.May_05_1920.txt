OFFICIAL REPORT
OF THE
DEBATES
OF THE
HOUSE OF COMMONS
OF THE
DOMINION OF CANADA
FOURTH SESSION—THIRTEENTH PARLIAMENT
10-11 GEORGE V, 1920
IN FIVE VOLUMES
Volume I: Pages 1-1034	Volume II: Pages 1035-2002
Volume III: Pages 2003-3078	Volume IV: Pages 3079-4017
Volume V: Pages 4019-4591
VOLUME CXLIII
COMPRISING THE PERIOD FROM THE FIFTH DAY OF MAY TO THE FIRST DAY OF
JUNE. 1920, INCLUSIVE.
OTTAWA
THOMAS MULVEY
PRINTER TO THE KING’S MOST EXCELLENT MAJESTY 1920
CANADA
IHotisc of Commons Debates
OFFICIAL EE PORT
Wednesday, May 5, 1920.
