OFFICIAL REPORT
OF THE
DEBATES
OF THE
HOUSE OF COMMONS
OF tub;
DOMINION OF CANADA
SIXTH SESSION-TWELFTH PARLIAMENT
6-7 GEORGE V, 1916
VOL. CXXIV
COMPRISING THE PERIOD FROM THE TWENTY-FIRST DAY OF MARCH TO THE SEVENTEENTH DAY OF APRIL, 1916, INCLUSIVE
OTTAWA
PRINTED BY J. de L. TACHE,
PRINTER TO THE KING’S MOST EXCELLENT MAJESTY
1916
CvY X A D A
Douse of Commons Debates
OFFICIAL REPORT—REVISED EDITION
HOUSE OF COMMONS.
Speaker: Hon. Albert Sevigny.
Tuesday, March 21, 1916.
