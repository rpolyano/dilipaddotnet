 at three o’clock.
PROROGATION OF PARLIAMENT
MESSAGE PROM THE GOVERNOR GENERAL’S SECRETARY
Mr. SPEAKER:	I have the honour to
inform the house that I have received the following message:
Ottawa, November 5, 1940.
Sir:
I have the honour to inform you that the Right Hon. Sir Lyman P. Duff, G.C.M.G., acting as the deputy of His Excellency the Governor General, will proceed to the Senate chamber on Tuesday, the fifth day of November, at 3.10 p.m., for the purpose of proroguing the present session of parliament.
I have the honour to be, sir,
Your obedient servant,
F. L. C. Pereira, Assistant Secretary to the Governor General.
NEW MEMBERS
Mr. SPEAKER: I have the honour to inform the house that the clerk of the house
has received from the chief electoral officer certificates of the election and return of the following members, viz.:
Of the Hon. Angus Lewis Macdonald, for the electoral district of Kingston City;
Of Louis O. Breithaupt, Esquire, for the electoral district of Waterloo North;
Of George Russell Boucher, Esquire, for the electoral district of Carleton;
Of Alfred Henry Bence, Esquire, for the electoral district of Saskatoon City.
NEW MEMBERS INTRODUCED
Hon. Angus Lewis Macdonald, member for the electoral district of Kingston City, introduced by Right Hon. W. L. Mackenzie King and Hon. J. L. Ralston.
Louis O. Breithaupt, Esquire, member for the electoral district of Waterloo North, introduced by Right Hon. W. L. Mackenzie King and Hon. C. D. Howe.
George Russell Boucher, Esquire, member for the electoral district of Carleton, introduced by Hon. R. B. Hanson and Mr. J. H. Harris.
Alfred Henry Bence, Esquire, member for the electoral district of Saskatoon City, introduced by Hon. R. B. Hanson and Mr. E. E. Perley.
HOUSE OF COMMONS
GIFT OP CHAIR USED BY SIR JOHN BOURINOT AS CLERK OP THE HOUSE FROM 1880 TO 1903
Mr. SPEAKER: I think it desirable to inform the house that during the recess Doctor Beauchesne, Clerk of the House, was in communication with Mr. Arthur Sidney Bourinot, son of Sir John Bourinot who was clerk of this house from 1880 to 1903, who expressed the desire to present to the house the chair which Sir John Bourinot used during those years and which had been in the possession of his family.
Doctor Beauchesne accepted the gift and gratefully acknowledged it, and has caused the chair to be placed at the table and it is now occupied by Doctor Beauchesne.
PROROGATION OF PARLIAMENT
A message was delivered by Major A. R. Thompson, Gentleman Usher of the Black Rod, as follows:
Mr. Speaker, His Honour the Deputy of His Excellency the Governor General desires the immediate attendance of this honourable house in the chamber of the honourable the Senate.
Accordingly, Mr. Speaker with the house went up to the Senate chamber.
NOVEMBER 5, 1940	2611
Governor General’s Speech
GOVERNOR GENERAL’S SPEECH
The deputy of His Excellency the Governor General was pleased to close the first session of the nineteenth parliament of the Dominion of Canada with the following speech: Honourable Members of the Senate:
Members of the House of Commons:
When the present session opened in May, one free country after another, in quick succession, had become the victim of nazi aggression. Before its adjournment on August 7, Italy had joined her axis partner as an open enemy, French resistance had collapsed, and the government of France had surrendered. Britain herself was threatened with invasion. The theatre of conflict had begun to spread into other lands beyond the confines of Europe. Japan and China were still at war. Among the nations of the world, the United Kingdom and the British dominions, alone, stood in arms, in the defence of the world’s freedom.
Canada has willingly accepted the widening responsibilities which events have placed upon her. The measures which you have taken have had in view the immediate task of sharing more completely in the defence of Britain and securing our own country more effectively against internal subversion and external attack. They have also had in view the long range task of ensuring the ultimate defeat of the enemy.
To serve these ends, the structure of the administration has been altered and enlarged. A Ministry of National Defence for Air and a Ministry of National Defence for Naval Services have been created. The scope of the Department of Munitions and Supply has been expanded and its organization strengthened. A
Department of National War Services has been established. The government has been empowered by the National Resources Mobilization Act to bring to the defence of Canada and the advancement of the common cause all the resources of the country, both human and material. In the different branches of war activity there has been a steady expansion and acceleration of training, transport, manufacture and production.
By the Unemployment Insurance Act you have made a valuable contribution to industrial and financial stability in time of war, and to social security and justice in time of peace. It is deeply gratifying that approval was given by all tbe provinces to the necessary amendment to the British North America Act to permit of the enactment of unemployment insurance by the parliament of Canada.
Members of the House of Commons:
I thank you for the financial appropriations which you have made. The determination of the Canadian people to support and advance the cause for which we have taken up arms, has been reflected in the unselfish acceptance by all of its heavy financial burdens.
Honourable Members of the Senate:
Members of the House of Commons:
It has become only too apparent that the lust for conquest will continue to enlarge the theatre of war. The struggle to preserve freedom will be long and hard. May Almighty God guide and uphold its brave defenders.
This concluded the first session of the nineteenth parliament.
END OF VOLUME III
DOMINION OF CANADA
OFFICIAL REPORT
OF
DEBATES
HOUSE OF COMMONS
FIRST SESSION—NINETEENTH PARLIAMENT
4 GEORGE VI, 1940
VOLUME III, 1940
COMPRISING THE PERIOD PROM THE TWENTY-SIXTH DAY OF JULY. 1940 TO THE FIFTH DAY OF NOVEMBER, 1940 INCLUSIVE
BEING
VOLUME CCXXIV FOR THE PERIOD 1875-1940
INDEX ISSUED IN A SEPARATE VOLUME
OTTAWA
EDMOND CLOUTIER
PRINTER TO THE KING’S MOST EXCELLENT MAJESTY 1940
CANADA
i>ous:e of
Commons; ©efcates
OFFICIAL REPORT
Friday, July 26, 1940
