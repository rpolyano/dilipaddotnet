 at Three o’clock.
Prayers.
BEPOETS PEESENTED.
Trade and Navigation Beturns for the fiscal year ending 80th June, 1884.—(Mr. Bowell.)
Beport of the Minister of Public Works for the year ending 30th June, 1884; also, Beport of the Minister of Justice on the Penitentiaries of the Dominion of Canada for the same year.—(Sir Hector Langevin.)
Public Accounts of Canada for the year ending 30th June, 1884; also, Beport of the Auditor General for the same year. (Sir Leonard Tilley.)
Beport of the Department of Indian Affairs for the year ending 30th June, 1884.—(Sir John A. Macdonald.)
Beport of the Inland Revenue Department for the year ending 30th June, 1884.—(Mr. Costigan.)
COST OF PRINTING^ AND ADVERTISING.
Mr. McMULLEN. I desire to enquire of the Government when they will bring down the Beturn ordered on the 14th of February last, showing :
The amounts paid by the Government to the Gazette Publishing Company, of Montreal, for printing, advertising, or any work done or material furnished during the years 1878, 1879, 1880,1881, 1882, 1883, in detail-
Sir John A. Macdonald.
The hon. gentleman suggested that the words “ and other newspapers of the Dominion ” should be added, and the years 1874, 1875, 1876 and 1877 be also included, so that we might have a Beturn for the last ten years. The hon. gentleman at that time promised he would give this Return prompt attention. Eleven months have passed since and no return has yet come down. I would like to know what the probability is of that Beturn being laid before the House at an early day.	•
Sir HECTOB LANGEYIN. I do not remember exactly the circumstances and have no doubt the Beturn was not ready at the end of the Session, but it will be brought down immediately.
Mr. McMULLEN. How soon ?
Sir BICHABD CARTWRIGHT. There was a discussion on the subject, and I took occasion to point out to the bon. gentleman that the addition he proposed would probably entail six months labor for a dozen clerks, involving an expenditure unnecessarily of some thousands of dollars. He declared that all pains would be taken and all possible despatch need to give us the Return. It should be ready, therefore, by this.
Sir HECTOR LANGEYIN. No doubt all those pains and this despatch were used, and the Return was not ready at the end of the Session. . That was the reason it was not brought down, but it will be brought down immediately.
GRAND TRUNK RAILWAY STOCKHOLDERS.
Mr. MITCHELL. At this stage I would like to ask the Government, and I take the earliest opportunity of doing so that there may be no excuse with regard to time, whether they are yet prepared to lay before the House, in accordance with the Order of the House last Session, a list of the stockholders of the Grand Trunk and their residences.
Sir JOHN A. MACDONALD. On account of the absence of the Secretary of State I cannot say whether that Return has yet been received or not. Of course it would be sent to him.
Mr. MITCHELL. I would like the hon. gentleman not to let it drift over for more than a week or two.
CIVIL SERVICE BILL.
Mr. CASEY; Seeing that the hon. Minister of Agriculture has been so prompt in giving notice of a couple of measures mentioned rather among the addenda in the Speech from the Throne, I would like to ask the Premier when the Civil Service Bill, also mentioned, will be brought down.
Sir JOHN A. MACDONALD. That measure is under the charge of the Secretary of State. I believe it will be introduced in a few days.
SUPREME COURT.
Mr. LANDRY (Montmagny) moved for leave to introduce Bill (No. 3) to limit the appellate jurisdiction of the Supreme Court.
Mr. CASGRAIN. Explain.
Mr. LANDBY. (Translation.) The explanations which are asked from me, Mr. Speaker, are very simple, and the Bill itself will give them in full at tho second reading. As the title indicates, it is intended to limit the jurisdiction of the Supreme Court as regards matters relating to civil law in the various Provinces. The object of the law is to withdraw those cases from the jurisdiction of the Supreme Court. If tho Government intend to introduce a measure which would be better than mine, and which would be calculated to meet our views, I will not object to having it
1885
29
COMMONS DEBATES.
substituted for my Bill, but if they do not introduce a measure of that kind, I intend to ask the vote of this House.
Bill read the first time.
DISTRIBUTION OF ASSETS OF INSOLVENT DEBTORS.
Mr. CURRAN, moved for leave to introduce Bill (No. 4) for the distribution of assets of insolvent debtors. He said : This Bill has been prepared under the direction of the Boards of Trade of Montreal, Toronto, Hamilton and Winnipeg, and embodies the views of the commercial community of the Dominion.
Mr, MITCHELL. I would like to ask the hon. the mover of the Bill whether it contains any provision for the discharge of the debtors when their property is taken away. Without such a clause the Bill will be incomplete.
Mr. CURRAN. The promoters of the Bill have not thought proper to include a clause for the discharge of insolvent debtors. This Bill merely aims at an equitable distribution of the assets. However, as the matter is to be referred to a committee later on, any additions thought necessary will be made there.
Mr. MITCHELL. I understand that the Bill simply provides to take away the property of unfortunate debtors, but does not admit of the discharge of the debtors; I think it is a very one-sided affair.
Bill read the first time.
.	FIRST READINGS.
Bill (No. 2) to regulate the employment of children and young persons and women in the Workshops, Mills and Factories of the Dominion of Canada.—(Mr. Bergin.)
Bill (No. 5) respecting the liability of carriers by land. —(Mr. Coughlin.)
Bill (No. 6) to further amend the law of evidence in criminal oases.—(Mr. Cameron, Huron.)
THE FACTORY BILL.
Mr. BLAKE enquired, Whetfcer it is the intention of the Government to introduce tho Factory Bill ?
Sir JOHN A. MACDONALD. That is under consideration.
’CHINESE IMMIGRATION.
Mr. SHAKESPEARE enquired, Is it the intention of the Government to introduce a measure this Session restricting the immigration of Chinese into the Dominion of Canada ?
Sir JOHN A. MACDONALD. Until the report of the Commission is received and considered, the Government have not come to any resolution on the subjeot.
PUBLIC DEBT OF CANADA.
Mr. CHARLTON enquired, The net amount of the public debt of Canada on January 1st, 1885 ?
Sir LEONARD TILLEY. The net debt of Canada on the 30th June, 1884, was $181,*719,931.30; the increase of the debt from the 1st July, 1884, to the 31st December, 1884, was $7,194,953.75; total, $188,914,855.05.
Sir RICHARD CARTWRIGHT. Has the hon. gentleman the gross amount besides ?
Sir LEONARD TILLEY. No, The question is not as to the gross amount.
CANADIAN PACIFIC RAILWAY LOAN.
Mr. CHARLTON enquired, Of the loan of $22,500,000 which Parliament at its last Session authorized the Government to make to the Canadian Pacific Railway Company, what amount has been paid over to that company up to January 29th, 1885 ?
Sir LEONARD TILLEY. The amount paid to the company up to the 29th January, 1885, is $18,591,600.
LIQUOR LICENSE ACT.
Mr. DESJARDINS enquired, Whether it is the intention of the Government to accept aB final the judgment rendered by the Supreme Court as to the competency of the Dominion Parliament to pass the Liquor License Act, 1883 ?
Sir JOHN A. MACDONALD. A report is being obtained as to the effect the decision of the Supreme Court will have on the laws of the different Provinces. Until that is received—and it will be received in a few days—the Government cannot express any intention on the matter.
RESIGNATION OF CAPT. LUDGER BOLDUC.
Mr. LANDRY (Hontmagny) moved for:
Return of all papers relating to the resignation of Gapt. Ludger Bolduc, after the collision which occurred on the 20th May, 1884, between La Canadienne and the brig Alliance, of Jersey ; covering complaint, enquiry, report, &c., and all correspondence relating to the matter.
Mr. LANGELIER. (Translation.) If the hoD. member has no objection, I shall ask him to add to bis motion a demand for papers concerning the appointment of that same Capt. Bolduc. The reason why I make the demand is this: Capt. Bolduc superseded Capt. Auguste Dupre, who commanded that steamer for several years without meeting with any accident whatever. For one reason or another means have been found to dismiss him and to appoint in his place Capt. Bolduc, to which the accident mentioned in the hon. member’s motion has happened. Therefore, I shall ask him to add to his motion a demand for papers relating to the appointment of Capt. Bolduc.
Mr. LANDRY. I believe the hon. member wishes to mix up two questions which are altogether distinct and separate. If he wishes to get information he is at liberty to do what I am doing, by making a motion to that effect.
Mr. LANGELIER. I believe it is useless to make two motions for the same object.
Mr. LANDRY. It is not altogether the same object.
Mr. LANGELIER. Nothing is more natural than to ask for the circumstances of the appointment while asking for the circumstances of the resignation.
Sir HECTOR LANGEVIN. I think the hon. member ought to give a notice of motion. I do not think there would be any objection, but at all events the Deputy Minister will be notified of the motion and he will be enabled to see whether the motion may be granted or not.
Motion agreed to.
GOVERNMENT DEPOSITS IN BANKS.
Sir RICHARD CARTWRIGHT moved for:
Statement showing the amount of money on deposit to the credit of the Government of Canada on the 1st day of January, 1885, whether in Canada or elsewhere, together with tho names of the banks wherein the said moneys are deposited, with the amount in each bank respectively ; also the amount at interest and the rate of interest, if any, allowed on the said deposits in each case.
He said: If the Finanoe Minister thinks that it would make
COMMON'S DEBATES
SO
February 3,
no special delay I would make that the 1st of February, and save another motion.
Sir LEONARD TILLEY. Yes.
Motion, as amended, agreed to.
GOODS IMPORTED FOR CONSUMPTION.
Sir RICHARD CARTWRIGHT moved for:
Summary statement, similar to No. 2, in the Trade and Navigation Returns, of the goods imported tor consumption, dutiable and free, in the Dominion of Canada, the amount of duty collected, and the rates of duty levied thereon, during the six months ending 31st December, 1884. Also, a similar statement to No. 5, in the Trade and Navigation Retnrns, of the goods, the produce and manufacture of Canada, exported from the Dominion of Canada for the six months ending 31st December, 1884.
Mr. BO WELL. There is do objection to the motion ; but the hon. gentleman must understand that it will be some time before the motion can be brought down as it involves a very large amount of labor and work.
Motion agreed to.
TIMBER LICENSES.
Mr. CHARLTON moved for:
RETURNS ORDERED.
Statement showing: 1st. The Christian and surnames of the present employes of the Immigration Office at Quebec, and the nature of their employment;
2nd. The amount of the yearly salary paid to each such employ^ on 31st December, 1884 ;
3rd. The amount of the yearly salary attached to the said office on 31st December, 1877.
Also, all correspondence respecting the increase or non-increase of the salary of any employe of the said office, between the two dates above named.—(Mr. Landry, Montmagny.)
Return cf the receipts and expenditure in detail, chargeable to the Consolidated Fund, from the 1st day of July, 1883, to the 31st day of January, 1884, and from the 1st day of July, 1884, to the 3lst day of January, 1885.—(Sir Richard Cartwright.)
Return in the form used in the statements usually published in the Gazette, of the exports and imports from the 1st day of July, 1883, to the 1st day of January, 1884, and from the 1st day of July, 1«84, to the 1st day of January, 1885, distinguishing the products ot Canada and those of other counties.—(Sir Richard Cartwright.)
Copies of the complaint, corresponder ce, documents and reports, relating to the enquiry respecting Captain Alphonse Miville DeChene, about the year 1879, at St. Roch des Anlnets —(Mr. Casgrain.)
Copies of all documents, correspondence and contracts between the Government or its officers and the several parties tendering for the supplying of wood to the lightship at the Lower Traverse, for the years 1883 and 1884.—(Mr. Casgrain.)
Sir JOHN A. MACDONALD moved the adjournment of the House.
1st. The total number of timber licenses or permits to cut timber granted since February 1st, 1883, and the total area covered by such licenses or permits;
2nd. The total amount of bonuses or premiums paid on such licenses or permits;	.
3rd. The name and residence of each grantee of a timber license or permit; the number of the license or permit; the area covered by each ; the date of application for the same; the bonus or premium per square mile paid upon each : whether the survey of each berth or area covered by license or permit was made by the Government previous to granting the same, for the purpose of obtaining information as to its value; and the information, if any, in the possession of the Government as to the quantity, quality and kind of timber upon each ; also, the location ot each berth or limit;
4th. The Crown dueB or stnmpage charged or chargeable on each license or permit;	%
5th. Whether in each case where a license or permit was granted the berth was first put up at public auction after public notice inviting tenders was given, and was sold to the highest bidder, or whether granted npon application from tbe grantee without public competition being invited;
6th, Copies of all petitions, remonstrances, claims, or communications sent or made to the Government respecting such timber licenses or permits ; and copies of all correspondence bad with the Government respecting such lands, licenses or timber, and the action of the Government thereon.	-
He said: I notice in a Return laid upon the Table of the House very late last Session, in a statement showing the number of timber licenses and leases granted, area, bonuses, total amount of dues, etc., that the names of the grantees are omitted in each case, in no instance are we able to say who were the parties to whom these leases were granted. I think this motion called for that information, and I consider it essential that the information should be given. I hope the Return to this motion will be made very soon, as there is nothing asked, for here except the licenses actually granted, and I hope when that Return is made we may have the names of the parties to whom the licenses were given.
Mr. CAMERON (Huron). I think it is desirable to add,
“ assignees of Government licenses and tbe consideration expressed in the assignment of licenses.”
Motion, as amended, agreed to.
Motion agreed to, and the House adjourned at 4:10 p.m.
HOUSE OE COMMONS.
Tuesday, 3rd February, 1885. The Speaker took the Chair at Three o’clock. Prayers.
SELECT STANDING COMMITTEES.
Sir JOHN A. MACDONALD, from the Special Committee appointed to prepare and report lists of members to compose the Select Standing Committees, ordered by the House on tho 29th ult., reported lists as follows:—
No. 1.—ON PRIVILEGES AND ELECTIONS. Messieurs
Abbott,
Amyot,
Belleau,
Blake,
B0S8&,
Cameron (Huron), Cameron (Victoria), Casgrain,
Colby,
Costigan,
Curran,
Daly,
Davies,
Desjardins,
Gironard,
Hall,
Laurier,
Lister,
Macdonald (Sir John), Mackenzie,
Macmaster,
McCarthy,
McIntyre,
Mclsaac,
Mills,
Ouimet,
Patterson (Essex), Robertson (Hamilton), Royal,
Shakespeare,
Temple,
Weldon,
Wells
White (Cardwell) and Woodworth.—35.
No. 2.—ON EXPIRING LAWS. Messieurs
STANDING COMMITTEES.
Sir JOHN A. MACDONALD moved :
That a Special Committee of seven members be appointed to prepare and report, with all convenient speed, lists of members to compose the Select Standing Committees, ordered by tbe House on Thursday, the 29th ultimo, and that Sir John A. Macdonald, Sir Leonard Tilley, Sir Hector Lange vin, Sir Richard Cartwright, and Messrs. McLelan, Blake and Tail do compose the said committee.
Motion agreed to.
Sir Richard Cartwright,	1
Armstrong,
Benson,
Billy,
Cameron (Inverness), Campbell (Renfrew), Campbell (Victoria), Casey,
Cochrane,
Coughlin,
Daonst,	Mclntvre,
De St. Georges,	McMillan (Vaudreuil),
Desaulniers (St. M’rice), Paint,
Dodd,	Pruyn,
Guillet,	Riufret,
Hackett,	Robertson	(Hastings),
Harley,	Tyrwhitt,
Hesson,	Valin and
Labrosse,	Yeo.—27.
And that the Quorum of the said Committee do consist'of Seven Members,
1885.
COMMONS DEBATES.
31
No. 3.—ON RAILWAYS, CANALS, AND TELEGRAPH LINES. Messieurs
Abbott,
Allen,
Amyot,
Bain,
Baker (Missisquoi), Barnard,
Beaty,
B6chard,
Bell,
Belleau,
Benoit,
Bergeron,
Bergin,
Bernier,
Blake,
Blondeau,
Boss6,
Bourassa,
Howell,
Bryson,
Burns,
Burpee (St. John), Burpee (Sunbury), Cameron (Huron), Cameron (Inverness), Cameron (Victoria), Carling,
*!aron,
Casey, ^
Casgrain,
Ohapleau,
Charlton,;
Cockburn,
Colby,
Cook,
Oostigac,
Coursol,
Curran,
Davies,
Dawson,
De tit. Georges, Desjardins,
Dickinson,
Dodd,
Dundas,
Edgar,
Fair bank,
Ferguson (Welland), Fisher,
Forbes,
Fortin,
Foster,
Gault,
GeofiFrion,
Girouard,
Glen,
Gordon,
Grandbois,
iar1-
Hay,
Hickey,
Billiard,
Holton,
1 rvine,
Ives,
Kilvert,
King,
Kinney.
Landerkin,;
Landry (Kent), Landry (Montmagny),
Langevin (Sir Hector), Townshend, Laurier,	m
Livingstone,
Macdonald (Sir John),
Mackenzie,
Mackintosh,
Macmaster,
Onimet,
Paint,
Patterson (Essex),
Pope,
Riopel;
Robertson (Hamilton),. Robertson (Hastings), Robertson (Shelburne), Ross,
Royal,
Rykert,
Scott,
Scriver,
Small,
Smyth,
Spronle,
Stairs,
Sutherland (Oxford), Sutherland (Selkirk), Tascherean,
Tasse,
Temple,
Thompson,
Tilley (Sir Leonard),
Trow,
Tnpper,
Vail,
V alin,
Vanasse,
Wallace (Albert),
Macmillan (Middlesex),Wallace (York),
McUallum, McCarthy, McOraney, McDougald (Pictou), McGreevy,
McIntyre,
Mclsaac,
McLelan,
Watso: ,
Weldon,
Wells,
White (Cardwell), White (Hastings),
White (Renfrew), Wigle,
________,	Willie ms,
McMillan (Vaudreuil), Wilson,
McMullen,	Wood (Brockville),
Mills,	Wood (Westmoreland),
Mitchell,	Woodworth and
Mulock,	Wright.—140.
Orton,
No. 4.—ON MISCELLANEOUS PRIVATE BILLS.
Messieurs Gillmor,
Girouard,
Glen,
Guilbault,
Hay,
Hickey,
Holton,
Homer,
Ives,
Jamieson,
Jenkins,
Kinney,
Kranz,
Labrosse,
,	Langelier,
Desaniniers (Mask’ngG),Landry (Kent),
DesaulniersOSt. M’rice), Landry (Montmagny)
Edgar,	Laurier,
Farrow,	Lesage,
Fleming,	Lister,
Foster,	Macmaster,
Gagne,	McDougall (0.	Breton),	Weldon,
Geoffrion,	Mclsaac,	Wells, and
McMullen,	Wright.—71.
And that the Quorum of the said.jGommittee do consist of Seven Members.
No. 5.—ON STANDING ORDERS.
Messieurs
Allen,
Amyot,
Baker (Missiequoi), Bell,
Benson,
Bourassa,
Burns,
Burpee (Sunbury), Cameron (Victoria), Oaron,
Casey,
Gatudal,
Cockburn,
Cuthbert,
Daoust,
Massue,
Moutplaisir,
Mulock,
Ouimet,
Pinsonneault,
Ray,
Reid,
Robertson (Shelburne), Scriver,
Small,
Smyth,
Springer,
Sproule,
Stairs,
Taschereau,
Tass6,
Taylor,
Thompson,
Tapper,
Vanasse,
Wallace (Albert),
Auger,
Bain,
Baker (Victoria), Beaty,
Bergeron,
Bourbeau,
Burnham,
Cameron (Middlesex),
Ferguson ( Leeds&Gren)Landerkin, Ferguson (Welland), Livingstone, Gaudet,	‘
Gault,
Gigault,
Giflmor,
Gordon,
Grandbois,
Macdonald (King's), McDougall (0. Breton), Macmillan (Middlesex), Massue,
Moffat,
Montplaisir,
O’Brien,
Paterson (Brant), Patterson (Essex), Rinfret,
Sutherland (Oxford)aud Wood (Brockville).—44.
And that the Quorum of the said Committee do consist of Seven Members.	....
No. 6. -ON PRINTING.
Messieurs
Casgrain,	Gunn,
Coughlin,	Hackett,
Dawson.	Hnrteau,
De St. Georges,	Innes,
Dodd,	Irvine,
Dundas,	Jackson,
Dnpont,	Kaulbach
Allison,
Baker (Missisquoi), Belleau,
Bergia,
Bourassa,
Bowell,
Desjardins,
Foster,
Innes,
Landry (Montmagny),
Somerville (Brant), Tass6,
Thompson,
Trow and
White (Cardwell).—15.
No. 7.—ON PUBLIC ACCOUNTS.
Messieurs
Baker (Victoria),
B6chard,
Bergeron,
Bergin,
Blake,
Bowell,
Carling,
Grandbois,
Holton,
Ives,
Kilvert,
King,
LaDgelier,
Rykert,
Scriver,
Somerville (Brant), Suthedand (Selkirk), Tascherean,
Tilley (Sir Leonard),
Macdonald (Sir John), Townshend,
Cartwright (Sir Rich.), Mackenzie,
Charlton,	McDougald (Pictou),
Colby,	McLelan,
Oostigan,	Massue,
OonrBol,	Mulock,
Davies,	Pope,
Desaulniers (St. M’rice),Rinfret,
Farrow,	Riopel,
Ferguson (Welland), Robertson (Shelburne),
And that the Quorum of the said Committee do consist of Nine Members.
Tupper,
Vail,
White (Cardwell), White (Hastings), White (Renfrew),
Wood (Brockville) and Wood (Westmoreland) —46.
No. 8.—ON BANKING AND COMMERCE. Messieurs
Dupont,
Fairbank,
Fleming,
Forb3S,
Fortin,
Gagne,
Gault,
Gigault,
Girouard,
Guillet,
Gunn,
Hackett, flaggart,
Hall,
Hesson,
Hilliard,
Innes,
Abbott,
A llison,
Baker (Victoria),
B6chard,
Bernier,
Blake,
Boss6,
Bourbeau,
Bowell,
Bryson,
Burnham,
Burpee (Sunbury),
Cameron ( Huron),
Cameron (Middlesex), Cameron (Victoria), Campbell (Victoria), Carling,
Cartwright (Sir Rich.), Ives,
Casgrain,
Catudal,
Charlton,
Cimon,
Cochrane,
Cook,
Coursol,
Curran,
Cuthbert,
Daly,
Davies,
Dawson,
Desjardins,
Dickinson,
Dugas,
Dundas,
And that the Quorum of the said Committee do consist
Members.
Jackson, Jamieson, Kaulbach, Kilvert, Kinney, Kirk, Kranz, Landerkin, Langelier, Lesage,
McCallum,
McCarthy,
McDougald (Pictou), McGreevy,
McMullen,
McNeill,
Massue,
Mitchell,
Moffat,
O’Brien,
Orton,
Ouimet,
Paterson (Brant), Platt,
Reid,
Robertson (Hamilton), Rykert,
Scott,
Scriver,
Shakespeare, Somerville (Brace), Sutherland (Oxford), Tilley (tiir Leonard), Vaii,
Vanasse,
Wallace (York), Weldon,
White (Cardwell),
Macdonald (Sir John), White (Renfrew),
Macdonald (King’s), Mackenzie, Mackintosh, Macmaster,
Williams,
Wood (Westmoreland), Wright, and Yeo.—100.
of Nine
No. 9.—ON IMMIGRATION AND COLONIZATION. Messieurs
Allen,
Allison,
Armstrong,
Auger,
Bain,
Barnard,
Beehard,
Bell,
Dugas,	Mitchell,
Edgar,	Montplaisir,
Fairbank,	Orton,
Farrow,	Paterson (Brant),
Ferguson (Leed8&Gren)Patterson (Essex), Fisher,	Pinsonneault,
Fortin,	Platt,
Gagne,	Pope,
82
COMMONS DEBATES.
February 3,
Benoit,
Billy,
Blondean,
Bonr&ssa,
Bryson,
Burnham,
Burns,
Cameron (Middlesex), Campbell (Renfrew), Catudal,
Ohapleau,
Cimon,
Cochrane,
Cockburn,
Colby,
Conghlin,
Daiy,
Dawson,
Deeaulniera (Mask’ng6 Dickinson,
Oaadet,
Gramlbois,
Guilbault,
Harley,
Hay,
Hesson,
Hickey,
Homer,
Hurteau,
Jenkins,
King,
Kirk,
Kranz,
Labrosse,
Landry (Kent),
Mackintosh,
McCallum,
McCraney,
McMillan ( Vaudreuil), McNeill,
Prayn,
Ray,
Robertson (Hastings), Ross,
Royal,
Scott,
Somerville (Bruce), Springer,
Sproule,
Sutherland (Selkirk), Taylor,
Trow,
Tyrwhitt,
Vi atson,
White (.Hastings), White (Renfrew), Wigle,
Wilson,
Wright and Yeo.—8i.
And that the Quorum of the said Committee do consist of Nine Members.
Sir JOHN A. MACDONALD moved that the said report be concurred in.
Motion agreed to.
MESSAGE FROM HIS EXCELLENCY.
Sir JOHN A. MACDONALD presented a Message from His Excellency the Governor General.
Mr. SPEAK EE read the Message as follows :—
Lassdownx.
The Governor General transmits to the House of Commons a copy of a despatch which be has received from the Right Honorable the Secretary of State for the Colonies, in reply to a Joint Address to tho Queen, eipre.-ging sympathy with Her Most Gracious Majesty, on the death of Eis Royal Highness the Duke of Albany.
GoVBBVirZNT ffOCSK,
Ottawa, 30th January, 1885.
(Copy—No. 57)
The Earl of Derby to the Governor General, Canada,
Dowkixo Strest, 1st May, 1881.
My Loud,—I have received and laid before the Queen your Despatch (Vo. 6%) of the 9’h ultimo,enclosing aJomt Address to Her Majestyfrom itie Seuat and Haase of Commons of Canada, expressing sympathy with Her Most Gracious Majesty on the death of His Royal Highness the Duke of Albany.	•
I am commanded by the Queen to request that you will convey Her Majesty's thanks to the Dominion Senate and House of Commons for this expression of their sympathy, which Her Majesty has received very gratefully.
I have, etc.,
-	DERBY.
Governor General
The Most Honorable
The Marquis of Lahsdowei, G-O.M.G.
Ac, Ac-, Ac.
OFFICIAL REPOET OF THE DEBATES.
Mr. WHITE (Cardwell) presented first report of the Committee appointed to supervise the official reporting of the Debates.
Mr. BLAKE. It would be convenient if the hon. member for Cardwell (Mr. White) would state when ho proposes to ask the House to concur in the report of the Debates Committee which he has laid on the Table.
Mr. WHITE (Cardweli). I should like to do it to day. I have given the usual notice, but I will do it to-morrow if the House will consent.
CONSOLIDATION OF THE STATUTES.
Sir JOHN A. MACDONALD laid on the Table reports of the Commissioners appointed to consolidate and revise the Statutes of Canada. He said: There are two parts, and some tables^are wanting to the second part. The whole of Sir John A. Macdonald.
the revised Statutes are here, and the tables will be finished by the printers to-day or to-morrow. Then the whole will be distributed, with the tables. The French copies are not yet ready, but will bo laid on the Table as Boon as they are printed.
Mr. BLAKE. I think that, with a reasonable notice of that kind, the hon. gentleman might move it, because it obviously involves a double impression being made in the meantime.
Sir JOHN A. MACDONALD. I suppose the report will be printed in the Yotes and Proceedings ?
Mr, BLAKE. Yes, I suppose so.
ADVANCES TO PROVINCES.
Sir LEONARD TILLEY moved for leave to introduce Bill (No. 7) to amend the Act 37 Vic., Cup. 17. He said : This is for the purpose of amending an Act which provides that the Dominion Government may, on application made by a Local Government, by Order in Council, advanoe to the Province or Provinces such sums of money as they may require for public works, out of the debt account. The amendment of the Bill requires that that application shall have the assent of the Legislature.
Mr. BLAKE. Of both Legislatures ?
Sir LEONARD TILLEY. No; but, for instance, if an application should come from the Province of New Brunswick, the assent of the Legislature of that Province must be obtained to the proposal that the amount should be withdrawn from the debt account.
Sir RICHARD CART WHIG ELT. Does the hon. gentleman propose that this Government should have full power to refuse such application, or is the power to be given to the Provinces, as of coarse, to draw at any moment ?
Sir LEONARD TILLEY. The law at the present moment is that this Government shall have the power to refuse any such application, but circumstances have occurred which have led the Government to believe that it is in the interest of all parties that the assent of the Legislature should be had to any such application.	w
Sir RICHARD CARTWRIGHT. That is the only alteration ?
Sir LEONARD TILLEY. Yes, that is the only alteration.
Mr. BLAKE. Of course it is quite competent for the Government, as an administrative act, to refuse to assent to the application of any Province unless it is backed by the assent of the Legislature. The Government is not under any compulsion to assent to any such application. Jt has power to say yea or nay; and, if at any time it thinks that the opinion of the Local Legislature, as well as that of the Local Executive should be given in regard to such an application, it has the power to say so.
Sir JOHN A. MACDONALD. That is true, but the Government think it is well that no such responsibility should be thrown upon the Dominion Government, as, in refusing the application of a Provincial Government, it is at once brought into collision with that Government. We think that a voto of the Local Legislature should be obtained before any sum of money is withdrawn, Of course, no Government—either Dominion or Provincial—ought to use money without consent of Parliament. Hitherto, under the law which was introduced for a very good purpose by* my hon. friend for East York (Mr. Mackenzie), the Provincial Government is enabled to apply for an advanoe out of the money at its credit for local purposes, but it does not appear on the face of the application whether there has been a vote of the Local Legislature or not; and this
1885,
COMMONS DEBATES.
33
amendment is proposed in ordor to avoid all question in regard to the matter, and to avoid all possible collision between the Provincial and Dominion Governments in regard to it.
Mr. MACKENZIE. I suppose even Ontario will participate ?
Sir JOHN A. MACDON ALD. Even Ontario will participate.
Motion agreed to, and Bill read the first time,
LETTER POSTAGE.
Mr. HESSON enquired, Whether it is the intention of the Government to reduce the rate of postage on letters to two cents per £ oz. ?
Mr. CARLING. It is not the intention of the Government during the present Session to reduce the postage on letters to two cents.
PACIFIC RAILWAY.
Mr. LANDRY (Montmagny) moved for :
Copies of all Orders in Council, instructions given, reports of engineers, and all documents •whatsoever, in relation to the selection of the shortest and best line for a railway between the present terminus of the Canadian Pacific Railway and one of the seaports of the Maritime Provinces.
He said (Translation) : Mr. Speaker, I do not intend to deal lengthily with this question, at this stage of the Session, for I understand that the papers, on which any debate ought to be based, to be of advantage to public interest generally, and to the Province of Quebec in particular, have not yet been brought down. But I take this opportunity of calling the attention of the Government to a certain rumor, now in circulation, and of which it is expedient to inform the Government. It has been recently stated that an exploration had been made from St. Charles station on the Intercolonial towards the Seven Island through the fifth or sixth range of the township of Montmagny By taking this line we, reach a certain knoll, a certain hill, whose summit is about 400 feet above the ordinary level in those parts. It seems a report on this question has been made to the Government who gave instruction to the explorer, Mr. Light, to discontinue the exploration. In spite of that, according to the rumor, the engineer took upon himself to make an exploration by going round the mountain, and discovered a route just as short, and even shorter, than the proposed line, if the grade is taken into consideration. Nevertheless, rumor has it—I do not wish to affirm anything, but rumors are always allowed to be repeated—that the Government have not yet given, or does not wish to give, instructions to this engineer to the effect that what is called an instrumental survey should be made on this new line. I wish to call the attention of the Government to these facts, at this stage of the Session, so that they may not, at a more advanced stage, come to us and say : We have encountered insuperable difficulties. Indeed, these difficulties are not unconquerable, as the Government may see for themselves, by referring to the last reports fylod by Mr. Light, the engineer. These are the only remarks I shall make to-day, and I have deemed it my duty to make them, so that I may not be charged, later on, with haviDg failed to make them in proper time.
Sir HECTOR LANGEYIN. (Translation.) Mr. Speaker, my colleague, the hon. Minister who is in charge of the Railway Department, is not present, but I shall not fail to repeat to him the remarks just made by the hon. member. I am convinced that he will be most happy to receive the information just given, and that he will not fail to give the necessary instructions.
Motion agreed to.
Cl
RETURN ORDERED.
Copies of all correspondence between the Federal and Ontario Governments and the Imperial Government, on the subject of the Imperial Act, 21-22 Victoria, Chapter 90, known a3 the British Medical Act, 1858 ; the Imperial Act 31-32 Victoria, Chapter 29, known as the British Medical Amendment Act, 1868 ; the Imperial Act 41-42 Victoria, Chapter 33, known as the Dentists’ Act, 1878 ; and the amendments proposed to be made thereto, dating the present Session of the Imperial Parliament.—(Mr. Bergin.)
Sir JOHN A. MACDONALD moved the adjournment of the House.
Motion agreed to, and the House adjourned at 3.55 p.m.
HOUSE OF COMMONS.
Wednesday, 4th February, 1885*
