 at three o’clock, the Speaker in the chair.
Mr. Speaker read a communication from the Governor General’s secretary, announcing that His Excellency the Governor General would proceed to the Senate chamber at three p.m. on this day, for the purpose of formally opening the session of the dominion parliament.
A message was delivered by Major A. R. Thompson, Gentleman Usher of the Black Rod, as follows:
Mr. Speaker, His Excellency the Governor General desires the immediate attendance of this honourable house in the chamber of the honourable the Senate.
Accordingly the house went up to the Senate chamber.
And the house having returned to the Commons chamber:
OATHS OF OFFICE
Right Hon. W. L. MACKENZIE KING (Prime Minister) moved for leave to introduce Bill No. 1, respecting the administration of oaths of office.
Motion agreed to and bill read tlhe first time.
GOVERNOR GENERAL’S SPEECH
Mr. SPEAKER:	I have the honour to
inform the house that when the house did attend His Excellency the Governor General this day in the Senate chamber, His Excellency was pleased to make a speech to both houses of parliament. To prevent mistakes, I have obtained a copy, which is as follows:
Honourable Members of the Senate:
Members of the House of Commons:
Since last you met for deliberation, all countries have come to have a clearer conception of the magnitude of the present conflict and 14873—1
its menace to civilization. Additional nations have been threatened with war, or have become involved in war. But little of freedom is now left in Europe. Force and fear have been supplemented by subtle intrigue. International tension has been heightened by the formation of an alliance between the axis powers and Japan.
While these events have added enormously to the uncertainties of the world situation, other events of even greater significance have served to confirm our confidence in the ultimate outcome of the struggle. First and foremost has been the magnificent resistance of the United Kingdom, For four months, Britain has constituted the front line of battle against the forces of aggression. The spectacular advance of the enemy has been halted by the indomitable spirit of her people.
The destruction of freedom throughout Europe has awakened, in the western hemisphere, a fuller consciousness of the nazi menace. In the face of the common peril there has arisen a closer association and an increasing measure of cooperation between the United States of America and the nations of the British commonwealth.	.	,
You have been summoned at this time that opportunity may be afforded for the fullest consideration and discussion of Canada’s war effort and of national problems which war has served to intensify or create. You will be fully advised of international, developments; of Canada’s cooperation with the United Kingdom, and of relations with the United States. The measures which will be submitted to you are such as seem necessary to my advisers for the welfare of the country, and for the prosecution of the war to the utmost of our strength.
Members of the House of Commons:
You will be asked to make financial provision for expenditure caused by the state of war which now exists. The estimates for the current fiscal year will be duly submitted to you for your consideration and approval.
Honourable Members of the Senate:
Members of the House of Commons:
In the discharge of your very responsible duties may Divine Providence guide and bless your deliberations.
Right Hon. W. L. MACKENZIE KING (Prime Minister) moved:
That the speech of His Excellency the Governor General to both houses of parliament be taken into consideration on Friday next, and that this order have precedence over all other business except the introduction of bills, government notices of motion and government orders, until disposed of.
Motion agreed to.
REVISED EDITION
2
Dominion-Provincial Relations
COMMONS
STANDING COMMITTEES
Right Hon. W. L. MACKENZIE KING (Prime Minister) moved:
That a special committee be appointed to prepare and report with all convenient speed, lists of members to compose the standing committees of this house, under standing order 63, said committee to be composed of Messrs. Mackenzie (Vancouver Centre), Casgrain, Casselman (Grenville-Dundas), Taylor and Johnston (Bow River).
Motion agreed to.
INTERNAL ECONOMY COMMISSION
Right Hon. W. L. MACKENZIE KING (Prime Minister) presented the following message from His Excellency the Governor General:
The Governor General transmits to the House of Commons a certified copy of an approved minute of council appointing the Honourable T. A. Crerar, Minister of Mines and Resources, the Right Honourable Ernest Lapointe, Minister of Justice, the Honourable J. L. Ralston, Minister of National Defence, and the Honourable J. L. Ilsley, Minister of Finance, to act with the Speaker of the House of Commons as commissioners for the purposes and under the provisions of chapter 145 of the revised statutes of Canada, 1927, intituled An Act Respecting the House of Commons.
WAR MEASURES ACT
TABLING OF ORDERS IN COUNCIL PASSED SINCE
JUNE 30, 1940
Right Hon. W. L. MACKENZIE KING (Prime Minister): Mr. Speaker, during the preceding session I tabled copies of orders in council which had been passed under authority of the War Measures Act from August 25, 1939, to June 30 of this year. I wish now to lay on the table copies of the orders in council which have been passed since June 30, until November 2.
DOMINION-PROVINCIAL RELATIONS
PROPOSED CONFERENCE WITH PROVINCES ON RECOMMENDATIONS MADE BY ROYAL COMMISSION
Right Hon. W. L. MACKENZIE KING (Prime Minister): Mr. Speaker, on Saturday, November 2, I sent in identical terms to each provincial premier a communication respecting the report of the royal commission on dominion-provincial relations. In that communication I mentioned that the government would seek an adjournment of the House of Commons during the month of January in
[Mr. Speaker.]
order to permit of the dominion having a conference with the provinces with respect to this report.
In the letter I stated that the report commended itself to the judgment of the government, and that we should like to have an opportunity of presenting our views to the provinces in order that there might be a full consideration of them. I expressed the hope that at that time it might be possible to reach an agreement between the provinces and the dominion with respect to the recommendations contained in the report.
I need not review what I set forth in the communication, but I would suggest that the letter itself might be printed in the votes and proceedings in order that it may appear in the records of the house and that hon. members may be in a position to read the text in full.
Mr. HANSON (York-Sunbury):	That is
satisfactory.
BUSINESS OF THE HOUSE
TABLING OF COMMUNICATIONS RESPECTING ADJOURNMENT OR PROROGATION OF PARLIAMENT ON NOVEMBER 5
Right Hon. W. L. MACKENZIE KING (Prime Minister):	Mr. Speaker, I have in
my hand copies of some communications which passed between the leader of the opposition (Mr. Hanson) and myself with respect to adjournment or prorogation of parliament on November 5. My hon. friend is agreeable, as I am, to having these communications tabled. I would suggest that they, too, appear in votes and proceedings, so that hon. members may have an opportunity to read them.
REMEMBRANCE DAY-—ADJOURNMENT FOR OBSERVANCE OF STATUTORY HOLIDAY
Right Hon. W. L. MACKENZIE KING (Prime Minister): Mr. Speaker, it has been intimated to me that, in view of the fact Monday next is the statutory holiday known as remembrance day, a number of hon. members would like to be present in their constituencies on that day. I am advised, too, that the legion would appreciate it very much if parliament should make that possible by not sitting on Monday next, unless such sitting were absolutely imperative.
In the circumstances I move:
That when the house adjourns on Friday, November 8, it stand adjourned until Tuesday, November 12.
Hon. R. B. HANSON (Leader of the Opposition): Mr. Speaker, I realize that this motion is not debatable. I rise not for the
3
NOVEMBER 7, 1940
The Address—Mr. Claxton
purpose of debating it but rather for that of expressing concurrence. In addition to the matter of the convenience of hon. members there is the point that remembrance day is a national and statutory holiday, and one dear to the hearts of a large number of our people. In my view it is most fitting that the house should adjourn for the purpose of observing remembrance day.
Motion agreed to.
ACTING DEPUTY SERGEANT-AT-ARMS
Mr. SPEAKER:	I have the honour to
inform the house that I have appointed J. Laundy to be acting deputy sergeant-at-arms during the present session.
On motion of Mr. Mackenzie King the house adjourned at 3.55 p.m.
Friday, November 8, 1940.
