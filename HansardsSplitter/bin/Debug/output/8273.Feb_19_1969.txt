Ssa

CANADA
HOUSE OF COMMONS DEBATES
OFFICIAL REPORT
FIRST SESSION—TWENTY EIGHTH PARLIAMENT
17 ELIZABETH II
VOLUME VI, 1969
COMPRISING THE PERIOD FROM THE NINETEENTH DAY OF FEBRUARY 1969 TO THE NINETEENTH DAY OF MARCH, 1969, INCLUSIVE.
INDEX ISSUED IN A SEPARATE VOLUME
29180—3611
THE QUEEN’S PRINTER, OTTAWA, 1969
5695
HOUSE OF COMMONS
Wednesday, February 19, 1969
