 at 3 p.m.
Prayers
After routine, which included the presentation of several petitions, and a report from the Committee on Banking and Commerce,
* * *
VOTE BY BALLOT
Mr. TREMBLAY introduced a Bill to provide for taking the poll at Parliamentary Elections by ballot, and it received first reading.
* * *
SUN INSURANCE COMPANY
Mr. WORKMAN introduced a Bill to amend the Act incorporating the Sun Insurance Company, and it was read a first time and referred to the Committee on Banking and Commerce.
* * *
QUEBEC PORT WARDEN
Hon. Sir FRANCIS HINCKS moved that the House go into Committee of the Whole on Friday next, to consider certain resolutions providing for the appointment of a Port Warden, for the Harbour of Quebec, &c. He explained that he wished to give to the City of Quebec, a system similar to that which now prevailed in Montreal, and which had been found to work exceedingly well.
The motion was carried.
* * *
BANK OF UPPER CANADA
Hon. Sir FRANCIS HINCKS moved that on Friday next, the House be resolved into Committee of the Whole, to amend the Act providing for the settlement of the affairs of the Bank of Upper Canada. He explained that the object of this measure was to give
the Government power to make advances from the Consolidated Fund, on most undoubted securities, viz: mortgages at seven per cent interest, to enable them to pay off a small number of creditors of the bank. He would give further explanations on moving the House into Committee.
The motion was carried.
* * *
HUDSON’S BAY COMPANY
Hon. Sir FRANCIS HINCKS moved that the House do on Friday next go into Committee of the Whole to consider the following Resolution:
That it is expedient to provide, that the loan of one million four hundred and sixty thousand dollars, or three hundred thousand pounds sterling, raised in England, with the guarantee of the Imperial Government for the payment of the interest thereon, under the authority of the Act of Canada 32 and 33 Vic., Cap. 1, for the purpose of paying a like sum to Hudson’s Bay Company, for the purposes set forth in the said Act,—be made the next charge on the consolidated Revenue Fund of Canada, after any charge thereon created or to be created thereon, under the Act of Canada passed in the 31st year of Her Majesty’s Reign, Chapter 41, for any loan for fortifications; and that further provision be made with respect to the loan first above mentioned in conformity to the requirements of the Act of the Imperial Parliament, 32 and 33 Vic. Cap. 101, under which the guarantee of the Imperial Government was given for the payment of the interest on the said loan.
Hon. Mr. HOLTON said the Government had informed the House that the loan had not yet been effected, while the wording of the resolution would indicate that it had.
Hon. Sir FRANCIS HINCKS said as far as the Imperial Government was concerned, it was virtually effected. It had passed out of their hands, and therefore this resolution was necessary.
Hon. Mr. HOLTON suggested that the wording of the resolution be changed so as to bring it into exact conformity with the facts of the case.
Hon. Sir FRANCIS HINCKS was obliged for the suggestion.
In reply to Hon. Sir A.T. Galt, Hon. Sir GEORGE-E.
CARTIER said there had as yet been no steps taken to raise the loan for fortifications.
COMMONS DEBATES
240
March 22, 1871
The motion was passed.
* * *
INDIAN RESERVES
Hon. Mr. HOWE introduced a Bill to prolong, for a limited period, the time allowed for the redemption of the lands reserved for Indians in the township of Dundee.
* * *
CUSTOMS ACT
The Act to amend the Act relating to the duties of Customs was read a second time and passed through Committee of the Whole.
On the motion for a third reading of the Bill tomorrow,
Hon. Mr. HOLTON moved an amendment that the Bill be referred back to the Committee of the Whole forthwith, for the purpose of so amending the same as to repeal the duties on coal, coke, flour and wheat.
Hon. Sir FRANCIS HINCKS said this was a most inopportune time to bring up this motion while the Joint High Commission was in session. Of course it was impossible to say what that body was discussing, but it was highly probable that they were dealing with this question. He hoped, therefore, that this motion would not be pressed until the result of the Commission should be made known.
Hon. Mr. HOLTON said it was evident that the Government had given up the measure on its merits, and could only plead for delay. The question was whether in the light of the past it was advisable to retain these duties or not. He believed that no good reason could be urged for the tax, and he therefore would press his motion.
Hon. Sir FRANCIS HINCKS wished to explain that his only reason for not discussing the question on its merits was because he considered that it was quite unnecessary and wholly out of place to do so just at present.
Hon. Sir GEORGE-E. CARTIER said these duties had already conferred great benefits on Canada, especially on Nova Scotia and Ontario. The Joint High Commission was no doubt discussing this as well as other questions affecting this country, and this country could afford to wait a while before repealing this duty. Already the United States Government had repealed their duty on coal.
Hon. Mr. HOLTON said that they were induced to do so, through no action on the part of the Dominion, but simply because they wished to repeal an absurd duty.
Hon. Sir GEORGE-E. CARTIER said nevertheless the action of the Dominion Government had been referred to in the debate in Congress, although he did not suppose that it had influenced the American Government to any extent.
Mr. WORKMAN said this duty had caused a great deal of trouble and had brought no commensurate return to the country. He did not approve of giving so much power to the Government. It was a dangerous power to give to them, and might be used for dangerous purposes. Let Parliament deal with the question in the usual way. He referred to the action of the American Government in remitting the duty on coal, and said it was absurd to suppose that any action of our Government had influenced them to do so.
Hon. Mr. TILLEY said that the Northern Transportation Co. had addressed the Canadian Government asking them why they had placed this oppressive duty on their vessels. The answer returned was that Canada had placed no higher duties on American products than the United States had on those from Canada. Similar communications had been read from other American companies, and the fact was that it had brought our neighbors to see that it was necessary to treat Canada liberally if they would themselves be dealt with in a similar manner.
Mr. MAGILL spoke in favour of the amendment and trusted that the Ministry would consent to the renewal of the duties on coal.
Mr. BEATY was opposed to the tax upon coal. It could not be defended. It was unjust because it was unequal in its operation. It pressed heavily on Ontario while other Provinces were not injured by it. He hoped it would be repealed, for he believed coal should be placed at the lowest possible price, and within the reach of the poor.
Mr. OLIVER quite agreed with the hon. member for Toronto East. The coal tax was a grievous burden to Ontario, as the duty on flour was to the people of the Maritime Provinces. He hoped this tax on the necessaries of life would be repealed.
Hon. Mr. TUPPER did not think that too great importance could be attached to the injury which could not fail to arise from the discussion on this motion. Still, he thought even greater injury might be brought on this country if the statements made during this discussion were allowed to pass unchallenged. When this measure was introduced last year, it was objected to on the grounds that it would provoke retaliation on the part of the American Government. But it had not been attended by any of the results that were feared by hon. members opposite. He would remind those who said that the duty on coal in the United States had not been repealed through the course pursued by this Government, that although efforts had frequently been made to remove the tax, it had not succeeded until the Canadian Government imposed the duty on coal and flour. If the action of the American Government was not propter hoc, it was, at least post hoc. He referred to the effect of the coal tax, and said that the trade in coal in Nova Scotia had increased one-sixth since the imposition of the duty. The export to Ontario and Quebec had increased within the same period fifty per cent. The increased
COMMONS DEBATES
March 22, 1871
241
competition had so cheapened the article that it had not added a cent to the cost of it to the people.
Mr. WORKMAN: Does the hon. member mean to say that coal is not dearer in Canada now than it was before?
Hon. Mr. TUPPER said it had shown the Pennsylvania monopolists that Canada was independent of them. No one could question the soundness of the policy which had by the imposition of this trivial tax, relieved this country from the power of the coal miners of Pennsylvania. The action of Congress went to prove that a wise and more politic course had never been taken, and this had been done without, in the slightest degree, oppressing the people of the Dominion. The hon. member for Oxford North had undertaken to show that the imposition of the duty on flour was of no value whatever to the produce of Ontario, and at the same time it had largely increased the cost of flour in the Maritime Provinces. That increased cost had arisen from exceptional circumstances, however, and there had certainly been an amount of benefit to the producer. The House had decided when the duties were imposed that the Policy was judicious.
Referring to the protection of the Fisheries, that protection had in a single year fostered and stimulated two great staple interests of the Dominion, the Fishing and the Shipping Interests, which had attained an amount of success unparallelled hitherto, and the fishers had been enabled, notwithstanding the enormous duties imposed by the United States, to compete in the American markets. The result had been that the highest executive authority in the United States had drawn the attention of the Parliament of that country to the great importance to them of obtaining the enjoyment of the Canadian Fisheries. Knowing, however, the indefeasible right of Canada to her Fisheries, and knowing her determination and that of England, to protect them, it was well understood by the Americans that they would not be able to gain access to the Fisheries without an equivalent.
Of course, nothing was known of the action or intention of the High Commission at Washington, but he would ask the House, including those who had been most forward in opposing the whole protective policy from its initiation, at the present crisis, and in view of the possible action of the High Commission not to do anything, either by discussion or even discussion that might weaken the hands of the representative of Canada, in obtaining terms which he could not obtain otherwise—even if it had not been proved that the Policy was wise and judicious, even if the reverse had been shown, was it just that the House should take away from Canada’s representative, the power to offer any return to the advantages America might offer to concede? He asked that the question should be dealt with as it affected the interests of the whole Dominion, and that nothing should be done which might induce the Americans to entertain the mistaken impression that Canada’s future prosperity was dependent on the policy of the United States.
Mr. WORKMAN said that anthracite coal must be imported from the United States, and consequently this duty did increase the cost of it.
Hon. Sir A.T. GALT did not think that the Hon. President of the Council had any provocation for taking the House to task in the manner which he had just done. While several hon. members had spoken against the duty, none excepting members of the Government had attempted to say one word in favor of it. In reply to his hon. friend’s argument that this system would foster the coal interest in Canada, and give cheap fuel to the people, he would simply point to the effect of the same policy in the United States. There it had created such a monopoly that coal had been immensely increased in value, until at last the Government was obliged to repeal the duty.
Hon. Sir FRANCIS HINCKS: But the duty is higher there.
Hon. Sir A.T. GALT said clearly then, if the system was a good one, the greater the protection afforded, the greater the advantage to the country. (Hear, hear.) He believed that this discussion would not in any way interfere with the Joint High Commission, but it could not fail to have a beneficial effect on the public mind in the United States by showing the public sentiment of Canada against the tax.
Mr. HARRISON said the duty had been tried, and he believed it was objectionable in every respect. As represented by the hon. member for Toronto (City) East, it was an unequal tax, pressing heavily upon Toronto, and especially upon people residing in cities. He did not believe it was calculated to foster our trade. The proper way to do that was to enlarge our canals. (Hear, hear.) He was not opposed to the tax on American vessels. That was a tax on foreigners, and there was no analogy between that and this tax on the necessaries of life. The duty had been given a fair trial, and it was now time to repeal it. (Hear, hear.)
Mr. CARTWRIGHT suggested that in view of the events of last year, the debate be adjourned until half-past seven, when no doubt the Government would announce a satisfactory policy. (Laughter.)
Mr. MILLS criticised the speech of the President of the Council, and said that the result of the elections this week had shown that this policy was not approved of in Ontario.
Hon. Mr. GRAY defended the duty on the ground that a national policy should be adopted, even though it should be found obnoxious to the people at the outset.
Mr. COLBY did not think one year’s trial of this tax was sufficient. But even though it had been it would not be advisable to make any change at present.
COMMONS DEBATES
242
March 22, 1871
Mr. BLANCHE T moved an amendment to the amendment, to the effect that the duty be removed also from salt, beans, barley, rye, oats, Indian corn, buckwheat, and all grains, Indian meal, oatmeal, and flour of every other grain. He said that he had supported the Government policy last year, and when he returned home he found that he had been received in a rather cool manner by his constituents in consequence. He believed that the tax had been tried long enough to show that it was obnoxious to the people. (Cheers.)
Hon. Mr. HOLTON was glad that the hon. member had supplemented the items to be included in the free list, and he would adopt it without hesitation. He (Hon. Mr. Holton) believed that the hon. member was only the exponent of the changed view of the Government on the subject. He inferred so from seeing the hon. gentleman conferring with the Government before proposing the amendment.
Hon. Sir FRANCIS HINCKS said the hon. member for Chateauguay was wrong in his inference, but he (Hon. Sir Francis Hincks) could say for the Government that if any articles of this description were to be admitted free, it should be en bloc and not a few articles selected by hon. members opposite, who wished to do a little log-rolling; therefore the Government would oppose the motion of the hon. member for Chateauguay but they would vote for the amendment to the amendment. (Cheers and laughter.) If any change was to be made at all, it should be a sweeping one and not extended to one or two articles merely. (Cheers.)
Mr. KIRKPATRICK was opposed to the duties on their merits, but if the Government put the question on the ground that the removal of these duties would be prejudicial to the country he would oppose both motions. He could not see how the Government could oppose the motion of the hon. member for Chateauguay and vote for the amendment to it.
Mr. JONES (Halifax) said he would support the motion for the hon. member for Chateauguay, believing that the policy of the Government was opposed to the interests of the country.
Hon. Mr. HOWE defended the course of the Government with respect to the imposition of the duty and their active protection of the Fisheries. He said it had not only drawn the attention of American statesmen to the importance of coming to some arrangement with the Dominion, and it had forced upon the attention of Great Britain, the necessity for putting an end to these as well as other international questions between Canada and the United States. The result of this was the appointment of the Joint High Commission and the question was, was it advisable while that Commission was in Session, to make any alterations in our tariff.
Mr. WHITEHEAD hoped the Government would support the stand they had taken last year. It might not be approved of by the
cities, but the country at large was not opposed to it and it was the country that made the towns.
Mr. RYAN (Montreal West) said that he had been entirely opposed to the imposition of the duties from the outset. If the Government intended to support the amendment of the hon. member for Levis for the purpose of defeating the motion of the hon. member for Chateauguay, he warned them that he would not support them in such a policy. Less coal had been imported into Ontario last year than previously.
Hon. Sir FRANCIS HINCKS said the policy of the Government was that it was inexpedient to make the change just now.
Mr. CAMERON (Huron South) and the Hon. Mr. ABBOTT
rose at the same time to speak, amid cries of ‘‘vote, vote.’’
The House rose for recess at six o’clock.
AFTER RECESS
FREDERICTON AND ST. MARY’S BRIDGE COMPANY
Mr. PICKARD moved the second reading of Bill No. 24, an Act to incorporate the Fredericton and St. Mary’s Bridge Company, as amended by Committee on Private Bills.
Hon. Mr. MORRIS thought the Bill should be referred to the Railway Committee, but had no particular objection to the Bill.
Mr. HARRISON said the main object of the Bill was to build a Bridge, allowing Railways to use it.
Mr. PICKARD explained the object of the Bill. The proposed bridge would be a link in the line of communication between the Provinces, and would do much to strengthen the commerical union of the different parts of the Dominion.
Hon. Mr. HOLTON spoke as to the proper way in which the Bill should be dealt with. He did not think there was anything to prevent its being considered.
The SPEAKER ruled that the motion should be proceeded with.
Motion carried, and Bill referred to a Committee of the Whole forthwith.
The House then went into Committee, Mr. HARRISON in the chair.
COMMONS DEBATES
March 22, 1871
243
The Bill passed through the Committee, to be read a third time tomorrow.
* * *
CUSTOMS DUTIES
The debate was resumed by
Mr. GIBBS: He had advocated the imposition of the duties at the previous session as tending to procure Reciprocity. The policy had not been sufficiently long in operation to enable anyone to see whether it was desirable or not,—and no one could found an argument on what effect had yet been produced. The question was whether they should deliberately weaken the hands of the representative of Canada at Washington, by following the course proposed by the hon. member for Chateauguay. Whatever his private views might be he would waive them rather than do so— and he should vote against both amendments. The policy inaugurated was a whole and should not be dealt with in part.
Mr. CAMERON (Huron South) said there appeared to be a want of harmony amongst the hon. members on the Treasury Benches on this subject. How then could the House be expected to stultify themselves by voting for one amendment and against the other. The hon. member for Toronto (City) West, notwithstanding his speech today, voted against exactly the same motion when proposed by the hon. member for Chateauguay last year. He (Mr. Cameron) would vote against both motions, and thus sustain the policy of the Government as he had done last session. He regarded that policy as a sound one, and he knew that, so far as his own county was concerned, the people would sustain it. The effect of it was that the Americans had come down from the high position they had occupied and were now prepared to deal fairly and equitably with Canada, and those in this country who were engaged in the carrying trade between the two countries found that they could enter into it with something like fairness and justice. That result was mainly owing to the stand taken by our Government, and until the result of the High Commission should be known, it would not be advisable to make any change in the tariff.
Mr. HARRISON defended himself from the attacks of the hon. member for Huron. He (Mr. Harrison) had voted for that tax last year, though he was opposed to the principle, believing that good would come of it. He had simply voted for it to give it a fair trial. That trial had proved it to be a bad policy for this country, and he now voted for the repeal of the tax.
Mr. BLAKE said he was glad that the hon. member for Toronto West had discovered that out of evil bad alone can come.
Mr. JACKSON supported the policy of the Government, believing that it was inopportune to make any changes in the tariff at present.
Mr. BEATY explained how he had voted for the imposition of the tax last year. He voted in favour of the general policy with the
understanding that coal was	not to be included. When the report
came up for concurrence he	was absent from the House and that
was why his vote was not recorded against the imposition of a duty on coal.	
A vote was then taken on the amendment of Mr. BLANCHET, which was carried: YEAS Messieurs	
Abbot	Anglin
Archambault	Barthe
Beaty	Beaubien
Bechard	Bellerose
Benoit	Blake
Blanchet	Bolton
Bourassa	Brousseau
Burpee	Cameron (Inverness)
Caron	Cartwright
Cheval	Chipman
Cimon	Coffin
Costigan	Coupal
Crawford (Leeds South)	Currier
Daoust	Delorme
Dufresne	Dunkin
Ferris	Forbes
Fortin	Fournier
Gaudet	Geoffrion
Gendron	Godin
Grant	Hagar
Harrison	Hincks (Sir Francis)
Holmes	Holton
Howe	Irvine
Jones (Halifax)	Kempt
Killam	Lacerte
Langevin	Langlois
Macdonald (Glengarry)	Macdonald (Antigonish)
Magill	Masson (Soulanges)
Masson (Terrebonne)	McDougall (Lanark North)
McDougall (Trois-Rivieres)	McMillan
McMonies	Merritt
Mills	Moffatt
Morris	Morison (Victoria North)
Morrison (Niagara)	Oliver
Paquet	Pearson
Pelletier	Pickard
Pouliot	Pozer
Ray	Renaud
Robitaille	Ross (Champlain)
Ross (Victoria)	Ross (Wellington Centre)
Ryan (King’s, N.B.)	Ryan (Montreal West)
Savary	Scatcherd
Simard	Simpson
Smith	Snider
Stirton	Sylvain
Thompson (Haldimand)	Thompson (Ontario North)
Tilley	Tourangeau
Tremblay	Tupper
Wallace	White
Workman Wright (York West)—101	Wright (Ottawa County) NAYS Messieurs
Ault	Baker
Bertrand	Bown
Cameron (Huron South)	Campbell
Colby	Crawford (Brockville)
Dobbie	Gibbs
Gray	Grover
Jackson	Jones (Leeds North and Grenville North)
Keeler	Lapum
Lawson	McDonald (Lunenburg)
McKeagney	Perry
Pinsonneault	Ross (Dundas)
Scriver	Shanly
Street	Webb
Whitehead	Willson—28
COMMONS DEBATES
244
March 22, 1871
Hon. Sir FRANCIS HINCKS said that the position of the Government was this (and they were a unit on the subject) that they deprecated at the present time any interference with the commercial policy of the country with regard to their duties. But at the same time when they found that a number of hon. gentlemen who supported the Government avowed their intention to support a motion which embraced certain particular articles which were subject to duty along with other articles, they certainly did think that all ought to be put in the same position, and that the House should vote upon the whole of them. But with regard to the whole of them, the Government were a unit in opposing the amendment as now amended. He would say further that when the Government were called upon to consider at an early period when they reduced the taxation, this question engaged their anxious consideration and the conclusion at which the Government had arrived was this—to maintain these duties, not on the abstract merits of them nor on the ground of revenue, but on the ground that it was not expedient during the present negotiations at Washington to interfere with them. He was perfectly certain in his own mind that it was not in the interest of the Dominion that these duties should be interfered with at present.
Hon. Mr. HOLTON said that unless the Hon. Finance Minister was prepared to tell the House that this question was definitely before the Commission, he could not withdraw his motion. The hon. gentleman could not—dared not say so, and he (Hon. Mr. Holton) did not believe that it was.
Hon. Sir FRANCIS HINCKS said he had just received since six o’clock a telegram from the first Minister of the Dominion at Washington to say that duties on coal and salt would not be taken off until December. He repeated at some length his arguments against removing the duties at present.
Hon. Mr. HOLTON replied denying that he was disposed to yield anything to the United States. He opposed these duties because he believed them to be prejudicial to the interests of the Dominion.
Hon. Sir GEORGE-E. CARTIER said the Government had only voted for the amendment of the hon. member for Levis in order to place the whole question fairly before the House, and not allow the question to be on a few articles only. Referring to what had been said by the members for Montreal, he was quite ready to explain why he had voted for duty on coal,—he had done so to encourage interprovincial trade. He referred to the position of each Province as regarded the duties, showing that the producers in Ontario were benefited, and Quebec was specially interested in the maintenance of the duties. He (Hon. Sir George-E. Cartier) might once have had to apologize for opposing the hon. member for Chateauguay, but the case might be reversed, and that hon. member might have to go to his constituents and apologize for having refused to protect the agricultural interests of his Province. The mineral possessions of Nova Scotia would be developed and utilized, and New Brunswick, though not so directly benefited, as
the other Provinces, received great indirect advantages from the increased prosperity of the other parts of the Dominion.
He then referred to the High Commission, and said that although nothing definite was known, it was certainly possible that, as in 1854, the Fishery question might bring up other matters of commercial interest to the two countries, and as the Governor in Council had power to do away with the duties on receiving equivalent benefits from the States, why should not the Government be trusted. That was the position of the Government; they had only voted in favour of the amendment of the member for Levis in order to bring the matter before the House in its entirety. He then repeated his remarks in French.
Mr. WORKMAN said he had opposed the policy of the Government when it had commenced, and he did so still. He believed the Government measure to be both wrong and ridiculous. The Fishery question had no connection whatever with the matter. There was no doubt whatever that the duty had increased the price of coal. It was absurd to imagine that the protective duties before the House would have any effect in obtaining Reciprocity. If the majority of the House was against the Government the Government ought to admit it, and not stoop to the miserable subterfuges to which they had been reduced, and he for one was determined not to be whipped in by anything the Minister of Militia might say.
Hon. Sir GEORGE-E. CARTIER had no desire to whip in either the member for Montreal or any other member of the House, and he appealed to the House that he had never attempted anything of the sort.
Hon. Mr. ANGLIN thought that those who voted with the Government on the previous decision should consider well their position. The amendment which had been carried was to all intents and purposes a Government motion and the Government were pledged to support it, and he did not see how anyone could consistently vote in favour of putting the various articles on the free list, and a few hours afterwards reverse their vote. The Minister of Militia in asking the House to leave the matter in the hands of the Government, asked them in point of fact to place themselves entirely dependent on the action of the Government at Washington. He maintained that the United States should not be considered, but that Canadian interests alone should be consulted. He protested against the fishery question being in any way mixed up with the matter. The increase in the coal trade of Nova Scotia had in no way arisen from the duty imposed on it. The tax on flour was most odious and intolerable to the people of New Brunswick. He gave his unqualified support not only to the motion of the member for Chateauguay but to the more enlarged motion of the Government, expressed through their supporter, the member for Levis.
Mr. COLBY said there was no doubt that Canada desired better trade relations with the States. For years subsequently to the abrogation of the Reciprocity Treaty the policy of the Government had not been framed to attain that object, but latterly, pressure had been brought on the Government, and they determined to treat the
COMMONS DEBATES
March 22, 1871
245
United States in the same way as they were treated. He was convinced that reciprocity with the States could only be obtained as a matter of bargain, and everything should be done that would strengthen the hands of those in the United States, who were willing to concede Reciprocity, and not sacrifice everything that could be given as an equivalent. The natural sequence of the High Commission would be the re-opening of trade negotiations, and the offering of some equivalent on the part of the United States in return for the enjoyment of the Canadian Fisheries. He moved an amendment, that all the words in the previous amendment be struck out, and that there be substituted, that it is inexpedient during the present Session of Parliament to make any alteration in the customs duties, on coal, coke, wheat, flour, salt, peas, beans, barley, rye, oats, meals, &c., &c.
Hon. Mr. McDOUGALL (Lanark North) raised the point of order that the amendment was a simple negative of the motion before the House, and was therefore out of order.
The SPEAKER ruled the amendment in order.
Hon. Mr. HOLTON thought the House had already expressed its opinion, and could scarcely change its decision at the present stage.
Hon. Mr. DUNKIN maintained that the House had simply changed the motion before the House but was not committed to a single item.
Hon. Mr. HOLTON thought the House had affirmed the expediency of going into committee on the amended proposition.
The SPEAKER ruled that the House had simply decided that certain words should be added to the first amendment, but that amendment had not been disposed of.
Hon. Mr. GRAY seconded the motion of the member for Stanstead, and said he sustained the policy on the ground of its being National, and on the ground that under any circumstances, the present was a very inexpedient time to make any change.
Hon. Mr. SMITH (Westmorland) said that on the present occasion the member for St. John, hardly represented nineteen-twentieths of the people of New Brunswick. Those people were unanimously opposed to the duties and would do anything to get rid of it. The duties might appear beneficial to Ontario, Quebec and Nova Scotia, but New Brunswick certainly suffered from the duty on each article. As to the High Commission, there was not the least evidence to show that that body would consider the question, and therefore, it could not be said that it was inexpedient to discuss the matter now on that account. If he felt that the country would be at all prejudiced by a discussion at the present time, he would certainly oppose it, but he could not think that anything of the sort could be the case.
Mr. MACDONALD (Glengarry) thought that the Opposition from Ontario could not be charged with want of consideration for the Lower Provinces, for a large majority had supported the repeal of the duties and in every measure that had passed the House, the Ontario Opposition had invariably assisted the Lower Provinces. Ontario was totally opposed to make bread dearer to the Lower Provinces, and if the coal duty was a benefit to Nova Scotia, it was a direct tax on Ontario. It was the most injurious tax that had ever been levied, and he trusted it would be wiped off the Statute Book.
Mr. ROSS (Prince Edward) wanted to speak in the interests of the farmers of the Dominion, and was ready to support the Government in retaining the protective duties. He was in favour of reciprocity, but in the absence of reciprocity the farmers should be protected.
Mr. GEOFFRION spoke in French in opposition to the Government.
Hon. Mr. HOLTON again spoke on the motion of order. He quoted authority to show that the amendment of the member for Stanstead was out of order.
Hon. Mr. DUNKIN maintained that the practice of the House was entirely opposed to the authority quoted, and quoted an instance similar to the present.
Mr. BLAKE maintained that English practice ruled in the House, and that the amendment was clearly out of order.
Hon. Sir GEORGE-E. CARTIER maintained that the amendment of the member for Chateauguay, as amended, had not been decided upon.
The SPEAKER ruled that certain words having been added, they could not be struck out, and the amendment of the member for Stanstead was therefore out of order.
Mr. PICKARD said that New Brunswick was almost unanimously opposed to the duties.
Mr. CURRIER moved an amendment that pork should be added to the free list.
Hon. Sir FRANCIS HINCKS trusted the hon. member would not press his amendment. Pork was an article on which there had long been a duty, and there had been no petitions for the repeal. The hon. member was really trifling with the tariff.
Mr. WRIGHT (Ottawa County) hoped the Minister of Finance would see the importance of repealing the duty on Pork.
COMMONS DEBATES
246
Mr. ROSS (Prince Edward) thought the policy of the restriction duties met with general approbation and was sorry the Government had not met the proposal of the hon. member for Chateauguay fairly and voted it down.
Hon. Sir FRANCIS HINCKS said that the amount of revenue derived from pork was $60,000.
Mr. HARRISON thought that pork should not be included, unless the entire tariff was abolished; the line must be drawn somewhere.
On the suggestion of Hon. Mr. HOLTON, Mr. Currier’s amendment was withdrawn.
Mr. McDONALD (Lunenburg) said that if pork was freed, other articles might be brought forward, until there was no tariff. He did not think the protective policy had been sufficiently tried, and he should support its being maintained. The duties before the House did not stand alone, but were only part of a large policy, and if part was repealed, the whole should go. The commercial prosperity of the country was certainly an evidence of the good effect of the policy commenced last year, and at the present time it was certainly inexpedient to interfere with it, and the House was bound to respect the statement of the Government, that the discussion might be prejudicial to the good result of the High Commission.
The motion of the member for Chateauguay as amended was then put with the following result: Yeas, 83; Nays, 55.
	YEAS
	Messieurs
Anglin	Barthe
Beaty	Beaubien
Bechard	Benoit
Blake	Blanchet
Bolton	Bourassa
Bowman	Brousseau
Burpee	Caron
Cartwright	Cheval
Chipman	Cimon
Coffin	Coupal
Crawford (Leeds South)	Currier
Delorme	Dufresne
Ferris	Forbes
Fortier	Fournier
Galt (Sir A.T.)	Gaudet
Geoffrion	Gendron
Godin	Hagar
Harrison	Holton
Irvine	Jones (Halifax)
Kempt	Killam
Kirkpatrick	Lacerte
Langlois	Little
Macdonald (Glengarry)	Magill
Masson (Soulanges)	Masson (Terrebonne)
McConkey	McDougall (Lanark North)
March 22, 1871
McDougall (Renfrew South)	McMonies
Merritt	Mills
Moffatt	Morison (Victoria North)
Oliver	Paquet
Pearson	Pelletier
Pickard	Pouliot
Pozer	Ray
Redford	Renaud
Ross (Champlain)	Ross (Victoria)
Ross (Wellington Centre)	Ryan (Montreal West)
Scatcherd	Smith
Snider	Stirton
Thompson (Haldimand)	Thompson (Ontario North)
Tourangeau	Tremblay
Wallace	Wells
Workman	Wright (Ottawa County)
Wright (York West)—83	NAYS
	Messieurs
Archambault	Ault
Baker	Bellerose
Bertrand	Bown
Cameron (Huron South)	Campbell
Cartier (Sir George-E.)	Colby
Costigan	Crawford (Brockville)
Daoust	Dobbie
Dunkin	Fortin
Gaucher	Gibbs
Grant	Gray
Grover	Hincks (Sir Francis)
Holmes	Howe
Jackson	Jones (Leeds North and Grenville North)
Keeler	Langevin
Lapum	Lawson
McDonald (Antigonish)	McDonald (Lunenburg)
McDougall (Trois-Rivieres)	McKeagney
McMillan	Morris
Morrison (Niagara)	Perry
Pinsonneault	Robitaille
Ross (Dundas)	Ross (Prince Edward)
Ryan (King’s, N.B.)	Savary
Scriver	Simard
Simpson	Street
Sylvain	Tilley
Tupper	Webb
White Willson—55	Whitehead
The House then went into Committee, Mr. MILLS in the chair, to consider amending the Bill to repeal the duties on the items listed in the resolutions passed.
Hon. Sir FRANCIS HINCKS suggested that time should be given to frame a proper amendment.
Hon. Mr. HOLTON assented and the Committee reported progress and asked leave to sit again.
The House adjourned at 12.45.
COMMONS DEBATES
March 23, 1871
247
HOUSE OF COMMONS
Thursday, March 23, 1871
