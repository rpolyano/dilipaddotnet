﻿using RegExTransformer;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace TopicFrequencyAnalyzer
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Drag file/folder:");
            string path = Console.ReadLine();
            path = path.Trim('"');

            bool isDir = (File.GetAttributes(path) & FileAttributes.Directory) == FileAttributes.Directory;

            String[] files = isDir ? Directory.GetFiles(path, "*.txt") : new string[] { path };
            List<String[]> allTopics = new List<string[]>();
            Regex topicRegex = new Regex(new Regex2().GetTopicPattern(), RegexOptions.Compiled | RegexOptions.Multiline);
            
            foreach (string file in files)
            {
                Console.WriteLine("Extra for " + Path.GetFileNameWithoutExtension(file) + "...");
                String text = File.ReadAllText(file);
                text = new Regex2().PreProccess(text);
                string[] topics = topicRegex.Matches(text)
                    .Cast<Match>()
                    .Select(
                        match => match
                            .Value
                            .ToLower()
                    //.Split(' ')
                            )
                    .Distinct()
                    .ToArray();
                allTopics.Add(topics);
            }
            Dictionary<string, int> counts = new Dictionary<string, int>();
            foreach (var doc in allTopics)
            {
                foreach (var topic in doc)
                {
                    if (counts.ContainsKey(topic))
                        counts[topic] += 1;
                    else
                        counts[topic] = 1;
                }
            }
            var ordered = counts.OrderByDescending(kvp => kvp.Value);
            string output = "";
            foreach (var kvp in ordered)
            {
                output += kvp.Key + " " + kvp.Value + "\n";
            }
            File.WriteAllText("topics.txt", output);
        }
    }
}
