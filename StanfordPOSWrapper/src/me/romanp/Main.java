package me.romanp;

import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.ling.Sentence;
import edu.stanford.nlp.ling.TaggedWord;
import edu.stanford.nlp.tagger.maxent.MaxentTagger;
import edu.stanford.nlp.tagger.maxent.TaggerConfig;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        MaxentTagger tagger = new MaxentTagger(args[0]);
        String line = "";
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            while (!(line = reader.readLine()).equals(""))
            {
                List<List<HasWord>> sentences = MaxentTagger.tokenizeText(new StringReader(line));
                List<HasWord> sentence = sentences.get(0);
                List<TaggedWord> taggedWords = tagger.tagSentence(sentence, false);
                String output = Sentence.listToString(taggedWords, false);
                System.out.println(output);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
