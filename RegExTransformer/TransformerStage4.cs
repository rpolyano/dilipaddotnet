﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace RegExTransformer
{
    public class TransformerStage4
    {
        public static int TOTAL_COUNT = 0;
        public static int UNLINKED_COUNT = 0;
        public static int FIXED_COUNT = 0;

        XDocument doc;
        String[] docMembersNames;
        Match[] docMembersMatched;
        MiniMember[] docMembersNamesExpanded;
        DateTime date;
        string fileName;
        static Regex NAME_PARSER;// = new Regex(@"^\*?(?<prefix>(?:[A-Z][a-z]+ ?)+\. |Miss |Madam |(?:The )?Acting )(?<firstname>(?:[A-Z][a-z-]+ ?)+? )?(?<initials>(?:[A-Z]\. )*)(?<lastname>(?:(?:[A-Z][a-z-]+|[a-z]{1,3}) ?)+)(?<constit>(?:\(.+?\) ?)*)(?<movement>moved)?", RegexOptions.Compiled);
        static Regex FALLBACK_NAME_PARSER;
        public TransformerStage4(XDocument doc, string filename)
        {
            
            this.doc = doc;
            this.date = DateTime.Parse(filename.Replace('_', '-'));
            this.fileName = filename;
            NAME_PARSER = new Regex(RegexManager.GetPattern(this.date, RegexFeature.NameParser).FullPattern, RegexOptions.Compiled);
            FALLBACK_NAME_PARSER = new Regex(RegexManager.GetPattern(this.date, RegexFeature.NameParser).FullPattern, RegexOptions.Compiled | RegexOptions.IgnoreCase);

        }

        public string Transform()
        {
            
            var speeches = doc.Descendants(TransformerStage3.NAMESPACES["pm"] + "speech");
            speeches = speeches.Union(doc.Descendants(TransformerStage3.NAMESPACES["dp"] + "speech"));
            docMembersNames = speeches
                .OrderBy(speech =>
                    speech.Attribute(TransformerStage3.NAMESPACES["pm"] + "id").Value)
                .Select(speech =>
                    speech.Attribute(TransformerStage3.NAMESPACES["pm"] + "speaker").Value.Trim())
                .ToArray();
            
            docMembersMatched = docMembersNames.Select(name => NAME_PARSER.Match(name)).ToArray();
            DisambiguateMembers();
            if (docMembersNames.Length == 0)
            {
                Log.LogError(fileName, "No speeches found", true);
                return "No speeches";
            }
            string report = docMembersNames.Zip(docMembersNamesExpanded, (one, two) => one + "\t" + two).AggregateBy("\r\n");

            var range = ParliamentarianManager.GetCommonDateRange(docMembersNamesExpanded);
 
            if (!range.Contains(date))
            {
                Log.LogError(fileName, "File date is wrong!");
            }
            FindMetaMembers();
            var relevantMembers = ParliamentarianManager.GetMembersForDateRange(range);

            var korchniski = relevantMembers.Where(mem => mem.member.name.last == "Korchinski");

            MemberInfo.MemberFile[] guids = docMembersNamesExpanded.Select(mem => ParliamentarianManager.MatchMember(mem, date, range, false, relevantMembers)).ToArray();

            var unlinked = docMembersNamesExpanded
                .Select((item,index)=>new { Index=index, Item=item })
                .Where(x=>guids[x.Index] == null && x.Item != MiniMember.EMPTY).Select(x=>x);

            var uniqueUnlinked = unlinked.Select(x => x.Item.ToString()).Where(mem => !mem.ToLower().StartsWith("an hon") && !mem.ToLower().StartsWith("some hon")).ToArray();//.Distinct().ToArray();
            int numUnlinked = uniqueUnlinked.Length;
            Log.LogMessage("Got " + numUnlinked + " unlinked members", fileName, ConsoleColor.Yellow);
            
            

            guids = guids.Select((memfile, index) =>
                {
                    if (memfile == null && docMembersNamesExpanded[index] != MiniMember.EMPTY)
                    {
                        return ParliamentarianManager.MatchMember(docMembersNamesExpanded[index], date, range, true, relevantMembers);
                    }
                    return memfile;
                }).ToArray();

            TOTAL_COUNT += docMembersNamesExpanded.Select(x => x.ToString()).Where(mem => !mem.ToLower().StartsWith("an hon") && !mem.ToLower().StartsWith("some hon")).Count();//Select(x => x.ToString()).Distinct().Count();


            unlinked = docMembersNamesExpanded
                .Select((item, index) => new { Index = index, Item = item })
                .Where(x => guids[x.Index] == null && x.Item != MiniMember.EMPTY).Select(x => x);
            uniqueUnlinked = unlinked.Select(x => x.Item.ToString()).Where(mem => !mem.ToLower().StartsWith("an hon") && !mem.ToLower().StartsWith("some hon")).ToArray();//.Distinct().ToArray();
            int numUnlinkedAfterOCRPass = uniqueUnlinked.Length;
            Log.LogMessage("After OCR correction " + numUnlinkedAfterOCRPass + " unlinked members", fileName, ConsoleColor.Yellow);

            FIXED_COUNT += (numUnlinked - numUnlinkedAfterOCRPass);
            UNLINKED_COUNT += unlinked.Select(x => x.Item.ToString()).Distinct().Count();
            //MemberInfo.MemberFile[] ocrlinked = unlinked.Select(mem => ParliamentarianManager.MatchMember(mem, date, true)).ToArray();

            report = docMembersNames
                .Zip(docMembersNamesExpanded, (one, two) => one + "\t" + two + "\t" + 
                    "Based On " + MatchBasisToString(ParliamentarianManager.GetMatchBasis(two, date)))
                .Zip(guids, (one, two) => one + "\t" + 
                    (two == null ? "NO MATCH" : 
                    two.member.name.first + " " + two.member.name.last + " " + two.member.id))
                .AggregateBy("\r\n");

            string csv = docMembersNames
                .Zip(docMembersNamesExpanded, (one, two) => '"' + one + "\",\"" + two + "\"," +
                    "\"Based On " + MatchBasisToString(ParliamentarianManager.GetMatchBasis(two, date)) + '"')
                .Zip(guids, (one, two) => one + "," +
                    (two == null ? "NO MATCH" :
                    '"' + two.member.name.first + " " + two.member.name.last + " " + two.member.id + '"'))
                .AggregateBy("\r\n");

            return csv;
            //Debugger.Break();

        }

        private string MatchBasisToString(RegExTransformer.ParliamentarianManager.MatchBasis mb)
        {
            string ret = "";
            Type mbType = typeof(RegExTransformer.ParliamentarianManager.MatchBasis);
            foreach (var name in Enum.GetNames(mbType))
            {
                RegExTransformer.ParliamentarianManager.MatchBasis value =
                    (RegExTransformer.ParliamentarianManager.MatchBasis)Enum.Parse(mbType, name);
                if (value != ParliamentarianManager.MatchBasis.None && mb.HasFlag(value))
                {
                    ret += name + ", ";
                }
            }
            return ret.TrimEnd(' ', ',');
        }

        private void FindMetaMembers()
        {
            for (int i = 0; i < docMembersNames.Length; i++)
            {
                //Speaker = Speaker
                //Deputy Speaker = Deputy Speaker
                //Chairman = Deputy Speaker and Chair of Committees
                //Deputy Chairman = Deputy Chair of Committees
                string speakerType = "";
                if (docMembersNames[i].Contains("Speaker"))
                {
                    speakerType = "Speaker";
                    if (docMembersNames[i].Contains("Deputy"))
                    {
                        speakerType = "Deputy " + speakerType;
                    }
                }
                if (docMembersNames[i].Contains("Chair"))
                {
                    speakerType = "Chair(man)? of Committees of the Whole";
                    if (!docMembersNames[i].Contains("Deputy"))
                    {
                        speakerType = "Deputy Speaker and " + speakerType;
                    }
                    else
                    {
                        speakerType = "Deputy " + speakerType;
                    }
                }
                if (speakerType != "")
                {
                    
                    if (docMembersNamesExpanded[i] == MiniMember.EMPTY)
                    {
                        docMembersNamesExpanded[i] = new MiniMember() { Constituency = speakerType, Speaker = true };
                        if (docMembersNames[i].Contains("("))
                        {
                            string info = Regex.Match(docMembersNames[i], @"\((.+)\)").Groups[1].Value;
                        }


                    }
                }
                if (Regex.IsMatch(docMembersNames[i], @"^[^(]+(\sof\s|\sto\s)"))
                {
                    string job = docMembersNames[i]
                        .TakeWhile(chr=>chr!='(')
                        .Select(chr=>chr.ToString())
                        .AggregateBy("").Trim(':');
                    var mp = docMembersNamesExpanded.Where(name =>
                        name.Constituency.StartsWith(job)
                        ).First();
                    docMembersNamesExpanded[i] = mp;
                    Log.LogMessage("Infered MP from job: " + docMembersNames[i] + "=>" + mp, fileName, ConsoleColor.Green);
                    
                }
            }
        }

        List<string> disambiguatedAlready = new List<string>();
        public void DisambiguateMembers()
        {
            docMembersNamesExpanded = new MiniMember[docMembersNames.Length];
            for (int reference = 0; reference < docMembersNames.Length; reference++)
            {
                //if (docMembersNamesExpanded[reference] != null)
                //    continue;

                Match referenceMatch = docMembersMatched[reference];
                if (!referenceMatch.Success)
                {
                    referenceMatch = FALLBACK_NAME_PARSER.Match(docMembersNames[reference]);
                }
                //TODO: No need for ToLower
                string refPrefix = referenceMatch.Groups["prefix"].Value.Trim();
                string refInitials = referenceMatch.Groups["initials"].Value.Trim();
                string refFirstName = referenceMatch.Groups["firstname"].Value.Trim();
                string refLastName = referenceMatch.Groups["lastname"].Value.Trim();
                string refConstit = referenceMatch.Groups["constit"].Value.Trim();
                refConstit = Regex.Match(refConstit, @"\((.+)\)").Groups[1].Value;

                if (refLastName.AlphaEquals("Speaker"))
                {
                    continue;
                }
                for (int instance = 0; instance < docMembersNames.Length; instance++)
                {

                    //if (docMembersNamesExpanded[instance] != null)
                    //    continue;

                    Match instanceMatch = docMembersMatched[instance];
                    if (!instanceMatch.Success)
                    {
                        instanceMatch = FALLBACK_NAME_PARSER.Match(docMembersNames[instance]);
                    }
                    string insPrefix = instanceMatch.Groups["prefix"].Value.Trim();
                    string insInitials = instanceMatch.Groups["initials"].Value.Trim();
                    string insFirstName = instanceMatch.Groups["firstname"].Value.Trim();
                    string insLastName = instanceMatch.Groups["lastname"].Value.Trim();
                    string insConstit = instanceMatch.Groups["constit"].Value.Trim();

                    if (insLastName.AlphaEquals("Speaker"))
                    {
                        continue;
                    }

                    insConstit = Regex.Match(insConstit, @"\((.+)\)").Groups[1].Value;

                    bool prefixMatch = insPrefix.AlphaEquals(refPrefix);
                    bool initialsMatch = insInitials.AlphaEquals(refInitials);
                    bool lastNameMatch = insLastName.AlphaEquals(refLastName);

                    //First names are exactly the same and not empty
                    bool _firstNameDirect = !String.IsNullOrEmpty(insFirstName) && insFirstName.AlphaEquals(refFirstName);
                    
                    //First name not empty, Initials not empty, firstName starts with first initial
                    bool _firstNameInitials = initialsMatch ||
                        (!String.IsNullOrEmpty(insFirstName) && !String.IsNullOrEmpty(refInitials) && insFirstName.StartsWith(refInitials.Substring(0,1)));
                    
                    //Someone does not have a first name or initials
                    bool _firstNameBlank = (String.IsNullOrEmpty(insInitials + "" + insFirstName)) ||
                        (String.IsNullOrEmpty(refInitials + "" + refFirstName));
                    
                    bool firstNameMatch = _firstNameBlank || _firstNameDirect || _firstNameInitials;
                    
                    //Ministers can have constituencies, so if one is a Minister we don't really get any info
                    bool consMatch = insConstit == refConstit || 
                        String.IsNullOrEmpty(insConstit) || String.IsNullOrEmpty(refConstit) ||
                        (Regex.IsMatch(insConstit, @"\sof\s|\sto\s") && !Regex.IsMatch(refConstit, @"\sof\s|\sto\s")); //TODO: Check for safety

                    if (lastNameMatch && firstNameMatch && consMatch)
                    {
                        
                        MiniMember expanded;
                        int repl;
                        string original;
                        if (!String.IsNullOrWhiteSpace(refInitials) ||
                            !String.IsNullOrWhiteSpace(refConstit) ||
                            refPrefix.Length > insPrefix.Length)
                        {

                            expanded = new MiniMember()
                            {
                                Prefix = refPrefix,
                                FirstName = refFirstName,
                                LastName = refLastName,
                                Initials = refInitials,
                                Constituency = refConstit
                            };
                            original = referenceMatch.Value;
                            repl = instance;
                        }
                        else
                        {
                            expanded = new MiniMember()
                            {
                                Prefix = insPrefix,
                                FirstName = insFirstName,
                                LastName = insLastName,
                                Initials = insInitials,
                                Constituency = insConstit
                            };
                            original = instanceMatch.Value;
                            repl = reference;
                        }
                        //expanded = Regex.Replace(expanded, @"\s{2,}", " ").Trim();
                       
                        //expanded = FixCase(expanded, original);
                        if (expanded.ToString().Contains("Acting Speaker"))
                        {
                            expanded = new MiniMember() { Constituency = "ASpeaker " + Regex.Match(expanded.ToString(), @"\((.+)\)").Groups[1].Value };
                        }
                        if (docMembersNamesExpanded[repl] == null ||
                                docMembersNamesExpanded[repl].Length < expanded.Length)
                        {
                            docMembersNamesExpanded[repl] = expanded;
                        }
                    }
                    else if (lastNameMatch && !disambiguatedAlready.Contains(referenceMatch.Value + instanceMatch.Value) && !disambiguatedAlready.Contains(instanceMatch.Value + referenceMatch.Value))
                    {
                        Log.LogMessage("Disambiguated similar: " + referenceMatch.Value + " and " + instanceMatch.Value, fileName);
                        disambiguatedAlready.Add(referenceMatch.Value + instanceMatch.Value);
                    }
                   

                }
            }
        }


        private string FixCase(string expanded, string original)
        {
            string[] origWords = original.Split(" ():".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            string[] words = expanded.Split(" @()".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            foreach (var word in words)
            {
                string origWord = origWords.First(o => o.ToLower().AlphaEquals(word));
                string escWord = Regex.Replace(word, "([^a-zA-Z0-9])", @"\$1");
                expanded = Regex.Replace(expanded, @"(?<=\b)" + escWord + @"(?=\b|\s)", origWord.Trim(':'));
            }
            return expanded;
        }
    }
}
