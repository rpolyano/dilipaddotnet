﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RegExTransformer
{
    public interface IRegexContainer
    {
        int[] GetYearsValidFor();
        string PreProccess(string text, Dictionary<string, string> meta = null);

        string GetTimeStampPattern();
        string GetTopicPattern();
        string GetMPSPeakerPattern();
        string GetOtherSpeakerPattern();
        string GetInterventionPattern();
        string GetQuestionPattern();
        string GetStandingOrderSubtitlesPattern();
        string GetPageNumbersPattern();
        string GetDatesPattern();
        string GetTablesPattern();
        string GetXMLTagPattern();
        string GetExtraCleanPattern();

        string[] GetPrimaryStageDirections();
        string[] GetSubtopic();
        string[] GetMainHeaders();
        string[] GetVotes();
    }
}
