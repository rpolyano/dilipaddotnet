﻿using NLPHelpers;
using RegExTransformer.MemberInfo;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace RegExTransformer
{
    

    public class TransformerStage4_1
    {
        string[] LAST_NAME_DB;
        string[] unmatchedLastnames;
        string[] matchedLastNames;

        List<StringOperation> rules = new List<StringOperation>();

        bool unfiltered = true;
        bool withContext = false;

        public TransformerStage4_1(string stage4Dir)
        {
            List<string> unmatchedln = new List<string>();
            string[] files = Directory.GetFiles(stage4Dir, "*.csv");
            foreach (var file in files)
            {
                string[][] cells = File.ReadAllLines(file)
                    .Select(line => line
                        .Split(new string[] { "\",\"" }, StringSplitOptions.RemoveEmptyEntries)
                        .Select(cell => cell.Trim('"'))
                        .ToArray())
                    .ToArray();
                if (cells[0][0].ToLower() == "no speeches")
                {
                    Log.LogMessage(Path.GetFileNameWithoutExtension(file) + " has no speeches", Path.GetFileNameWithoutExtension(file), ConsoleColor.Yellow);
                    continue;
                }
                unmatchedln.AddRange(cells.Where(row => !row[1].StartsWith("@") && row[2].EndsWith("NO MATCH")).Select(row => row[1]));
            }
            unmatchedLastnames = unmatchedln.Select(GetLastWord).Distinct().ToArray();
            LAST_NAME_DB = ParliamentarianManager.members.Select(mem => mem.member.name.last).Select(GetLastWord).ToArray();
            File.WriteAllLines("names.txt", LAST_NAME_DB);
            matchedLastNames = new string[unmatchedLastnames.Length];
        }

        public MemberFile FindQuick(string lastName)
        {
            return ParliamentarianManager.members.FirstOrDefault(mem => mem.member.name.last.AlphaEquals(lastName, softmatch: true));
        }

        public string GetLastWord(string str)
        {
            str = str.Split('@')[0];//Regex.Replace(str, "[^a-zA-Z. ]", "", RegexOptions.Compiled);;
            return Regex.Split(str, @"\b", RegexOptions.Compiled).Last(w => !String.IsNullOrWhiteSpace(w) && w != "moved");
        }

        public void Transform()
        {

            for (int i = 0; i < unmatchedLastnames.Length; i++)
            {
                var levenshtein = LAST_NAME_DB.Select(lnref => new Levenshtein(unmatchedLastnames[i], lnref)).ToArray();
                for (int l = 0; l < levenshtein.Count(); l++)
                {
                    levenshtein[l].ComputeMatrix();
                    levenshtein[l].ComputeOperations(withContext);
                }

                levenshtein = levenshtein.OrderBy(lev => lev.GetScore()).ToArray();

                int score = levenshtein.First().GetScore();
                if (score > levenshtein.First().Target.Length / 2)
                {
                    matchedLastNames[i] = unmatchedLastnames[i] + "\tDISTANCE TOO GREAT\t" + score + "\t";
                }
                else
                {
                    var closestLevs = levenshtein.Where(lev => lev.GetScore() == score);
                    string[] closest = closestLevs.Select(lev => lev.Target).Distinct().ToArray();
                    if ((closest.Length == 1 || unfiltered) && closestLevs.First().GetScore() != 0)
                    {
                        rules.AddRange(closestLevs.SelectMany(lev => lev.Operations));
                        matchedLastNames[i] = unmatchedLastnames[i] + "\t" + closest.AggregateBy(", ") + "\t" + score + "\t";
                    }
                    else
                    {
                        matchedLastNames[i] = unmatchedLastnames[i] + "\t" + closest.AggregateBy(", ") + "\t!" + score + "\t";
                    }
                }
            }

            var commonRules = rules.GroupBy(op => op).OrderByDescending(grp => grp.Count()).Select(grp => new { num = grp.Count(), op = grp.First() }).ToArray();
            string namesReport = matchedLastNames.AggregateBy("\r\n");
            string rulesReport = commonRules.Select(a => a.num + "\t" + a.op.ToString(false)).AggregateBy("\r\n");

            var combos = rules.GroupBy(op => op.Source + "|" + op.Target).ToArray();
            var distinctRules = rules.Distinct().ToArray();

           
            //Dictionary<Tuple<StringOperation, StringOperation, bool>, float> P = new Dictionary<Tuple<StringOperation, StringOperation, bool>, float>();

            List<ConditionalRule> conditionalRules = new List<ConditionalRule>();

            Dictionary<StringOperation, float> pRule1 = new Dictionary<StringOperation, float>();

            int count = 0;
            foreach (var r1 in distinctRules)
            {
                var combosWithR1 = combos.Where(combo => combo.Contains(r1)).ToArray();
                pRule1[r1] = (float)combosWithR1.Length / combos.Length;
                foreach (var r2 in distinctRules)
                {
                    count++;
                    Console.Write("\r" + 100 * count / (distinctRules.Length * distinctRules.Length) + "%");
                    if (r2 == r1)
                        continue;

                    //var combosWithR2 = combos.Where(combo => combo.Contains(r2));

                    //float px = (float)combosWithR2.Intersect(combosWithR1).Count() / combos.Count();
                    //float pb = (float)combosWithR1.Count() / combos.Count();

                    //float pp = px / pb;
                    //int Paib = combos.Where(combo => combo.Contains(r1) && combo.Contains(r2)).Count();

                    //var combosWithR2 = (float)combos.Where(combo => combo.Contains(r1) && combo.Contains(r2)).Count()/combos.Where(combo=>combo.Contains(r2)).Count();
                    var R1SubsetWithR2 = combosWithR1.Where(combo => combo.Contains(r2)).ToArray();
                    float p = (float)R1SubsetWithR2.Length / combosWithR1.Count();

                    //if (r1.Type == StringOperationType.Deletion && r1.From == 'l' &&
                    //    r2.Type == StringOperationType.Substitution && r2.From == 't' && r2.To == 'k')
                    //    Debugger.Break();

                    bool consecutive = R1SubsetWithR2.All(cmb =>
                        {
                            var allr1s = cmb.Where(ri => ri.Equals(r1));
                            var allr2s = cmb.Where(ri => ri.Equals(r2));

                            return allr2s.Any(rii => allr1s.Any(ri => rii.Index == ri.Index + 1));
                        });
                    ConditionalRule rule = new ConditionalRule()
                    {
                        Rule1 = r1,
                        Rule2 = r2,
                        Consecutive = consecutive,
                        ConditionalProbability = p,
                        Rule1Probability = pRule1[r1]
                    };
                    conditionalRules.Add(rule);
                    
                }
            }
            XmlSerializer singleSer = new XmlSerializer(typeof(SingleRule[]));

            using (var stream = File.Open("ocr_single_rules.xml", FileMode.Create))
            {
                singleSer.Serialize(stream, distinctRules.Select(rl => new SingleRule()
                    {
                        Rule = rl,
                        Probability = pRule1[rl]
                    }).ToArray());
            }
            Console.WriteLine();
            Console.WriteLine("Sorting");
            
            //var ordered = P.Where(kvp => kvp.Value > 0.1).OrderByDescending(kvp => pRule1[kvp.Key.Item1]).ToArray();//.ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
            var ordered = conditionalRules.Where(rule => rule.ConditionalProbability > 0.01f).OrderByDescending(rule => rule.Rule1Probability).ToArray();
            
            Console.WriteLine("Printing");
            
            StringBuilder report = new StringBuilder();
            count = 0;
            foreach (var rule in ordered)
            {
                count++;
                Console.Write("\r" + 100 * count / (ordered.Length) + "%");
                string line = String.Format("{0}\t{1}\t{2}\t{3}\t{4}", rule.Rule1, rule.Rule2, rule.Consecutive, rule.ConditionalProbability, rule.Rule1Probability);
                report.AppendLine(line);
            }

            string conditionalRulesReport = report.ToString();
            conditionalRulesReport = "r1\tr2\tConsecutive\tP(r2|r1)\tP(r1)\r\n" + conditionalRulesReport;

            //XmlRootAttribute root = new XmlRootAttribute("Rules");

            XmlSerializer ser = new XmlSerializer(typeof(ConditionalRule[]));
            
            using (var stream = File.Open("ocr_rules.xml", FileMode.Create))
            {
                ser.Serialize(stream, ordered);
            }


            Debugger.Break();
        }
    }
}
