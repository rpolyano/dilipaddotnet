﻿using NLPHelpers;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;

namespace RegExTransformer
{
    public static class MemberInfoExtensions
    {
        public static RegExTransformer.ParliamentarianManager.DateRange AsDateRange(this MemberInfo.memberMembershipPeriod period)
        {
            return new RegExTransformer.ParliamentarianManager.DateRange(DateTime.ParseExact(period.from.Trim(), "yyyy-MM-dd", null), 
                period.till.Trim() == "present" ? DateTime.Now : DateTime.ParseExact(period.till.Trim(), "yyyy-MM-dd", null));
        }
    }
    public static class StringExtensions
    {
        public static string AggregateBy(this IEnumerable<string> arr, string delim)
        {
            return arr.Aggregate((a, b) => a + delim + b);
        }

        [ThreadStatic]
        static Dictionary<string, bool> levenshteinCache = new Dictionary<string, bool>();
        static char[] BAD_OCR_CHARS = new char[] { 'i', 'l', 'I', 'o', 't', 'f', 'O', 's', 'e', 'r', 'c', 'a' };

        /// <summary>
        /// Removes all non alphabetical characters from this string and b, then compares this to b.
        /// </summary>
        /// <param name="a">This string</param>
        /// <param name="b">String to compare to</param>
        public static bool AlphaEquals(this string a, string b, bool caseless = true, bool removeNonNouns = true, bool softmatch = false, bool ocrflexible = false)
        {
            if (String.IsNullOrEmpty(a) || String.IsNullOrEmpty(b))
            {
                return String.IsNullOrEmpty(a) && String.IsNullOrEmpty(b);
            }

            //string aOrig = a.AlphaProcessString(caseless, removeNonNouns);//, false);
            //string bOrig = b.AlphaProcessString(caseless, removeNonNouns);//, false);
            a = a.AlphaProcessString(caseless, removeNonNouns);//, ocrflexible);
            b = b.AlphaProcessString(caseless, removeNonNouns);//, ocrflexible);

            if (softmatch)
            {
                return b.Contains(a) || a.Contains(b);//Regex.IsMatch(aOrig, b) || Regex.IsMatch(bOrig, a);//a.Contains(b) || b.Contains(a);
            }
            //if (ocrflexible)
            //{
            //    string key = Math.Min(a.GetHashCode(), b.GetHashCode()) + " " + Math.Max(a.GetHashCode(), b.GetHashCode());
            //    if (levenshteinCache == null)
            //        levenshteinCache = new Dictionary<string, bool>();
            //    if (!levenshteinCache.ContainsKey(key))
            //    {
            //        Levenshtein lv = new Levenshtein(a, b);
            //        lv.ComputeMatrix();
            //        levenshteinCache[key] = lv.GetScore() <= 1;
            //        if (!levenshteinCache[key] && lv.GetScore() <= lv.Source.Length/2)
            //        {
            //            lv.ComputeOperations(false);
            //            levenshteinCache[key] = lv.Operations.All(op => op.From.Any(ch => BAD_OCR_CHARS.Contains(ch)));
            //        }
            //    }

            //    return levenshteinCache[key];
            //}
            return a.Equals(b);//return Regex.IsMatch(aOrig, "^" + b + "$") || Regex.IsMatch(bOrig, "^" + a + "$");//b == a;
        }


        static Dictionary<string, string> OCR_REPL_CHARS = new Dictionary<string, string>()
        {
            {"l", "tif"},
            {"i", "tlf"},
            {"I", "lt"},
            {"f", "t"},
            {"t", "f"}
        };
        public static string AlphaProcessString(this string a, bool caseless, bool removeNonNouns)//, bool ocrflexible)
        {
            a = a.RemoveDiacritics();

            StringBuilder aBuilder = new StringBuilder();
            for (int i = 0; i < a.Length; i++)
            {
                if (Char.IsLetter(a[i]) || Char.IsWhiteSpace(a[i]))
                {
                    aBuilder.Append(a[i]);
                }
                else if (Char.IsPunctuation(a[i]))
                {
                    aBuilder.Append(' ');
                }
            }

            a = aBuilder.ToString();

            if (removeNonNouns)
            {
                string[] remWords = new string[]
                {
                    "to",
                    "of",
                    "the",
                    "from",
                    "for",
                    "and"
                };
                foreach (var remWord in remWords)
                {
                    a = a.Replace(" " + remWord + " ", "");
                }
                //string pattern = @"\b(to|of|the|from|for|and)\b";
                //a = Regex.Replace(a, pattern, "", RegexOptions.Compiled);

            }

            //a = Regex.Replace(a, "[^a-zA-Z]", "", RegexOptions.Compiled);

            //if (ocrflexible)
            //{
            //    foreach (var kvp in OCR_REPL_CHARS)
            //    {
            //        a = a.Replace(kvp.Key, "{" + kvp.Key + "}");
            //    }
            //    foreach (var kvp in OCR_REPL_CHARS)
            //    {
            //        a = a.Replace("{" + kvp.Key + "}", "[" + kvp.Key + kvp.Value + "]");
            //    }
            //}

            if (caseless)
            {
                a = a.ToLower();
            }
            return a;
        }

        public static string ReplaceAt(this string input, int index, char newChar)
        {
            if (input == null)
            {
                throw new ArgumentNullException("input");
            }
            char[] chars = input.ToCharArray();
            chars[index] = newChar;
            return new string(chars);
        }

        public static bool AlphaStartsWith(this string a, string b, bool caseless = true, bool removeNonNouns = true, bool twoway = false)
        {
            if (String.IsNullOrEmpty(a) || String.IsNullOrEmpty(b))
            {
                return String.IsNullOrEmpty(a) && String.IsNullOrEmpty(b);
            }

            a = a.AlphaProcessString(caseless, removeNonNouns);
            b = b.AlphaProcessString(caseless, removeNonNouns);

            return Regex.IsMatch(a, "^" + b) || (twoway && Regex.IsMatch(b, "^" + a));
        }

        public static string RemoveDiacritics(this string text)
        {
            var normalizedString = text.Normalize(NormalizationForm.FormD);
            var stringBuilder = new StringBuilder();

            foreach (var c in normalizedString)
            {
                var unicodeCategory = CharUnicodeInfo.GetUnicodeCategory(c);
                if (unicodeCategory != UnicodeCategory.NonSpacingMark)
                {
                    stringBuilder.Append(c);
                }
            }

            return stringBuilder.ToString().Normalize(NormalizationForm.FormC);
        }

        public static int SimpleCompare(this string baseStr, string fullStr)
        {
            return string.Compare(baseStr.Trim(), fullStr.Substring(0, Math.Min(baseStr.Length, fullStr.Length)));
        }
    }
    public static class XmlDocumentExtensions
    {
        public static void IterateThroughAllNodes(
            this XmlDocument doc,
            Action<XmlNode> elementVisitor)
        {
            if (doc != null && elementVisitor != null)
            {
                foreach (XmlNode node in doc.ChildNodes)
                {
                    doIterateNode(node, elementVisitor);
                }
            }
        }

        private static void doIterateNode(
            XmlNode node,
            Action<XmlNode> elementVisitor)
        {
            elementVisitor(node);

            foreach (XmlNode childNode in node.ChildNodes)
            {
                doIterateNode(childNode, elementVisitor);
            }
        }
    }
}
