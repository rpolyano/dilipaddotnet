﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace RegExTransformer
{
    /// <summary>
    /// Post-processing.
    /// 
    /// This stage adds all missing elements and attributes required to adhere to the 
    /// PM schema. Also adds namespaces and performs "cosmetic" fixes
    /// </summary>
    class TransformerStage3
    {
        XDocument doc;
        string fileName;

        public static Dictionary<string, XNamespace> NAMESPACES = new Dictionary<string, XNamespace>()
            {
                { "owl", "http://www.w3.org/2002/07/owl#"},
                { "pmx" ,"http://www.politicalmashup.nl/extra"},
                { "dc","http://purl.org/dc/elements/1.1/" },
                { "pmd","http://www.politicalmashup.nl/docinfo"  },
                { "openpx","https://openparliament.ca/extra" },
                { "dp","http://dilipad.history.ac.uk" },
                { "dcterms","http://purl.org/dc/terms/"  },
                { "html","http://www.w3.org/1999/xhtml"  },
                { "rdf","http://www.w3.org/1999/02/22-rdf-syntax-ns#"  },
                { "xsd","http://www.w3.org/2001/XMLSchema-datatypes" },
                { "xsi","http://www.w3.org/2001/XMLSchema-instance" },
                { "pm","http://www.politicalmashup.nl" }
            };
        
        public TransformerStage3(XDocument doc, string fileName)
        {
            this.doc = doc;
            this.fileName = fileName;
        }

        public XDocument Transform()
        {
            // Move timestamps into attributes
            SetElementAsAttribute("time-stamp", "time", str => Util.NormalizeText(str, Util.NormalizationMode.Time));

            //Regex fixes
            string text = doc.ToString();
            
            //Remove speaker reminder tags
            text = Regex.Replace(text, @"\[(Sir|M\.|Mr\.|Mr\,|Hon\.|The\sHon\.|Right\sHon\.|The\sRight\sHon\.|Miss|Mrs\.|Ms\.) ?[^\n]+\]", "");
            
            doc = XDocument.Parse(text);
            
            PutLooseTextInStageDir(doc.Root);

            //Make all scenes type=topic
            var scenes = doc.Root.Descendants(XName.Get("scene"));
            foreach (var scene in scenes)
            {
                scene.SetAttributeValue(XName.Get("type"), "topic");
            }

            //Rename main_topic to topic
            var mainTopics = doc.Root.Descendants(XName.Get("main_topic")).ToArray();
            foreach (var topic in mainTopics)
            {
                topic.Name = XName.Get("topic");
            }

            

            SetTextInPTags();
            SetIds();
            
            
            SetNamespaces();

            //Topics can't have both speeches and scenes in them (either or)
            //so we move the speeches into stage directions if a topic has a scene
            //EDIT: stage-directions can't have speeches in them either. So we make them our own speeches.
            var topicsWithScenes = doc.Root.Descendants(NAMESPACES["pm"] + "topic").Where(xelem => xelem.Elements(NAMESPACES["pm"] + "scene").Count() > 0).ToArray();
            foreach (var topicWithScene in topicsWithScenes)
            {
                var looseSpeeches = topicWithScene.Elements(NAMESPACES["pm"] + "speech").ToArray();
                foreach (var looseSpeech in looseSpeeches)
                {
                    looseSpeech.Name = NAMESPACES["dp"] + "speech";
                }
            }


            AddMeta();
            return doc;
        }

        /// <summary>
        /// Recursively finds loose text and wraps in a stage-firection element
        /// </summary>
        /// <param name="node">The node to strat looking in</param>
        private void PutLooseTextInStageDir(XContainer node)
        {
            var children = node.Nodes().ToArray();
            foreach (var child in children)
            {
                if (child.NodeType == System.Xml.XmlNodeType.Text && 
                    child.Parent.Name.LocalName != "speech" && 
                    child.Parent.Name.LocalName != "stage-direction" &&
                    child.Parent.Name.LocalName != "vote")
                {
                    string value = ((XText)child).Value;
                    child.ReplaceWith(new XElement(XName.Get("stage-direction"), value));
                }
                else if (child is XContainer)
                {
                    PutLooseTextInStageDir((XContainer)child);
                }
            }
        }

        /// <summary>
        /// Adds all meta info
        /// </summary>
        private void AddMeta()
        {
            doc.Root.AddFirst(
                new XElement(NAMESPACES["pmd"] + "docinfo",
                new XElement(NAMESPACES["pm"] + "comment")
                {
                    Value = "Data is valid with respect to the Relax NG schema http://schema.politicalmashup.nl/proceedingsX.html. This is the open version of http://schema.politicalmashup.nl/proceedings.html"
                }),
                new XElement(XName.Get("meta"),
                new XElement(NAMESPACES["dc"] + "identifier"),
                new XElement(NAMESPACES["dc"] + "format", "text/xml"),
                new XElement(NAMESPACES["dc"] + "subject", ""),
                new XElement(NAMESPACES["dc"] + "type", "Proceedings"),
                new XElement(NAMESPACES["dc"] + "contributor", "http://www.politicalmashup.nl"),
                new XElement(NAMESPACES["dc"] + "coverage", 
                    new XElement(XName.Get("country"), 
                        new XAttribute(NAMESPACES["dcterms"] + "ISO3166-1", "CA"), "Canada")),
                new XElement(NAMESPACES["dc"] + "creator", "http://dilipad.history.ac.uk"),
                new XElement(NAMESPACES["dc"] + "language", 
                    new XElement(NAMESPACES["pm"] + "language", 
                        new XAttribute(NAMESPACES["dcterms"] + "ISO639-2", "eng"), "English")),
                new XElement(NAMESPACES["dc"] + "publisher", "http://www.parl.gc.ca/parlinfo/"),
                new XElement(NAMESPACES["dc"] + "rights", "http://www.parl.gc.ca/parlinfo/"),
                new XElement(NAMESPACES["dc"] + "date", DateTime.UtcNow.ToString("yyyy-MM-dd+hh:mm")),
                new XElement(NAMESPACES["dc"] + "title", "Canadian House of Commons Proceedings"),
                new XElement(NAMESPACES["dc"] + "description", "Canadian House of Commons Proceedings"),
                new XElement(NAMESPACES["dc"] + "source", 
                    new XElement(NAMESPACES["dc"] + "source",
                        new XAttribute(NAMESPACES["pm"] + "used-source","true"),
                            new XElement(NAMESPACES["pm"] + "link", 
                                new XAttribute(NAMESPACES["pm"] + "linktype","trusted"),
                                new XAttribute(NAMESPACES["pm"] + "source","http://www.parl.gc.ca/parlinfo"),
                                "http://www.parl.gc.ca/parlinfo"))),
                new XElement(NAMESPACES["dc"] + "relation", "All pm:party-ref attributes appearing in http://www.politicalmashup.nl documents refer to http://resolver.politicalmashup.nl/[pm:party-ref ")

                    ));

        }

        /// <summary>
        /// Iterates though all alements (and attributes) in the document and assigns the appropriate namespaces.
        /// </summary>
        private void SetNamespaces()
        {
            
            XAttribute[] namespaceAttrs = NAMESPACES.Select(kvp => new XAttribute(XNamespace.Xmlns + kvp.Key, kvp.Value)).ToArray();
            XElement root = new XElement(XName.Get("root"), namespaceAttrs);
            root.Add(doc.Root);
            doc = new XDocument(root);
            var allElements = doc.Root.Descendants().ToArray();
            foreach (var element in allElements)
            {
                element.Name = MakeFullName(element.Name);
                XAttribute[] attrs = element.Attributes().ToArray();
                foreach (var attr in attrs)
                {
                    string value = attr.Value;
                    element.SetAttributeValue(attr.Name, null);
                    element.SetAttributeValue(MakeFullName(attr.Name), value);
                }
            }

        }

        /// <summary>
        /// Assigns a namespace to the given XName based on the LocalName.
        /// 
        /// TODO: This may need other information to correctky set namespace if there are
        /// different elements with the same LocalName from different namespaces
        /// </summary>
        /// <param name="xname">The name of the element which needs a namespace</param>
        public static XName MakeFullName(XName xname)
        {
            XNamespace ns;
            string name = xname.LocalName;
            if (name == "table" ||
                name == "main_topic" ||
                name == "subtopic" ||
                name == "question-num" ||
                name == "question-author" ||
                name == "td" ||
                name == "tr" ||
                name == "time"
                )
            {
                ns = NAMESPACES["dp"];
            }
            else
            {
                ns = NAMESPACES["pm"];
            }
            return ns + name;
        }

        /// <summary>
        /// Generates Ids for every element, starting with the document root
        /// </summary>
        private void SetIds()
        {
            DateTime dt = DateTime.Parse(fileName.Replace('_', ' '));
            string prefix = String.Format("ca.proc.d.{0}-{1}-{2}", dt.Year, dt.Month, dt.Day);
            SetChildrenIds(doc.Root, prefix);
            doc.Root.SetAttributeValue(XName.Get("id"), prefix);
        }

        /// <summary>
        /// Makes an element into an attribute of its parent
        /// </summary>
        /// <param name="element">The element tag name to move</param>
        /// <param name="attribute">The attribute to make it into</param>
        /// <param name="normalization">The function to process the element's value before making it an attribute/param>
        private void SetElementAsAttribute(string element, string attribute, Func<string, string> normalization = null)
        {
            var elements = doc.Descendants(XName.Get(element)).ToArray();
            foreach (var elem in elements)
            {
                string value = elem.Value;
                if (normalization != null)
                {
                    value = normalization(value);
                }
                elem.Parent.SetAttributeValue(XName.Get(attribute), value.Trim());
                elem.Remove();
            }
        }

        /// <summary>
        /// Puts all text inside elements into &lt;p&gt; tags, split by \n\n
        /// </summary>
        private void SetTextInPTags()
        {
            var elements = doc.Descendants(XName.Get("speech")).Union(doc.Descendants(XName.Get("stage-direction"))).ToArray();
            foreach (var element in elements)
            {
                string text = element.Value.Trim();
                element.Value = "";
                string[] paragraphs;
                if (text.Contains("\n\n\n"))
                {
                    paragraphs = text.Split(new string[] { "\n\n\n" }, StringSplitOptions.RemoveEmptyEntries);
                }
                else
                {
                    paragraphs = new string[] { text };
                }
                foreach (var paragraph in paragraphs)
                {
                    element.Add(new XElement(XName.Get("p"), paragraph));
                }
            }

        }

        /// <summary>
        /// Recursively sets the Ids of the children of node. Does not set node's Id.
        /// </summary>
        /// <param name="node">The node whose children to recurse on</param>
        /// <param name="prefix">The prefix for the id, i.e. the id 'do far'</param>
        private void SetChildrenIds(XElement node, string prefix)
        {
            var children = node.Elements();
            int id = 0;
            foreach (var child in children)
            {
                id++;
                child.SetAttributeValue(XName.Get("id"), prefix + "." + id);
                SetChildrenIds(child, prefix + "." + id);
            }
        }

    }
}
