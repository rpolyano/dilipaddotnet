﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace RegExTransformer
{
    /// <summary>
    /// Second stage of parsing.
    /// This stage does topic hierarchy reconstruction.
    /// 
    /// End result will have topics labeled as scene/subtopic/main_topic and nested correctly.
    /// </summary>
    class TransformerStage2
    {
        struct Topic
        {
            public int level;
            public bool hasSub;
            public string title;
            public Topic(XElement topicElement, TransformerStage2 trans)
            {
                level = trans.GetLevel(topicElement, out hasSub);
                title = topicElement.Value.Trim();
            }

            public override string ToString()
            {
                return title + " [" + level + "]" + (hasSub ? " SUB" : "");
            }
        }
        enum FlagMatchType
        {
            Regex,
            Keyword
        }

        XDocument doc;
        XDocument hier;
        IEnumerable<XElement> levels;
        int catchAll;

        public static string[] hierarchies;
        public TransformerStage2(XDocument doc, int year)
        {
            this.doc = doc;


            string hfile = hierarchies.First(file =>
                {
                    string[] nums = Path.GetFileNameWithoutExtension(file).Split('-');
                    int min = Int32.Parse(nums[0]);
                    int max = Int32.Parse(nums[1]);
                    return min <= year && year <= max;
                });
            hier = XDocument.Load(hfile);
            catchAll = Int32.Parse(hier.Descendants("catch-all").First().Attribute("level").Value);
            levels = hier.Root.Descendants("level");
        }

        public XDocument Transform()
        {
            SetTopicLevels();

            XDocument newDoc = NestTopics();
            return newDoc;
        }

        /// <summary>
        /// Moves topic elements inside each other, according to the level attribute.
        /// </summary>
        /// <returns>A reconstructed document with all the topics properly nested. 
        /// The original document is not modified.</returns>
        private XDocument NestTopics()
        {
            var newDoc = new XDocument();
            newDoc.AddFirst(new XElement(XName.Get("proceedings")));

            XElement lastContainer = null;
            int lastLevel = 0;

            var nodes = doc.Root.Nodes();
            foreach (var node in nodes)
            {
                if (node.NodeType == XmlNodeType.Text)
                {
                    newDoc.Root.Add(node);
                    continue;
                }
                var xelem = new XElement((XElement)node);
                string name = xelem.Name.LocalName;

                if (name.StartsWith("topic"))
                {
                    int level = Int32.Parse(xelem.Attribute("level").Value);

                    if (level == 1)
                    {
                        xelem.Name = XName.Get("main_topic");
                    }
                    else
                    {
                        if (level == 2)
                        {
                            xelem.Name = XName.Get("scene");
                        }
                        else
                        {
                            xelem.Name = XName.Get("subtopic");
                        }
                    }

                    xelem.SetAttributeValue(XName.Get("title"), Util.NormalizeText(xelem.Value.Trim(), Util.NormalizationMode.Attributes));
                    xelem.Value = "";
                    xelem.SetAttributeValue(XName.Get("level"), null);

                    if (lastContainer != null)
                    {
                        if (level > lastLevel)
                        {
                            //xelem.Remove();
                            lastContainer.Add(xelem);
                            lastContainer = xelem;
                        }
                        else
                        {
                            int curLevel = lastLevel;
                            while (curLevel >= level)
                            {
                                lastContainer = lastContainer.Parent;
                                curLevel--;
                            }
                            //xelem.Remove();
                            if (lastContainer == null)
                            {
                                Log.LogError(doc.BaseUri, "Weird topic arrangement for element " + xelem.ToString(), false);
                                lastContainer = newDoc.Root;
                            }
                            lastContainer.Add(xelem);
                            lastContainer = xelem;
                        }

                        lastLevel = level;
                    }
                    else
                    {
                        newDoc.Root.Add(xelem);
                        lastContainer = xelem;
                        lastLevel = level;
                    }
                }
                else if (lastContainer != null)
                {
                    //xelem.Remove();
                    lastContainer.Add(xelem);
                }

            }
            return newDoc;
        }

        /// <summary>
        /// Assigns a level to each topic element, based on hierarchy and structure.
        /// </summary>
        public void SetTopicLevels()
        {

            int numInARow = 0;
            //int lastLevel = 0;
            //int forcedSubLevel = 0;
            //int lastLevelWithSub = 0;

            Stack<Topic> topics = new Stack<Topic>();
            Topic DOC_ROOT_TOPIC = new Topic() { level = 0, hasSub = true };
            topics.Push(DOC_ROOT_TOPIC);
            var nodes = doc.Root.Nodes().ToArray();
            foreach (var node in nodes)
            {
                if (node.NodeType == XmlNodeType.Text)
                {
                    Log.LogError(doc.BaseUri, "Found loose text");

                    continue;
                }
                string name = ((XElement)node).Name.LocalName;

                if (name.StartsWith("topic"))
                {

                    //if (((XElement)node).Value.Contains("WAYS AND MEANS"))
                    //{
                    //    Debugger.Break();
                    //}

                    numInARow++;

                    Topic topic = new Topic((XElement)node, this);
                    Topic prevTopic = topics.Peek();//topics.Count > 0 ? topics.Pop() : DOC_ROOT_TOPIC;
                    //We don't know the topic level
                    if (topic.level == -1 || numInARow > 1)
                    {

                        //We have a gurantee that this one is going to be a child of the previous
                        if (numInARow > 1)
                        {
                            topic.level = prevTopic.level + 1;
                        }
                        else
                        {
                            prevTopic = topics.Pop();
                            while (!prevTopic.hasSub && prevTopic.level >= catchAll)
                                prevTopic = topics.Pop();
                            topics.Push(prevTopic);
                            topic.level = prevTopic.level + 1;
                        }
                        ////We don't know where this should go
                        //else
                        //{
                        //    topic.level = Math.Min(catchAll, prevTopic.level + 1);
                        //}
                    }
                    else
                    {
                        while (topic.level <= prevTopic.level)
                            prevTopic = topics.Pop();
                        topics.Push(prevTopic);
                    }
                    if (topic.level > prevTopic.level)
                        topics.Push(topic);

                    ((XElement)node).SetAttributeValue("level", topic.level);
                }
                else
                {
                    numInARow = 0;
                }
            }
        }

        /// <summary>
        /// Returns a level
        /// </summary>
        /// <param name="topic"></param>
        /// <param name="hasSub"></param>
        /// <returns></returns>
        private int GetLevel(XElement topic, out bool hasSub)
        {
            foreach (var level in levels)
            {
                int lvlNum = Int32.Parse(level.Attribute("num").Value);
                var flags = level.Elements("flag");
                foreach (var flag in flags)
                {
                    if (Match(topic, flag))
                    {
                        hasSub = Boolean.Parse(flag.Attribute("has-sub").Value);
                        return lvlNum;
                    }
                }
            }
            hasSub = false;
            return -1;

        }

        public bool Match(XElement topic, XElement flag)
        {
            string topicStr = topic.Value.ToLower();
            string match = flag.Attribute("value").Value;
            FlagMatchType matchType = (FlagMatchType)Enum.Parse(typeof(FlagMatchType), flag.Attribute("match").Value);
            switch (matchType)
            {
                case FlagMatchType.Regex:
                    return Regex.IsMatch(topicStr, match);
                case FlagMatchType.Keyword:
                    string[] keywords = match.Split(',');

                    foreach (string keyword in keywords)
                    {
                        if (!topicStr.Contains(keyword))
                        {
                            return false;
                        }
                        topicStr = topicStr.Replace(keyword, "");
                    }
                    if (topicStr.Split("., ".ToCharArray(), StringSplitOptions.RemoveEmptyEntries).Where(s=>s.Length > 2).Count() > keywords.Length)
                        return false;
                    return true;
            }
            return false;
        }

    }
}
