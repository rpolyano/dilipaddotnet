﻿
using NLPHelpers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
namespace RegExTransformer
{
    /// <summary>
    /// First stage of parsing. This stage finds all the basic elements and tags them.
    /// Tagged elements include speeches (with assigned speaker attribute), topics (main and normal),
    /// time stamps, tables, questions, stage-directions, and votes.
    /// 
    /// Other than time stamps (which can be inside speeches) and columns and rows
    /// (td and tr, which can be inside tables) the XML produced by this
    /// stage is flat.
    /// </summary>
    class TransformerStage1
    {
        IRegexContainer regex;
        string text;
        string fileName;
        public static int NUM_TOPICS_FIXED = 0;
        static String NUM_TOPICS_FIXED_LOCK = "lock";
        DateTime fileDate;

        public TransformerStage1(IRegexContainer regex, string text, string fileName)
        {
            this.regex = regex;
            this.text = text;
            this.fileName = fileName.Replace("_", " ");
            this.fileDate = DateTime.Parse(this.fileName);
        }

        public string Transform()
        {
            Cleanup();
            FixTopicsBrokenByNewLine();

            TryMarkupEntityFeatureByRegexPattern(RegexFeature.SpeakerName, "speaker", fileDate);
            TryMarkupEntityFeatureByRegexPattern(RegexFeature.Intervention, "speaker", fileDate);
            TryMarkupEntityFeatureByRegexPattern(RegexFeature.OtherSpeakerName, "speaker", fileDate);
            MarkupEntityByList(regex.GetPrimaryStageDirections(), "stage-direction", false, true);
            MarkupEntityByList(regex.GetMainHeaders(), "topic_main", true, true);
            
            text = Regex.Replace(text, "(^YEAS$)|(^NA[YT]S$)", "++$0", RegexOptions.Multiline | RegexOptions.Compiled);
            TryMarkupEntityFeatureByRegexPattern(RegexFeature.Topic, "topic", fileDate);
            text = Regex.Replace(text, "(?<=<topic>).+?(?=</topic>)", new MatchEvaluator(RemoveNewLinesFromMatch), RegexOptions.Singleline | RegexOptions.Compiled);
            RemoveBadTopics();

            MarkupVotes();

            

            //Fix speakers
            string[] lines = text.Split('\n');
            for (int i = 0; i < lines.Length; i++)
            {
                if (lines[i].Contains("<speaker>"))
                {
                    lines[i + 1] = Util.NormalizeText(lines[i + 1], Util.NormalizationMode.Attributes);
                }
            }
            text = lines.AggregateBy("\n");

            TryMarkupEntityFeatureByRegexPattern(RegexFeature.Question, null, fileDate);
            //TODO: ...............................................................(?=<[a-z])
            text = Regex.Replace(text + "<speaker", @"<speaker>(.+?)</speaker>(.+?)(?=<topic|<speaker|<speech|<stage|<vote)", "<speech speaker=\"$1\">\n$2</speech>\n", RegexOptions.Singleline | RegexOptions.Compiled);
            text = text.Substring(0, text.Length - "<speaker".Length);
            //Throw all loose text (text between closing and opening tags) into stage dirs //TODO: Check if this is safe
            text = Regex.Replace(text, "(</[a-z_-]+>)(.+?)(?=<[a-z])", "$1\n<stage-direction>\n$2\n</stage-direction>\n", RegexOptions.Singleline | RegexOptions.Compiled);

            //Markup elements that are not top level

            //Remove empty stage dirs
            text = Regex.Replace(text, @"<stage-direction>\s+?</stage-direction>", "", RegexOptions.Compiled);

            //Merge repeated stage dirs TODO: Check if this is safe
            text = Regex.Replace(text, @"</stage-direction>\s<stage-direction>", "", RegexOptions.Compiled);


            MarkupTables();

            //Timestamps. Will get marked up wherever they are.
            TryMarkupEntityFeatureByRegexPattern(RegexFeature.Timestamp, "time-stamp", fileDate, false);

            //One-stop-shop solution for <speech> tags - did not quite work but may come in handy
            //text = Regex.Replace(text, @"^((?:Sir|M\.|Mr\.|Mr\,|Hon\.|The\sHon\.|Right\sHon\.|The\sRight\sHon\.|Miss|Mrs\.|Ms\.)\s(?:[A-Zdv][\-\w\.']{1,25}\s{0,1}){1,4}\s{0,1}(?:\(.+?\)){0,1}\s{0,1}(?:(?:moved:)|(?:moved)|:|;))([^<]+)(?=^((?:Sir|M\.|Mr\.|Mr\,|Hon\.|The\sHon\.|Right\sHon\.|The\sRight\sHon\.|Miss|Mrs\.|Ms\.)\s(?:[A-Zdv][\-\w\.']{1,25}\s{0,1}){1,4}\s{0,1}(?:\(.+?\)){0,1}\s{0,1}(?:(?:moved:)|(?:moved)|:|;))|<)", "<speech speaker=\"$1\">\n$2\n</speech>\n", RegexOptions.Multiline);

            //Cleanup line breaks
            text = Regex.Replace(text, "(?<==\")(\n)(.+)(\n)", "$2", RegexOptions.Compiled);
            text = Regex.Replace(text, @"(?<=>)\s+(?=[^<])", "\n", RegexOptions.Compiled);
            return "<proceedings>\n" + text + "\n</proceedings>";
        }

        private void MarkupVotes()
        {
            int preHash = text.GetHashCode();
            
            TryMarkupEntityFeatureByRegexPattern(RegexFeature.Votes, null, fileDate);
            int postHash = text.GetHashCode();
            if (preHash != postHash)
            {
                Log.LogMessage("Found vote", fileName, ConsoleColor.Green);

                preHash = postHash;
                text = Regex.Replace(text, "<vote>(.+?YEAS.+?-([0-9]+).+?NA[TY]S[^<]+-([0-9]+).+?)</vote>", "<vote yeas=\"$2\" nays=\"$3\">$1</vote>\n", RegexOptions.Singleline | RegexOptions.Compiled);
                postHash = text.GetHashCode();

                if (preHash != postHash)
                {
                    Log.LogMessage("Enhanced vote", fileName, ConsoleColor.Green);
                }

                //text = Regex.Replace(text, "(?<=<vote.+?>).+?(?=</vote>)", new MatchEvaluator(RemoveNewLinesFromMatch), RegexOptions.Singleline);
                var votes = Regex.Matches(text, "<vote.*?>(.+?)(?=</vote>)", RegexOptions.Singleline | RegexOptions.Compiled);
                foreach (Match vote in votes)
                {
                    text = text.Remove(vote.Groups[1].Index, vote.Groups[1].Length).Insert(vote.Groups[1].Index, vote.Groups[1].Value.Replace("\n", " ").Replace("\r", ""));
                }
            }

        }

        private void RemoveBadTopics()
        {
            string currentMonth = fileDate.ToString("MMMM").ToUpper();

            string currentYear = fileDate.Year.ToString();
            currentYear = OCRNumberVariance(currentYear);

            string currentDay = fileDate.Day.ToString();
            currentDay = currentDay.Replace("9", "[9GS]").Replace("6", "[6GS,]").Replace("5", "[5S,]").Replace("0", "[0oO]");

            string[] badTopicRegexes = new string[]
            {
                "(?:(?:HOUSE OF)|(?:COMMONS)|(?:HOUSE OF COMMONS)|(?:DEBATES)|(?:COMMONS DEBATES))",
                "[RKB] ?E ?V ?I ?S ?E ?[DO] ?E ?[DO] ?I ?T ?I ?[OD] ?N",
                ".{1,3}", //Three letters or less
                //"(?:FIRST|SECOND|THIRD) READINGS?", //Bill readings
                "[SH][0-9SGO]{2,4}", //Standing orders
                @".+?\.{3}", //Topics ending in ...
                @"[A-Z][a-z]+", //Accidental vote names
                String.Format("{0} ?(?:{1}.?)? ?(?:{2})?", currentMonth, currentDay, currentYear) //Today's date
            };
            foreach (var badTopicRegex in badTopicRegexes)
            {
                var matches = Regex.Matches(text, "<topic>" + badTopicRegex + "</topic>");
                text = Regex.Replace(text, "<topic>" + badTopicRegex + ".{0,3}</topic>", "", RegexOptions.Multiline | RegexOptions.Compiled);
            }
        }

        private string OCRNumberVariance(string numbers)
        {
            return numbers.Replace("9", "[9GS]").Replace("6", "[6GS,]").Replace("5", "[5S,]").Replace("0", "[0oO]").Replace("1", "[1X]");
        }

        [ThreadStatic]
        static Dictionary<string, Regex> regexCache = new Dictionary<string, Regex>();
        private void TryMarkupEntityFeatureByRegexPattern(RegexFeature feature, string tag, DateTime fileDate, bool newline = true)
        {
            if (regexCache == null)
                regexCache = new Dictionary<string, Regex>();
            RegexPattern currentPattern = RegexManager.GetPattern(fileDate, feature);
            Regex regex;
            if (currentPattern != null)
            {
                string pattern = currentPattern.FullPattern;
                if (regexCache.ContainsKey(pattern))
                {
                    regex = regexCache[pattern];
                }
                else
                {

                    regex = new Regex(pattern, RegexOptions.Compiled |
                        (currentPattern.multiLine ? RegexOptions.Multiline : RegexOptions.None) |
                        (currentPattern.singleLine ? RegexOptions.Singleline : RegexOptions.None));
                    regexCache[pattern] = regex;
                }
                if (String.IsNullOrWhiteSpace(currentPattern.Markup))
                {
                    text = regex.Replace(text, String.Format("<{0}>{1}$0{1}</{0}>\n", tag, newline ? "\n" : ""));
                }
                else
                {
                    text = regex.Replace(text, currentPattern.Markup);
                }
            }
            else
            {
                Log.LogError(this.fileName, "Could not find regex for " + feature + " for date " + fileDate.ToString("yyyy-MM-dd"));
            }

        }


        private string RemoveNewLinesFromMatch(Match match)
        {
            return match.Value.Replace("\n", " ").Replace("\r", "").Trim();
            //StringBuilder sb = new StringBuilder(text);
            //sb.Remove(match.Index, match.Length);
            //sb.Insert(match.Index, match.Value.Replace("\n","").Replace("\r",""));
            //text = sb.ToString();
            //for (int i = match.Index; i < match.Index + match.Length; i++)
            //{
            //    if (text[i] == '\n')
            //    {
            //        text.
            //    }
            //}
        }

        private void FixTopicsBrokenByNewLine()
        {
            Regex topicRegex = new Regex(regex.GetTopicPattern(), RegexOptions.Compiled);
            string[] lines = text.Split('\n');

#if X_PLATFORM_TOPIC_VALIDATION
            CrossPlatformTopicValidator TopicValidator = new CrossPlatformTopicValidator();
#endif

            for (int i = 0; i < lines.Length - 1; i++)
            {
                if (topicRegex.IsMatch(lines[i]) &&
                    topicRegex.IsMatch(lines[i + 1]) &&
                    lines[i].Length > 2 &&
                    lines[i + 1].Length > 2)
                {
                    if (TopicValidator.SentenceContinues(lines[i]) ||
                        TopicValidator.SentenceIsContinuation(lines[i + 1]))
                    {
                        lines[i + 1] = lines[i] + " " + lines[i + 1];
                        lines[i] = "\n";
                        Log.LogMessage("Fixed broken topic: " + lines[i + 1], fileName, ConsoleColor.DarkMagenta);
                        lock (NUM_TOPICS_FIXED_LOCK)
                        {
                            NUM_TOPICS_FIXED++;
                        }
                    }
                }
            }

            text = lines.AggregateBy("\n");
        }

        /// <summary>
        /// Surrounds all tables in text with table, row, and column tags
        /// </summary>
        private void MarkupTables()
        {
            string[] lines = text.Split('\n');
            bool inTable = false;
            Regex columnRegex = new Regex(RegexManager.GetPattern(fileDate, RegexFeature.Table).FullPattern, RegexOptions.Compiled);
            for (int i = 0; i < lines.Length; i++)
            {
                //Find all columns in this line
                var matches = columnRegex.Matches(lines[i])
                    .Cast<Match>()
                        .Select(m => m.Value)
                        .ToArray();
                if (matches.Length > 0)
                {
                    //Construct row out of current line
                    string tr = "<tr>\n<td>" + matches.AggregateBy("</td><td>") + "</td>\n</tr>\n";
                    if (!inTable)
                    {
                        //Check to make sure we are not starting a table on the last line or that the next line does
                        //not have enough columns. 
                        //We don't want a table with one row or just 1 or two columns (because its probably not actually a table)
                        if (i == lines.Length - 1 || columnRegex.Matches(lines[i + 1]).Count < 3 || matches.Length < 3)
                        {
                            continue;
                        }
                        inTable = true;
                        lines[i] = "<table>\n" + tr;
                    }
                    else
                    {
                        lines[i] = tr;
                    }
                }
                else
                {
                    if (inTable)
                    {
                        //Close the table, but preserve what ever was on the line
                        lines[i] = "</table>\n" + lines[i];
                        inTable = false;
                    }
                }
            }
            text = lines.AggregateBy("\n");
        }

        /// <summary>
        /// Cleans text by normalizing OCR errored symbols,
        /// fixing hyphenated words, and removing unused elements
        /// </summary>
        void Cleanup()
        {
            text = Util.NormalizeText(text, Util.NormalizationMode.Unicode);
            text = Util.NormalizeText(text, Util.NormalizationMode.XML);

            text = regex.PreProccess(text, new Dictionary<string, string>() { { "DATE", fileName } });
            text = text.Replace(" YEAS", "\n YEAS");
            text = text.Replace(" NAYS", "\n NAYS");

            //Glue hyphenated words
            text = Regex.Replace(text, @"\-\n(?=[a-z])", " ", RegexOptions.Multiline);

            //Move "moved" back onto its original line
            text = Regex.Replace(text, @"\n(?=moved)", " ", RegexOptions.Multiline);

            //Correct OCR error for (in front of division)
            text = Regex.Replace(text, @"\n.Division", "\n(Division", RegexOptions.Multiline);

            text = Regex.Replace(text, regex.GetStandingOrderSubtitlesPattern(), "\n", RegexOptions.Multiline);

            //Remove the stars TODO: Figure out what the indicate and use them as anchors?
            text = Regex.Replace(text, @"\s\*\s\*\s\*\s", "\n", RegexOptions.Multiline);

            text = Regex.Replace(text, regex.GetPageNumbersPattern(), "\n", RegexOptions.Multiline);

            //Any extra cleaning
            if (regex.GetExtraCleanPattern() != "")
            {
                text = Regex.Replace(text, regex.GetExtraCleanPattern(), "\n", RegexOptions.Multiline);
            }
            //Restore words hyphenated and put on different lines
            text = Regex.Replace(text, "(?<=[a-z])-\r?\n(?=[a-z])", "", RegexOptions.Multiline);
            //text = Regex.Replace(text, @"\-\r?\n(?=[\sA-Za-z])", "- ", RegexOptions.Multiline);

            //Remove date reminders
            text = Regex.Replace(text, regex.GetDatesPattern(), "\n", RegexOptions.Multiline);

            //Remove [Tags] like [English] TODO: Make them into attributes?
            text = Regex.Replace(text, @"^(\[[^ \n]+\].?)$", "\n", RegexOptions.Multiline);

            //Fix Mr. \nSpeaker issue TODO: Make this more general
            text = Regex.Replace(text, @"Mr\..?\n", "Mr. ", RegexOptions.Multiline);

            //Questions onto new lines
            text = Regex.Replace(text, @"(?:.uestion\sN..\s([0-9ilI\,\|]{1,})\-)", "\n$0", RegexOptions.Multiline);

            //Replace column.............column with tabs
            text = Regex.Replace(text, @"(\.|\. | \.){5,}", "\t");

            //text = Regex.Replace(text, @"(\t\s+(?=\t))|\t$", "");

            text = Regex.Replace(text, @"^\s?\[DOT\]\s?(?=[A-Z])", "", RegexOptions.Multiline);

            //Remove windows line endings
            text = text.Replace("\r", "");

        }

        /// <summary>
        /// Surrounds every instance of every element in list with <tag> and </tag>
        /// </summary>
        /// <param name="list">List of elements to tag</param>
        /// <param name="tag">The tag to use</param>
        /// <param name="upper">Makes each element uppercase before checking it</param>
        /// <param name="wholeLine">Tags the entire line if it starts with an element</param>
        void MarkupEntityByList(string[] list, string tag, bool upper = false, bool wholeLine = false)
        {
            string[] lines = text.Split('\n');
            for (int i = 0; i < lines.Length; i++)
            {
                foreach (string elem in list)
                {
                    string element = elem;
                    if (upper)
                        element = elem.ToUpper();
                    if (lines[i].StartsWith(element))
                    {
                        if (wholeLine)
                        {
                            lines[i] = String.Format("<{0}>{1}</{0}>", tag, lines[i]);
                        }
                        else
                        {
                            lines[i] = lines[i].Replace(element, String.Format("<{0}>{1}</{0}>", tag, element));
                        }
                    }
                }
            }

            text = lines.AggregateBy("\n");
        }
    }
}
