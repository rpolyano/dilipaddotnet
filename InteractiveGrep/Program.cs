﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace InteractiveGrep
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Pattern: ");
            string pattern = Console.ReadLine();
            Console.Write("Directory: ");
            string folder = Console.ReadLine();
            folder = folder.Trim('"');
            string[] files = Directory.GetFiles(folder, "*.*", SearchOption.AllDirectories);

            Regex re = new Regex(pattern, RegexOptions.Compiled | RegexOptions.Multiline | RegexOptions.Singleline);
            Match match;
            string text;
            string log = Path.Combine(folder, "grep.txt");
            files = files.OrderBy(f => f.Split('-').Last()).ToArray();
            foreach (var file in files)
            {
               text = File.ReadAllText(file);
                if ((match = re.Match(text)) != null && match.Success)
                {
                    int line = GetLineNumber(text, match.Index);
                    string message = "Match " + Path.GetFileName(file) + ":" + line;
                    Console.WriteLine(message);
                    File.AppendAllText(log, message + " ::: " + text.Split('\n')[line - 1] + Environment.NewLine);
                }
            }
        }

        static int GetLineNumber(string text, int index)
        {
            int line = 1;
            for (int i = 0; i < index;i++)
            {
                if (text[i] == '\n')
                    line++;
            }
            return line;
        }
    }
}
