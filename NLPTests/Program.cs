﻿using NLPHelpers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace NLPTests
{
    class Program
    {
        static void Main(string[] args)
        {
            XmlSerializer ser = new XmlSerializer(typeof(ConditionalRule[]));
            ConditionalRule[] allRules;
            using (var stream = File.Open("ocr_rules.xml", FileMode.Open))
            {
                allRules = (ConditionalRule[])ser.Deserialize(stream);
            }


            ser = new XmlSerializer(typeof(SingleRule[]));
            SingleRule[] singleRules;
            using (var stream = File.Open("ocr_single_rules.xml", FileMode.Open))
            {
                singleRules = (SingleRule[])ser.Deserialize(stream);
            }
            Console.WriteLine("Loaded {0} Combinational OCR Rules", allRules.Length);
            Console.WriteLine("Loaded {0} Individual OCR Rules", singleRules.Length);
            Console.WriteLine("Loading names");
            string[] authList = File.ReadAllLines("names.txt").Distinct().ToArray();
            Console.WriteLine("Loaded {0} last names", authList.Length);
            Console.WriteLine("Enter bad string: ");
            string input;
            List<String> inputs = new List<string>();
            while ((input = Console.ReadLine()) != "DONE")
            {
                inputs.Add(input);
            }

            Console.WriteLine("Building tree");
            OCRRuleApplicator ocr = new OCRRuleApplicator(authList, allRules);
            StreamWriter writer = new StreamWriter("correction.tsv");
            foreach (var inp in inputs)
            {

                writer.WriteLine(inp + "\t" + Correct(authList, inp, singleRules));
                writer.Flush();
            }
            Console.ReadLine();
            //Console.WriteLine("s:");
            //string s = Console.ReadLine();

            //Console.WriteLine("t:");
            //string t = Console.ReadLine();

            //Levenshtein lv = new Levenshtein(s, t);
            //lv.ComputeMatrix();
            //lv.ComputeOperations();
            //Console.WriteLine();
            //Console.Write(lv.Visualize());
            //Console.WriteLine();
            //Console.ReadLine();


            //string dataPath = Environment.CurrentDirectory.TrimEnd('/', '\\') + "/../../../SanfordPOSTagger";

            //TopicValidator.Init(dataPath, dataPath + "/models");
            //var tagged = TopicValidator.TagSentence("Minister of State (Treasury Board).");
            //Debugger.Break();
            //Console.WriteLine("Enter sentence:");
            //string sentence = Console.ReadLine();
            //TopicValidator.SentenceContinues(sentence);
        }

        static string Correct(string[] authList, string wrong, SingleRule[] rules)
        {
            var levsToAuth = authList.Select(auth =>
            {
                var lev = new Levenshtein(wrong, auth);
                lev.ComputeMatrix();
                lev.ComputeOperations(false);
                return lev;
            }).ToArray().OrderBy(lv => lv.GetScore()).ToArray();

            Dictionary<StringOperationType, float> opTypeDistr = new Dictionary<StringOperationType, float>();
            var enums = (StringOperationType[])Enum.GetValues(typeof(StringOperationType));
            foreach (var val in enums)
            {
                opTypeDistr[val] = (float)rules.Count(rl=>rl.Rule.Type == val) / rules.Length;
            }

            Dictionary<String, float> probs = new Dictionary<string, float>();
            int prog = 0;
            foreach (var lev in levsToAuth)
            {
                //if (lev.Target == "Quelch")
                //    Debugger.Break();
                prog++;
                Console.Write("\r" + (prog * 100 / levsToAuth.Length));
                var ops = lev.Operations;
                var applicableRules = rules.Where(rl => lev.Operations.Contains(rl.Rule)).ToList();

                if (applicableRules.Count == 0)
                {
                    probs[lev.Target] =  (1f - (float)lev.Operations.Length / lev.Source.Length);
                    continue;
                }

                float totalProb = (1f - (float)lev.Operations.Length / lev.Source.Length) +
                    ((applicableRules.Count == 0 ? 0 : applicableRules.Max(rl => rl.Probability * opTypeDistr[rl.Rule.Type])));
                probs[lev.Target] = totalProb;
            }

            var srt = probs.OrderByDescending(kvp => kvp.Value).ToList();
            Console.WriteLine("\n" + wrong + " -> " + srt.First().Key + " " + srt.First().Value);
            return srt.First().Key;
        }
    }
}
