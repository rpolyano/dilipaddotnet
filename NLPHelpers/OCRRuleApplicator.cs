﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NLPHelpers
{
    public struct ConditionalRule
    {
        public StringOperation Rule1 { get; set; }
        public StringOperation Rule2 { get; set; }
        public bool Consecutive { get; set; }
        public float ConditionalProbability { get; set; }
        public float Rule1Probability { get; set; }

        public string Apply(string str, int rule1Ind, int rule2Ind)
        {
            str = Rule1.Apply(str, rule1Ind);
            return Rule2.Apply(str, rule2Ind);
        }
    }

    public struct SingleRule
    {
        public StringOperation Rule { get; set; }
        public float Probability { get; set; }
    }

    public class OCRRuleApplicator
    {
        string[] AuthoritativeList;
        IGrouping<StringOperation, ConditionalRule>[] ruleGroups;
        IEnumerable<ConditionalRule> rules;
        public OCRRuleApplicator(string[] authList, ConditionalRule[] rules)
        {
            AuthoritativeList = authList;
            ruleGroups = rules.GroupBy(cr => cr.Rule1).OrderByDescending(grp => grp.First().Rule1Probability).ToArray();
            this.rules = rules;
        }

        public string Correct(string wrong)
        {

            
            //RuleTreeNode root = new RuleTreeNode(null, 
            //    wrong, 
            //    null, 
            //    null, 
            //    rules.Where(r=>r.Rule1.Type != StringOperationType.Insertion && r.Rule2.Type != StringOperationType.Insertion && r.Rule1Probability > 0.001 && r.ConditionalProbability > 0).ToArray(),
            //    Int32.MaxValue,
            //    AuthoritativeList);
            //root.MakeChildren();
            //string latex = @"\Tree" + root.ToLatex();
            //RuleTreeNode[] allNodes = root.Walk();
            //Console.WriteLine(allNodes.Length + " nodes built");
            //int total = allNodes.Length * AuthoritativeList.Length;
            //int count = 0;
            //long prevPercent = 0;
            //Console.WriteLine("Generating closest matches");
            //var insertions = allNodes.Select(node => 
            //    new {
            //        Node = node,
            //        Lev = AuthoritativeList.Select(auth =>
            //        {
            //            var lev = new Levenshtein(node.WordState, auth);
            //            lev.ComputeMatrix();
            //            lev.ComputeOperations(false);
            //            count++;
            //            long percent = ((long)count * 100 / total);
            //            if (percent != prevPercent)
            //            {
            //                Console.Write("\r" + percent + "% " + count + "/" + total);
            //                prevPercent = percent;
            //            }

            //            return lev;
            //        })
            //        .Where(lev => lev.Operations.Count() == 0 || lev.Operations.All(op => op.Type == StringOperationType.Insertion)).ToArray()
            //    }
            //    ).Where(a=>a.Lev.Length > 0).ToArray();

            //string bestResult;
            //var insRules = rules.Where(r=>r.Rule1.Type == StringOperationType.Insertion || r.Rule2.Type == StringOperationType.Insertion).ToArray();
            //float minScore = Single.MaxValue;
            //Levenshtein minLev = null;
            //foreach (var a in insertions)
            //{
            //    foreach (var lev in a.Lev)
            //    {
            //        float score = (float)lev.GetScore()/lev.Source.Length;
            //        if (lev.Operations.Length != 0)
            //        {

            //            var levRules = insRules.Where(rule => lev.Operations.Contains(rule.Rule1) || lev.Operations.Contains(rule.Rule2)).ToArray();


            //            float maxProb = 1;
            //            if (levRules.Length != 0)
            //            {
            //                maxProb -= levRules.Max(lr =>
            //                {
            //                    if (lev.Operations.Contains(lr.Rule1) && lev.Operations.Contains(lr.Rule2))
            //                    {
            //                        return lr.Rule1Probability * lr.ConditionalProbability;
            //                    }
            //                    else if (lev.Operations.Contains(lr.Rule1))
            //                    {
            //                        return lr.Rule1Probability;
            //                    }
            //                    else
            //                    {
            //                        var eqr = rules.Where(r => r.Rule1 == lr.Rule2).ToArray();
            //                        if (eqr.Length != 0)
            //                            return eqr.First().Rule1Probability;
            //                        return lr.Rule1Probability * lr.ConditionalProbability;
            //                    }

            //                });
            //            }
            //            score = score * maxProb;
            //        }
            //        if (score < minScore)
            //        {
            //            minScore = score;
            //            minLev = lev;
            //        }
            //    }
            //}
            //if (minLev != null)
            //    bestResult = minLev.Target;
            //else
            //    bestResult = "NO MATCH";

            //int cd = allNodes.OrderBy(rl => rl.ClosestDistance).First().ClosestDistance;
            //bestResult += "\t" + allNodes.OrderBy(rl => rl.ClosestDistance).TakeWhile(rl=>rl.ClosestDistance == cd).Select(rl=>rl.LevenshteinsToAuthoritative.First().Target).Aggregate((one, two)=>one +","+two);

            ////RuleTreeNode[] matchingNodes = allNodes.Where(node => AuthoritativeList.Contains(node.WordState)).ToArray();
            //return bestResult;

            var levsToAuth = AuthoritativeList.Select(auth =>
            {
                var lev = new Levenshtein(wrong, auth);
                lev.ComputeMatrix();
                lev.ComputeOperations(false);
                return lev;
            }).ToArray().OrderBy(lv => lv.GetScore()).ToArray();

            Dictionary<String, float> probs = new Dictionary<string, float>();
            int prog = 0;
            foreach (var lev in levsToAuth)
            {
                //if (lev.Target == "Quelch")
                //    Debugger.Break();
                prog++;
                Console.Write("\r" + (prog * 100 / levsToAuth.Length));
                var ops = lev.Operations;
                var applicableRules = rules.Where(rl => lev.Operations.Contains(rl.Rule1)).ToList();

                if (applicableRules.Count == 0)
                {
                    continue;
                }
                var applicableRulePairs = applicableRules.Where(rl => lev.Operations.Contains(rl.Rule1) &&
                    lev.Operations.Contains(rl.Rule2) &&
                    (!rl.Consecutive || lev.Source.GetIndexOfConsecutive(rl.Rule1.From[0], rl.Rule2.From[0], new int[0]) != -1)).ToList();

                applicableRules = applicableRules.Except(applicableRulePairs).ToList();

                float totalProb = (1 - (float)lev.GetScore() / lev.Source.Length) + 
                    ((applicableRules.Count == 0 ? 0 : applicableRules.Max(rl=>rl.Rule1Probability)) + 
                    (applicableRulePairs.Count == 0 ? 0 : applicableRulePairs.Max(rl => rl.Rule1Probability * rl.ConditionalProbability)));
                probs[lev.Target] = totalProb;
            }

            var srt = probs.OrderByDescending(kvp => kvp.Value).ToList();
            Console.WriteLine("\n" + wrong + " -> " + srt.First().Key);
            return srt.First().Key;

            SingleRuleTreeNode root = new SingleRuleTreeNode(null,
                wrong,
                null,
                null,
                rules.Where(r => r.Rule1.Type != StringOperationType.Insertion && r.Rule1Probability >= 0.0001).Select(cr => cr.Rule1).Distinct().ToArray(),
                Int32.MaxValue,
                AuthoritativeList);
            root.MakeChildren();
            string latex = @"\Tree" + root.ToLatex();
            SingleRuleTreeNode[] allNodes = root.Walk();
            Console.WriteLine(allNodes.Length + " nodes built");
            int total = allNodes.Length * AuthoritativeList.Length;
            int count = 0;
            long prevPercent = 0;
            Console.WriteLine("Generating closest matches");
            var insertions = allNodes.Select(node =>
                new
                {
                    Node = node,
                    Lev = AuthoritativeList.Select(auth =>
                    {
                        var lev = new Levenshtein(node.WordState, auth);
                        lev.ComputeMatrix();
                        lev.ComputeOperations(false);
                        count++;
                        long percent = ((long)count * 100 / total);
                        if (percent != prevPercent)
                        {
                            Console.Write("\r" + percent + "% " + count + "/" + total);
                            prevPercent = percent;
                        }

                        return lev;
                    })
                    .Where(lev => lev.Operations.Count() == 0 || lev.Operations.All(op => op.Type == StringOperationType.Insertion)).ToArray()
                }
                ).Where(a => a.Lev.Length > 0).ToArray();

            string bestResult;
            var insRules = rules.Where(r => r.Rule1.Type == StringOperationType.Insertion || r.Rule2.Type == StringOperationType.Insertion).ToArray();
            float minScore = Single.MaxValue;
            Levenshtein minLev = null;
            foreach (var a in insertions)
            {
                foreach (var lev in a.Lev)
                {
                    float score = (float)lev.GetScore() / lev.Source.Length;
                    if (lev.Operations.Length != 0)
                    {

                        var levRules = insRules.Where(rule => lev.Operations.Contains(rule.Rule1) || lev.Operations.Contains(rule.Rule2)).ToArray();


                        float maxProb = 1;
                        if (levRules.Length != 0)
                        {
                            maxProb -= levRules.Max(lr =>
                            {
                                if (lev.Operations.Contains(lr.Rule1) && lev.Operations.Contains(lr.Rule2))
                                {
                                    return lr.Rule1Probability * lr.ConditionalProbability;
                                }
                                else if (lev.Operations.Contains(lr.Rule1))
                                {
                                    return lr.Rule1Probability;
                                }
                                else
                                {
                                    var eqr = rules.Where(r => r.Rule1 == lr.Rule2).ToArray();
                                    if (eqr.Length != 0)
                                        return eqr.First().Rule1Probability;
                                    return lr.Rule1Probability * lr.ConditionalProbability;
                                }

                            });
                        }
                        score = score * maxProb;
                    }
                    if (score < minScore)
                    {
                        minScore = score;
                        minLev = lev;
                    }
                }
            }
            if (minLev != null)
                bestResult = minLev.Target;
            else
                bestResult = "NO MATCH";

            int cd = allNodes.OrderBy(rl => rl.ClosestDistance).First().ClosestDistance;
            bestResult += "\t" + allNodes.OrderBy(rl => rl.ClosestDistance).TakeWhile(rl => rl.ClosestDistance == cd).Select(rl => rl.LevenshteinsToAuthoritative.First().Target).Aggregate((one, two) => one + "," + two);

            //RuleTreeNode[] matchingNodes = allNodes.Where(node => AuthoritativeList.Contains(node.WordState)).ToArray();
            return bestResult;



        }

    }
}
