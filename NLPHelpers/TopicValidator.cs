﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
#if !X_PLATFORM_TOPIC_VALIDATION
using edu.stanford.nlp.ling;
using edu.stanford.nlp.tagger.maxent;
using java.util;
#endif
namespace NLPHelpers
{
    public class TopicValConstants
    {
        public static string[] BAD_SENTENCE_ENDINGS = new string[]{
            //Penn-Treebank POS notation
            "CC", //Coordinating Conjunction
            "DT", //Determiner
            "IN", //Preposition or subordinating conjunction
            "MD", //Modal
            "PDT", //Predeterminer
            "PRP$", //Possesive pronoun
            "RP", //Particle
            "TO", //The word 'to'
            "WDT", //Wh determiner
            "WP", //Wh pronoun
            "WP$", //Possesive wh pronoun
            "POS", //Possesive ending
            "RB"//Adverb TODO: Is this safe?
        };

        public static string[] BAD_SENTENCE_BEGINNINGS = new string[]{
            //Penn-Treebank POS notation
            "CC", //Coordinating Conjunction
            "IN", //Preposition or subordinating conjunction
            "PRP$", //Possesive pronoun
            "RP", //Particle
            "TO", //The word 'to'
            "WDT", //Wh determiner
            "WP", //Wh pronoun
            "WP$", //Possesive wh pronoun
            "POS" //Possesive ending
        };
    }
}
#if !X_PLATFORM_TOPIC_VALIDATION

namespace NLPHelpers
{
    public class TopicValidator
    {
        static MaxentTagger posTagger;
        public static void Init(string jarRootDir, string modelsDir)
        {
            posTagger = new MaxentTagger(modelsDir + "/wsj-0-18-bidirectional-nodistsim.tagger");
        }

        

        public static bool SentenceContinues(string sent)
        {
            string tagged = TagSentence(sent);
            string last = tagged.Split(' ').Reverse().Skip(1).First();
            string pos = last.Split('/').Last().Trim();
            if (TopicValConstants.BAD_SENTENCE_ENDINGS.Contains(pos))
                return true;
            return false;
        }

        public static bool SentenceIsContinuation(string sent)
        {
            string tagged = TagSentence(sent);
            string first = tagged.Split(' ').First();
            string pos = first.Split('/').Last().Trim();
            if (TopicValConstants.BAD_SENTENCE_BEGINNINGS.Contains(pos))
                return true;
            return false;
        }

        public static string TagSentence(string sent)
        {
            sent = sent[0] + sent.ToLower().Substring(1) + ".";
            var sentences = MaxentTagger.tokenizeText(new java.io.StringReader(sent)).toArray();
            ArrayList sentence = (ArrayList)sentences[0];
            var taggedSentence = posTagger.tagSentence(sentence);
            var tagged = Sentence.listToString(taggedSentence, false);
            return tagged;
        }
    }
} 
#endif
