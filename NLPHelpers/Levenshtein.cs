﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NLPHelpers
{
    public enum StringOperationType
    {
        Deletion,
        Insertion,
        Substitution,
        TwoForOneSubstitution,
        OneForTwoSubstitution
    }
    public class StringOperation
    {
        public StringOperationType Type { get; set; }
        public int Index { get; set; }

        public string From { get; set; }
        public string To { get; set; }

        public string Source { get; set; }
        public string Target { get; set; }

        public bool HasContext { get; set; }
        public char ContexPre { get; set; }
        public char ContexPost { get; set; }

        public StringOperation()
        {
            From = "";
            To = "";
            HasContext = false;
            Type = StringOperationType.Deletion;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is StringOperation))
            {
                return false;
            }
            StringOperation op = (StringOperation)obj;
            return op.Type == Type &&
                op.From == From &&
                op.To == To &&
                (((op.HasContext || HasContext) && op.ContexPost == ContexPost && op.ContexPre == ContexPre) ||
                (!op.HasContext && !HasContext));
        }

        public override int GetHashCode()
        {
            int res = 11;
            res *= 17;
            res += Type.GetHashCode();
            res *= 17;
            res += From.GetHashCode();
            res *= 17;
            res += To.GetHashCode();
            res *= 17;
            if (HasContext)
            {
                res += ContexPre.GetHashCode();
                res *= 17;
                res += ContexPost.GetHashCode();
                res *= 17;
            }
            return res;

        }

        public static bool operator ==(StringOperation a, StringOperation b)
        {
            if (object.ReferenceEquals(a, null))
            {
                return object.ReferenceEquals(b, null);
            }
            return a.Equals(b);
        }

        public static bool operator !=(StringOperation a, StringOperation b)
        {
            if (object.ReferenceEquals(a, null))
            {
                return !object.ReferenceEquals(b, null);
            }
            return !a.Equals(b);
        }

        public override string ToString()
        {
            return ToString(true);
        } 

        public string ToString(bool printIndex)
        {
            return Type.ToString() + (printIndex ? " at " + Index : "") +
                (Type == StringOperationType.Substitution ? " from " + From + " to " + To : " of " + From) +
                (HasContext ? " between " + ContexPre + " and " + ContexPost : "");
        }

        /// <summary>
        /// Applies the rule to a given string at the given index
        /// </summary>
        /// <param name="str">String to apply to</param>
        /// <param name="ind">Index to apply the rule at</param>
        /// <returns></returns>
        public string Apply(string str, int ind)
        {
            if (ind >= str.Length)
            {
                return str;
            }
            StringBuilder sb = new StringBuilder(str);
            switch (Type)
            {
                case StringOperationType.Deletion:
                    sb.Remove(ind, 1);
                    break;
                case StringOperationType.Insertion:
                    sb.Insert(ind, From);
                    break;
                case StringOperationType.Substitution:
                    sb.Remove(ind, 1).Insert(ind, To);
                    break;
                case StringOperationType.TwoForOneSubstitution:
                    sb.Remove(ind, 2).Insert(ind, To);
                    break;
                case StringOperationType.OneForTwoSubstitution:
                    sb.Remove(ind, 1).Insert(ind, To);
                    break;
            }
            return sb.ToString();
        }

        public string ToShortLatexString()
        {
            return @"\textit{" + Type.ToString().Substring(0,3) + " " + (Type == StringOperationType.Substitution ? From + " to " + To : From.ToString()) + "}";
        }
    }
    public class Levenshtein
    {
        public string Source
        {
            get
            {
                return s;
            }
        }
        private string s;

        public string Target
        {
            get
            {
                return t;
            }
        }
        private string t;

        private int m
        {
            get
            {
                return s.Length;
            }
        }

        private int n
        {
            get
            {
                return t.Length;
            }
        }

        private int[,] matrix;

        private StringOperation[] operations;
        public StringOperation[] Operations
        {
            get
            {
                return operations;
            }
        }

        public Levenshtein(string s, string t)
        {
            this.s = s;
            this.t = t;
            matrix = new int[m + 1, n + 1];
        }

        public void ComputeMatrix()
        {

            for (int i = 1; i <= m; i++)
            {
                matrix[i, 0] = i;
            }

            for (int i = 1; i <= n; i++)
            {
                matrix[0, i] = i;
            }

            for (int j = 1; j <= n; j++)
            {
                for (int i = 1; i <= m; i++)
                {
                    if (s[i - 1] == t[j - 1])
                    {
                        matrix[i, j] = matrix[i - 1, j - 1];
                    }
                    else
                    {

                        int delScore = matrix[i - 1, j] + 1;
                        int insScore = matrix[i, j - 1] + 1;
                        int subScore = matrix[i - 1, j - 1] + 1;
                        matrix[i, j] = Math.Min(Math.Min(delScore, insScore), subScore);
                    }
                }
            }
        }

        public void ComputeOperations(bool storeContext = true)
        {
            operations = new StringOperation[matrix[m, n]];
            int opi = operations.Length - 1;
            int x = m;
            int y = n;

            int val = matrix[x, y]; ;

            while (val != 0)
            {
                int valDiag = ((x > 0 && y > 0) ? matrix[x - 1, y - 1] : Int32.MaxValue);
                int valLeft = (x > 0 ? matrix[x - 1, y] : Int32.MaxValue);
                int valUp = (y > 0 ? matrix[x, y - 1] : Int32.MaxValue);

                if (valDiag <= valLeft && valDiag <= valUp && (valDiag == val || valDiag == val - 1))
                {

                    if (valDiag != val)
                    {
                        operations[opi] = new StringOperation()
                        {
                            Index = x - 1,
                            Type = StringOperationType.Substitution,
                            From = s[x - 1].ToString(),
                            To = t[y - 1].ToString(),
                            HasContext = storeContext,
                            ContexPre = (x - 2 < 0 ? '^' : s[x - 2]),
                            ContexPost = (x >= s.Length ? '$' : s[x])
                        };
                        opi--;
                    }
                    x -= 1;
                    y -= 1;

                }
                else if (valLeft <= valUp && (valLeft == val || valLeft == val - 1))
                {
                    if (valLeft != val)
                    {
                        operations[opi] = new StringOperation()
                        {
                            Index = x - 1,
                            Type = StringOperationType.Deletion,
                            From = s[x - 1].ToString(),
                            HasContext = storeContext,
                            ContexPre = (x - 2 < 0 ? '^' : s[x - 2]),
                            ContexPost = (x >= s.Length ? '$' : s[x])
                        };
                        opi--;
                    }
                    x -= 1;
                }
                else
                {
                    if (valUp != val)
                    {
                        operations[opi] = new StringOperation()
                        {
                            Index = x - 1,
                            Type = StringOperationType.Insertion,
                            From = t[y - 1].ToString(),
                            HasContext = storeContext,
                            ContexPre = (x - 2 < 0 ? '^' : s[x - 2]),
                            ContexPost = (x >= s.Length ? '$' : s[x])
                        };
                        opi--;
                    }
                    y -= 1;
                }
                val = matrix[x, y];
            }
            for (int i = 0; i < operations.Length; i++)
            {
                operations[i].Source = s;
                operations[i].Target = t;
                if (i >= 1)
                {
                    if (((operations[i-1].Type == StringOperationType.Deletion && 
                        operations[i].Type == StringOperationType.Substitution) ||
                        (operations[i-1].Type == StringOperationType.Substitution && 
                        operations[i].Type == StringOperationType.Deletion)) &&
                        operations[i].Index == operations[i-1].Index + 1)
                    {

                        operations[i].From = operations[i - 1].From + operations[i].From;
                        operations[i].To = (operations[i].Type == StringOperationType.Substitution ? operations[i].To : operations[i - 1].To);
                        operations[i].Type = StringOperationType.TwoForOneSubstitution;
                        operations[i].Index -= 1;
                        operations[i - 1] = null;
                    }
                    else if (((operations[i-1].Type == StringOperationType.Insertion && 
                        operations[i].Type == StringOperationType.Substitution) ||
                        (operations[i-1].Type == StringOperationType.Substitution && 
                        operations[i].Type == StringOperationType.Insertion)) &&
                        operations[i].Index == operations[i-1].Index + 1)
                    {
                        
                        operations[i].From = (operations[i].Type == StringOperationType.Substitution ? operations[i].From : operations[i - 1].From);
                        operations[i].To = (operations[i].Type == StringOperationType.Substitution ? operations[i-1].From + operations[i].To : operations[i - 1].To + operations[i].From);
                        operations[i].Type = StringOperationType.OneForTwoSubstitution;
                        operations[i - 1] = null;
                    }
                }
            }

            operations = operations.Where(op => op != null).ToArray();
            
        }

        public int GetScore()
        {
            if (matrix != null)
            {
                return matrix[m, n];
            }
            return 0;
        }

        public string Visualize()
        {

            string print = "";
            print += "Matrix:\n\n";
            if (matrix == null)
            {
                print += "Not computed\n";
            }
            else
            {
                print += "    " + s.ToCharArray().Select(c => c.ToString()).Aggregate((one, two) => one + " " + two) + "\n";
                string tName = " " + t;
                for (int j = 0; j <= n; j++)
                {
                    print += tName[j] + " ";
                    for (int i = 0; i <= m; i++)
                    {
                        print += matrix[i, j] + " ";
                    }
                    print += "\n";
                }
            }
            print += "\n\nOperations:\n\n";
            if (operations == null)
            {
                print += "Not computed\n";
            }
            else
            {
                print += operations
                    .Select(op => op.ToString())
                    .Aggregate((one, two) => one + "\n" + two);
            }



            return print;

        }

        public override string ToString()
        {
            return Source + " > " + Target;
        }
    }
}
